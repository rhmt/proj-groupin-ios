//
//  AboutChannelController.swift
//  Groupin
//
//  Created by Macbook pro on 8/16/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift

class AboutChannelController: UIViewController {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCreated: UILabel!
    @IBOutlet weak var lblJlm: UILabel!
    @IBOutlet weak var tbMember: UITableView!
    
    var models = tb_room()
    var idChannel = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "About Channel"
        
        models = try! Realm().objects(tb_room.self).filter("id_channel == %@", self.idChannel).first!
        
        lblName.text = models.name
//        let jlm = models.member.count
//        lblJlm.text = String(jlm)
        
        tbMember.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEditChannel" {
            let conn = segue.destination as! CreateChannelController
            conn.statusEdit = 1
            conn.idChannel = self.idChannel
            conn.navigationItem.title = "Edit Channel"
        }
    }
    
    @IBAction func btnEditTapped(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showEditChannel", sender: self)
    }
}
