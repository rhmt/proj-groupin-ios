//
//  AboutGroupController.swift
//  Groupin
//
//  Created by Macbook pro on 8/4/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class AboutGroupController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewHead: UIView!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var viewDesc: UIView!
    @IBOutlet weak var viewParent: UIView!
    @IBOutlet weak var viewChild: UIView!
    @IBOutlet weak var viewMember: UIView!
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblNama: UILabel!
    @IBOutlet weak var lblCreated: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblCat: UILabel!
    @IBOutlet weak var lblDesk: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblJmlParent: UILabel!
    @IBOutlet weak var lblJmlChild: UILabel!
    @IBOutlet weak var lblJlmMember: UILabel!
    
    @IBOutlet weak var tbParent: UITableView!
    @IBOutlet weak var tbChild: UITableView!
    @IBOutlet weak var tbMember: UITableView!
    
    @IBOutlet weak var conViewMain: NSLayoutConstraint!
    @IBOutlet weak var ConViewParent: NSLayoutConstraint!
    @IBOutlet weak var conTbParent: NSLayoutConstraint!
    @IBOutlet weak var conViewChild: NSLayoutConstraint!
    @IBOutlet weak var conTbChild: NSLayoutConstraint!
    @IBOutlet weak var conViewMember: NSLayoutConstraint!
    @IBOutlet weak var conTbMember: NSLayoutConstraint!
    
    
    var token = ""
    var idGroup = 0
    var idUser = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rightMenu()
        
        self.navigationItem.title = "Group Info"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //Get id_group form session
        let idSession = try! Realm().objects(session.self).first
        self.idGroup = idSession!.id_group
        self.token = idSession!.token
        self.idUser = idSession!.id
        
        self.viewDesign()
        
        self.loadLocalData()
        
        self.makeSerialize()
    }
    
    func viewDesign(){
        //View Design
        self.btnEdit.isHidden = true
        
        viewName.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        viewHead.layer.shadowColor = UIColor.lightGray.cgColor
        viewHead.layer.shadowOffset = CGSize.zero
        viewHead.layer.shadowOpacity = 0.5
        viewHead.layer.shadowRadius = 3
        
        viewDesc.layer.shadowColor = UIColor.lightGray.cgColor
        viewDesc.layer.shadowOffset = CGSize.zero
        viewDesc.layer.shadowOpacity = 0.5
        viewDesc.layer.shadowRadius = 3
        
        viewParent.layer.shadowColor = UIColor.lightGray.cgColor
        viewParent.layer.shadowOffset = CGSize.zero
        viewParent.layer.shadowOpacity = 0.5
        viewParent.layer.shadowRadius = 3
        
        viewChild.layer.shadowColor = UIColor.lightGray.cgColor
        viewChild.layer.shadowOffset = CGSize.zero
        viewChild.layer.shadowOpacity = 0.5
        viewChild.layer.shadowRadius = 3
        
        viewMember.layer.shadowColor = UIColor.lightGray.cgColor
        viewMember.layer.shadowOffset = CGSize.zero
        viewMember.layer.shadowOpacity = 0.5
        viewMember.layer.shadowRadius = 3
        
        //Constraint default
        self.conViewMain.constant -= 135
        self.ConViewParent.constant = 45
        self.conTbParent.constant = 1
        self.conViewChild.constant = 45
        self.conTbChild.constant = 1
        self.conViewMember.constant = 45
        self.conTbMember.constant = 1
    }
    
    var dataMember = [listMember]()
    var jlmParent = 0
    var nameParent = ""
    var avatarParent = ""
    var dataChild = [listChild]()
    func loadLocalData(){
        //Data Group and Parent
        let dataGroup = try! Realm().objects(tb_group.self).filter("id = %@", self.idGroup).first!
        if dataGroup.avatar != "" {
            self.imgAva.loadImageUsingCacheWithUrlString(dataGroup.avatar)
        }else{
            self.imgAva.image = UIImage(named: "dafault-ava-group")
        }
        self.lblNama.text = dataGroup.name
        self.lblCreated.text = "Created by " + dataGroup.user_create
        let formatFrom = "YYYY-MM-dd HH:mm:ss"
        let formatTo = "MMM dd, YYYY hh:mm a"
        let dataFormat = dataGroup.date_create
        self.lblCreatedDate.text = "at " + Config().convertDate(dataFormat, from: formatFrom, to: formatTo)
        self.lblCat.text = dataGroup.category
        self.lblDesk.text = dataGroup.desc
        self.lblLocation.text = dataGroup.city + ", " + dataGroup.country
        
        if dataGroup.user_level == "1" {
            self.btnEdit.isHidden = true
        }else{
            self.btnEdit.isHidden = false
            self.navigationItem.setRightBarButtonItems([self.rightMenuButtonItem], animated: true)
        }
        
        if dataGroup.name_parent != "" {
            self.jlmParent = 1
            self.nameParent = dataGroup.name_parent
            self.avatarParent = dataGroup.avatar_parent
            self.conViewMain.constant += 55
            self.ConViewParent.constant = 95
            self.conTbParent.constant = 55
        }
        self.lblJmlParent.text = String(self.jlmParent)
        
        self.lblJmlParent.isHidden = true
        self.lblJmlChild.isHidden = true
        
        //Data Child
        let dataChilds = try! Realm().objects(listChild.self).filter("id_group = %@", self.idGroup).sorted(byKeyPath: "name", ascending: true)
        self.lblJmlChild.text = String(dataChilds.count)
        self.dataChild.removeAll()
        for item in dataChilds {
            self.dataChild += [item]
        }
        
        //Data Member
        let dataMembers = try! Realm().objects(listMember.self).filter("id_group = %@", self.idGroup).sorted(byKeyPath: "name", ascending: true)
        self.dataMember.removeAll()
        for item in dataMembers {
            self.dataMember += [item]
        }
        self.lblJlmMember.text = String(self.dataMember.count)
        
        //Refresh
        if self.paramsJson == 1 {
            self.tbParent.reloadData()
            self.tbChild.reloadData()
            self.tbMember.reloadData()
        }else{
            //Constraint
            let jlmChild = dataChilds.count
            let resultChild = 55 * jlmChild
            self.conViewMain.constant += CGFloat(resultChild)
            self.conViewChild.constant += CGFloat(resultChild)
            self.conTbChild.constant += CGFloat(resultChild)
            let jlmMember = dataMembers.count
            let resultMember = 55 * jlmMember
            self.conViewMain.constant += CGFloat(resultMember)
            self.conViewMember.constant += CGFloat(resultMember)
            self.conTbMember.constant += CGFloat(resultMember)
        }
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idGroup as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            self.getData(base64Encoded)
        }
    }
    
    var paramsJson = 0
    func getData(_ message: String){
        let urlString = Config().urlGroup + "detail_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                let data = jsonData["msg"]
                print(data)
                
                if let modelUpdate = try! Realm().objects(tb_group.self).filter("id = %@", self.idGroup).first {
                    try! Realm().write({ 
                        if let imgUrl = data["avatar"].string {
                            modelUpdate.avatar = imgUrl
                        }
                        modelUpdate.name = data["group_name"].string!
                        modelUpdate.date_create = data["date_create"].string!
                        modelUpdate.user_create = data["user_create"].string!
                        modelUpdate.category = data["category"].string!
                        modelUpdate.desc = data["desc"].string!
                        modelUpdate.city = data["city"].string!
                        modelUpdate.country = data["country"].string!
                        modelUpdate.jlm_member = data["total_member"].string!
                        modelUpdate.is_promot = data["is_promote"].string!
                        modelUpdate.is_public = data["is_public"].string!
                        
                        for (key, _) in data["parent"] {
                            modelUpdate.id_parent = key
                            modelUpdate.name_parent = data["parent"][key]["group_name"].string!
                            modelUpdate.avatar_parent = data["parent"][key]["avatar"].string!
                        }
                    })
                }
            }else{
                print("Something Went Wrong..")
            }
        }.responseData { Response in
            self.getChild(message)
        }
    }
    
    func getChild(_ message: String){
        let urlString = Config().urlGroup + "list_child_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                let dataChilds = jsonData["msg"]
                
                for (key, _) in dataChilds {
                    if let modelUpdate = try! Realm().objects(listChild.self).filter("id_child = %@", Int(key)!).first {
                        try! Realm().write({ 
                            modelUpdate.name = dataChilds[key]["group"].string!
                            if let imgUrl = dataChilds[key]["avatar"].string {
                                modelUpdate.avatar = imgUrl
                            }
                        })
                    }else{
                        let model = listChild()
                        model.id_child = Int(key)!
                        model.id_group = self.idGroup
                        model.name = dataChilds[key]["group"].string!
                        model.avatar = dataChilds[key]["avatar"].string!
                        DBHelper.insert(model)
                        self.conViewMain.constant += 55
                        self.conViewChild.constant += 55
                        self.conTbChild.constant += 55
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.getMember(message)
        }
    }
    
    func getMember(_ message: String){
        let urlString = Config().urlUser + "list_member_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                let listMembers = jsonData["msg"]
                self.paramsJson = 1
                
                for (key, _) in listMembers {
                    if let model = try! Realm().objects(listMember.self).filter("id_member = %@", Int(key)!).first {
                        try! Realm().write({ 
                            
                            if let imgUrl = listMembers[key]["avatar"].string {
                                model.avatar = imgUrl
                            }
                            model.name = listMembers[key]["name"].string!
                            model.status_level = listMembers[key]["user_level"].string!
                        })
                    }else{
                        let modelSave = listMember()
                        modelSave.id_member = Int(key)!
                        modelSave.id_group = self.idGroup
                        modelSave.avatar = listMembers[key]["avatar"].string!
                        modelSave.name = listMembers[key]["name"].string!
                        modelSave.status_level = listMembers[key]["user_level"].string!
                        modelSave.from_group = listMembers[key]["from_group"].string!
                        DBHelper.insert(modelSave)
                        self.conViewMain.constant += 55
                        self.conViewChild.constant += 55
                        self.conTbChild.constant += 55
                    }
                    
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.loadLocalData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEditGroup"{
            self.tabBarController?.tabBar.isHidden = true
            let conn = segue.destination as! EditGroupController
            conn.navigationItem.title = "Edit Group"
        } else if segue.identifier == "showInviteContact" {
            let conn = segue.destination as! InviteContactController
            conn.dari = "about"
        } else if segue.identifier == "showInviteGroup" {
            let conn = segue.destination as! InviteGroupController
            conn.dari = "about"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Data Parent, Child, Member
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tbParent {
            return self.jlmParent
        }else if tableView == tbChild {
            return self.dataChild.count
        }else{
            return self.dataMember.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = ListGeneralCell()
        if tableView == tbParent {
            cell = tableView.dequeueReusableCell(withIdentifier: "listParent", for: indexPath) as! ListGeneralCell
            
            if self.avatarParent != "" {
                cell.imgAva.loadImageUsingCacheWithUrlString(self.avatarParent)
            }else{
                cell.imgAva.image = UIImage(named: "dafault-ava-group")
            }
            cell.imgAva.layer.cornerRadius = 5
            cell.imgAva.clipsToBounds = true
            cell.lblTitle.text = self.nameParent
        }else if tableView == tbChild {
            cell = tableView.dequeueReusableCell(withIdentifier: "listChild", for: indexPath) as! ListGeneralCell
            
            let data = self.dataChild[indexPath.row]
            
            let imgUrl = data.avatar
            cell.imgAva.loadImageUsingCacheWithUrlString(imgUrl)
            cell.imgAva.layer.cornerRadius = 5
            cell.imgAva.clipsToBounds = true
            cell.lblTitle.text = data.name
        }else if tableView == tbMember {
            cell = tableView.dequeueReusableCell(withIdentifier: "listMember", for: indexPath) as! ListGeneralCell
            
            let data = self.dataMember[indexPath.row]
            
            if data.avatar != ""{
                cell.imgAva.loadImageUsingCacheWithUrlString(data.avatar)
            }else{
                cell.imgAva.image = UIImage(named: "default-avatar")
            }
            cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
            cell.imgAva.clipsToBounds = true
            cell.lblTitle.text = data.name
            
            let admin = data.status_level
            cell.lblSubTitle.layer.cornerRadius = 5
            cell.lblSubTitle.clipsToBounds = true
            if admin == "3" {
                cell.lblSubTitle.text = "Super Admin"
            }else if admin == "2" {
                cell.lblSubTitle.text = "Admin"
            }else{
                cell.lblSubTitle.isHidden = true
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tbParent {
            self.tbParent.reloadData()
        }else if tableView == tbChild {
            self.tbChild.reloadData()
        }else if tableView == tbMember {
            let data = self.dataMember[indexPath.row]
            self.idFriend = String(data.id_member)
            if self.idUser != data.id_member {
                self.memberTapped()
            }
        }
    }
    
    @IBAction func btnEditTapped(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showEditGroup", sender: self)
    }
    
    //Methode Nav
    var rightMenuButtonItem:UIBarButtonItem!
    func rightMenu(){
        rightMenuButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(AboutGroupController.addTapped))
    }
    
    func addTapped(_ sender:UIButton){
        let alert:UIAlertController=UIAlertController(title: "Invite", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.modalPresentationStyle = .popover
        
        let cameraAction = UIAlertAction(title: "Invite Contact", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.performSegue(withIdentifier: "showInviteContact", sender: self)
        }
        let gallaryAction = UIAlertAction(title: "Invite Group", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.performSegue(withIdentifier: "showInviteGroup", sender: self)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds;
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    var idFriend = ""
    func memberTapped(){
        let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showMember") as! PopupMemberController
        self.addChildViewController(Popover)
        Popover.view.frame = self.view.frame
        Popover.idFriend = self.idFriend
        self.view.addSubview(Popover.view)
        Popover.didMove(toParentViewController: self)
    }
    
}
