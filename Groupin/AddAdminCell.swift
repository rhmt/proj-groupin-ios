//
//  AddAdminCell.swift
//  Groupin
//
//  Created by Macbook pro on 8/12/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import Foundation
import UIKit

class AddAdminCell: UITableViewCell {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
}