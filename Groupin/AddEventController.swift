//
//  AddEventController.swift
//  Groupin
//
//  Created by Macbook pro on 9/9/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import DropDown
import GoogleMaps
import GooglePlaces
import RealmSwift
import SwiftyJSON
import Alamofire
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class AddEventController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate, UITextFieldDelegate, UIPopoverPresentationControllerDelegate, UIScrollViewDelegate {
    
    let listCountry = DropDown()
    let listCity = DropDown()
    let listMataUang = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.listCountry,
            self.listCity,
            self.listMataUang
        ] }()
    
    var imgPicker = UIImagePickerController()
    var coor = CLLocationCoordinate2D()
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var heightViewMain: NSLayoutConstraint!
    @IBOutlet weak var btnAva: UIButton!
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtDesc: UITextView!
    @IBOutlet weak var lblLeftDesc: UILabel!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var btnMap: UIButton!
    @IBOutlet weak var btnFromTanggal: UIButton!
    @IBOutlet weak var btnFromJam: UIButton!
    @IBOutlet weak var btnToTanggal: UIButton!
    @IBOutlet weak var btnToJam: UIButton!
    @IBOutlet weak var btnDeadTgl: UIButton!
    @IBOutlet weak var btnDeadJam: UIButton!
    @IBOutlet weak var btnAddCP: UIButton!
    @IBOutlet weak var btnAddTicket: UIButton!
    @IBOutlet weak var swPhysicalTicket: UISwitch!
    @IBOutlet weak var viewPhysicalTicket: UIView!
    @IBOutlet weak var heightPhysicalTicket: NSLayoutConstraint!
    @IBOutlet weak var swTicketOTS: UISwitch!
    @IBOutlet weak var viewTicketOTS: UIView!
    @IBOutlet weak var txtTicketLocation: UITextField!
    @IBOutlet weak var btnTicketFromDate: UIButton!
    @IBOutlet weak var btnTicketSelectLocation: UIButton!
    @IBOutlet weak var btnTicketFromTime: UIButton!
    @IBOutlet weak var btnTicketToDate: UIButton!
    @IBOutlet weak var btnTicketToTime: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var tbTicket: UITableView!
    @IBOutlet weak var hieghtTBTicket: NSLayoutConstraint!
    @IBOutlet weak var heightConn: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewTbCP: NSLayoutConstraint!
    @IBOutlet weak var tbCP: UITableView!
    @IBOutlet weak var heightTbCP: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewTbStaff: NSLayoutConstraint!
    @IBOutlet weak var tbStaff: UITableView!
    @IBOutlet weak var heightTbStaff: NSLayoutConstraint!
    
    @IBOutlet weak var viewMainDatePicker: UIView!
    @IBOutlet weak var viewDatePicker: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var btnDoneDatePicker: UIButton!
    
    @IBOutlet weak var conSpaceBotView: NSLayoutConstraint!
    
    var untuk = ""
    var idEventEdit = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationItem.title = "Add Event"
        self.loadData()
        
        self.imgPicker.delegate = self
        self.txtName.delegate = self
        self.txtCountry.delegate = self
        self.txtCity.delegate = self
        self.txtAddress.delegate = self
        self.txtTicketLocation.delegate = self
        self.scrollView.delegate = self
        
        self.heightViewMain.constant = 1346
        self.viewPhysicalTicket.isHidden = true
        self.viewTicketOTS.isHidden = true
        self.heightPhysicalTicket.constant = 0
    }
    
    var idUser = 0
    var idGroup = 0
    var token = ""
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        //self.loadData()
    }
    
    func loadData(){
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.idGroup = idSession!.id_group
        self.token = idSession!.token
        
        txtDesc.delegate = self
        //Desc Rounded
        txtDesc.layer.cornerRadius = 5
        txtDesc.layer.borderColor = UIColor.lightGray.cgColor
        txtDesc.layer.borderWidth = 0.5
        
        //Img Ava
        let tapPhoto = UITapGestureRecognizer(target: self, action: #selector(AddEventController.takePhotoTapped(_:)))
        tapPhoto.numberOfTapsRequired = 1
        imgAva.isUserInteractionEnabled = true
        imgAva.addGestureRecognizer(tapPhoto)
        imgAva.layer.cornerRadius = 5
        imgAva.clipsToBounds = true
        
        //Rounded Button
        //        btnAddTicket.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
        //        btnAddTicket.layer.cornerRadius = 5
        //        btnAddTicket.layer.borderWidth = 1
        //        btnAddTicket.layer.borderColor = UIColor.lightGrayColor().CGColor
        btnSave.backgroundColor = UIColor(red: 83, green: 186, blue: 32)
        btnSave.layer.cornerRadius = 5
        btnSave.layer.borderWidth = 1
        btnSave.layer.borderColor = UIColor.lightGray.cgColor
        btnMap.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
        btnMap.layer.cornerRadius = 5
        btnMap.layer.borderWidth = 1
        btnMap.layer.borderColor = UIColor.lightGray.cgColor
        btnTicketSelectLocation.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
        btnTicketSelectLocation.layer.cornerRadius = 5
        btnTicketSelectLocation.layer.borderWidth = 1
        btnTicketSelectLocation.layer.borderColor = UIColor.lightGray.cgColor
        
        //DatePicker
        self.dateFormatter = DateFormatter()
        self.saveDateFormatter = DateFormatter()
        self.viewMainDatePicker.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        if untuk == "edit"{
            print("test test oii edit")
            self.makeSerializeEdit()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(AddEventController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AddEventController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func makeSerializeEdit(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idEventEdit as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Edit(jsonString!)
    }
    
    func makeBase64Edit(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getDataEdit(base64Encoded)
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    var jsonDataTicket: JSON!
    var jsonDataCP: JSON!
    func getDataEdit(_ message: String){
        let urlString = Config().urlEvent + "detail_event"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)["msg"]
                let jasonData = JSON(value)["msg"]
                print("Edit")
                print(self.jsonData)
                if self.jsonData.count > 0 {
                    self.paramsJson = 1
                    
                    self.imgAva.loadImageUsingCacheWithUrlString(jasonData["image"].string!)
                    self.txtName.text = jasonData["name"].string!
                    self.txtDesc.text = jasonData["description"].string!
                    self.txtDesc.textColor = UIColor.black
                    let desc = jasonData["description"].string!
                    let countDesc = desc.characters.count
                    self.lblLeftDesc.text = String(500 - countDesc)
                    
                    self.idCountrySelected = jasonData["fk_country"].intValue
                    self.idCitySelected = jasonData["fk_city"].intValue
                    self.txtCountry.text = jasonData["country"].string!
                    self.txtCity.text = jasonData["city"].string!
                    self.txtAddress.text = jasonData["address"].string!
                    self.latAddress = jasonData["latitude"].string!
                    self.longAddress = jasonData["longitude"].string!
                    
                    let dateFormatterTgl = DateFormatter()
                    dateFormatterTgl.dateFormat = "yyyy-MM-dd"
                    let dateFormatterTglPrint = DateFormatter()
                    dateFormatterTglPrint.dateFormat = "MMMM dd, YYYY"
                    let dateFormatterJam = DateFormatter()
                    dateFormatterJam.dateFormat = "h:mm:ss"
                    let dateFormatterJamPrint = DateFormatter()
                    dateFormatterJamPrint.dateFormat = "HH.mm a"
                    
                    let dateFrom = jasonData["date_from"].string!
                    print("cari ", dateFrom)
                    let fromTgl = dateFrom.substring(with: (dateFrom.characters.index(dateFrom.startIndex, offsetBy: 0) ..< dateFrom.characters.index(dateFrom.endIndex, offsetBy: -9)))
                    let fromJam = dateFrom.substring(with: (dateFrom.characters.index(dateFrom.startIndex, offsetBy: 11) ..< dateFrom.characters.index(dateFrom.endIndex, offsetBy: 0)))
                    if let date = dateFormatterTgl.date(from: fromTgl) {
                        let dateView = dateFormatterTglPrint.string(from: date)
                        self.btnFromTanggal.setTitle(dateView, for: UIControlState.normal)
                    }

                    self.btnFromTanggal.setTitleColor(UIColor.black, for: .normal)
                    self.saveFromTgl = fromTgl
                    if let date = dateFormatterJam.date(from: fromJam) {
                        let dateView = dateFormatterJamPrint.string(from: date)
                        self.btnFromJam.setTitle(dateView, for: UIControlState.normal)
                    }
                    self.btnFromJam.setTitleColor(UIColor.black, for: .normal)
                    self.saveFromJam = fromJam

                    let dateTo = jasonData["date_to"].string!
                    let toTgl = dateTo.substring(with: (dateTo.characters.index(dateTo.startIndex, offsetBy: 0) ..< dateTo.characters.index(dateTo.endIndex, offsetBy: -9)))
                    let toJam = dateTo.substring(with: (dateTo.characters.index(dateTo.startIndex, offsetBy: 11) ..< dateTo.characters.index(dateTo.endIndex, offsetBy: 0)))
                    if let date = dateFormatterTgl.date(from: toTgl) {
                        let dateView = dateFormatterTglPrint.string(from: date)
                        self.btnToTanggal.setTitle(dateView, for: UIControlState.normal)
                    }
                    self.btnToTanggal.setTitleColor(UIColor.black, for: .normal)
                    self.saveToTgl = toTgl
                    if let date = dateFormatterJam.date(from: toJam) {
                        let dateView = dateFormatterJamPrint.string(from: date)
                        self.btnToJam.setTitle(dateView, for: UIControlState.normal)
                    }
                    self.btnToJam.setTitleColor(UIColor.black, for: .normal)
                    self.saveToJam = toJam
                    
                    let dateSub = jasonData["date_submission"].string!
                    let subTgl = dateSub.substring(with: (dateSub.characters.index(dateSub.startIndex, offsetBy: 0) ..< dateSub.characters.index(dateSub.endIndex, offsetBy: -9)))
                    let subJam = dateSub.substring(with: (dateSub.characters.index(dateSub.startIndex, offsetBy: 11) ..< dateSub.characters.index(dateSub.endIndex, offsetBy: 0)))
                    if let date = dateFormatterTgl.date(from: subTgl) {
                        let dateView = dateFormatterTglPrint.string(from: date)
                        self.btnDeadTgl.setTitle(dateView, for: UIControlState.normal)
                    }
                    self.btnDeadTgl.setTitleColor(UIColor.black, for: .normal)
                    self.saveDeadTgl = subTgl
                    if let date = dateFormatterJam.date(from: subJam) {
                        let dateView = dateFormatterJamPrint.string(from: date)
                        self.btnDeadJam.setTitle(dateView, for: UIControlState.normal)
                    }
                    self.btnDeadJam.setTitleColor(UIColor.black, for: .normal)
                    self.saveDeadJam = subJam
                    
                    self.jsonDataTicket = jasonData["ticket"]
                    let jlmTicket = self.jsonDataTicket.count
                    self.heightViewMain.constant = self.heightViewMain.constant + (100 * CGFloat(jlmTicket))
                    self.hieghtTBTicket.constant = 100 * CGFloat(jlmTicket)
                    self.heightConn.constant = 100 * CGFloat(jlmTicket)
                    self.dataNameTicket.removeAll()
                    self.dataTypePriceTicket.removeAll()
                    self.dataPriceTicket.removeAll()
                    self.dataQtyTicket.removeAll()
                    
                    self.jsonDataCP = jasonData["contact_event"]
                    let jlmCP = self.jsonDataCP.count
                    self.heightViewMain.constant = self.heightViewMain.constant + (100 * CGFloat(jlmCP))
                    self.heightViewTbCP.constant = 100 * CGFloat(jlmCP)
                    self.heightTbCP.constant = 100 * CGFloat(jlmCP)
                    self.dataNameCP.removeAll()
                    self.dataNumberCP.removeAll()
                    self.dataCodeCallCP.removeAll()
                    
                    let isPysical = jasonData["is_physical_ticket"].intValue
                    if isPysical == 1 {
                        self.swPhysicalTicket.isOn = true
                        self.heightViewMain.constant += 40
                        self.heightPhysicalTicket.constant = 40
                        self.viewPhysicalTicket.isHidden = false
                    }else{
                        self.swPhysicalTicket.isOn = false
                    }
                    
                    if let isOTS = jasonData["physical_ticket_address"].string {
                        self.swTicketOTS.isOn = false
                        self.heightViewMain.constant += 140
                        self.heightPhysicalTicket.constant = 180
                        self.viewTicketOTS.isHidden = false
                        
                        self.txtTicketLocation.text = isOTS
                        self.latAddressTicket = jasonData["physical_ticket_latitude"].string!
                        self.longAddressTicket = jasonData["physical_ticket_longitude"].string!
                        
                        let dateTicketFrom = jasonData["physical_ticket_date_start"].string!
                        let ticketFromTgl = dateTicketFrom.substring(with: (dateTicketFrom.characters.index(dateTicketFrom.startIndex, offsetBy: 0) ..< dateTicketFrom.characters.index(dateTicketFrom.endIndex, offsetBy: -9)))
                        let ticketFromJam = dateTicketFrom.substring(with: (dateTicketFrom.characters.index(dateTicketFrom.startIndex, offsetBy: 11) ..< dateTicketFrom.characters.index(dateTicketFrom.endIndex, offsetBy: 0)))
                        if let date = dateFormatterTgl.date(from: ticketFromTgl) {
                            let dateView = dateFormatterTglPrint.string(from: date)
                            self.btnTicketFromDate.setTitle(dateView, for: UIControlState.normal)
                        }
                        self.btnTicketFromDate.setTitleColor(UIColor.black, for: .normal)
                        self.saveTicketFromTgl = ticketFromTgl
                        if let date = dateFormatterJam.date(from: ticketFromJam) {
                            let dateView = dateFormatterJamPrint.string(from: date)
                            self.btnTicketFromTime.setTitle(dateView, for: UIControlState.normal)
                        }
                        self.btnTicketFromTime.setTitleColor(UIColor.black, for: .normal)
                        self.saveTicketFromJam = ticketFromJam
                        
                        let dateTicketTo = jasonData["physical_ticket_date_end"].string!
                        let ticketToTgl = dateTicketTo.substring(with: (dateTicketTo.characters.index(dateTicketTo.startIndex, offsetBy: 0) ..< dateTicketTo.characters.index(dateTicketTo.endIndex, offsetBy: -9)))
                        let ticketToJam = dateTicketTo.substring(with: (dateTicketTo.characters.index(dateTicketTo.startIndex, offsetBy: 11) ..< dateTicketTo.characters.index(dateTicketTo.endIndex, offsetBy: 0)))
                        if let date = dateFormatterTgl.date(from: ticketToTgl) {
                            let dateView = dateFormatterTglPrint.string(from: date)
                            self.btnTicketToDate.setTitle(dateView, for: UIControlState.normal)
                        }
                        self.btnTicketToDate.setTitleColor(UIColor.black, for: .normal)
                        self.saveTicketToTgl = ticketToTgl
                        if let date = dateFormatterJam.date(from: ticketToJam) {
                            let dateView = dateFormatterJamPrint.string(from: date)
                            self.btnTicketToTime.setTitle(dateView, for: UIControlState.normal)
                        }
                        self.btnTicketToTime.setTitleColor(UIColor.black, for: .normal)
                        self.saveTicketToJam = ticketToJam
                    }else{
                        self.swTicketOTS.isOn = true
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.tbCP.reloadData()
                self.tbTicket.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnAvaTapped(_ sender: UIButton) {
        BrowseFoto(sender)
    }
    
    func takePhotoTapped(_ sender: UITapGestureRecognizer) {
        BrowseFoto(btnAva)
    }
    func BrowseFoto(_ sender: UIButton) {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.modalPresentationStyle = .popover
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.cameraTapped()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.libraryTapped()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds;
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    // Camera
    func cameraTapped(){
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            self.imgPicker.delegate = self;
            
            self.imgPicker.sourceType = UIImagePickerControllerSourceType.camera
            self.imgPicker.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
            self.imgPicker.allowsEditing = true
            self.imgPicker.videoMaximumDuration = 15
            self.present(self.imgPicker, animated: true, completion: nil)
        }
        else
        {
            libraryTapped()
        }
    }
    // Library Image
    func libraryTapped(){
        self.imgPicker.allowsEditing = true
        self.imgPicker.sourceType = .photoLibrary
        present(imgPicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    var postImageName = ""
    var postImageUrl = ""
    var paramsUploadAva = 0
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var pickedImage: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            pickedImage = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            pickedImage = originalImage
        }
        
        imgAva.contentMode = .scaleToFill
        imgAva.image = pickedImage
        
        let imageName = UUID().uuidString
        postImageName = imageName
        let imagePath = getDocumentsDirectory().appendingPathComponent(imageName+".jpg")
        postImageUrl = imagePath
        paramsUploadAva = 1
        
        let jpegData = UIImageJPEGRepresentation(pickedImage!, 0.8)
        try? jpegData!.write(to: URL(fileURLWithPath: imagePath), options: [.atomic])
        
        let imageSize = jpegData?.count
        print("size of image in KB: ", imageSize! / 1024)
        
        dismiss(animated: true, completion: nil)
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtDesc.text == "Description" {
            txtDesc.text = nil
            txtDesc.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtDesc.text!.isEmpty {
            txtDesc.text = "Description"
            txtDesc.textColor = UIColor.gray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count
        self.lblLeftDesc.text = String(500 - numberOfChars)
        return numberOfChars < 500;
    }
    
    var idCountrySelected: Int!
    var nameCountrySelected = ""
    var listDataCountry = [""]
    @IBAction func txtCountryEditing(_ sender: AnyObject) {
        let urlString = Config().urlMain + "get_country"
        let word = txtCountry.text
        Alamofire.request(urlString, method: .post, parameters: ["like" : word!]).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                self.listDataCountry.removeAll()
                for (key, _) in jsonData{
                    let dataCountry = jsonData[key]["country"]
                    self.listDataCountry.append(String(describing: dataCountry))
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                if self.txtCountry.text != "" {
                    self.setupCountry()
                    self.listCountry.show()
                }else{
                    self.listCountry.hide()
                }
        }
    }
    
    var idCitySelected: Int!
    var nameCitySelected = ""
    var listDataCity = [""]
    @IBAction func txtCityEditing(_ sender: AnyObject) {
        if self.idCountrySelected == nil {
            self.msgError = "Please select country first"
            self.paramsError = 1
            self.alertStatus()
            return
        }
        let urlString = Config().urlMain + "get_city/" + String(idCountrySelected)
        let word = txtCity.text
        Alamofire.request(urlString, method: .post, parameters: ["like" : word!]).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                self.listDataCity.removeAll()
                for (key, value) in jsonData{
                    self.idCitySelected = Int(key)!
                    self.listDataCity.append(String(describing: value))
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                if self.txtCity.text != "" {
                    self.setupCity()
                    self.listCity.show()
                }else{
                    self.listCity.hide()
                }
        }
    }
    
    func getIdCountryOrCity(_ params: String){
        var urlString = ""
        var like = ""
        var keySave = ""
        
        if params == "country" {
            urlString = Config().urlMain + "get_country"
            like = self.nameCountrySelected
        }else{
            urlString = Config().urlMain + "get_city/" + String(self.idCountrySelected)
            like = self.nameCitySelected
        }
        
        Alamofire.request(urlString, method: .post, parameters: ["like" : like]).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                for (key, _) in jsonData{
                    keySave = key
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                if params == "country" {
                    self.idCountrySelected = Int(keySave)!
                    print(self.idCountrySelected)
                }else{
                    self.idCitySelected = Int(keySave)!
                    print(self.idCitySelected)
                }
        }
    }
    
    //Dropdown Country and City
    func setupCountry() {
        listCountry.anchorView = txtCountry
        listCountry.bottomOffset = CGPoint(x: 0, y: txtCountry.bounds.height)
        listCountry.direction = .bottom
        listCountry.dataSource = listDataCountry
        
        listCountry.selectionAction = { [unowned self] (index, item) in
            self.txtCountry.text = item
            self.txtCountry.resignFirstResponder()
            self.nameCountrySelected = item
            self.getIdCountryOrCity("country")
        }
    }
    
    func setupCity() {
        listCity.anchorView = txtCity
        listCity.bottomOffset = CGPoint(x: 0, y: txtCity.bounds.height)
        listCity.direction = .bottom
        listCity.dataSource = listDataCity
        
        listCity.selectionAction = { [unowned self] (index, item) in
            self.txtCity.text = item
            self.txtCity.resignFirstResponder()
            self.nameCitySelected = item
            self.getIdCountryOrCity("city")
        }
    }
    
    var dateFrom = ""
    @IBAction func btnFromTglTapped(_ sender: AnyObject) {
        self.showDateTimePicker("from tgl")
    }
    
    @IBAction func btnFromJamTapped(_ sender: AnyObject) {
        self.showDateTimePicker("from jam")
    }
    
    @IBAction func btnToTglTapped(_ sender: AnyObject) {
        self.showDateTimePicker("to tgl")
    }
    
    @IBAction func btnToJamTapped(_ sender: AnyObject) {
        self.showDateTimePicker("to jam")
    }
    
    @IBAction func btnDeadTglTapped(_ sender: AnyObject) {
        self.showDateTimePicker("dead tgl")
    }
    
    @IBAction func btnDeadJamTapped(_ sender: AnyObject) {
        self.showDateTimePicker("dead jam")
    }
    
    @IBAction func btnTicketFromDateTapped(_ sender: AnyObject) {
        self.showDateTimePicker("ticket from tgl")
    }
    
    @IBAction func btnTicketFromTimeTapped(_ sender: AnyObject) {
        self.showDateTimePicker("ticket from jam")
    }
    
    @IBAction func btnTicketToDateTapped(_ sender: AnyObject) {
        self.showDateTimePicker("ticket to tgl")
    }
    
    @IBAction func btnTicketToTimeTapped(_ sender: AnyObject) {
        self.showDateTimePicker("ticket to jam")
    }
    
    @IBAction func datePickerAction(_ sender: UIDatePicker) {} //Gakepake
    
    var dateFormatter: DateFormatter!
    var saveDateFormatter: DateFormatter!
    var saveFromTgl = ""
    var saveFromJam = ""
    var saveToTgl = ""
    var saveToJam = ""
    var saveDeadTgl = ""
    var saveDeadJam = ""
    var saveTicketFromTgl = ""
    var saveTicketFromJam = ""
    var saveTicketToTgl = ""
    var saveTicketToJam = ""
    func showDateTimePicker(_ sender: String){
        if self.activeField != nil {
            self.activeField?.resignFirstResponder()
        }else{
            self.view.endEditing(true)
        }
        let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showDateTimePicker") as! DateTimePicker
        self.addChildViewController(Popover)
        Popover.view.frame = self.view.frame
        Popover.dari = 1
        Popover.untuk = sender
        if sender == "from tgl" {
            Popover.datePicker.datePickerMode = UIDatePickerMode.date
            Popover.datePicker.minimumDate = Date()
        }
        if sender == "from jam" {
            Popover.datePicker.datePickerMode = UIDatePickerMode.time
            Popover.datePicker.minimumDate = .none
        }
        if sender == "to tgl" {
            Popover.datePicker.datePickerMode = UIDatePickerMode.date
            self.dateFormatter.dateFormat = "YYYY-MM-dd"
            Popover.datePicker.minimumDate = self.dateFormatter.date(from: saveFromTgl)
        }
        if sender == "to jam" {
            Popover.datePicker.datePickerMode = UIDatePickerMode.time
            Popover.datePicker.minimumDate = .none
        }
        if sender == "dead tgl" {
            Popover.datePicker.datePickerMode = UIDatePickerMode.date
            Popover.datePicker.minimumDate = Date()
        }
        if sender == "dead jam" {
            Popover.datePicker.datePickerMode = UIDatePickerMode.time
            Popover.datePicker.minimumDate = .none
        }
        if sender == "ticket from tgl" {
            Popover.datePicker.datePickerMode = UIDatePickerMode.date
            Popover.datePicker.minimumDate = Date()
        }
        if sender == "ticket from jam" {
            Popover.datePicker.datePickerMode = UIDatePickerMode.time
            Popover.datePicker.minimumDate = .none
        }
        if sender == "ticket to tgl" {
            Popover.datePicker.datePickerMode = UIDatePickerMode.date
            self.dateFormatter.dateFormat = "YYYY-MM-dd"
            Popover.datePicker.minimumDate = self.dateFormatter.date(from: saveTicketFromTgl)
        }
        if sender == "ticket to jam" {
            Popover.datePicker.datePickerMode = UIDatePickerMode.time
            Popover.datePicker.minimumDate = .none
        }
        self.view.addSubview(Popover.view)
        print("add sub view")
        Popover.didMove(toParentViewController: self)
    }
    
    var datePicked: Date!
    var wbFromTo = ""
    @IBAction func doneSelectDateTime(_ sender: UIStoryboardSegue){
        if wbFromTo == "from tgl" {
            self.dateFormatter.dateFormat = "MMMM dd, YYYY"
            self.btnFromTanggal.setTitle(self.dateFormatter.string(from: datePicked), for: UIControlState())
            self.btnFromTanggal.setTitleColor(UIColor.black, for: UIControlState())
            //Save
            self.saveDateFormatter.dateFormat = "YYYY-MM-dd"
            self.saveFromTgl = self.saveDateFormatter.string(from: datePicked)
        }
        if wbFromTo == "from jam" {
            self.dateFormatter.dateFormat = "HH.mm a"
            self.btnFromJam.setTitle(self.dateFormatter.string(from: datePicked), for: UIControlState())
            self.btnFromJam.setTitleColor(UIColor.black, for: UIControlState())
            //Save
            self.saveDateFormatter.dateFormat = "HH:mm:ss"
            self.saveFromJam = self.saveDateFormatter.string(from: datePicked)
        }
        if wbFromTo == "to tgl" {
            self.dateFormatter.dateFormat = "MMMM dd, YYYY"
            self.btnToTanggal.setTitle(self.dateFormatter.string(from: datePicked), for: UIControlState())
            self.btnToTanggal.setTitleColor(UIColor.black, for: UIControlState())
            //Save
            self.saveDateFormatter.dateFormat = "YYYY-MM-dd"
            self.saveToTgl = self.saveDateFormatter.string(from: datePicked)
        }
        if wbFromTo == "to jam" {
            self.dateFormatter.dateFormat = "HH.mm a"
            self.btnToJam.setTitle(self.dateFormatter.string(from: datePicked), for: UIControlState())
            self.btnToJam.setTitleColor(UIColor.black, for: UIControlState())
            //Save
            self.saveDateFormatter.dateFormat = "HH:mm:ss"
            self.saveToJam = self.saveDateFormatter.string(from: datePicked)
        }
        if wbFromTo == "dead tgl" {
            self.dateFormatter.dateFormat = "MMMM dd, YYYY"
            self.btnDeadTgl.setTitle(self.dateFormatter.string(from: datePicked), for: UIControlState())
            self.btnDeadTgl.setTitleColor(UIColor.black, for: UIControlState())
            //Save
            self.saveDateFormatter.dateFormat = "YYYY-MM-dd"
            self.saveDeadTgl = self.saveDateFormatter.string(from: datePicked)
        }
        if wbFromTo == "dead jam" {
            self.dateFormatter.dateFormat = "HH.mm a"
            self.btnDeadJam.setTitle(self.dateFormatter.string(from: datePicked), for: UIControlState())
            self.btnDeadJam.setTitleColor(UIColor.black, for: UIControlState())
            //Save
            self.saveDateFormatter.dateFormat = "HH:mm:ss"
            self.saveDeadJam = self.saveDateFormatter.string(from: datePicked)
        }
        if wbFromTo == "ticket from tgl" {
            self.dateFormatter.dateFormat = "MMMM dd, YYYY"
            self.btnTicketFromDate.setTitle(self.dateFormatter.string(from: datePicked), for: UIControlState())
            self.btnTicketFromDate.setTitleColor(UIColor.black, for: UIControlState())
            //Save
            self.saveDateFormatter.dateFormat = "YYYY-MM-dd"
            self.saveTicketFromTgl = self.saveDateFormatter.string(from: datePicked)
        }
        if wbFromTo == "ticket from jam" {
            self.dateFormatter.dateFormat = "HH.mm a"
            self.btnTicketFromTime.setTitle(self.dateFormatter.string(from: datePicked), for: UIControlState())
            self.btnTicketFromTime.setTitleColor(UIColor.black, for: UIControlState())
            //Save
            self.saveDateFormatter.dateFormat = "HH:mm:ss"
            self.saveTicketFromJam = self.saveDateFormatter.string(from: datePicked)
        }
        if wbFromTo == "ticket to tgl" {
            self.dateFormatter.dateFormat = "MMMM dd, YYYY"
            self.btnTicketToDate.setTitle(self.dateFormatter.string(from: datePicked), for: UIControlState())
            self.btnTicketToDate.setTitleColor(UIColor.black, for: UIControlState())
            //Save
            self.saveDateFormatter.dateFormat = "YYYY-MM-dd"
            self.saveTicketToTgl = self.saveDateFormatter.string(from: datePicked)
        }
        if wbFromTo == "ticket to jam" {
            self.dateFormatter.dateFormat = "HH.mm a"
            self.btnTicketToTime.setTitle(self.dateFormatter.string(from: datePicked), for: UIControlState())
            self.btnTicketToTime.setTitleColor(UIColor.black, for: UIControlState())
            //Save
            self.saveDateFormatter.dateFormat = "HH:mm:ss"
            self.saveTicketToJam = self.saveDateFormatter.string(from: datePicked)
        }
    }
    
    @IBAction func btnDoneDatePickerTapped(_ sender: AnyObject) { } //Gakepake
    
    var latAddress = ""
    var longAddress = ""
    @IBAction func txtAddressTapped(_ sender: AnyObject) {
        
    }
    
    @IBAction func btnMapTapped(_ sender: AnyObject) {
        self.untukLocMap = 0
        self.performSegue(withIdentifier: "showSelectMapsEvent", sender: self)
    }
    
//    var tagBtnCallCode = 0
    @IBAction func btnCallCodeTapped(_ sender: AnyObject) {
        let tag = sender.tag
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showCallCode") as! CallCodes
        vc.modalPresentationStyle = .popover
        vc.tagBtn = tag!
        vc.popoverPresentationController!.delegate = self
        vc.popoverPresentationController!.permittedArrowDirections = .down
        vc.popoverPresentationController!.permittedArrowDirections = .up
        vc.popoverPresentationController?.sourceView = sender as? UIView
        vc.popoverPresentationController?.sourceRect = sender.bounds
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func txtNameCPTyping(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview as! TicketCell
        let indexPath = self.tbCP.indexPath(for: cell)
        
        dataNameCP[(indexPath?.row)!] = cell.txtTicketName.text!
    }
    
    @IBAction func txtPhoneCPTyping(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview as! TicketCell
        let indexPath = self.tbCP.indexPath(for: cell)
        
        dataNumberCP[(indexPath?.row)!] = cell.txtPrice.text!
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        // Force popover style
        return UIModalPresentationStyle.none
    }
    
    @IBAction func backToAddEvent(_ sender: UIStoryboardSegue){
        print("welcome back")
        self.tbStaff.reloadData()
    }
    
    @IBAction func btnMataUangTapped(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview as! TicketCell
        let indexPath = self.tbTicket.indexPath(for: cell)
        
        listMataUang.anchorView = cell.btnMataUang
        listMataUang.bottomOffset = CGPoint(x: 0, y: cell.btnMataUang.bounds.height)
        listMataUang.direction = .bottom
        listMataUang.dataSource = ["IDR","USD","EUR"]
        
        listMataUang.selectionAction = { [unowned self] (index, item) in
            cell.btnMataUang.setTitle(item, for: UIControlState())
            self.dataTypePriceTicket[(indexPath?.row)!] = item.lowercased()
        }
        
        listMataUang.show()
    }
    
    @IBAction func txtTicketNameEditing(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview as! TicketCell
        let indexPath = self.tbTicket.indexPath(for: cell)
        
        dataNameTicket[(indexPath?.row)!] = cell.txtTicketName.text!
    }
    
    @IBAction func txtTicketPriceEditing(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview as! TicketCell
        let indexPath = self.tbTicket.indexPath(for: cell)
        
        dataPriceTicket[(indexPath?.row)!] = cell.txtPrice.text!
    }
    
    @IBAction func txtTicketQtyEditing(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview as! TicketCell
        let indexPath = self.tbTicket.indexPath(for: cell)
        
        dataQtyTicket[(indexPath?.row)!] = cell.txtQty.text!
    }
    
    var dataNameCP = [""]
    var dataCodeCallCP = ["+62"]
    var dataNumberCP = [""]
    @IBAction func btnAddCPTapped(_ sender: AnyObject) {
        dataNameCP.append("")
        dataCodeCallCP.append("+62")
        dataNumberCP.append("")
        
        tbCP.reloadData()
        
        let height: CGFloat = 82
        heightViewMain.constant += height
        heightViewTbCP.constant += height
        heightTbCP.constant += height
    }
    
    var dataNameTicket = [""]
    var dataTypePriceTicket = ["idr"]
    var dataPriceTicket = [""]
    var dataQtyTicket = [""]
    @IBAction func btnAddTicketTapped(_ sender: AnyObject) {
        dataTicket.append("")
        dataNameTicket.append("")
        dataTypePriceTicket.append("idr")
        dataPriceTicket.append("")
        dataQtyTicket.append("")
        
        tbTicket.reloadData()
        
        // Possition
        let height: CGFloat = 82
        heightViewMain.constant += height
        hieghtTBTicket.constant += height
        heightConn.constant += height
    }
    
    // Table
    var dataTicket = [""]
    var idtbTicket = [String]()
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tbCP {
            if self.paramsJson == 1 {
                return self.jsonDataCP.count
            }
            return dataNameCP.count
        }else if tableView == tbTicket {
            if self.paramsJson == 1 {
                return self.jsonDataTicket.count
            }
            return dataTicket.count
        }else if tableView == tbStaff {
            return nameStaffSelected.count + 1
        }
        return 0
    }
    
    var nameStaffSelected = [String]()
    var avaStaffSelected = [String]()
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ticketCell", for: indexPath) as! TicketCell
        
        if tableView == tbCP {
            if self.paramsJson == 1 {
                let data = self.jsonDataCP[indexPath.row]
                cell.txtTicketName.text = data["name"].string
                cell.txtPrice.text = data["phone"].string
                
                self.dataNameCP.append(data["name"].string!)
                self.dataCodeCallCP.append("+62")
                self.dataNumberCP.append(data["phone"].string!)
            }else{
                cell.btnMataUang.tag = indexPath.row
                cell.btnMataUang.setTitle(self.dataCodeCallCP[indexPath.row], for: UIControlState())
            }
        }else if tableView == tbTicket {
            if self.paramsJson == 1 {
                let data = self.jsonDataTicket[indexPath.row]
                cell.txtTicketName.text = data["ticket_name"].string
                let mataUang = data["price_type"].string
                cell.btnMataUang.setTitle(mataUang?.uppercased(), for: .normal)
                cell.txtPrice.text = data["price"].string
                cell.txtQty.text = data["quota"].string
                
                self.idtbTicket.append(data["idtb_ticket_event"].string!)
                self.dataNameTicket.append(data["ticket_name"].string!)
                self.dataTypePriceTicket.append(data["price_type"].string!)
                self.dataPriceTicket.append(data["price"].string!)
                self.dataQtyTicket.append(data["quota"].string!)
            }
        }else if tableView == tbStaff {
            if indexPath.row < self.nameStaffSelected.count {
                let ava = avaStaffSelected[indexPath.row]
                if ava != "" {
                    cell.imgAva.loadImageUsingCacheWithUrlString(ava)
                }else{
                    cell.imgAva.image = UIImage(named: "default-avatar")
                }
                cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
                cell.imgAva.clipsToBounds = true
                cell.lblTitle.text = nameStaffSelected[indexPath.row]
                
                if self.heightTbStaff.constant < 160 {
                    self.heightViewTbStaff.constant += 55
                    self.heightTbStaff.constant += 55
                    self.heightViewMain.constant += 55
                }
            }else{
                //Add
                cell.imgAva.image = UIImage(named: "ico_add_user")
                cell.lblTitle.text = "Add Staff"
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath){
        if tableView == tbStaff {
            if indexPath.row < self.nameStaffSelected.count {
            }else{
                self.performSegue(withIdentifier: "showChoiceMethodeStaff", sender: self)
            }
        }
    }
    
    @IBAction func swPhysicalTicketAction(_ sender: AnyObject) {
        if swPhysicalTicket.isOn == true {
            self.heightViewMain.constant += 40
            self.heightPhysicalTicket.constant = 40
            self.viewPhysicalTicket.isHidden = false
        }else{
            self.heightViewMain.constant -= 40
            self.heightPhysicalTicket.constant = 0
            self.viewPhysicalTicket.isHidden = true
            if swTicketOTS.isOn == false {
                self.viewTicketOTS.isHidden = true
                self.swTicketOTS.isOn = true
                self.heightViewMain.constant -= 140
            }
        }
    }
    
    @IBAction func swTicketOTSAction(_ sender: AnyObject) {
        if swTicketOTS.isOn == false {
            self.heightViewMain.constant += 140
            self.heightPhysicalTicket.constant = 180
            self.viewTicketOTS.isHidden = false
        }else{
            self.heightViewMain.constant -= 140
            self.heightPhysicalTicket.constant = 40
            self.viewTicketOTS.isHidden = true
        }
    }
    
    var latAddressTicket = ""
    var longAddressTicket = ""
    var untukLocMap = 0
    @IBAction func btnTicketSelectLocationTapped(_ sender: AnyObject) {
        self.untukLocMap = 1
        self.performSegue(withIdentifier: "showSelectMapsEvent", sender: self)
    }
    
    var alertLoading: UIAlertController!
    @IBAction func btnSaveTapped(_ sender: AnyObject) {
        self.alertLoading = UIAlertController(title: "", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        self.present(self.alertLoading, animated: true, completion: nil)
        if self.untuk == "edit" {
            makeSerializeEditTapped()
        }else{
            makeSerialize()
        }
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        var listInterest = [String:AnyObject]()
        var arrayInterest = [NSString]()
        var i = 0
        for name in self.dataNameTicket {
            listInterest = [
                "name" : name as AnyObject,
                "price" : self.dataPriceTicket[i] as AnyObject,
                "price_type" : self.dataTypePriceTicket[i] as AnyObject,
                "quota" : self.dataQtyTicket[i] as AnyObject
            ]
            let data = try! JSONSerialization.data(withJSONObject: listInterest, options: .prettyPrinted)
            let stringData = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            arrayInterest += [stringData!]
            i += 1
        }
        
        var listCP = [String:AnyObject]()
        var arrayCP = [NSString]()
        var iCP = 0
        for name in self.dataNameCP {
            listCP = [
                "name" : name as AnyObject,
                "phone" : self.dataCodeCallCP[iCP] + self.dataNumberCP[iCP] as AnyObject
            ]
            let data = try! JSONSerialization.data(withJSONObject: listCP, options: .prettyPrinted)
            let stringData = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            arrayCP += [stringData!]
            iCP += 1
        }
        
        var dateFromSave = ""
        if self.saveFromTgl != "" {
            dateFromSave = self.saveFromTgl + " " + self.saveFromJam
        }
        var dateTo = ""
        if self.saveToTgl != "" {
            dateTo = self.saveToTgl + " " + self.saveToJam
        }
        var dateSubmission = ""
        if self.saveDeadTgl != "" {
            dateSubmission = self.saveDeadTgl + " " + self.saveDeadJam
        }
        var idCountry = ""
        if self.idCountrySelected != nil {
            idCountry = String(self.idCountrySelected)
        }
        var idCity = ""
        if self.idCitySelected != nil {
            idCity = String(self.idCitySelected)
        }
        
        //Data Ticket
        var isPhysical = 0
        if swPhysicalTicket.isOn == true {
            isPhysical = 1
        }
        var ticketOTS = 0
        var ticketAddress = ""
        var ticketFrom = ""
        var ticketTo = ""
        if swTicketOTS.isOn == false {
            ticketOTS = 1
            ticketFrom =  self.saveTicketFromTgl + " " + self.saveTicketFromJam
            ticketTo = self.saveTicketToTgl + " " + self.saveTicketToJam
            ticketAddress = self.txtTicketLocation.text!
        }
        //---
        
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "id_group" : self.idGroup as AnyObject,
            "image" : self.postImageName as AnyObject,
            "name" : txtName.text! as AnyObject,
            "fk_country" : idCountry as AnyObject,
            "fk_city" : idCity as AnyObject,
            "description" : txtDesc.text! as AnyObject,
            "address" : txtAddress.text! as AnyObject,
            "date_from" : dateFromSave as AnyObject,
            "date_to" : dateTo as AnyObject,
            "date_submission" : dateSubmission as AnyObject,
        ]
        
        var arrayDataSecond = [String:AnyObject]()
        arrayDataSecond = [
            "latitude" : self.latAddress as AnyObject,
            "langitude" : self.longAddress as AnyObject,
            "use_physical_ticket" : isPhysical as AnyObject,
            "physical_ticket_address" : ticketAddress as AnyObject,
            "physical_ticket_latitude" : self.latAddressTicket as AnyObject,
            "physical_ticket_longitude" : self.longAddressTicket as AnyObject,
            "physical_ticket_date_start" : ticketFrom as AnyObject,
            "physical_ticket_date_end": ticketTo as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        let strJason = String(jsonString!)
        let jasonData = String(strJason.characters.dropLast())
        
        //for arrayDataSecond
        let dataSecond = try! JSONSerialization.data(withJSONObject: arrayDataSecond, options: .prettyPrinted)
        let jsonStringSecond = NSString(data: dataSecond, encoding: String.Encoding.utf8.rawValue)
        let strJasonSecond = String(jsonStringSecond!)
        let jsonDataSecond = String(strJasonSecond.characters.dropLast())
        let jasonDataSecond = String(jsonDataSecond.characters.dropFirst())
        
        let passData = ("\(jasonData),\n\(jasonDataSecond),\n\"ticket\" : \(arrayInterest),\n\"contact\" : \(arrayCP)\n}")
        print("array data: ", passData)
        
        //Validation ketika ada data kosong
        for (key, value) in arrayData {
            let isi = String(describing: value)
            let countIsi = isi.characters.count
            if countIsi == 0 {
                print("nill: ", key)
                self.msgError = "Please complete the data"
                self.paramsError = 1
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
                return
            }
        }
        
        print("kirim")
        print(passData)
        
        makeBase64(passData as NSString)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            if let base64Decoded = Data(base64Encoded: base64Encoded, options:   NSData.Base64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: String.Encoding.utf8.rawValue) })
            {
                
            }
            
            postData(base64Encoded)
        }
    }
    
    var msgError = ""
    var paramsError = 0 // 0:Sukses Create, 1:Error, 2:Sukses Update
    func postData(_ message: String){
        let urlString = Config().urlEvent + "add_event"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print("params: \(parameters)")
        //Upload Image
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            
            multipartFormData.append(URL(fileURLWithPath: self.postImageUrl), withName: "event")
            
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: urlString,
        encodingCompletion: {
            encodingResult in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON {
                    response in
                    
                    if let value = response.result.value {
                        let jsonData = JSON(value)
                        print(jsonData)
                        if jsonData["code"] == "1"{
                            print("Success")
                            self.msgError = ""
                            self.paramsError = 0
                            self.idEvent = String(jsonData["msg"].intValue)
                        }else{
                            self.msgError = jsonData["msg"].string!
                            self.paramsError = 1
                        }
                        self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                self.msgError = "Something went wrong, try again later"
                self.paramsError = 1
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
            }
        })
    }
    
    func makeSerializeEditTapped(){
        var arrayData = [String:AnyObject]()
        var listInterest = [String:AnyObject]()
        var arrayInterest = [NSString]()
        var i = 0
        for name in self.dataNameTicket {
            listInterest = [
                "name" : name as AnyObject,
                "price" : self.dataPriceTicket[i] as AnyObject,
                "price_type" : self.dataTypePriceTicket[i] as AnyObject,
                "quota" : self.dataQtyTicket[i] as AnyObject
            ]
            let data = try! JSONSerialization.data(withJSONObject: listInterest, options: .prettyPrinted)
            let stringData = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            
            if i < self.jsonDataTicket.count {
                let strJason = String(stringData!)
                let jasonData = String(strJason.characters.dropLast())
                let idtbInterest = self.idtbTicket[i]
                let plusIdtbInterest = ("\(jasonData),\n\"id_ticket\" : \(idtbInterest)\n}")
                arrayInterest += [NSString(string: plusIdtbInterest)]
            }else{
                arrayInterest += [stringData!]
            }
            i += 1
        }
        
        var listCP = [String:AnyObject]()
        var arrayCP = [NSString]()
        var iCP = 0
        for name in self.dataNameCP {
            listCP = [
                "name" : name as AnyObject,
                "phone" : self.dataCodeCallCP[iCP] + self.dataNumberCP[iCP] as AnyObject
            ]
            let data = try! JSONSerialization.data(withJSONObject: listCP, options: .prettyPrinted)
            let stringData = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            arrayCP += [stringData!]
            iCP += 1
        }
        var dateFromSave = ""
        if self.saveFromTgl != "" {
            dateFromSave = self.saveFromTgl + " " + self.saveFromJam
        }
        var dateTo = ""
        if self.saveToTgl != "" {
            dateTo = self.saveToTgl + " " + self.saveToJam
        }
        var dateSubmission = ""
        if self.saveDeadTgl != "" {
            dateSubmission = self.saveDeadTgl + " " + self.saveDeadJam
        }
        
        var idCountry = 0
        if self.idCountrySelected != nil {
            idCountry = self.idCountrySelected
        }
        var idCity = 0
        if self.idCitySelected != nil {
            idCity = self.idCitySelected
        }
        
        //Data Ticket
        var isPhysical = 0
        if swPhysicalTicket.isOn == true {
            isPhysical = 1
        }
        var ticketOTS = 0
        var ticketAddress = ""
        var ticketFrom = ""
        var ticketTo = ""
        if swTicketOTS.isOn == false {
            ticketOTS = 1
            ticketFrom =  self.saveTicketFromTgl + " " + self.saveTicketFromJam
            ticketTo = self.saveTicketToTgl + " " + self.saveTicketToJam
            ticketAddress = self.txtTicketLocation.text!
        }
        //---
        
        arrayData = [
            "id_event" : self.idEventEdit as AnyObject,
            "id_user" : self.idUser as AnyObject,
            "id_group" : self.idGroup as AnyObject,
            "image" : self.postImageName as AnyObject,
            "name" : txtName.text! as AnyObject,
            "fk_country" : idCountry as AnyObject,
            "fk_city" : idCity as AnyObject,
            "description" : txtDesc.text! as AnyObject,
            "address" : txtAddress.text! as AnyObject
        ]
        
        var arrayDataSecond = [String:AnyObject]()
        arrayDataSecond = [
            "date_from" : dateFromSave as AnyObject,
            "date_to" : dateTo as AnyObject,
            "date_submission" : dateSubmission as AnyObject,
            "latitude" : self.latAddress as AnyObject,
            "langitude" : self.longAddress as AnyObject,
            "use_physical_ticket" : isPhysical as AnyObject,
            "physical_ticket_address" : ticketAddress as AnyObject,
            "physical_ticket_latitude" : self.latAddressTicket as AnyObject,
            "physical_ticket_longitude" : self.longAddressTicket as AnyObject,
            "physical_ticket_date_start" : ticketFrom as AnyObject,
            "physical_ticket_date_end": ticketTo as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        let strJason = String(jsonString!)
        let jasonData = String(strJason.characters.dropLast())
        let passData = ("\(jasonData),\n\(arrayDataSecond),\n\"ticket\" : \(arrayInterest),\n\"contact\" : \(arrayCP)\n}")
        
        makeBase64EditTapped(passData as NSString)
    }
    
    func makeBase64EditTapped(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            print("Encoded:  \(base64Encoded)")
            
            if let base64Decoded = Data(base64Encoded: base64Encoded, options:   NSData.Base64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: String.Encoding.utf8.rawValue) })
            {
                print("INI EDIT")
                print("Decoded:  \(base64Decoded!)")
            }
            
            postDataEditTapped(base64Encoded)
        }
    }
    
    func postDataEditTapped(_ message: String){
        let urlString = Config().urlEvent + "update_event"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        
        //Upload Image
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            
            if self.paramsUploadAva == 1 {
                multipartFormData.append(URL(fileURLWithPath: self.postImageUrl), withName: "event")
            }
            
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: urlString,
        encodingCompletion: {
            encodingResult in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON {
                    response in
                    
                    if let value = response.result.value {
                        let jsonData = JSON(value)
                        print(jsonData)
                        if jsonData["code"] == "1"{
                            self.msgError = "Success"
                            self.paramsError = 2
                        }else{
                            self.msgError = jsonData["msg"].string!
                            self.paramsError = 1
                        }
                        self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                self.msgError = "Something went wrong, try again later"
                self.paramsError = 1
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
            }
        })
    }
    
    var idEvent = ""
    func alertStatus(){
        if self.paramsError == 0 {
            let alert = UIAlertController(title: "", message: "Event has been published", preferredStyle: UIAlertControllerStyle.alert)
            self.present(alert, animated: true, completion: nil)
            alert.dismiss(animated: true, completion: backToListEvent)
        }else{
            let alert = UIAlertController(title: "Info", message: msgError, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                if self.paramsError == 2 {
                    self.performSegue(withIdentifier: "backToListEvent", sender: self)
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func backToListEvent(){
        self.performSegue(withIdentifier: "backToListEvent", sender: self)
    }
    
    func showAddStaff(){
        self.performSegue(withIdentifier: "showAddStaff", sender: self)
    }
    
    @IBAction func backToCreateEvent(_ segue: UIStoryboardSegue){
        print("Back to event")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSelectMapsEvent" {
            let conn = segue.destination as! SelectMapsEventController
            conn.navigationItem.title = "Select Location"
            conn.untuk = self.untukLocMap
        }
        if segue.identifier == "showCallCode" {
            let conn = segue.destination as! CallCodes
            conn.status = 3
        }
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if activeField != nil{
//            activeField?.resignFirstResponder()
//        }else{
//            self.view.endEditing(true)
//        }
//    }
    var activeField: UITextField?
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            self.conSpaceBotView.constant = keyboardSize!.height
//            let possKeyboard = self.view.frame.height - keyboardSize!.height - activeField!.frame.height
//            if activeField?.frame.origin.y > possKeyboard {
//                if (!CGRectContainsPoint(aRect, activeField!.frame.origin)){
//                    if view.frame.origin.y == 0{
//                        self.view.frame.origin.y -= keyboardSize!.height
//                    }
//                }
//            }
        }
        self.conSpaceBotView.constant = keyboardSize!.height
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.conSpaceBotView.constant = 0
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
}
