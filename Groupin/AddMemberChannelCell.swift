//
//  AddMemberChannelCell.swift
//  Groupin
//
//  Created by Macbook pro on 8/15/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class AddMemberChannelCell: UITableViewCell {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgSelect: UIImageView!
    
}
