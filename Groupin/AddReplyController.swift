//
//  AddReplyController.swift
//  Groupin
//
//  Created by Macbook pro on 9/8/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire


class AddReplyController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate {
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var colImage: UICollectionView!
    
    @IBOutlet weak var conSpaceBotColView: NSLayoutConstraint!
    
    var idUser = 0
    var idGroup = 0
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(AddForumController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AddForumController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.loadData()
    }
    
    func loadData(){
        self.navigationItem.title = "Add reply"
        let rightMenuButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.saveReply))
        self.navigationItem.setRightBarButtonItems([rightMenuButtonItem], animated: true)
        
        self.textView.text = "Write content here..."
        imgPicker.delegate = self
        
        //Session
        let dataSession = try! Realm().objects(session.self).first
        self.idUser = dataSession!.id
        self.idGroup = dataSession!.id_group
        self.token = dataSession!.token
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if self.textView.text == "Write content here..." {
            self.textView.text = nil
            self.textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if self.textView.text!.isEmpty {
            self.textView.text = "Write content here..."
            self.textView.textColor = UIColor.gray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count
        return 1000 > numberOfChars
    }
    
    //CollectionView
    var arrayImgData = [Data]()
    func numberOfSectionsInCollectionView(_ collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrayImgData.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgSelectedThreadCell", for: indexPath) as! LikeLessThreadCell
        
        let count = arrayImgData.count
        if indexPath.row < count {
            let imgData = arrayImgData[indexPath.row]
            cell.imgLiker.image = UIImage(data: imgData)
            cell.imgLiker.layer.borderColor = UIColor.lightGray.cgColor
            cell.imgLiker.layer.borderWidth = 0.5
        }else{
            cell.imgLiker.image = UIImage(named: "ico_plus_gray_nobg")
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath)
    {
        let count = arrayImgData.count
        if indexPath.row < count {
            let imgData = arrayImgData[indexPath.row]
        }else{
            if 10 > count {
                self.dari = "insert"
                self.libraryTapped()
            }
        }
    }
    
    // Library Image
    let imgPicker = UIImagePickerController()
    func libraryTapped(){
        self.imgPicker.allowsEditing = false
        self.imgPicker.sourceType = .photoLibrary
        present(imgPicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    var dari = ""
    var postImageName = ""
    var postImageUrl = ""
    var paramsUploadAva = 0
    var arrayImgPath = [URL]()
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var pickedImage: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            pickedImage = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            pickedImage = originalImage
        }
        
        let imageName = UUID().uuidString
        let imagePath = getDocumentsDirectory().appendingPathComponent(imageName+".jpg")
        
        paramsUploadAva = 1
        postImageUrl = imagePath
        postImageName = imageName
        
        let jpegData = UIImageJPEGRepresentation(pickedImage!, 0.8)
        try? jpegData!.write(to: URL(fileURLWithPath: imagePath), options: [.atomic])
        
        if dari == "insert"{
            arrayImgData.append(jpegData!)
            let imgUrl = URL(fileURLWithPath: imagePath)
            arrayImgPath.append(imgUrl)
            colImage.reloadData()
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    //==============================================
    
    var alertLoading: UIAlertController!
    func saveReply(){
        self.alertLoading = UIAlertController(title: "", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        self.present(self.alertLoading, animated: true, completion: nil)
        makeSerialize()
    }
    
    var idThread = ""
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "id_thread" : self.idThread as AnyObject,
            "thumb" : self.postImageName as AnyObject,
            "content" : textView.text! as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        let strJason = String(jsonString!)
        
        makeBase64(strJason as NSString)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            print("Encoded:  \(base64Encoded)")
            
            if let base64Decoded = Data(base64Encoded: base64Encoded, options:   NSData.Base64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: String.Encoding.utf8.rawValue) })
            {
                print("Decoded:  \(base64Decoded!)")
            }
            
            postData(base64Encoded)
            
        }
    }
    
    var msgError = ""
    var paramsError = 0
    func postData(_ message: String){
        let urlString = Config().urlThread + "reply_thread"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        
        //Upload Image
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            
            if self.paramsUploadAva == 1 {
                multipartFormData.append(URL(fileURLWithPath: self.postImageUrl), withName: "thumb")
            }
            
            if self.arrayImgPath.count > 0 {
                var i = 0
                for _ in self.arrayImgPath {
                    multipartFormData.append(self.arrayImgPath[i], withName: "imgthread[\(i)]")
                    i += 1
                }
            }
            
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: urlString,
           encodingCompletion: {
                encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON {
                        response in
                        
                        if let value = response.result.value {
                            let jsonData = JSON(value)
                            print(jsonData)
                            if jsonData["code"] == "1"{
                                self.msgError = "Success"
                                self.paramsError = 1
                            }else{
                                self.msgError = jsonData["msg"].string!
                                self.paramsError = 2
                            }
                            self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        })
    }
    
    func alertStatus(){
        if msgError != "" {
            let alert = UIAlertController(title: "Info", message: msgError, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                //redirect
                if self.msgError == "Success" {
                    self.performSegue(withIdentifier: "backToDetailForum", sender: self)
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        self.conSpaceBotColView.constant = keyboardSize!.height
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.conSpaceBotColView.constant = 8
    }
}
