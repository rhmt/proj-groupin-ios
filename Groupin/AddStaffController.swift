//
//  AddStaffController.swift
//  Groupin
//
//  Created by Macbook pro on 12/21/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import Foundation
import UIKit

class AddStaffController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.navigationItem.title = "Add Staff"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    var TableArray = ["memberCell","nonCell"]
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.TableArray[indexPath.row], for: indexPath) as UITableViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.performSegue(withIdentifier: "showAddStaffMember", sender: self)
        }else{
            self.performSegue(withIdentifier: "showAddStaffNon", sender: self)
        }
    }
}
