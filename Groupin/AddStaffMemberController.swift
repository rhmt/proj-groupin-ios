//
//  AddStaffMemberController.swift
//  Groupin
//
//  Created by Macbook pro on 1/9/17.
//  Copyright © 2017 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class AddStaffMemberController: UIViewController {
    
    @IBOutlet weak var viewIco: UIView!
    @IBOutlet weak var imgIco: UIImageView!
    @IBOutlet weak var tbUser: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Design
        self.viewIco.layer.cornerRadius = self.viewIco.frame.width / 2
        self.imgIco.clipsToBounds = true
        
        let idSession = try! Realm().objects(session.self).first
        self.idGroup = idSession!.id_group
        self.token = idSession!.token
        
        self.navigationItem.title = "Add From Group Member"
        
        self.rightMenu()
        self.loadLocal()
        self.makeSerialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var idGroup = 0
    var token = ""
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    func rightMenu(){
        let rightMenuButtonItem:UIBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(AddStaffMemberController.btnAddStaffTapped(_:)))
        
        self.navigationItem.setRightBarButtonItems([rightMenuButtonItem], animated: true)
    }
    
    var dataMember = [listMember]()
    func loadLocal(){
        //Data Member
        let dataMembers = try! Realm().objects(listMember.self).filter("id_group = %@", self.idGroup).sorted(byKeyPath: "name", ascending: true)
        self.dataMember.removeAll()
        for item in dataMembers {
            self.dataMember += [item]
        }
        
        //Refresh
        if self.paramsJson == 1 {
            self.tbUser.reloadData()
        }
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idGroup as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            self.getMember(base64Encoded)
        }
    }
    
    var paramsJson = 0
    func getMember(_ message: String){
        let urlString = Config().urlUser + "list_member_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                let listMembers = jsonData["msg"]
                self.paramsJson = 1
                print(listMembers)
                
                for (key, _) in listMembers {
                    if let model = try! Realm().objects(listMember.self).filter("id_member = %@", Int(key)!).first {
                        try! Realm().write({
                            
                            if let imgUrl = listMembers[key]["avatar"].string {
                                model.avatar = imgUrl
                            }
                            model.name = listMembers[key]["name"].string!
                            model.status_level = listMembers[key]["user_level"].string!
                        })
                    }else{
                        let modelSave = listMember()
                        modelSave.id_member = Int(key)!
                        modelSave.id_group = self.idGroup
                        modelSave.avatar = listMembers[key]["avatar"].string!
                        modelSave.name = listMembers[key]["name"].string!
                        modelSave.status_level = listMembers[key]["user_level"].string!
                        modelSave.from_group = listMembers[key]["from_group"].string!
                        DBHelper.insert(modelSave)
                    }
                    
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.loadLocal()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataMember.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listMember", for: indexPath) as! InviteContactCell
        
        let data = self.dataMember[indexPath.row]
        
        if data.avatar != ""{
            cell.imgAva.loadImageUsingCacheWithUrlString(data.avatar)
        }else{
            cell.imgAva.image = UIImage(named: "default-avatar")
        }
        cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
        cell.imgAva.clipsToBounds = true
        cell.lblNama.text = data.name
        
        cell.imgSelect.isHidden = true
        for index in currentRow {
            if index == indexPath.row {
                cell.imgSelect.isHidden = false
            }
        }
        
        return cell
    }
    
    var nameMemberSelect = [String]()
    var avaMemberSelect = [String]()
    var phoneSelect = [Int]()
    var currentRow = [Int]()
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        // Find phone is exist or not in phoneSelect
        let data = dataMember[indexPath.row]
        var status = ""
        for index in phoneSelect {
            if index == data.id_member {
                status = "ada"
            }
        }
        
        // add or remove phone in phoneSelect
        let selectedRowIndex = indexPath.row
        if status == "" {
            //Select
            phoneSelect.append(data.id_member)
            nameMemberSelect.append(data.name)
            avaMemberSelect.append(data.avatar)
            currentRow.append(selectedRowIndex)
        }else{
            //Deselect
            let idData = self.phoneSelect.index(of: data.id_member)
            phoneSelect.remove(at: idData!)
            let inx = self.currentRow.index(of: indexPath.row)
            currentRow.remove(at: inx!)
        }
        tbUser.reloadData()
    }
    
    func btnAddStaffTapped(_ sender: UIButton) {
        print("done")
        self.performSegue(withIdentifier: "backToAddEvent", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "backToAddEvent" {
            let conn = segue.destination as! AddEventController
            conn.nameStaffSelected += self.nameMemberSelect
            conn.avaStaffSelected += self.avaMemberSelect
        }
    }
}
