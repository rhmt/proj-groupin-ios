//
//  AddStaffNonMemberController.swift
//  Groupin
//
//  Created by Macbook pro on 1/9/17.
//  Copyright © 2017 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class AddStaffNonMemberController: UIViewController, UINavigationControllerDelegate, UIPopoverPresentationControllerDelegate {
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var btnCode: UIButton!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    
    var token = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sess = try! Realm().objects(session.self).first!
        self.token = sess.token
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.navigationItem.title = "Add Staff"
        
        self.rightMenu()
    }
    
    func rightMenu(){
        let rightMenuButtonItem:UIBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(AddStaffNonMemberController.btnAddStaffTapped(_:)))
        
        self.navigationItem.setRightBarButtonItems([rightMenuButtonItem], animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCallCode" {
            let conn = segue.destination as! CallCodes
            conn.status = 4
        }
        if segue.identifier == "backToAddEvent" {
            let conn = segue.destination as! AddEventController
            conn.nameStaffSelected.append(self.txtName.text!)
            conn.avaStaffSelected.append("")
        }
    }
    
    @IBAction func btnCodeTapped(_ sender: AnyObject) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showCallCode") as! CallCodes
        vc.modalPresentationStyle = .popover
        vc.popoverPresentationController!.delegate = self
        vc.popoverPresentationController!.permittedArrowDirections = .down
        vc.popoverPresentationController!.permittedArrowDirections = .up
        vc.popoverPresentationController?.sourceView = sender as? UIButton
        vc.popoverPresentationController?.sourceRect = (sender as! UIButton).frame
        self.present(vc, animated: true, completion: nil)
//        self.performSegueWithIdentifier("showCallCode", sender: self)
    }
    
    func btnAddStaffTapped(_ sender: UIButton) {
        print("done")
        self.performSegue(withIdentifier: "backToAddEvent", sender: self)
    }
    
    var codeCall = ""
    @IBAction func backToAddStaffNonMember(_ sender: UIStoryboardSegue){
        self.btnCode.setTitle(self.codeCall, for: UIControlState())
        print("wb")
    }
}
