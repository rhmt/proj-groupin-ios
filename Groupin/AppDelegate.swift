
//
//  AppDelegate.swift
//  Groupin
//
//  Created by Macbook pro on 6/24/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

import Firebase
import FirebaseMessaging
import FirebaseInstanceID

import GoogleMaps
import GooglePlaces

import FileBrowser
import RealmSwift
import DropDown
import Alamofire
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //UINavigationBar.appearance().isTranslucent = false
        
        //Register DropDown
        DropDown.startListeningToKeyboard()
        
        //Google Maps
        GMSServices.provideAPIKey("AIzaSyAUP7gKhr1RYKC1c5NuOCo0nnhN0Lu9I0Q")
        GMSPlacesClient.provideAPIKey("AIzaSyAUP7gKhr1RYKC1c5NuOCo0nnhN0Lu9I0Q")
        
        //Notif
        FIRApp.configure()
        
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        
        NotificationCenter.default.addObserver(self, selector: #selector(tokenRefreshNotification(_:)),name: NSNotification.Name.firInstanceIDTokenRefresh, object: nil)
        
        //Handle Session
        let demo = try! Realm().objects(intro.self).first
        let login = try! Realm().objects(session.self).first
        if demo != nil {
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.makeKeyAndVisible()
            let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if login != nil && login!.last_login == 1 {
                print("showMenuStaff")
                let vc = storyboard.instantiateViewController(withIdentifier: "showMenuStaff") as! HomeStaffController
                window?.rootViewController = vc
            }else{
                if login != nil && login!.remember == 1 {
                    print("showHome")
                    let vc = storyboard.instantiateViewController(withIdentifier: "showHome") as! HomeController
                    window?.rootViewController = vc
                }else{
                    print("showLogin")
                    let vc = storyboard.instantiateViewController(withIdentifier: "showLogin") as! LoginController
                    window?.rootViewController = vc
                }
            }
        }
        
        return true
    }
    
    func migrationsDataRealm(){
        //Migration Database Realm
        Realm.Configuration.defaultConfiguration = Realm.Configuration(
            schemaVersion: 1,
            migrationBlock: { migration, oldSchemaVersion in
                migration.enumerateObjects(ofType: listFriend.className()) { oldObject, newObject in
                    if oldSchemaVersion < 1 {
                        newObject!["is_contact"] = 0
                    }
                }
                migration.enumerateObjects(ofType: tb_chat.className()) { oldObject, newObject in
                    if oldSchemaVersion < 1 {
                        newObject!["is_read"] = 0
                    }
                }
        })
        _ = try! Realm()
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        connectToFcm()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        let data = JSON(userInfo)
        print("notif masuk")
        print(data)
        
        
        //Show Notification Center
        if let strGroup = data["from_id"].string {
            if Int(strGroup) != nil {
                self.showNotificationCenter(userInfo, application: application, sound: true)
            }else{
                if let idGroup = data["id_group"].string {
                    if let model = try! Realm().objects(tb_group.self).filter("id = %@", idGroup).first {
                        if model.is_notif == 1 {
                            if model.is_sound == 1 {
                                self.showNotificationCenter(userInfo, application: application, sound: true)
                            }else{
                                self.showNotificationCenter(userInfo, application: application, sound: false)
                            }
                        }
                    }
                }
            }
        }
        
        //Keterangan
//        Notification
//        1:request_friend
//        2:request_user_to_group
//        3:request_child_to_group
//        4:request_user_to_room
//        5:reminder_attendance
//        6:sending_request_friend
//        7:accept_request_friend
//        8:chat_personal || di server 6
//        9:chat_group || di server 7
        
//        Update
//        1:update_status_profile
//        2:reply_thread
//        3:update_profile_picture
//        4:create_thread
//        5:comment_forum
//        6:like_thread
//        7:set_attendance
    
        let type = data["jenis_notif"].intValue
        switch type {
        case 6:
            if application.applicationState != .active {
                saveDataNotifChatToLocal(userInfo)
            }
        case 7:
            if application.applicationState != .active {
                saveDataNotifChatToLocal(userInfo)
            }
        default:
            saveDataNotifToLocal(userInfo)
        }
        
        //Badge on icon apps
        application.applicationIconBadgeNumber += 1
        
        completionHandler(UIBackgroundFetchResult.newData)
        
    }
    
    func showNotificationCenter(_ userInfo: [AnyHashable: Any], application: UIApplication, sound: Bool){
        let data = JSON(userInfo)
        
        var body = ""
        var title = ""
        if let bodies = data["body"].string {
            body = bodies
        }
        if let titles = data["title"].string {
            title = titles
        }
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: title), object: nil)
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            content.userInfo = userInfo
            content.title = title
            content.body = body
            if sound {
                content.sound = UNNotificationSound.default()
            }
            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 3.0, repeats: false)
            let request = UNNotificationRequest(identifier:"notificationCenter", content: content, trigger: trigger)
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().add(request){(error) in
                if (error != nil){
                    print(error!.localizedDescription)
                }
            }
        } else {
            // Fallback on earlier versions
            if application.applicationState == .active {
                //Foreground
                //let alert = UIAlertView(title: title, message: body, delegate: self, cancelButtonTitle: "Okay")
                //alert.show()
            }else{
                let notifLocal = UILocalNotification()
                notifLocal.userInfo = userInfo
                notifLocal.alertTitle = title
                notifLocal.alertBody = body
                if sound {
                    notifLocal.soundName = UILocalNotificationDefaultSoundName
                }
                application.scheduledLocalNotifications?.append(notifLocal)
            }
        }
    }
    
    func saveDataNotifToLocal(_ userInfo: [AnyHashable: Any]){
        //save to local
        print("Save for Notifications")
        
        let data = JSON(userInfo)
        
        let model = tb_notif()
        if let icon = data["icon"].string {model.icon = icon}
        if let date = data["date"].string {model.date = date}
        let type = data["jenis_notif"].intValue
        model.type = type
        if type == 1 {
            if let id = data["id_inviter"].string {model.id_inviter = Int(id)!; print("ada ga idnya"); print(id)}
        }
        if type == 2 {
            if let id = data["id_inviter"].int {model.id_inviter = id}
            if let idGroupTujuan = data["id_group_tujuan"].int { model.id_group = idGroupTujuan }
        }
        if type == 3 {
            if let id = data["id_inviter"].int {model.id_inviter = id}
            if let idGroupTujuan = data["id_group_tujuan"].int { model.id_group = idGroupTujuan }
            if let idGroupParent = data["id_group_parent"].int { model.id_group_parent = idGroupParent }
        }
        if type == 4 {
            if let id = data["id_inviter"].int {model.id_inviter = id}
            if let idGroupTujuan = data["id_group_tujuan"].int { model.id_group = idGroupTujuan }
            if let idRoomTujuan = data["id_room_tujuan"].int { model.id_group = idRoomTujuan }
        }
        if type == 5 {
            if let idEvent = data["id_event"].int {model.id_event = idEvent}
            if let idGroupTujuan = data["id_group"].int { model.id_group = idGroupTujuan }
        }
        if let bodies = data["body"].string {
            model.body = bodies
        }
        if let titles = data["title"].string {
            model.title = titles
        }
        DBHelper.insert(model)
    }
    
    func saveDataNotifChatToLocal(_ userInfo: [AnyHashable: Any]){
        print("Save for Chat")
        
        let data = JSON(userInfo)
        let model = tb_chat()
        if let key = data["key"].string {model.id_chat = key}
        var str = ""
        if let fromId = data["from_id"].string {model.from = fromId; str = fromId}
        if let toId = data["to_id"].string {model.to = toId}
        if let text = data["text"].string {model.text = text}
        //if let time = data["timestamp"].int {model.time = time}
        model.time = Date().timeIntervalSince1970
        if let imageUrl = data["image_url"].string {model.image_url = imageUrl}
        if let imageHeight = data["image_height"].int {model.image_height = imageHeight}
        if let imageWidth = data["image_width"].int {model.image_width = imageWidth}
        if let videoUrl = data["video_url"].string {model.video_url = videoUrl}
        if let audioUrl = data["audio_url"].string {model.audio_url = audioUrl}
        if let fileUrl = data["fileUrl"].string {model.file_url = fileUrl}
        if let mediaName = data["media_name"].string {model.mediaName = mediaName}
        if let mediaSize = data["media_size"].int {model.mediaSize = mediaSize}
        if let mediaType = data["media_type"].string {model.mediaType = mediaType}
        if let lat = data["latitude"].int {model.latitude = Float(lat)}
        if let long = data["longitude"].int {model.longitude = Float(long)}
        if let address = data["name_address"].string {model.nameAdrress = address}
        if let address = data["detail_address"].string {model.detailAdrress = address}
        if let keyReply = data["key_reply"].string {model.keyReply = keyReply}
        
        let category = try! Realm().object(ofType: tb_chat.self, forPrimaryKey: data["key"].stringValue)
        if (category != nil) {
            print("Error Primary Key")
        }else{
            DBHelper.insert(model)
        }
        
        let charset = CharacterSet(charactersIn: "g")
        if str.lowercased().rangeOfCharacter(from: charset) == nil {
            print("save list personal chat")
            saveDataListChatPersonal(userInfo)
        }
    }
    
    func saveDataListChatPersonal(_ userInfo: [AnyHashable: Any]){
        let chat = JSON(userInfo)
        var idSender = ""
        if let id = chat["from_id"].string {idSender = id}
        //Find name and avatar form tb_friend
        var userName = ""
        var userAva = ""
        if let nameAvailable = chat["title"].string {
            userName = nameAvailable
        }
        if let imgAvailable = chat["icon"].string {
            userAva = imgAvailable
        }
//        if let dataFriend = try! Realm().objects(listFriend.self).filter("id_friend = %@", Int(idSender)!).first {
//            userName = dataFriend.name
//            userAva = dataFriend.avatar
//        }
        //-----------------------------------
        
        if let modelUpdate = try! Realm().objects(listDataChatPersonal.self).filter("id_sender = %@", Int(idSender)!).first {
            try! Realm().write({
                modelUpdate.name = userName
                modelUpdate.ava = userAva
                if let text = chat["text"].string {modelUpdate.text = text}
                if let type = chat["media_type"].string {
                    if type == "Image" {modelUpdate.text = "Photo"}
                    if type == "Audio" {modelUpdate.text = "Audio"}
                    if type == "Video" {modelUpdate.text = "Video"}
                    if type == "File" {modelUpdate.text = "File"}
                    if type == "Location" {modelUpdate.text = "Location"}
                    if type == "Voice" {modelUpdate.text = "Voice"}
                }
                modelUpdate.dateLastChat = Int(Date().timeIntervalSince1970)
            })
        }else{
            let modelChat = listDataChatPersonal()
            modelChat.id_sender = Int(idSender)!
            modelChat.name = userName
            modelChat.ava = userAva
            if let text = chat["text"].string {modelChat.text = text}
            if let type = chat["media_type"].string {
                if type == "Photo" {modelChat.text = "Photo"}
                if type == "Audio" {modelChat.text = "Audio"}
                if type == "File" {modelChat.text = "File"}
                if type == "Location" {modelChat.text = "Location"}
                if type == "Voice" {modelChat.text = "Voice"}
            }
            modelChat.dateLastChat = Int(Date().timeIntervalSince1970)
            DBHelper.insert(modelChat)
        }
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        let userInfo = notification.userInfo
        print("Tapped in local notification")
        let data = JSON(userInfo!)
        print(data)

        if application.applicationState == .active {
            //Foreground -> do nothing
        }else{
            let type = data["jenis_notif"].intValue
            if type == 6 {
                print("Direct ke Chat kan")
                let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                var id = ""
                if let idAvailable = data["from_id"].string {
                    id = idAvailable
                }
                let idUser = id.replacingOccurrences(of: "G", with: "")
                var name = ""
                if let nameAvailable = data["title"].string {
                    name = nameAvailable
                }
                var img = ""
                if let imgAvailable = data["icon"].string {
                    img = imgAvailable
                }
                
                let vc = storyboard.instantiateViewController(withIdentifier: "showHome") as! HomeController
                if let data = try! Realm().objects(session_notif.self).first {
                    try! Realm().write({
                        data.fromNotif = 1
                        data.id = Int(idUser)!
                        data.name = name
                        data.avatar = img
                    })
                }else{
                    let model = session_notif()
                    model.fromNotif = 1
                    model.id = Int(idUser)!
                    model.name = name
                    model.avatar = img
                    DBHelper.insert(model)
                }
                let charset = CharacterSet(charactersIn: "g")
                if id.lowercased().rangeOfCharacter(from: charset) == nil {
                    //Chat Personal
                    vc.selectedIndex = 3
                }else{
                    //Chat Group
                    vc.selectedIndex = 0
                }
                
                window = UIWindow(frame: UIScreen.main.bounds)
                window?.makeKeyAndVisible()
                window?.rootViewController = vc
            }else{
                //Open Tab Notif
                let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "showHome") as! HomeController
                vc.selectedIndex = 2
                window = UIWindow(frame: UIScreen.main.bounds)
                window?.makeKeyAndVisible()
                window?.rootViewController = vc
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        FIRMessaging.messaging().subscribe(toTopic: "/topics/global")
        // With swizzling disabled you must set the APNs token here.
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("token baru")
        print(deviceTokenString)
        
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.prod)
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.unknown)
    }
    
    func tokenRefreshNotification(_ notification: Notification) {
        // NOTE: It can be nil here
        let refreshedToken = FIRInstanceID.instanceID().token()
        print("InstanceID token: \(refreshedToken)")
        
        connectToFcm()
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
        guard FIRInstanceID.instanceID().token() != nil else {
            return;
        }
        
        // Disconnect previous FCM connection if it exists.
        FIRMessaging.messaging().disconnect()
        
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    // Support for background fetch
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler:@escaping (UIBackgroundFetchResult) -> Void) {
        print("background service")
        completionHandler(.newData)
    }
    
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        
        print("Notification being triggered")
        
        completionHandler( [.alert,.sound,.badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        print("Tapped in notification")
        print(userInfo)
        
        let data = JSON(userInfo)
        print(data)
        
        //if application.applicationState == .active {
            //Foreground -> do nothing
        //}else{
            let type = data["jenis_notif"].intValue
            if type == 6 {
                print("Direct ke Chat")
                let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                var id = ""
                if let idAvailable = data["from_id"].string {
                    id = idAvailable
                }
                let idUser = id.replacingOccurrences(of: "G", with: "")
                var name = ""
                if let nameAvailable = data["title"].string {
                    name = nameAvailable
                }
                var img = ""
                if let imgAvailable = data["icon"].string {
                    img = imgAvailable
                }
                
                if let data = try! Realm().objects(session_notif.self).first {
                    try! Realm().write({
                        data.fromNotif = 1
                        data.id = Int(idUser)!
                        data.name = name
                        data.avatar = img
                    })
                }else{
                    let model = session_notif()
                    model.fromNotif = 1
                    model.id = Int(idUser)!
                    model.name = name
                    model.avatar = img
                    DBHelper.insert(model)
                }

                
                let vc = storyboard.instantiateViewController(withIdentifier: "showHome") as! HomeController
                
                let charset = CharacterSet(charactersIn: "g")
                if id.lowercased().rangeOfCharacter(from: charset) == nil {
                    //Chat Personal
                    vc.selectedIndex = 3
                }else{
                    //Chat Group
                    vc.selectedIndex = 0
                }
                
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
            }else{
                print("Direct ke Notif")
                let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "showHome") as! HomeController
                vc.selectedIndex = 2
                self.window = UIWindow(frame: UIScreen.main.bounds)
                window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
            }
        //}
        
        completionHandler()
    }
}
// [END ios_10_message_handling]
// [START ios_10_data_message_handling]
extension AppDelegate : FIRMessagingDelegate {
    // Receive data message on iOS 10 devices while app is in the foreground.
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
}
// [END ios_10_data_message_handling]
