//
//  BirthdayDate.swift
//  Groupin
//
//  Created by Macbook pro on 7/25/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class BirthdayDate: UIViewController {
    
    @IBOutlet weak var tanggal: UIDatePicker!
    
    var kirimTanggal = ""
    var phone = ""
    var password = ""
    var name = ""
    var gender = ""
    var negara = ""
    var kota = ""
    var interest = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSetProfile" {
            let conn = segue.destination as! SetProfileController
            
//            conn.birthday = self.kirimTanggal
            conn.phone = self.phone
            conn.password = self.password
//            conn.name = self.name
//            conn.gender = self.gender
//            conn.negara = self.negara
//            conn.kota = self.kota
//            conn.interest = self.interest
        }
    }
    
    @IBAction func btnDoneTapped(_ sender: AnyObject) {
        let formater = DateFormatter()
        formater.dateFormat = "d MMMM YYYY"
        self.kirimTanggal = formater.string(from: tanggal.date)

        self.performSegue(withIdentifier: "showSetProfile", sender: self)
    }
}
