//
//  BlockedContactController.swift
//  Groupin
//
//  Created by Macbook pro on 11/12/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class BlockedContactController: UIViewController {
    
    @IBOutlet weak var tbContact: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    
    var idUser = 0
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Blocked Contact(s)"
        self.tbContact.tableFooterView = UIView()
        self.tabBarController?.tabBar.isHidden = true
        
        self.loadLocal()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        //Session
        let sess = try! Realm().objects(session.self).first
        idUser = sess!.id
        token = sess!.token
        
        makeSerialize()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    var dataData = [listBlocked]()
    func loadLocal(){
        dataData = DBHelper.getAllBlocked()
        if self.dataData.count > 0 {
            self.tbContact.isHidden = false
            self.lblNoData.isHidden = true
        }else{
            self.tbContact.isHidden = true
            self.lblNoData.isHidden = false
        }
        if self.paramsJson == 1 {
            self.paramsJson = 0
            self.tbContact.reloadData()
        }
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idUser as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    var paramsJson = 0
    func getData(_ message: String){
        let urlString = Config().urlUser + "list_member_block"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)["msg"]
                if jsonData.count > 0 {
                    self.paramsJson = 1
                    for (key, _) in jsonData {
                        let id = jsonData[key]["id"].intValue
                        let modelUpdate = try! Realm().objects(listBlocked.self).filter("id_user = %@", id)
                        if modelUpdate.count == 0 {
                            let model = listBlocked()
                            model.id_user = jsonData[key]["id"].intValue
                            model.name = jsonData[key]["name"].string!
                            if let img = jsonData[key]["avatar"].string {
                                model.ava = img
                            }else{
                                model.ava = ""
                            }
                            DBHelper.insert(model)
                        }
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.loadLocal()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listBlockedContactCell", for: indexPath) as! ListGeneralCell
        
        let data = dataData[indexPath.row]
        
        if data.ava != "" {
            cell.imgAva.loadImageUsingCacheWithUrlString(data.ava)
        }else{
            cell.imgAva.image = UIImage(named: "default-avatar")
        }
        
        cell.lblTitle.text = data.name
        
        //Rounded Ava
        cell.imgAva.layer.cornerRadius = 20
        cell.imgAva.clipsToBounds = true
        
        return cell
    }
    
    var idFriend = 0
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath){
        let data = dataData[indexPath.row]
        idFriend = data.id_user
        let nameFriend = data.name
        let alert = UIAlertController(title: "", message: "Unblock will allow \(nameFriend) to send you messages", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.deleteLocal()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteLocal(){
        let obj = try! Realm().objects(listBlocked.self).filter("id_user = %@", idFriend).first!
        try! Realm().write(){
            try! Realm().delete(obj)
        }
        
        self.makeSerializeUnblock()
    }
    
    func makeSerializeUnblock(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_login" : self.idUser as AnyObject,
            "id" : self.idFriend as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Unblock(jsonString!)
    }
    
    func makeBase64Unblock(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getDataUnblock(base64Encoded)
        }
    }
    
    func getDataUnblock(_ message: String){
        let urlString = Config().urlUpdate + "unblock_friend"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)["msg"]
                print(jsonData)
                self.paramsJson = 1
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.loadLocal()
        }
    }
    
}
