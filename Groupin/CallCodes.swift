//
//  CallCodes.swift
//  Groupin
//
//  Created by Macbook pro on 7/19/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire

class CallCodes: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{
    
//    let searchController = UISearchController(searchResultsController: nil)
    
    var phone = ""
    var pass = ""
    var code = ""
    
    @IBOutlet weak var tbCallCodes: UITableView!
    var table_data = Array<TableData>()
    
    struct TableData {
        var section:String = ""
        var country: [String] = [String]()
        var code: [String] = [String]()
    }
    
    @IBOutlet weak var searchCountry: UISearchBar!
    var listSearching = TableData()
    var isSearching : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tbCallCodes.delegate = self
        self.tbCallCodes.dataSource = self
        self.searchCountry.delegate = self
        
        Alamofire.request("http://pempekpalembanggaby.com/uploads/CountryCodes.json", method: .post)
            .responseJSON { response in
                
                if let jason = response.result.value {
                    let get = JSON(jason)
                    let jumlah = get.count
                    
                    var new_elements:TableData
                    self.table_data.removeAll()
                    
                    var i = 0
                    
                    repeat {
                        new_elements = TableData()
                        new_elements.section = "Section 1"
                        
                        new_elements.country.append(get[i]["name"].stringValue)
                        new_elements.code.append(get[i]["dial_code"].stringValue)
                    
                        self.table_data.append(new_elements)
                        self.tbCallCodes.reloadData()
                        
                        i = i + 1
                    } while i < jumlah
                }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CallCodes", for: indexPath) as! CallCodesCell
        
        if self.isSearching == true {
            cell.lblCountry.text = listSearching.country[indexPath.row]
            cell.lblCode.text = listSearching.code[indexPath.row]
        }else {
            cell.lblCountry.text = table_data[indexPath.section].country[indexPath.row]
            cell.lblCode.text = table_data[indexPath.section].code[indexPath.row]
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSearching == true {
            return self.listSearching.country.count
        }else {
            return table_data[section].country.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.isSearching == true {
            return self.listSearching.country.count
        }else {
            return table_data.count
        }
    }
    
    var status = 0 //1:Login, 2:Reg, 3:AddCp, 4:AddStaff
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isSearching == true {
            self.code = listSearching.code[indexPath.row]
        }else {
            self.code = table_data[indexPath.section].code[indexPath.row]
        }
        if self.status == 1 {
            print(self.code)
            self.performSegue(withIdentifier: "showLogin", sender: self)
            UIView.setAnimationsEnabled(false)
        }else if self.status == 2 {
            self.performSegue(withIdentifier: "showReg", sender: self)
            UIView.setAnimationsEnabled(false)
        }else if self.status == 3 {
            self.performSegue(withIdentifier: "backToAddEvent", sender: self)
        }else if self.status == 4 {
            self.performSegue(withIdentifier: "backToAddStaffNonMember", sender: self)
        }
        
    }
    
    var tagBtn = 0
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showLogin"{
            let conn = segue.destination as! LoginController
            conn.codeCall = self.code
            conn.phone = self.phone
            conn.pass = self.pass
        }
        if segue.identifier == "showReg"{
            let nav = segue.destination as! UINavigationController
            let conn = nav.topViewController as! RegPhoneController
            conn.codeCall = self.code
            conn.phone = self.phone
        }
        if segue.identifier == "backToAddEvent" {
            self.tagBtn = -1
            let conn = segue.destination as! AddEventController
            conn.dataCodeCallCP[tagBtn] = self.code
            conn.tbCP.reloadData()
        }
        if segue.identifier == "backToAddStaffNonMember" {
            let conn = segue.destination as! AddStaffNonMemberController
            conn.codeCall = self.code
        }
    }
    
    // MARK: - UISearchBar delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchCountry.text!.isEmpty {
            
            // set searching false
            self.isSearching = false
            
            // reload table view
            tbCallCodes.reloadData()
            
        }else{
            
            // set searghing true
            self.isSearching = true
            
            // empty searching array
            self.listSearching.country.removeAll(keepingCapacity: false)
            self.listSearching.code.removeAll(keepingCapacity: false)
            
            // find matching item and add it to the searcing array
            for i in 0..<self.table_data.count {
                
                let listCountry : String = table_data[i].country[0]
                if listCountry.lowercased().range(of: searchCountry.text!.lowercased()) != nil {
                    self.listSearching.country.append(listCountry)
                    //Code
                    let listCode : String = table_data[i].code[0]
                    self.listSearching.code.append(listCode)
                }
            }
            
            tbCallCodes.reloadData()
        }
        
    }
    
    // hide kwyboard when search button clicked
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("Tapped")
        searchCountry.showsCancelButton = true
        searchCountry.resignFirstResponder()
    }
    
    // hide keyboard when cancel button clicked
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("Cancel")
        searchCountry.showsCancelButton = false
        searchCountry.text = ""
        searchCountry.resignFirstResponder()
        
        tbCallCodes.reloadData()
    }
}
