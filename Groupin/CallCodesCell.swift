//
//  CallCodesCell.swift
//  Groupin
//
//  Created by Macbook pro on 7/25/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class CallCodesCell: UITableViewCell {
    
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
