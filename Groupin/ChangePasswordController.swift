//
//  ChangePasswordController.swift
//  Groupin
//
//  Created by Macbook pro on 8/2/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ChangePasswordController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtOldPass: UITextField!
    @IBOutlet weak var txtNewPass: UITextField!
    @IBOutlet weak var txtConfirmPass: UITextField!
    @IBOutlet weak var btnSet: UIButton!
    
    var idUser = 0
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtOldPass.delegate = self
        self.txtNewPass.delegate = self
        self.txtConfirmPass.delegate = self
        self.tabBarController?.tabBar.isHidden = true
        
        //Get Phone from Session
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.token = idSession!.token
        
        //Rounded Button
        btnSet.backgroundColor = UIColor(red: 83, green: 186, blue: 32)
        btnSet.layer.cornerRadius = 5
        btnSet.layer.borderWidth = 1
        btnSet.layer.borderColor = UIColor.lightGray.cgColor
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChangePasswordController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChangePasswordController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    var alertLoading: UIAlertController!
    @IBAction func btnSetTapped(_ sender: AnyObject) {
        self.alertLoading = UIAlertController(title: "Processing", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        DispatchQueue.main.async(execute: {
            self.present(self.alertLoading, animated: true, completion: nil)
        })
        
        if txtNewPass.text != txtConfirmPass.text{
            self.alertLoading.dismiss(animated: true, completion: nil)
            
            let alert = UIAlertController(title: "Error", message: "New password does not match", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                print("Password Not Match")
            }))
            self.present(alert, animated: true, completion: nil)
        }else{
            if txtNewPass.text?.characters.count > 7 {
                makeSerialize()
            }else{
                let alert = UIAlertController(title: "Error", message: "Type 8 or more letters", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    print("Minimal 8 characters")
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "old_password" : txtOldPass.text! as AnyObject,
            "password" : txtNewPass.text! as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    var msgError = ""
    func getData(_ message: String){
        let urlString = Config().urlUser + "change_password"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(urlString)
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                self.msgError = jsonData["msg"].string!
                self.changeSuccess = JSON(value)["code"].intValue
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
        }
    }
    
    func updateLocal(){
        let model = try! Realm().objects(tb_user.self).first!
        model.password = txtNewPass.text!
        DBHelper.insert(model)
    }
    
    var changeSuccess = 0
    func alertStatus(){
        let alert = UIAlertController(title: "Info", message: msgError, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            if self.changeSuccess > 0 {
                self.txtOldPass.text = ""
                self.txtNewPass.text = ""
                self.txtConfirmPass.text = ""
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    var activeField: UITextField?
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            let possKeyboard = self.view.frame.height - keyboardSize!.height - activeField!.frame.height
            if activeField?.frame.origin.y > possKeyboard {
//                if (!CGRectContainsPoint(aRect, activeField!.frame.origin)){
                    if view.frame.origin.y == 0{
                        self.view.frame.origin.y -= keyboardSize!.height
                    }
//                }
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
            else {
                
            }
        }
    }
}
