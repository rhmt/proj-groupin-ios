//
//  Message.swift
//  Groupin
//
//  Created by Macbook pro on 9/20/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import Firebase
import RealmSwift
import Realm

class Chat: NSObject {
    
    var idChat: String?
    var fromId: String?
    var text: String?
    var timestamp: Double?
    var toId: String?
    
    var imageUrl: String?
    var imageHeight: NSNumber?
    var imageWidth: NSNumber?
    
    var videoUrl: String?
    var audioUrl: String?
    var fileUrl: String?
    
    var mediaName: String?
    var mediaSize: NSNumber?
    var mediaType: String?
    
    var latitude: Float!
    var longitude: Float!
    var nameAdrress: String?
    var detailAdrress: String?
    
    var keyReply: String?
    
    var isRead: NSNumber?
    
//    func chatPartnerId() -> String? {
//        return fromId == FIRAuth.auth()?.currentUser?.uid ? toId : fromId
//    }
    
    init(dictionary: (tb_chat) ){
        super.init()
        
        idChat = dictionary.id_chat
        fromId = dictionary.from
        text = dictionary.text
        timestamp = dictionary.time as Double?
        toId = dictionary.to
        
        imageUrl = dictionary.image_url
        imageHeight = dictionary.image_height as NSNumber?
        imageWidth = dictionary.image_width as NSNumber?
        
        videoUrl = dictionary.video_url
        audioUrl = dictionary.audio_url
        fileUrl = dictionary.file_url
        
        mediaName = dictionary.mediaName
        mediaSize = dictionary.mediaSize as NSNumber?
        mediaType = dictionary.mediaType
        
        latitude = dictionary.latitude
        longitude = dictionary.longitude
        nameAdrress = dictionary.nameAdrress
        detailAdrress = dictionary.detailAdrress
        
        keyReply = dictionary.keyReply
        
//        isRead = dictionary["isRead"] as? NSNumber
    }
}
