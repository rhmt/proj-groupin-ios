//
//  ChatCell.swift
//  Groupin
//
//  Created by Macbook pro on 9/20/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import AVFoundation
import UICircularProgressRing

class ChatCell: UICollectionViewCell, UITextViewDelegate {
    
    var player: AVPlayer?
    var updater : CADisplayLink! = nil
    var showTime = ""
    var countTime = ""
    
    var message: Chat? {
        didSet {
            let seconds = self.message?.timestamp
            let sec = seconds! / 1000
            let timestampDate = Date(timeIntervalSince1970: TimeInterval(sec)) //Date(timeIntervalSince1970: sec)
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = .short
            dateFormatter.dateStyle = .none
            dateFormatter.dateFormat = "HH:mm"
            timeStamp.text = dateFormatter.string(from: timestampDate)
            
            //Play
            if let _ = self.message?.audioUrl {
                if self.message?.mediaType == "Voice" {
                    let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
                    let documentsDirectory = paths[0]
                    let masterPath = documentsDirectory.appending("/Media")
                    let audioNameLocal = self.message!.mediaName
                    let pathString = "file://" + masterPath + "/" + audioNameLocal!
                    
                    player = AVPlayer(url: URL(string: pathString)!)
                    player?.volume = 1.0
                    
                    let duration = player?.currentItem?.asset.duration
                    let sec = CMTimeGetSeconds(duration!)
                    
                    updater = CADisplayLink(target: self, selector: #selector(self.trackAudio))
                    updater.frameInterval = 1
                    updater.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
                    
                    self.sliderVoice.maximumValue = Float(ceil(sec))
                    self.sliderVoice.isContinuous = true
                    self.sliderVoice.addTarget(self, action: #selector(self.seekAction(_:)), for: .valueChanged)
                    
                    //Total waktu
                    let devide = Int(ceil(sec)) / 60
                    let showMin = devide
                    let showSec = Int(ceil(sec)) - (devide * 60)
                    var nulSec = ""
                    var nulMin = ""
                    if showSec < 10 {
                        nulSec = "0"
                    }
                    if showMin < 10 {
                        nulMin = "0"
                    }
                    
                    self.showTime = nulMin + String(showMin) + ":" + nulSec + String(showSec)
                    
                    self.timeVoice.text = self.showTime
                }
            }
        }
    }
    
    var chatController: ChatController?
    
    let activityIndicatorView: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        aiv.color = UIColor(red: 63, green: 157, blue: 247)//UIColor.black
        aiv.translatesAutoresizingMaskIntoConstraints = false
        aiv.hidesWhenStopped = true
        return aiv
    }()
    
    lazy var playButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "ico_recored")
        button.tintColor = UIColor.white
        button.setImage(image, for: UIControlState())
        button.addTarget(self, action: #selector(handlePlay), for: .touchUpInside)
        return button
    }()
    
    lazy var playAudioButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 5
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handlePlayAudio)))
        
        return imageView
    }()
    
    lazy var fileButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 5
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleOpenFile)))
        
        return imageView
    }()
    
    let mediaName: UILabel = {
        let tv = UILabel()
        tv.font = UIFont.systemFont(ofSize: 12)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = UIColor.clear
        tv.textColor = .blue
        return tv
    }()
    
    let mediaSize: UILabel = {
        let tv = UILabel()
        tv.font = UIFont.systemFont(ofSize: 12)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = UIColor.clear
        tv.textColor = .lightGray
        return tv
    }()
    
    func handlePlay() {
        if let videoName = message?.mediaName{
            print(videoName)
            chatController?.playVideo(videoName)
//            activityIndicatorView.startAnimating()
        }
    }
    
    func handlePlayAudio() {
        if let audioName = message?.mediaName {
            chatController?.playAudio(audioName)
//            activityIndicatorView.startAnimating()
            playButton.isHidden = true
            
            print("Attempting to play audio......???")
        }
    }
    
    func handleOpenFile(){
        if let fileUrlString = message?.fileUrl, let url = URL(string: fileUrlString), let name = message?.mediaName {
            chatController?.openFile(url, mediaName: name)
        }
    }
    
    func handleZoomTap(_ tapGesture: UITapGestureRecognizer) {
        if message?.videoUrl != "" {
            handlePlay()
        }else{
            if let imageView = tapGesture.view as? UIImageView {
                self.chatController?.performZoomInForStartingImageView(imageView)
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        activityIndicatorView.stopAnimating()
    }
    
    let textView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = UIColor.clear
        tv.textColor = .black
        tv.isEditable = false
        tv.isSelectable = true
        tv.dataDetectorTypes = .link
        tv.isScrollEnabled = false
        return tv
    }()
    
    let nameSender: UILabel = {
        let tv = UILabel()
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = UIColor.clear
        tv.textColor = .blue
        return tv
    }()
    
    let timeStamp: UILabel = {
        let tv = UILabel()
        tv.font = UIFont.systemFont(ofSize: 12)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = UIColor.clear
        tv.textColor = .lightGray
        return tv
    }()
    
    let statusSend: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    static let inColor = UIColor.white
    static let outColor = UIColor.white
    
    let bubbleView: UIView = {
        let view = UIView()
        view.backgroundColor = outColor
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 5
        view.layer.shadowRadius = 5
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowOpacity = 0.3
        view.layer.shadowColor = UIColor.black.cgColor
        //view.layer.masksToBounds = true
        view.backgroundColor = UIColor.red
        return view
    }()
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 16
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let senderImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let uploadingTask: UIView = {
        let upload = UIView()
        upload.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        upload.translatesAutoresizingMaskIntoConstraints = false
        upload.layer.masksToBounds = true
        return upload
    }()
    
    lazy var shareMedia: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ico_share")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ChatCell.handleShareTap(_:))))
        return imageView
    }()
    
    func handleShareTap(_ tapGesture: UITapGestureRecognizer) {
        if let imageView = tapGesture.view as? UIImageView {
            self.chatController?.handleShareMedia(imageView)
        }
    }
    
    lazy var messageImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 0
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ChatCell.handleZoomTap(_:))))
        return imageView
    }()
    
    //Voice
    var timer: Timer!
    func playVoiceTapped(_ sender: UIGestureRecognizer){
        if self.showTime != "00:00" {
            if player?.rate == 0{
                player!.play()
                self.playVoiceImageView.image = UIImage(named: "ico_voice_pause")
                
                timer = nil
                timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerDurationVoice), userInfo: nil, repeats: true)
                timer.fire()
            } else {
                player!.pause()
                self.playVoiceImageView.image = UIImage(named: "ico_voice_play")
                
                if timer != nil {
                    timer.invalidate()
                    timer = nil
                }
            }
        }
    }
    
    lazy var playVoiceImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "ico_voice_play")
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ChatCell.playVoiceTapped(_:))))
        
        return imageView
    }()
    
    let micImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "ico_record")
        return imageView
    }()
    
    let timeVoice: UILabel = {
        let tv = UILabel()
        tv.font = UIFont.systemFont(ofSize: 12)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = UIColor.clear
        tv.textColor = .gray
        tv.text = "00:00"
        return tv
    }()
    
    let sliderVoice: UISlider = {
        let slider = UISlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        return slider
    }()
    
    func seekAction(_ playbackSlider: UISlider){
        let seconds : Int64 = Int64(playbackSlider.value)
        let targetTime:CMTime = CMTimeMake(seconds, 1)
        
        player!.seek(to: targetTime)
    }
    
    var normalizedTimeAudioSeek:Float!
    func trackAudio(){
        let currTime = CMTimeGetSeconds(player!.currentTime())
        let durr = CMTimeGetSeconds(player!.currentItem!.asset.duration)
        normalizedTimeAudioSeek = Float(currTime * durr / durr)
        sliderVoice.value = normalizedTimeAudioSeek
        
        if self.countTime > self.showTime {
            self.countTime = ""
            self.timeVoice.text = self.showTime
            self.secon = 0
            self.minute = 0
            player?.pause()
            let timeNull = CMTimeMake(Int64(0), 1)
            player?.seek(to: timeNull)
            if timer != nil {
                timer.invalidate()
                timer = nil
            }
        }
        if player?.rate == 0{
            self.playVoiceImageView.image = UIImage(named: "ico_voice_play")
        }else{
            self.playVoiceImageView.image = UIImage(named: "ico_voice_pause")
        }
    }
    
    var secon = 0
    var minute = 0
    func timerDurationVoice(){
        let devide = Int(ceil(normalizedTimeAudioSeek)) / 60
        let showMin = devide
        let showSec = Int(ceil(normalizedTimeAudioSeek)) - (devide * 60)
        minute = showMin
        secon = showSec
        
        if secon < 60-1 {
            secon += 1
        }else{
            secon = 0
            minute += 1
        }
        var nulSec = ""
        var nulMin = ""
        if secon < 10 {
            nulSec = "0"
        }
        if minute < 10 {
            nulMin = "0"
        }
        
        self.countTime = nulMin + String(minute) + ":" + nulSec + String(secon)
        
        self.timeVoice.text = self.countTime
    }
    
    //Reply
    let selectedView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 63, green: 157, blue: 247).withAlphaComponent(0.7)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func viewReplyTapped(_ sender: UIGestureRecognizer){
        chatController!.tapOnReplyView(sender)
    }
    
    lazy var viewReply: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ChatCell.viewReplyTapped(_:))))
        return view
    }()
    let viewIndicatorReply: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
        return view
    }()
    let nameReply: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor(red: 63, green: 157, blue: 247)
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    let contentReply: UITextView = {
        let label = UITextView()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 14)
        label.backgroundColor = UIColor.clear
        label.isEditable = false
        label.isSelectable = false
        label.isScrollEnabled = false
        return label
    }()
    let mediaReply: UIImageView = {
        let img = UIImageView()
        img.isUserInteractionEnabled = true
        img.contentMode = UIViewContentMode.scaleAspectFit
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()

    
    //Share Location
    let nameLocation: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor(red: 63, green: 157, blue: 247)
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    let detailLocation: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 2
        return label
    }()
    func locationTapped(_ tapGesture: UITapGestureRecognizer){
        print("chat location")
        self.chatController?.locationTapped(tapGesture)
    }
    lazy var messageLocationImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 0
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.locationTapped(_:))))
        return imageView
    }()
    
    let circularProgress: UICircularProgressRingView = {
        let cp = UICircularProgressRingView()
        cp.frame = CGRect(x: 50, y: 50, width: 100, height: 100)
        cp.maxValue = 100
        cp.innerRingColor = UIColor(red: 63, green: 157, blue: 247)
        cp.outerRingColor = UIColor.clear
        return cp
    }()
    
    var bubbleWidthAnchor: NSLayoutConstraint?
    var bubbleTopAnchor: NSLayoutConstraint?
    var bubbleTopSenderAnchor: NSLayoutConstraint?
    var bubbleViewRightAnchor: NSLayoutConstraint?
    var bubbleViewLeftAnchor: NSLayoutConstraint?
    
    var textViewTopAnchor: NSLayoutConstraint?
    var textViewBottomAnchor: NSLayoutConstraint?
    var textViewHeigthComAnchor: NSLayoutConstraint?
    
    var messageImageHeigthAnchor: NSLayoutConstraint?
    var inShareAnchor: NSLayoutConstraint?
    var outShareAnchor: NSLayoutConstraint?
    
    var heightNameLocation: NSLayoutConstraint?
    var heightDetailLocation: NSLayoutConstraint?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(bubbleView)
        addSubview(textView)
        addSubview(profileImageView)
        addSubview(senderImageView)
        addSubview(nameSender)
        addSubview(timeStamp)
        addSubview(statusSend)
        addSubview(mediaName)
        addSubview(mediaSize)
        addSubview(shareMedia)
        addSubview(micImageView)
        addSubview(sliderVoice)
        addSubview(playVoiceImageView)
        addSubview(timeVoice)
        
        bubbleView.addSubview(messageImageView)
        messageImageView.leftAnchor.constraint(equalTo: bubbleView.leftAnchor).isActive = true
        messageImageView.topAnchor.constraint(equalTo: bubbleView.topAnchor).isActive = true
        messageImageView.widthAnchor.constraint(equalTo: bubbleView.widthAnchor).isActive = true
        messageImageHeigthAnchor = messageImageView.heightAnchor.constraint(equalTo: bubbleView.heightAnchor)
        messageImageHeigthAnchor?.isActive = true
        
        bubbleView.addSubview(playButton)
        playButton.leftAnchor.constraint(equalTo: messageImageView.leftAnchor, constant: 8).isActive = true
        playButton.bottomAnchor.constraint(equalTo: messageImageView.bottomAnchor, constant: -8).isActive = true
        playButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        playButton.heightAnchor.constraint(equalToConstant: 11).isActive = true
        
        bubbleView.addSubview(playAudioButton)
        playAudioButton.centerXAnchor.constraint(equalTo: bubbleView.centerXAnchor).isActive = true
        playAudioButton.topAnchor.constraint(equalTo: bubbleView.topAnchor, constant: 16).isActive = true
        playAudioButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        playAudioButton.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        bubbleView.addSubview(fileButton)
        fileButton.centerXAnchor.constraint(equalTo: bubbleView.centerXAnchor).isActive = true
        fileButton.topAnchor.constraint(equalTo: bubbleView.topAnchor, constant: 16).isActive = true
        fileButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        fileButton.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        bubbleView.addSubview(activityIndicatorView)
        activityIndicatorView.centerXAnchor.constraint(equalTo: bubbleView.centerXAnchor).isActive = true
        activityIndicatorView.centerYAnchor.constraint(equalTo: bubbleView.centerYAnchor).isActive = true
        activityIndicatorView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        activityIndicatorView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        bubbleView.addSubview(uploadingTask)
        uploadingTask.rightAnchor.constraint(equalTo: bubbleView.rightAnchor).isActive = true
        uploadingTask.topAnchor.constraint(equalTo: bubbleView.topAnchor).isActive = true
        uploadingTask.widthAnchor.constraint(equalTo: bubbleView.widthAnchor).isActive = true
        uploadingTask.heightAnchor.constraint(equalTo: bubbleView.heightAnchor).isActive = true
        
        senderImageView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        senderImageView.topAnchor.constraint(equalTo: bubbleView.topAnchor).isActive = true
        senderImageView.widthAnchor.constraint(equalToConstant: 10).isActive = true
        senderImageView.heightAnchor.constraint(equalToConstant: 10).isActive = true
        
        profileImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 32).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 32).isActive = true
        profileImageView.topAnchor.constraint(equalTo: bubbleView.topAnchor).isActive = true
        
        nameSender.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: 6).isActive = true
        nameSender.widthAnchor.constraint(equalToConstant: 200).isActive = true
        nameSender.heightAnchor.constraint(equalToConstant: 22).isActive = true
        nameSender.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        
        inShareAnchor = shareMedia.leftAnchor.constraint(equalTo: bubbleView.rightAnchor, constant: 8)
        outShareAnchor = shareMedia.rightAnchor.constraint(equalTo: bubbleView.leftAnchor, constant: -8)
        shareMedia.widthAnchor.constraint(equalToConstant: 15).isActive = true
        shareMedia.heightAnchor.constraint(equalToConstant: 15).isActive = true
        shareMedia.centerYAnchor.constraint(equalTo: bubbleView.centerYAnchor).isActive = true
        
        micImageView.topAnchor.constraint(equalTo: bubbleView.topAnchor, constant: 8).isActive = true
        micImageView.rightAnchor.constraint(equalTo: bubbleView.rightAnchor, constant: -8).isActive = true
        micImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        micImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        playVoiceImageView.leftAnchor.constraint(equalTo: bubbleView.leftAnchor, constant: 8).isActive = true
        playVoiceImageView.topAnchor.constraint(equalTo: bubbleView.topAnchor, constant: 8).isActive = true
        playVoiceImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        playVoiceImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        sliderVoice.topAnchor.constraint(equalTo: bubbleView.topAnchor, constant: 8).isActive = true
        sliderVoice.rightAnchor.constraint(equalTo: micImageView.leftAnchor, constant: -8).isActive = true
        sliderVoice.leftAnchor.constraint(equalTo: playVoiceImageView.rightAnchor, constant: 8).isActive = true
        
        timeVoice.rightAnchor.constraint(equalTo: timeStamp.leftAnchor, constant: -8).isActive = true
//        timeVoice.centerXAnchor.constraintEqualToAnchor(timeStamp.centerXAnchor).active = true
        timeVoice.bottomAnchor.constraint(equalTo: bubbleView.bottomAnchor, constant: -2).isActive = true
        timeVoice.leadingAnchor.constraint(equalTo: sliderVoice.leadingAnchor).isActive = true
        timeVoice.heightAnchor.constraint(equalToConstant: 18).isActive = true
        
        bubbleViewRightAnchor = bubbleView.rightAnchor.constraint(equalTo: senderImageView.leftAnchor , constant: -8)
        bubbleViewRightAnchor?.isActive = true
        bubbleViewLeftAnchor = bubbleView.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: 8)
        bubbleTopAnchor = bubbleView.topAnchor.constraint(equalTo: self.topAnchor)
        bubbleTopAnchor?.isActive = true
        bubbleTopSenderAnchor = bubbleView.topAnchor.constraint(equalTo: nameSender.bottomAnchor)
        bubbleWidthAnchor = bubbleView.widthAnchor.constraint(equalToConstant: 200)
        bubbleWidthAnchor?.isActive = true
        bubbleView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        //ios 9 constraints
        //x,y,w,h
        textView.leftAnchor.constraint(equalTo: bubbleView.leftAnchor, constant: 8).isActive = true
        textViewTopAnchor = textView.topAnchor.constraint(equalTo: bubbleView.topAnchor)
        textViewTopAnchor?.isActive = true
        textViewBottomAnchor = textView.bottomAnchor.constraint(equalTo: timeStamp.topAnchor)//, constant: -8)
//        textViewBottomAnchor?.active = true
        textView.rightAnchor.constraint(equalTo: bubbleView.rightAnchor).isActive = true
        textViewHeigthComAnchor = textView.heightAnchor.constraint(equalTo: self.heightAnchor)
        textViewHeigthComAnchor?.isActive = true
        
        mediaName.leftAnchor.constraint(equalTo: bubbleView.leftAnchor, constant: 8).isActive = true
        mediaName.bottomAnchor.constraint(equalTo: mediaSize.topAnchor, constant: -4).isActive = true
        mediaName.widthAnchor.constraint(equalToConstant: 180).isActive = true
        mediaName.heightAnchor.constraint(equalToConstant: 18).isActive = true
        
        mediaSize.leftAnchor.constraint(equalTo: bubbleView.leftAnchor, constant: 8).isActive = true
        mediaSize.bottomAnchor.constraint(equalTo: statusSend.topAnchor, constant: -4).isActive = true
        mediaSize.widthAnchor.constraint(equalToConstant: 180).isActive = true
        mediaSize.heightAnchor.constraint(equalToConstant: 18).isActive = true
        
        timeStamp.rightAnchor.constraint(equalTo: bubbleView.rightAnchor, constant: -8).isActive = true
        timeStamp.bottomAnchor.constraint(equalTo: bubbleView.bottomAnchor, constant: -2).isActive = true
        timeStamp.widthAnchor.constraint(equalToConstant: 36).isActive = true
        timeStamp.heightAnchor.constraint(equalToConstant: 18).isActive = true
        
        statusSend.leftAnchor.constraint(equalTo: bubbleView.leftAnchor, constant: 8).isActive = true
        statusSend.bottomAnchor.constraint(equalTo: bubbleView.bottomAnchor, constant: -4).isActive = true
        statusSend.widthAnchor.constraint(equalToConstant: 16).isActive = true
        statusSend.heightAnchor.constraint(equalToConstant: 12).isActive = true
        
        addSubview(selectedView)
//        selectedView.widthAnchor.constraintEqualToConstant(100).active = true
        selectedView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        selectedView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        selectedView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        selectedView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        //Reply
        addSubview(viewReply)
        self.viewReply.topAnchor.constraint(equalTo: bubbleView.topAnchor, constant: 8).isActive = true
        self.viewReply.bottomAnchor.constraint(equalTo: textView.topAnchor, constant: -8).isActive = true
        self.viewReply.leftAnchor.constraint(equalTo: bubbleView.leftAnchor, constant: 8).isActive = true
        self.viewReply.rightAnchor.constraint(equalTo: bubbleView.rightAnchor, constant: -8).isActive = true
        
        viewReply.addSubview(viewIndicatorReply)
        self.viewIndicatorReply.widthAnchor.constraint(equalToConstant: 3).isActive = true
        self.viewIndicatorReply.leftAnchor.constraint(equalTo: viewReply.leftAnchor).isActive = true
        self.viewIndicatorReply.topAnchor.constraint(equalTo: viewReply.topAnchor).isActive = true
        self.viewIndicatorReply.bottomAnchor.constraint(equalTo: viewReply.bottomAnchor).isActive = true
        
        viewReply.addSubview(nameReply)
        self.nameReply.topAnchor.constraint(equalTo: viewReply.topAnchor).isActive = true
        self.nameReply.leftAnchor.constraint(equalTo: viewIndicatorReply.rightAnchor, constant: 8).isActive = true
        self.nameReply.rightAnchor.constraint(equalTo: viewReply.rightAnchor, constant: -8).isActive = true
        self.nameReply.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        viewReply.addSubview(contentReply)
        self.contentReply.topAnchor.constraint(equalTo: nameReply.bottomAnchor).isActive = true
        self.contentReply.bottomAnchor.constraint(equalTo: viewReply.bottomAnchor).isActive = true
        self.contentReply.leftAnchor.constraint(equalTo: viewIndicatorReply.rightAnchor, constant: 4).isActive = true
        self.contentReply.rightAnchor.constraint(equalTo: viewReply.rightAnchor, constant: -8).isActive = true
        
        viewReply.addSubview(mediaReply)
        self.mediaReply.topAnchor.constraint(equalTo: viewReply.topAnchor).isActive = true
//        self.mediaReply.bottomAnchor.constraintEqualToAnchor(viewReply.bottomAnchor).active = true
        self.mediaReply.rightAnchor.constraint(equalTo: viewReply.rightAnchor).isActive = true
        self.mediaReply.widthAnchor.constraint(equalToConstant: 50).isActive = true
        self.mediaReply.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //Location
        addSubview(messageLocationImage)
        self.messageLocationImage.topAnchor.constraint(equalTo: bubbleView.topAnchor).isActive = true
        self.messageLocationImage.leftAnchor.constraint(equalTo: bubbleView.leftAnchor).isActive = true
        self.messageLocationImage.rightAnchor.constraint(equalTo: bubbleView.rightAnchor).isActive = true
        self.messageLocationImage.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        addSubview(nameLocation)
        self.nameLocation.topAnchor.constraint(equalTo: messageLocationImage.bottomAnchor, constant: 3).isActive = true
        self.nameLocation.leftAnchor.constraint(equalTo: bubbleView.leftAnchor, constant: 8).isActive = true
        self.nameLocation.rightAnchor.constraint(equalTo: bubbleView.rightAnchor, constant: -8).isActive = true
        self.heightNameLocation = self.nameLocation.heightAnchor.constraint(equalToConstant: 25)
        self.heightNameLocation!.isActive = true
        
        addSubview(detailLocation)
        self.detailLocation.topAnchor.constraint(equalTo: nameLocation.bottomAnchor).isActive = true
        self.detailLocation.bottomAnchor.constraint(equalTo: timeStamp.topAnchor).isActive = true
        self.detailLocation.leftAnchor.constraint(equalTo: bubbleView.leftAnchor, constant: 8).isActive = true
        self.detailLocation.rightAnchor.constraint(equalTo: bubbleView.rightAnchor, constant: -8).isActive = true
        self.heightDetailLocation = self.detailLocation.heightAnchor.constraint(equalToConstant: 25)
        self.heightDetailLocation!.isActive = true
        
        //Circular Progress
        bubbleView.addSubview(circularProgress)
        self.circularProgress.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.circularProgress.heightAnchor.constraint(equalToConstant: 100).isActive = true
        self.circularProgress.centerXAnchor.constraint(equalTo: bubbleView.centerXAnchor).isActive = true
        self.circularProgress.centerYAnchor.constraint(equalTo: bubbleView.centerYAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
