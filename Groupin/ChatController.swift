
//
//  ChatController.swift
//  Groupin
//
//  Created by Macbook pro on 9/20/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import Firebase
import MobileCoreServices
import AVKit
import AVFoundation
import FileBrowser
import DropDown
import Realm
import RealmSwift
import SwiftyJSON
import Alamofire
import CoreLocation
import GooglePlaces
import GooglePlacePicker
import AudioToolbox
import MapleBacon
import AGEmojiKeyboard
import Photos
import UICircularProgressRing

class ChatController: UICollectionViewController, UITextFieldDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, CLLocationManagerDelegate, AVAudioRecorderDelegate, AGEmojiKeyboardViewDelegate, AGEmojiKeyboardViewDataSource {
    
    var titleNav = ""
    var limitedChatView = 5
    
    var containerViewBottomAnchor: NSLayoutConstraint?
    
    var messages = [Chat]()
    
    let cellId = "cellId"
    let attach = UIButton()
    
    var locationManager:CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isTranslucent = true
        
        //KeyBoard
        self.setupKeyboardObservers()
        
        //Input textfield
        self.yInput = self.view.frame.height - self.heightInput
        self.newChatInputContainerView = makeChatInputContainerView(self.yInput, height: self.heightInput)
        self.view.addSubview(newChatInputContainerView)
        //Collection view
        collectionView?.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
        collectionView?.alwaysBounceVertical = true
        collectionView?.backgroundColor = UIColor.clear
        collectionView?.register(ChatCell.self, forCellWithReuseIdentifier: cellId)
        // Background Color
        let imgSrc = UIImage(named: "bg_chat")
        let imgBG = UIImageView(image: imgSrc)
        imgBG.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        imgBG.contentMode = UIViewContentMode.scaleAspectFill
        self.view.addSubview(imgBG)
        self.view.sendSubview(toBack: imgBG)
        collectionView?.keyboardDismissMode = .interactive
        
        DispatchQueue.main.async(execute: {
            if self.messages.count > 0 {
                self.collectionView?.reloadData()
                let indexPath = IndexPath(item: self.messages.count - 1, section: 0)
                self.collectionView?.scrollToItem(at: indexPath, at: .bottom, animated: false)
            }
        })
        
        //Recorder
        recordingSession = AVAudioSession.sharedInstance()
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async(execute: {
                    if allowed {
//                        self.loadRecordingUI()
                        print("access record okay")
                    } else {
                        // failed to record!
                        print("access record denied")
                    }
                })
            }
        } catch {
            // failed to record!
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        //KeyBoard
        self.setupKeyboardObservers()
        
        //Location
        placesClient = GMSPlacesClient.shared()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.requestAlwaysAuthorization()
    }
    
    //Data user friend
    var idFriend = [String]()
    var nameFriend = [String]()
    var avaFriend = [String]()
    
    var newChatInputContainerView: UIView!
    var paramsKindChat = ""
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        //KeyBoard
        //self.setupKeyboardObservers()
        
        //Get ID user
        let idSession = try! Realm().objects(session.self).first
        self.fromId = String(idSession!.id)
        
        //Observe Message
        observeMessages()
        checkingServer()
        
        //Nav
        let button =  UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 100, height: 40) as CGRect
        button.setTitle(self.titleNav, for: UIControlState())
        button.addTarget(self, action: #selector(ChatController.profileTapped), for: UIControlEvents.touchUpInside)
        self.navigationItem.titleView = button
        //Right
        let image = UIImage(named: "ico_attachment")
        let attach = UIButton()
        attach.setImage(image, for: UIControlState())
        attach.addTarget(self, action: #selector(ChatController.attachTapped), for: UIControlEvents.touchDown)
        attach.frame=CGRect(x: 0, y: 0, width: 26, height: 15)
        let barAttach = UIBarButtonItem(customView: attach)
        self.navigationItem.setRightBarButtonItems([barAttach], animated: true)
    }
    
    var showProfileFromTab = 0
    func profileTapped(_ sender: UITapGestureRecognizer){
        if self.paramsKindChat == "personal" {
            self.idFriendMentionSelected = Int(self.toId)!
            self.performSegue(withIdentifier: "showFriendProfile", sender: self)
        }else{
            self.performSegue(withIdentifier: "showAboutGroup", sender: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // ----- COLLECTION VIEW
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ChatCell
        
        cell.chatController = self
        let message = messages[indexPath.item]
        
        //Text Message
        cell.textView.text = message.text
        cell.textView.delegate = self
        cell.textView.resolveHashTags()
        cell.message = message
        
        //Detail Media
        if message.mediaName != "" {
            let mediaName = message.mediaName
            cell.mediaName.text = mediaName
        }
        if message.mediaSize != 0 {
            let mediaSize = message.mediaSize
            cell.mediaSize.text = String(describing: mediaSize!)
        }
        
        //Image
        let nameImage = message.mediaName
        let nameImageLocal = nameImage!.replacingOccurrences(of: " ", with: "")
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let getImagePath = paths.appendingPathComponent("/Media/\(nameImageLocal).jpg")
        print(getImagePath)
        let imgImage = UIImage(contentsOfFile: getImagePath)
        if imgImage != nil {
            cell.messageImageView.image = imgImage
        }
//        if let imgUrl = URL(string: message.imageUrl!) {
//            cell.messageImageView.setImage(withUrl: imgUrl, placeholder: imgImage, crossFadePlaceholder: true, cacheScaled: true, completion: nil)
//        }else{
//            cell.messageImageView.image = UIImage(named: "default-avatar")
//        }
        
        //hidden all attribute bubble
        cell.textView.isHidden = true
        cell.messageImageView.isHidden = true
        cell.mediaName.isHidden = true
        cell.mediaSize.isHidden = true
        cell.uploadingTask.isHidden = true
        cell.shareMedia.isHidden = false
        cell.shareMedia.tag = indexPath.row
        
        cell.playVoiceImageView.isHidden = true
        cell.sliderVoice.isHidden = true
        cell.micImageView.isHidden = true
        cell.timeVoice.isHidden = true
        
        cell.selectedView.isHidden = true
        
        cell.viewReply.isHidden = true
        
        cell.messageLocationImage.isHidden = true
        cell.nameLocation.isHidden = true
        cell.detailLocation.isHidden = true
        
        cell.circularProgress.isHidden = true
        
        cell.playButton.isHidden = message.videoUrl == ""
        cell.playAudioButton.isHidden = message.audioUrl == ""
        cell.fileButton.isHidden = message.fileUrl == ""
        
        // Style Buble
        if message.imageUrl != "" {
            cell.bubbleView.backgroundColor = UIColor.white
            cell.bubbleWidthAnchor?.constant = 200 // Panjang 200
            
            cell.textViewTopAnchor?.isActive = true
            cell.textViewBottomAnchor?.isActive = false
            cell.textViewHeigthComAnchor?.isActive = true
            
            cell.messageImageView.isHidden = false
            cell.messageImageHeigthAnchor?.isActive = true
            cell.messageImageView.bottomAnchor.constraint(equalTo: cell.bubbleView.bottomAnchor).isActive = true
            
            cell.timeStamp.textColor = UIColor.white

            if message.text != "" { // Jika Ada Caption nya
                let text = message.text
                cell.textView.isHidden = false // Text View tampil
                cell.textViewTopAnchor?.isActive = false
                cell.textViewBottomAnchor?.isActive = true
                cell.textViewHeigthComAnchor?.isActive = false // Tinggi Text View
                let heightPlusTextView = estimateFrameForText(text!).height + 20
                cell.textView.heightAnchor.constraint(equalToConstant: heightPlusTextView).isActive = true
                
                // Tinggi Image
                cell.messageImageView.bottomAnchor.constraint(equalTo: cell.textView.topAnchor).isActive = true
                cell.messageImageHeigthAnchor?.isActive = false
                cell.timeStamp.textColor = UIColor.lightGray
            }
        } else if message.text != "" {
            let text = message.text
            let widthParams = estimateFrameForText(text!).width + 32
            if widthParams <= 70 {
                cell.bubbleWidthAnchor?.constant = 70
            }else{
                cell.bubbleWidthAnchor?.constant = estimateFrameForText(text!).width + 32
            }
            
            cell.textView.isHidden = false
            cell.textViewTopAnchor?.isActive = true
            cell.textViewBottomAnchor?.isActive = false
            cell.textViewHeigthComAnchor?.isActive = true
            
            cell.messageImageHeigthAnchor?.isActive = true
            
            cell.timeStamp.textColor = UIColor.lightGray
            cell.shareMedia.isHidden = true
        }else if message.audioUrl != "" {
            if message.mediaType == "Voice"{
                cell.shareMedia.isHidden = true
                cell.bubbleWidthAnchor?.constant = 200
                
                cell.playAudioButton.image = nil
                cell.playVoiceImageView.isHidden = false
                cell.sliderVoice.isHidden = false
                cell.micImageView.isHidden = false
                cell.timeVoice.isHidden = false
                
            }else{
                cell.bubbleWidthAnchor?.constant = 200
                
                cell.playAudioButton.image = UIImage(named: "ico_media_music")
                
                cell.mediaName.isHidden = false
                cell.mediaSize.isHidden = false
                
                cell.timeStamp.textColor = UIColor.lightGray
            }
        }else if message.fileUrl != "" {
            cell.bubbleWidthAnchor?.constant = 200
            
            cell.fileButton.image = UIImage(named: "ico_media_file")
            
            cell.mediaName.isHidden = false
            cell.mediaSize.isHidden = false
            
            cell.timeStamp.textColor = UIColor.lightGray
        } else
//        if message.mediaType == "Video" {
//            cell.mediaName.hidden = false
//            cell.mediaSize.hidden = false
//            
//            cell.messageImageHeigthAnchor?.active = false
//            cell.messageImageView.bottomAnchor.constraintEqualToAnchor(cell.mediaName.topAnchor, constant: -8).active = true
//        } else
        if message.latitude != 0 {
            cell.shareMedia.isHidden = true
            cell.messageLocationImage.isHidden = false
            cell.nameLocation.isHidden = false
            cell.detailLocation.isHidden = false
            
            let staticMapUrl: String = "http://maps.google.com/maps/api/staticmap?markers=color:red|\(message.latitude!),\(message.longitude!)&\("zoom=17&size=400x400")&sensor=true"
            print(staticMapUrl)
            let mapUrl: URL = URL(string: staticMapUrl.addingPercentEscapes(using: String.Encoding.utf8)!)!
            if let imgData = try? Data(contentsOf: mapUrl) {
                let image: UIImage = UIImage(data: imgData)!
                cell.messageLocationImage.image = image
            }else{
                cell.messageLocationImage.image = UIImage(named: "ico_chat_location_no_connect")
            }
            
            var text = ""
            if message.nameAdrress != "" {
                cell.nameLocation.text = message.nameAdrress!
                cell.detailLocation.text = message.detailAdrress!
            }else{
                cell.heightNameLocation?.constant = 1
                text = message.detailAdrress!
                cell.detailLocation.text = message.detailAdrress!
            }
            
            if estimateFrameForText(text).height > 15 {
               cell.heightDetailLocation?.constant = 36
            }else{
               cell.heightDetailLocation?.constant = 25
            }
            cell.bubbleWidthAnchor?.constant = 200
        }
        
        //Ada Reply
        if message.keyReply != "" {
            //Text
            if let data = try! Realm().objects(tb_chat.self).filter("id_chat = %@", message.keyReply!).first {
                cell.viewReply.isHidden = false
                
                var nameReply = ""
                let idSelected = data.from
                if idSelected != self.fromId {
                    let id = self.idFriend.index(of: idSelected)
                    let data = self.nameFriend[id!]
                    nameReply = data
                }else{
                    nameReply = "You"
                }
                cell.nameReply.text = nameReply
                
                let heightReply = estimateFrameForText(data.text).height + 38
                cell.viewReply.heightAnchor.constraint(equalToConstant: heightReply).isActive = true
                
                cell.textViewTopAnchor?.isActive = false
                cell.textViewBottomAnchor?.isActive = false//true
                cell.textViewHeigthComAnchor?.isActive = false
                let text = message.text
                let heightPlusTextView = estimateFrameForText(text!).height + 20
                cell.textView.heightAnchor.constraint(equalToConstant: heightPlusTextView).isActive = true
                
                let widthText = self.estimateFrameForText(text!).width
                let widthReply = self.estimateFrameForText(data.text).width
                let widthNameReply = self.estimateFrameForText(nameReply).width
                
                var width: CGFloat = 0
                if widthReply > widthText {
                    width = widthReply + 42
                }else{
                    width = widthText + 42
                }
                if widthNameReply > widthReply {
                    width = widthNameReply + 42
                }
                
                let type = data.mediaType
                print(type)
                var content  = ""
                if type == "" || type == "Reply" {
                    content = data.text
                    cell.mediaReply.image = nil
                }else
                if type == "Image" {
                    if data.text != "" {
                        content = data.text
                    }else{
                        content = "Photo"
                    }
//                    cell.mediaReply.loadImageUsingCacheWithUrlString(data.image_url)
                    if let imgUrl = URL(string: data.image_url) {
                        cell.mediaReply.setImage(withUrl: imgUrl)
                    }else{
                        cell.mediaReply.image = UIImage(named: "default-avatar")
                    }
                    width += 50
                }else
                if type == "Video" {
                    content = data.mediaName
//                    cell.mediaReply.loadImageUsingCacheWithUrlString(data.image_url)
                    if let imgUrl = URL(string: data.image_url) {
                        cell.mediaReply.setImage(withUrl: imgUrl)
                    }else{
                        cell.mediaReply.image = UIImage(named: "default-avatar")
                    }
                    width += self.estimateFrameForText(data.mediaName).width + 50
                }else
                if type == "Audio"{
                    content = data.mediaName
                    cell.mediaReply.image = UIImage(named: "ico_media_music")
                    width += self.estimateFrameForText(data.mediaName).width + 50
                }else
                if type == "Voice" {
                    content = "Voice Message"
                    cell.mediaReply.image = UIImage(named: "ico_record")
                    width += self.estimateFrameForText(content).width + 50
                }else
                if type == "Location" {
                    if data.nameAdrress != "" {
                        content = data.nameAdrress
                        width += self.estimateFrameForText(data.nameAdrress).width
                    }else{
                        content = "Location"
                    }
                    cell.mediaReply.image = UIImage(named: "ico_chat_location")
                    width += 50
                }else{
                    content = data.mediaName
                    cell.mediaReply.image = UIImage(named: "ico_media_file")
                    width += self.estimateFrameForText(data.mediaName).width + 50
                }
                
                if width > 300{//200 {
                    cell.bubbleWidthAnchor?.constant = 300//200
                }else{
                    cell.bubbleWidthAnchor?.constant = width
                }
                cell.contentReply.text = content
                
            }
        }
        
        //Loading Uploading
        self.circularProgressView = cell.circularProgress
        if message.idChat == "000" {
            cell.statusSend.isHidden = true
            cell.timeStamp.isHidden = true
            cell.uploadingTask.isHidden = false
            
            //Loading Animation
            //cell.activityIndicatorView.isHidden = false
            //cell.activityIndicatorView.startAnimating()
            //cell.bubbleWidthAnchor?.constant = 200
            cell.circularProgress.isHidden = false
            
            
            //kesana
            
        }
        
        //Long Press
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressCell(_:)))
        longPressRecognizer.minimumPressDuration = 1.00
        cell.addGestureRecognizer(longPressRecognizer)
        
        setupCell(cell, message: message)
        
        return cell
    }
    
    fileprivate func setupCell(_ cell: ChatCell, message: Chat) {
        //        if let profileImageUrl = self.user?.profileImageUrl {
        //            cell.profileImageView.loadImageUsingCacheWithUrlString(profileImageUrl)
        //        }
        
        if message.fromId == self.fromId {
            //outgoing message
            cell.bubbleView.backgroundColor = ChatCell.outColor
            cell.textView.textColor = UIColor.black
            cell.senderImageView.image = UIImage(named: "ico_chat_sended")
            cell.nameSender.isHidden = true
            cell.senderImageView.isHidden = false
            cell.profileImageView.isHidden = true
            cell.statusSend.image = UIImage(named: "ico_chat_read")
            
            cell.statusSend.isHidden = false
            cell.timeStamp.isHidden = false
            cell.bubbleTopAnchor?.isActive = true
            cell.bubbleTopSenderAnchor?.isActive = false
            cell.bubbleViewRightAnchor?.isActive = true
            cell.bubbleViewLeftAnchor?.isActive = false
            
            cell.uploadingTask.isHidden = true
            cell.inShareAnchor?.isActive = false
            cell.outShareAnchor?.isActive = true
        } else {
            //incoming message
            cell.bubbleView.backgroundColor = ChatCell.inColor
            cell.textView.textColor = UIColor.black
            if self.paramsKindChat == "personal" {
                cell.nameSender.isHidden = true
                cell.nameSender.heightAnchor.constraint(equalToConstant: 0).isActive = true
                
                if self.avaFriend[0] != "" {
                    let imgUrl = URL(string: self.avaFriend[0])!
                    cell.profileImageView.setImage(withUrl: imgUrl)
                }else{
                    cell.profileImageView.image = UIImage(named: "dafault-ava-group")
                }
            }else if self.paramsKindChat == "group"{
                cell.nameSender.isHidden = false
                cell.nameSender.heightAnchor.constraint(equalToConstant: 22).isActive = true
                
                let idMember = self.idFriend.index(of: message.fromId!)
                let nameMember = self.nameFriend[idMember!]
                cell.nameSender.text = nameMember
                if self.avaFriend[idMember!] != "" {
                    let imgUrl = URL(string: self.avaFriend[idMember!])!
                    cell.profileImageView.setImage(withUrl: imgUrl)
                }else{
                    cell.profileImageView.image = UIImage(named: "dafault-ava-group")
                }
            }
            cell.senderImageView.isHidden = true
            cell.profileImageView.isHidden = false
            
            cell.statusSend.isHidden = true
            cell.timeStamp.isHidden = false
            cell.bubbleTopAnchor?.isActive = false
            cell.bubbleTopSenderAnchor?.isActive = true
            cell.bubbleViewRightAnchor?.isActive = false
            cell.bubbleViewLeftAnchor?.isActive = true
            
            cell.uploadingTask.isHidden = true
            cell.inShareAnchor?.isActive = true
            cell.outShareAnchor?.isActive = false
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var height: CGFloat = 80
        
        let message = messages[indexPath.item]
        
        if message.imageUrl != "" {
            if message.imageWidth != 0 && message.imageHeight != 0 {
                let imageWidth = message.imageWidth?.floatValue
                let imageHeight = message.imageHeight?.floatValue
                height = CGFloat(imageHeight! / imageWidth! * 200)
                if message.text != "" {
                    let text = message.text
                    let heightPlus = estimateFrameForText(text!).height + 30
                    height = CGFloat((imageHeight! / imageWidth! * 200) + Float(heightPlus))
                }
            }else{
                height = 200
            }
            if message.videoUrl != "" {
                height = 200
            }
        }else if message.text != "" {
            let text = message.text
            height = estimateFrameForText(text!).height + 30
        }else if message.audioUrl != "" {
            if message.mediaType == "Voice" {
                height = 70
            }else{
                height = 200
            }
        }else if message.fileUrl != "" {
            height = 200
        }else if message.latitude != 0 {
            height = 215
            let text = message.detailAdrress!
            if message.nameAdrress != "" {
                height += 28
            }else{
                height += 4
            }
            
            if estimateFrameForText(text).height > 15 {
                height += 40
            }else{
                height += 25
            }
        }
        if message.keyReply != "" {
            if let data = try! Realm().objects(tb_chat.self).filter("id_chat = %@", message.keyReply!).first {
                height += estimateFrameForText(data.text).height + 38
                height += estimateFrameForText(message.text!).height + 8
            }
        }
//        if message.idChat == "000" {
//            height = 200
//        }
        
        let width = UIScreen.main.bounds.width
        if message.fromId == self.fromId{
            return CGSize(width: width, height: height)
        }else{
            if self.paramsKindChat == "group" {
                height += 22
            }
            return CGSize(width: width, height: height)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showFriendProfile" {
            let conn = segue.destination as! FriendProfileController
            conn.idFriend = String(idFriendMentionSelected)
        }
        if segue.identifier == "showSendImage" {
            let conn = segue.destination as! ChatSendImagePreview
            let imgData = UIImageJPEGRepresentation(self.selectedImage, 1)
            conn.arrayImgData.append(imgData!)
            conn.numberOfImage += 1
            conn.fromId = self.fromId
            conn.toId = self.toId
        }
        if segue.identifier == "showLocation" {
            let conn = segue.destination as! EventViewMapsController
            conn.lat = Double(self.latSelected)
            conn.long = Double(self.longSelected)
            conn.chat = 1
            conn.chatLoc = self.nameLocSelected
            conn.chatLocDetail = self.detailLocSelected
            conn.navigationItem.title = "View Map"
        }
    }
    
    var idFriendMentionSelected = 0
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        // Mention
        let scheme = URL.scheme!
        print(URL)
        
        switch scheme {
        case "mention" :
            let nsText = textView.text!
            let words = nsText.components(separatedBy: CharacterSet.whitespacesAndNewlines)
            var strMention = ""
            for word in words {
                if word.hasPrefix("@") {
                    strMention = word as String
                }
            }
            let nameMention = strMention.replacingOccurrences(of: "@", with: "")
            if let name = try! Realm().objects(listMember.self).filter("name CONTAINS[c] %@", nameMention).first {
                idFriendMentionSelected = name.id_member
                self.performSegue(withIdentifier: "showFriendProfile", sender: self)
            }else{
                print("Mention not found")
            }
        default:
            print("just a regular url")
        }
        
        return true
    }
    
    var resultCount = 0
    var lastContentOffset: CGFloat = 0
    
    func countMessage() {
        let messagesRef = FIRDatabase.database().reference().child("messages")
        messagesRef.observe(.value) { (snapshot: FIRDataSnapshot!) in
            self.resultCount = Int(snapshot.childrenCount)
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        countMessage()
        let offsetY: CGFloat = scrollView.contentOffset.y
        if (offsetY < -100){
            self.limitedChatView += 5
            if self.limitedChatView > self.resultCount {
                print("UJUNG")
                
            }else{
                print("load")
//                loadMore()
//                observeMessages()
            }
        }
        self.lastContentOffset = scrollView.contentOffset.y
//        self.inputTextView.resignFirstResponder()
    }
    
//    func loadMore(){
//        self.messages.removeAll()
//        let messagesRef = FIRDatabase.database().reference().child("messages").queryLimitedToLast(UInt(limitedChatView))
//        messagesRef.observeEventType(.ChildAdded) { (snapshot: FIRDataSnapshot!) in
//            
//            guard let dictionary = snapshot.value as? [String: AnyObject] else {
//                return
//            }
//            
//            self.messages.append(Chat(dictionary: dictionary))
//            print("Append Load More")
//            dispatch_async(dispatch_get_main_queue(), {
//                self.collectionView?.reloadData()
//            })
//        }
//        
//    }
    
    // ----- STYLE
    // ----- input Text
    var heightInput: CGFloat = 50
    var yInput: CGFloat!
    var inputTextView: UITextView!
    var topAnchorInputText: NSLayoutConstraint!
    var bottomAnchorInputText: NSLayoutConstraint!
    var inputSendButton: UIButton!
    var inputUploadButton: UIImageView!
    var voiceButton: UIButton!
    var replyView: UIView!
    var replyName: UILabel!
    var replyContent: UITextView!
    var replyMedia: UIImageView!
    var emotButton: UIImageView!
    var lblDurationRecord: UILabel!
    func makeChatInputContainerView(_ y: CGFloat, height: CGFloat) ->
        ChatInputContainerView {
            let rect = CGRect(x: 0, y: y, width: self.view.frame.width, height: height)
            let chatInputContainerView = ChatInputContainerView(frame: rect)
            self.inputTextView = chatInputContainerView.inputTextField
            self.topAnchorInputText = chatInputContainerView.topInputTextAnchor
            self.bottomAnchorInputText = chatInputContainerView.bottomInputTextAnchor
            self.inputSendButton = chatInputContainerView.sendButton
            self.inputUploadButton = chatInputContainerView.uploadImageView
            self.voiceButton = chatInputContainerView.sendVoiceButton
            self.emotButton = chatInputContainerView.emoticonView
            self.replyView = chatInputContainerView.viewReply
            self.replyName = chatInputContainerView.nameReply
            self.replyContent = chatInputContainerView.contentReply
            self.replyMedia = chatInputContainerView.mediaReply
            self.lblDurationRecord = chatInputContainerView.lblDurationRecord
            chatInputContainerView.chatController = self
            return chatInputContainerView
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    fileprivate func estimateFrameForText(_ text: String) -> CGRect {
        let size = CGSize(width: 200, height: 10000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)], context: nil)
    }
    
//    var timeText: CGFloat = 1
//    var timeMin: CGFloat = 0
    
    //Typing
    func textChange(){
        // Height
        let widthView = self.inputTextView.frame.width
        let textMessage = self.inputTextView.text
        let widthText = estimateFrameForText(textMessage!).width
        
        let heightText = self.inputTextView.contentSize.height
//        let paramsHeightText = Int(heightText)
//        var timeHeight = 1
        
//        if paramsHeightText > 35 * timeHeight {
//            timeHeight += 1
//            let heightView = 30 * timeHeight
//            self.heightInput = CGFloat(heightView)
//        }
        self.heightInput = heightText + 10
        if widthText < widthView {
            self.heightInput = 50
        }
        
        var height: CGFloat!
        if self.paramsReply == 0 {
            height = self.heightInput
        }else{
            height = self.heightReply
        }
        self.yInput = self.view.frame.height - height
        let rect = CGRect(x: 0, y: self.yInput, width: self.view.frame.width, height: height)
//        self.newChatInputContainerView.frame = rect
        
        //Mention
        let nsText:NSString = self.inputTextView.text as NSString
        let words:[String] = nsText.components(separatedBy: CharacterSet.whitespacesAndNewlines)
        let wordCount = words.count
        let wordParam = wordCount - 1
        let word = words[wordParam]
        var wordSearch: String! = ""
        if word.hasPrefix("@") {
            wordSearch = word.replacingOccurrences(of: "@", with: "")

            //Data Mention
            let sess = try! Realm().objects(session.self).first!
            let idGroup = sess.id_group
            let modelsMention = try! Realm().objects(listMember.self).filter("name CONTAINS[c] %@ AND id_group = %@", wordSearch, idGroup)
            dataMention.removeAll()
            dataMentionAva.removeAll()
            
            //Show Mention
            for model in modelsMention {
                dataMention.append(model.name)
                dataMentionAva.append(model.avatar)
            }
            if dataMention.isEmpty == true {
                list.hide()//.hide()
            }else{
                setupDropDowns()
                list.show()//.show()
            }
        }else{
            list.hide()
        }
    }
    
    // DropDown Mention
    let list = DropDown()
    lazy var dropDowns: [DropDown] = { return [ self.list ] }()
    var dataMention = [""]
    var dataMentionAva = [""]
    func setupDropDowns() {
        let widthLayout: CGFloat = self.newChatInputContainerView.frame.width - self.inputSendButton.frame.width - 5.0
        list.anchorView = self.newChatInputContainerView
        list.width = widthLayout
        list.cornerRadius = 0
        list.direction = .any
        list.topOffset = CGPoint(x: 5, y: 0 - newChatInputContainerView.bounds.height)
        list.dataSource = dataMention
        
        let nsText:NSString = self.inputTextView.text as NSString
        let words = nsText.components(separatedBy: CharacterSet.whitespacesAndNewlines)
        let wordCount = words.count
        let wordParam = wordCount - 1
        let word = words[wordParam]
        var input = ""
        if word as String == "@" {
            input = String(self.inputTextView.text.characters.dropLast())
            print(input)
        }else{
            input = self.inputTextView.text.replacingOccurrences(of: word as String, with: "")
        }
        list.selectionAction = { [unowned self] (index, item) in
            print(input)
            print(item)
            self.inputTextView.text = input + "@" + item
        }
        
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().textFont = UIFont.systemFont(ofSize: 14)
        DropDown.appearance().cellHeight = 45
        list.cellNib = UINib(nibName: "CellDropDown", bundle: nil)
        list.customCellConfiguration = { (index , item , cell) -> Void in
            guard let cell = cell as? CellDropDown else { return }
            cell.imgAva.loadImageUsingCacheWithUrlString(self.dataMentionAva[index])
            cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
            cell.imgAva.clipsToBounds = true
        }
    }
    
    // ------ SEND
    var fromId = ""
    var toId = ""
    var sendCaption = ""
    func handleSend() {
        print("send chat")
        if self.sendImageCaptionParams.isEmpty {
            let properties: [String: AnyObject]!
            if self.paramsReply == 0 {
                properties = ["text": self.inputTextView.text as AnyObject]
            }else{
                properties = ["text": self.inputTextView.text as AnyObject, "mediaType": "Reply" as AnyObject, "keyReply": self.idChatReply as AnyObject]
                
                //Change text input to default
                self.yInput = self.view.frame.height - self.heightInput
                let rect = CGRect(x: 0, y: self.yInput, width: self.view.frame.width, height: self.heightInput)
                self.newChatInputContainerView.frame = rect
                self.topAnchorInputText.isActive = true
                self.bottomAnchorInputText.isActive = false
                self.replyView.isHidden = true
                self.heightReply = 0
                self.idChatReply = ""
                self.paramsReply = 0
                
            }
            sendMessageWithProperties(properties)
            self.sendImageCaptionParams = ""
        }else {
            uploadToFirebaseStorageUsingImage(self.selectedImage, completion: { (imageUrl, metadata) in
                self.sendMessageWithImageUrl(imageUrl, image: self.selectedImage, metadata: metadata)
            })
            sendedImageWithCaption()
        }
        
        // Change Send Button
        self.inputSendButton.setBackgroundImage(UIImage(named: "ico_send_disable"), for: UIControlState())
        self.inputSendButton.isEnabled = false
        self.inputSendButton.isHidden = true
        self.inputSendButton.layoutIfNeeded()
        self.inputUploadButton.isHidden = false
        self.inputUploadButton.layoutIfNeeded()
        self.voiceButton.isHidden = false
        self.voiceButton.layoutIfNeeded()
        self.sendCaption = ""//self.inputTextView.text
        
        self.paramsReply = 0
        self.heightReply = 0
        
    }
    
    fileprivate func sendMessageWithProperties(_ properties: [String: AnyObject]) {
        let ref = FIRDatabase.database().reference().child("messages")
        let childRef = ref.childByAutoId()
        let timestamp: NSNumber = NSNumber.init(value: Date().timeIntervalSince1970 * 1000)
        var values: [String: AnyObject] = ["toId": toId as AnyObject, "fromId": fromId as AnyObject, "timestamp": timestamp as AnyObject, "isRead": 0 as AnyObject]
        
        //append properties dictionary onto values somehow??
        //key $0, value $1
        properties.forEach({values[$0] = $1})
        
        childRef.updateChildValues(values) { (error, ref) in
            if error != nil {
                //                print(error)
                return
            }
            
            self.inputTextView.text = nil
            
            let userMessagesRef = FIRDatabase.database().reference().child("user-messages").child(self.fromId).child(self.toId)
            
            let messageId = childRef.key
            userMessagesRef.updateChildValues([messageId: 1])
            
            let recipientUserMessagesRef = FIRDatabase.database().reference().child("user-messages").child(self.toId).child(self.fromId)
            recipientUserMessagesRef.updateChildValues([messageId: 1])
        }
        
        self.sendNotif(values, key: childRef.key)
        self.removeBubbleTmpForUploadLoading()
        //Observe Message
        self.observeMessages()
        self.checkingServer()
        
    }
    
    fileprivate func sendMessageWithImageUrl(_ imageUrl: String, image: UIImage, metadata: FIRStorageMetadata) {
        let mediaName = Config().nameMediaFile(metadata.name!, idUser: self.fromId)
        let properties: [String: AnyObject] = ["imageUrl": imageUrl as AnyObject, "imageWidth": image.size.width as AnyObject, "imageHeight": image.size.height as AnyObject, "text": self.sendCaption as AnyObject, "mediaName": mediaName as AnyObject, "mediaType": "Image" as AnyObject, "mediaSize": Int(metadata.size) as AnyObject]
        sendMessageWithProperties(properties)
    }
    
    // ------ Emoticon
    var statusEmot = 0
    func handleEmotTap(){
        print("Emot Tap")
        if statusEmot == 0 {
            // Show Emot keyboard
            self.inputTextView.resignFirstResponder()
            let keyboardRect = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.view.frame.size.width), height: CGFloat(216))
            let emojiKeyboardView = AGEmojiKeyboardView(frame: keyboardRect, dataSource: self)
            emojiKeyboardView?.autoresizingMask = .flexibleHeight
            emojiKeyboardView?.delegate = self
            emojiKeyboardView?.tintColor = UIColor.lightGray
            self.inputTextView.inputView = emojiKeyboardView
            self.inputTextView.becomeFirstResponder()
            self.statusEmot = 1
            self.emotButton.image = UIImage(named: "ico_keyboard")
        }else{
            // Show standard keyboard
            self.inputTextView.resignFirstResponder()
            self.inputTextView.inputView = nil
            self.inputTextView.reloadInputViews()
            self.inputTextView.becomeFirstResponder()
            self.statusEmot = 0
            self.emotButton.image = UIImage(named: "ico_insert_emot")
        }
    }
    
    //AGEmojiKeyboard
    func emojiKeyBoardView(_ emojiKeyBoardView: AGEmojiKeyboardView!, didUseEmoji emoji: String!) {
        if let range = self.inputTextView.selectedTextRange {
            if range.start == range.end {
                self.inputTextView.replace(range, withText: emoji)
            }
        }
        
        // Change Send Button
        self.inputSendButton.setBackgroundImage(UIImage(named: "ico_send_selected"), for: UIControlState())
        self.inputSendButton.isEnabled = true
        self.inputSendButton.isHidden = false
        self.inputSendButton.layoutIfNeeded()
        self.inputUploadButton.isHidden = true
        self.inputUploadButton.layoutIfNeeded()
        self.voiceButton.isHidden = true
        self.voiceButton.layoutIfNeeded()
    }
    
    func emojiKeyBoardViewDidPressBackSpace(_ emojiKeyBoardView: AGEmojiKeyboardView!) {
        let currentText = self.inputTextView.text!
        if currentText != "" {
            self.inputTextView.text = currentText.substring(to: currentText.index(before: currentText.endIndex))
//            if let range = self.inputTextView.selectedTextRange {
//                if range.start == range.end {
//                    let cursorPosition = self.inputTextView.offset(from: self.inputTextView.beginningOfDocument, to: range.start)
//                    let newPosition = self.inputTextView.position(from: range.start, offset: -1)
//                    let rangeBefore = self.inputTextView.textRange(from: newPosition!, to: range.start)
//                    let charBefore = self.inputTextView.text(in: rangeBefore!)
//                    
//                    let ret = self.inputTextView.text.replacingOccurrences(of: charBefore!, with: "")
//                    self.inputTextView.text = ret
//                }
//            }
        }
        let totalChar = currentText.characters.count
        if totalChar == 1 {
            // Change Send Button
            self.inputSendButton.setBackgroundImage(UIImage(named: "ico_send_disable"), for: UIControlState())
            self.inputSendButton.isEnabled = false
            self.inputSendButton.isHidden = true
            self.inputSendButton.layoutIfNeeded()
            self.inputUploadButton.isHidden = false
            self.inputUploadButton.layoutIfNeeded()
            self.voiceButton.isHidden = false
            self.voiceButton.layoutIfNeeded()
        }
    }
    
    func emojiKeyboardView(_ emojiKeyboardView: AGEmojiKeyboardView!, imageForNonSelectedCategory category: AGEmojiKeyboardViewCategoryImage) -> UIImage! {
        let img: UIImage? = self.storeImageEmot().withRenderingMode(.alwaysOriginal)
        img?.withRenderingMode(.alwaysOriginal)
        return img
    }
    
    func emojiKeyboardView(_ emojiKeyboardView: AGEmojiKeyboardView!, imageForSelectedCategory category: AGEmojiKeyboardViewCategoryImage) -> UIImage! {
        let img: UIImage? = self.storeImageEmot().withRenderingMode(.alwaysOriginal)
        img?.withRenderingMode(.alwaysOriginal)
        emojiKeyboardView.contentMode = .scaleToFill
        return img?.withRenderingMode(.alwaysOriginal)
    }
    
    public func backSpaceButtonImage(for emojiKeyboardView: AGEmojiKeyboardView!) -> UIImage! {
        let image = UIImage(named: "ico_emot_backspace")
        emojiKeyboardView.contentMode = .scaleToFill
        return image
    }
    
    //Image Keyboard store
    var imgIndex = 0
    func storeImageEmot() -> UIImage {
        self.imgIndex += 1
        let strImage = "ico_emot_cat_" + String(self.imgIndex)
        if let img: UIImage = UIImage(named: strImage) {
            img.withRenderingMode(.alwaysOriginal)
            return img
        }else{
            let img = UIImage()
            return img
        }
    }
    
    // ------ BROWSE PICTURE AND VIDEO
    
    func handleUploadTap() {
        self.inputTextView.resignFirstResponder()
        BrowseFoto(UIButton.init())
    }
    
    func BrowseFoto(_ sender: UIButton) {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.modalPresentationStyle = .popover
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.cameraTapped()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.libraryTapped()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds;
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // Camera
    func cameraTapped(){
        if Config().checkAccessCamera() == false {
            print("access denied")
            let msg = "Please allow the app to access your camera through the Settings."
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                if granted == true {
                    // User granted
                    Config().alertForAllowAccess(self, msg: msg)
                }else{
                    // User Rejected
                    Config().alertForAllowAccess(self, msg: msg)
                }
            });
        }else{
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
            {
                let imagePickerController = UIImagePickerController()
                imagePickerController.delegate = self;
                
                imagePickerController.sourceType = UIImagePickerControllerSourceType.camera
                imagePickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
                imagePickerController.allowsEditing = true
                imagePickerController.videoMaximumDuration = 15
                self.present(imagePickerController, animated: true, completion: nil)
            }
            else
            {
                libraryTapped()
            }
        }
    }
    
    // Library Image
    func libraryTapped(){
        if Config().checkAccessPhotos() == false {
            print("access denied")
            let msg = "Please allow the app to access your photos through the Settings."
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.denied {
                    print("denied")
                    Config().alertForAllowAccess(self, msg: msg)
                }else{
                    print("another")
                    Config().alertForAllowAccess(self, msg: msg)
                }
            })
        }else{
            let imagePickerController = UIImagePickerController()
            
            imagePickerController.allowsEditing = false
            imagePickerController.sourceType = .photoLibrary
            imagePickerController.delegate = self
            imagePickerController.mediaTypes = [kUTTypeImage as String]
            
            present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let videoUrl = info[UIImagePickerControllerMediaURL] as? URL {
            handleVideoSelectedForUrl(videoUrl)
        } else {
            handleImageSelectedForInfo(info as [String : AnyObject])
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    var circularProgressView: UICircularProgressRingView!
    fileprivate func handleVideoSelectedForUrl(_ url: URL) {
        let filename = UUID().uuidString + ".mov"
        let uploadTask = FIRStorage.storage().reference().child("message_movies").child(filename).putFile(url, metadata: nil, completion: { (metadata, error) in
            
            if error != nil {
                print("Failed upload of video:")//, error)
                return
            }
            
            self.bubbleTmpForUploadLoading(100, height: 100)
            
            if let videoUrl = metadata?.downloadURL()?.absoluteString {
                if let thumbnailImage = self.thumbnailImageForFileUrl(url) {
                    
                    self.uploadToFirebaseStorageUsingImage(thumbnailImage, completion: { (imageUrl, metadata) in
                        let mediaName = Config().nameMediaFile(metadata.name!, idUser: self.fromId) + ".mov"
                        let properties: [String: AnyObject] = ["imageUrl": imageUrl as AnyObject, "imageWidth": thumbnailImage.size.width as AnyObject, "imageHeight": thumbnailImage.size.height as AnyObject, "videoUrl": videoUrl as AnyObject, "mediaName": mediaName as AnyObject, "mediaType": "Video" as AnyObject, "mediaSize": Int(metadata.size) as AnyObject]
                        print(metadata.size)
                        self.sendMessageWithProperties(properties)
                        
                    })
                }
            }
        })
        //Kesini
        uploadTask.observe(.progress) { (snapshot) in
            print("lagi upload sis")
            if let completedUnitCount = snapshot.progress?.completedUnitCount {
                print("lagi upload gan")
                let percentComplete = 100.0 * Double(completedUnitCount)
                    / Double(snapshot.progress!.totalUnitCount)
                
                self.circularProgressView.isHidden = false
                self.circularProgressView.setProgress(value: CGFloat(percentComplete), animationDuration: 0.5, completion: {
                    print("beres 2")
                })
            }
        }
        
        uploadTask.observe(.success) { (snapshot) in
            print("di hapus")
            self.removeBubbleTmpForUploadLoading()
        }
    }
    
    func bubbleTmpForUploadLoading(_ width: Int, height: Int){
        let model = tb_chat()
        model.id_chat = "000"
        model.from = self.fromId
        model.to = self.toId
        model.image_url = "dummy.jpg"
        model.image_width = width
        model.image_height = height
        let category = try! Realm().object(ofType: tb_chat.self, forPrimaryKey: "000")
        if (category != nil) {
            print("Error Primary Key")
            return
        }else{
            print("Succes")
            DBHelper.insert(model)
            self.observeMessages()
        }
        self.collectionView?.reloadData()
    }
    
    func removeBubbleTmpForUploadLoading() {
        let idChat = "000"
        DBHelper.deleteByIdChat(idChat)
        print("Deleted")
    }
    
    fileprivate func thumbnailImageForFileUrl(_ fileUrl: URL) -> UIImage? {
        let asset = AVAsset(url: fileUrl)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        
        do {
            let thumbnailCGImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60), actualTime: nil)
            return UIImage(cgImage: thumbnailCGImage)
            
        } catch let err {
            print(err)
        }
        
        return nil
    }
    
    fileprivate func handleImageSelectedForInfo(_ info: [String: AnyObject]) {
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            //sendImageWithCaption(selectedImage)
            self.selectedImage = selectedImage
            self.performSegue(withIdentifier: "showSendImage", sender: self)
        }
    }
    
    
    var sendImageCaptionParams = ""
    var selectedImage: UIImage!
    
    var sendBlackBackgroundView: UIView?
    var sendStartingImageView: UIImageView?
    var sendStartingFrame: CGRect?
    var zoomingImageView: UIImageView?
    
    fileprivate func sendImageWithCaption(_ image: UIImage){
        // Change Send Button
        self.inputSendButton.setBackgroundImage(UIImage(named: "ico_send_selected"), for: UIControlState())
        self.inputSendButton.isEnabled = true
        self.inputSendButton.layoutIfNeeded()
        self.inputUploadButton.isHidden = true
        self.inputUploadButton.layoutIfNeeded()
        self.inputTextView.text = ""
        
        self.sendImageCaptionParams = "Send Image With Caption"
        self.selectedImage = image
        
        sendStartingImageView = UIImageView(image: image)
        sendStartingImageView?.isHidden = true
        
        self.sendStartingFrame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        
        zoomingImageView = UIImageView(frame: self.sendStartingFrame!)
        zoomingImageView!.backgroundColor = UIColor.clear
        zoomingImageView!.image = self.sendStartingImageView!.image
        zoomingImageView!.isUserInteractionEnabled = true
        
        let viewText = self.newChatInputContainerView.frame.height
        if let keyWindow = UIApplication.shared.keyWindow {
            self.sendBlackBackgroundView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - viewText))//keyWindow.frame)
            self.sendBlackBackgroundView?.backgroundColor = UIColor.black
            self.sendBlackBackgroundView?.alpha = 0
            keyWindow.addSubview(self.sendBlackBackgroundView!)
            
            keyWindow.addSubview(zoomingImageView!)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.sendBlackBackgroundView?.alpha = 1
                self.newChatInputContainerView.alpha = 1
//                
//                self.inputTextView.text = ""
//                self.inputSendButton.setBackgroundImage(UIImage(named: "ico_send_selected"), forState: UIControlState.Normal)
//                self.inputSendButton.enabled = true
//                self.inputSendButton.layoutIfNeeded()
//                self.inputUploadButton.hidden = true
//                self.inputUploadButton.layoutIfNeeded()
                
                let height = self.sendStartingFrame!.height / self.sendStartingFrame!.width * keyWindow.frame.width
                self.zoomingImageView!.frame = CGRect(x: 0, y: 0, width: keyWindow.frame.width, height: height)
                self.zoomingImageView!.center = keyWindow.center
                
                }, completion: { (completed) in
                    //                    do nothing
            })
            
        }
        
    }
    
    func sendedImageWithCaption(){
        zoomingImageView!.layer.cornerRadius = 16
        zoomingImageView!.clipsToBounds = true
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.zoomingImageView!.frame = CGRect(x: self.view.frame.width/2 - 25, y: 0, width: 50, height: 50)
            self.sendBlackBackgroundView?.alpha = 0
            self.newChatInputContainerView.alpha = 1
            
            }, completion: { (completed) in
                self.zoomingImageView!.removeFromSuperview()
                self.sendStartingImageView?.isHidden = false
        })
    }
    
    fileprivate func uploadToFirebaseStorageUsingImage(_ image: UIImage, completion: @escaping (_ imageUrl: String, _ metadata: FIRStorageMetadata) -> ()) {
        let imageName = UUID().uuidString
        let ref = FIRStorage.storage().reference().child("message_images").child(imageName)
        
        if let uploadData = UIImageJPEGRepresentation(image, 0.2) {
            let uploadTask = ref.put(uploadData, metadata: nil, completion: { (metadata, error) in
                
                if error != nil {
                    print("Failed to upload image:")//, error)
                    return
                }
                
                if let imageUrl = metadata?.downloadURL()?.absoluteString {
                    completion(imageUrl, metadata!)
                }
                
            })
            
            uploadTask.observe(.progress) { (snapshot) in
//                if let completedUnitCount = snapshot.progress?.completedUnitCount {
                    let width = Int(image.size.width)
                    let height = Int(image.size.height)
                    self.bubbleTmpForUploadLoading(width, height: height)
//                }
            }
            
            uploadTask.observe(.success) { (snapshot) in
                self.removeBubbleTmpForUploadLoading()
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //Recorde
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var timer: Timer!
    func handleRecord(){
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        timer = nil
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ChatController.timerRecord), userInfo: nil, repeats: true)
        timer.fire()
        
        if audioRecorder == nil {
            print("Start Record")
            self.startRecording()
        }
    }
    
    func handleSendRecord(){
        if audioRecorder != nil {
            self.finishRecording(true)
        }else {
            self.finishRecording(false)
        }
    }
    
    var voiceName = ""
    var voiceUrlFile: URL!
    func startRecording() {
        //Unique File Name
        let date = Int(Date().timeIntervalSince1970)
        voiceName = "Record_Pitung_" + self.fromId + "_" + String(date) + ".m4a"
        //-------
        
        let audioFilename = getDocumentsDirectory().appendingPathComponent(voiceName)
        voiceUrlFile = URL(fileURLWithPath: audioFilename)
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: voiceUrlFile, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
        } catch {
            finishRecording(false)
        }
    }
    
    func finishRecording(_ success: Bool) {
        self.stopRecord()
        
        if success {
            self.uploadRecord(voiceName, filePath: voiceUrlFile)
        } else {
            // Failed
            self.alertRecord("Failed to record, please check your settings")
        }
    }
    
    func stopRecord(){
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        if audioRecorder != nil {
            print("stop record")
            audioRecorder.stop()
            audioRecorder = nil
        }
        if timer != nil {
            print("stop timer")
            timer.invalidate()
            timer = nil
        }
        secon = 0
        minute = 0
    }
    
    var secon = 0
    var minute = 0
    func timerRecord(){
        if secon < 60-1 {
            secon += 1
        }else{
            secon = 0
            minute += 1
        }
        var nulSec = ""
        var nulMin = ""
        if secon < 10 {
            nulSec = "0"
        }
        if minute < 10 {
            nulMin = "0"
        }
        
        self.lblDurationRecord.text = nulMin + String(minute) + ":" + nulSec + String(secon)
        
        if minute == 10 {
            self.stopRecord()
            self.alertRecord("Maximum duration is 10 minutes")
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(false)
        }
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    func alertRecord(_ msg: String){
        let alert = UIAlertController(title: "", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
            print("Alert Record")
        }))
    }
    
    func uploadRecord(_ fileName: String, filePath: URL){
        let uploadAudio = FIRStorage.storage().reference().child("message_audio").child(fileName).putFile(filePath, metadata: nil, completion: { (metadata, error) in
            
            if error != nil {
                print("Failed upload of audio: ")//, error)
                return
            }
            
            if let audioUrl = metadata?.downloadURL()?.absoluteString {
                let mediaName = metadata!.name!
                let properties: [String: AnyObject] = ["audioUrl": audioUrl as AnyObject, "mediaName": mediaName as AnyObject, "mediaType": "Voice" as AnyObject, "mediaSize": Int((metadata?.size)!) as AnyObject]
                self.sendMessageWithProperties(properties)
            }
        })
        
        uploadAudio.observe(.progress) { (snapshot) in
            if (snapshot.progress?.completedUnitCount) != nil {
                //                self.navigationItem.title = String(completedUnitCount)
                self.bubbleTmpForUploadLoading(200, height: 70)
            }
        }
        
        uploadAudio.observe(.success) { (snapshot) in
            print("upload voice success")
            //            self.navigationItem.title = "Name"
            self.removeBubbleTmpForUploadLoading()
        }
    }
    
    // Attach
    func attachTapped(_ sender: UIButton){
        self.inputTextView.resignFirstResponder()
        popupAttach(self.attach)
    }
    
    func popupAttach(_ sender: UIButton) {
        let alert:UIAlertController=UIAlertController(title: "Attachment", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.modalPresentationStyle = .popover
        
        let audioAction = UIAlertAction(title: "Audio", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.audioAttachTapped()
        }
        let videoAction = UIAlertAction(title: "Video", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.videoAttachTapped()
        }
        let fileAction = UIAlertAction(title: "File", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.fileAttachTapped()
        }
        let locationAction = UIAlertAction(title: "Location", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.locationAttachTapped()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        alert.addAction(audioAction)
        alert.addAction(videoAction)
        alert.addAction(fileAction)
        alert.addAction(locationAction)
        alert.addAction(cancelAction)
        
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds;
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func audioAttachTapped(){
        var fileName = ""
        var filePath: URL!
        
//        let test = NSFileManager.defaultManager().currentDirectoryPath
//        let path = NSURL(string: test)
        let file = FileBrowser()//initialPath: path!)
        
        file.excludesFileExtensions = ["pdf"]
        file.didSelectFile = { (item: FBFile) -> Void in
            let ext = item.fileExtension
            fileName = item.displayName
            if ext == "mp3" || ext == "wav"{
                filePath = item.filePath
                self.uploadAudio(fileName, filePath: filePath)
            }
        }
        self.present(file, animated: true, completion: nil)
    }
    
    func videoAttachTapped(){
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
        imagePickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
        
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func fileAttachTapped(){
        var fileName = ""
        var filePath: URL!
        let file = FileBrowser()//initialPath: path)
        file.excludesFileExtensions = ["mp3", "wav"]
        file.didSelectFile = { (item: FBFile) -> Void in
            let ext = item.fileExtension
            fileName = item.displayName
            if ext == "pdf" || ext == "zip" || ext == "txt" {
                filePath = item.filePath
                self.uploadFile(fileName, filePath: filePath)
            }
        }
        self.present(file, animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.currentLat = manager.location?.coordinate.latitude
        self.currentLong = manager.location?.coordinate.longitude
        self.locationManager.stopUpdatingLocation()
    }
    
    var placesClient: GMSPlacesClient!
    var currentLat: CLLocationDegrees!
    var currentLong: CLLocationDegrees!
    func locationAttachTapped(){
//        placesClient.currentPlace { (placeLikelihoodList, error) in
//            if let error = error {
//                print("Pick Place error: \(error.localizedDescription)")
//                return
//            }
//            
//            if let placeLikelihoodList = placeLikelihoodList {
//                for likelihood in placeLikelihoodList.likelihoods {
//                    let place = likelihood.place
//                    print("Current Place name \(place.name) at likelihood \(likelihood.likelihood)")
//                    print("Current Place address \(place.formattedAddress)")
//                    print("Current Place attributions \(place.attributions)")
//                    print("Current PlaceID \(place.placeID)")
//                }
//            }
//        }
        let statusAccess = CLLocationManager.authorizationStatus()
        if statusAccess == .denied {
            let msg = "Please allow the app to access your location through the Settings."
            Config().alertForAllowAccess(self, msg: msg)
        }else if statusAccess == .restricted {
            let msg = "Please allow the app to access your location through the Settings."
            Config().alertForAllowAccess(self, msg: msg)
        }else if statusAccess == .notDetermined {
            let msg = "Please allow the app to access your location through the Settings."
            Config().alertForAllowAccess(self, msg: msg)
        }else{
            let center = CLLocationCoordinate2DMake(currentLat, currentLong)
            let northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001)
            let southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001)
            let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
            let config = GMSPlacePickerConfig(viewport: viewport)
            let placePicker = GMSPlacePicker(config: config)
            
            placePicker.pickPlace { (place, error) in
                if let error = error {
                    print("Pick Place error: \(error.localizedDescription)")
                    return
                }
                
                if let place = place {
                    let name = place.name
                    let address = place.formattedAddress
                    let lat = place.coordinate.latitude
                    let long = place.coordinate.longitude
                    print("Place name \(name)")
                    print("Place address \(address)")
                    print(lat)
                    print(long)
                    
                    self.uploadLocation(lat, long: long, nameAddress: name, detailAddress: address!)
                } else {
                    print("No place selected")
                }
            }
        }
    }
    
    func uploadAudio(_ fileName: String, filePath: URL){
        let uploadAudio = FIRStorage.storage().reference().child("message_audio").child(fileName).putFile(filePath, metadata: nil, completion: { (metadata, error) in
            
            if error != nil {
                print("Failed upload of audio: ", error!)
                return
            }
            print(metadata!.size)
            if let audioUrl = metadata!.downloadURL()?.absoluteString {
                print(metadata!.size)
                let mediaName = Config().nameMediaFile(metadata!.name!, idUser: self.fromId)
                let properties: [String: AnyObject] = ["audioUrl": audioUrl as AnyObject, "mediaName": mediaName as AnyObject, "mediaType": "Audio" as AnyObject, "mediaSize": Int(metadata!.size) as AnyObject]
                self.sendMessageWithProperties(properties)
            }
        })
        
        uploadAudio.observe(.progress) { (snapshot) in
            if (snapshot.progress?.completedUnitCount) != nil {
//                self.navigationItem.title = String(completedUnitCount)
                self.bubbleTmpForUploadLoading(100, height: 100)
            }
        }
        
        uploadAudio.observe(.success) { (snapshot) in
//            self.navigationItem.title = "Name"
            self.removeBubbleTmpForUploadLoading()
        }
    }
    
    func uploadFile(_ fileName: String, filePath: URL){
        let uploadFile = FIRStorage.storage().reference().child("message_file").child(fileName).putFile(filePath, metadata: nil, completion: { (metadata, error) in
            
            if error != nil {
                print("Failed upload of file: ")//, error)
                return
            }
            
            if let fileUrl = metadata?.downloadURL()?.absoluteString {
                let mediaName = Config().nameMediaFile(metadata!.name!, idUser: self.fromId)
                let properties: [String: AnyObject] = ["fileUrl": fileUrl as AnyObject, "mediaName": mediaName as AnyObject, "mediaType": (metadata?.contentType)! as AnyObject, "mediaSize": Int((metadata?.size)!) as AnyObject]
                self.sendMessageWithProperties(properties)
            }
        })
        
        uploadFile.observe(.progress) { (snapshot) in
            if (snapshot.progress?.completedUnitCount) != nil {
//                self.navigationItem.title = String(completedUnitCount)
                self.bubbleTmpForUploadLoading(100, height: 100)
            }
        }
        
        uploadFile.observe(.success) { (snapshot) in
//            self.navigationItem.title = "Name"
            self.removeBubbleTmpForUploadLoading()
        }
    }
    
    func uploadLocation(_ lat: Double, long: Double, nameAddress: String, detailAddress: String){
        self.bubbleTmpForUploadLoading(100, height: 100)
        let properties: [String: AnyObject] = ["latitude": lat as AnyObject, "longitude": long as AnyObject, "nameAddress": nameAddress as AnyObject, "detailAddress": detailAddress as AnyObject, "mediaType": "Location" as AnyObject]
        self.sendMessageWithProperties(properties)
    }
    
    // ------ PLAY VIDEO, Audio
    func playVideo(_ videoName: String){
        var player: AVPlayer?
        let playerViewController = AVPlayerViewController()
        
        //Local Path
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory = paths[0]
        let masterPath = documentsDirectory.appending("/Media")
        let videoNameLocal = videoName.replacingOccurrences(of: " ", with: "")
        let pathLocal = "file://" + masterPath + "/" + videoNameLocal + ".mov"
        
        player = AVPlayer(url: URL(string: pathLocal)!)
        playerViewController.player = player
        self.present(playerViewController, animated: true){
            playerViewController.player?.play()
        }
    }
    
    func playAudio(_ audioName: String){
        print(audioName)
        var player: AVPlayer?
        let playerViewController = AVPlayerViewController()
        
        //Local Path
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory = paths[0]
        let masterPath = documentsDirectory.appending("/Media")
        let audioNameLocal = audioName.replacingOccurrences(of: " ", with: "")
        let pathLocal = "file://" + masterPath + "/" + audioNameLocal + ".mp3"
        print(pathLocal)
        
        player = AVPlayer(url: URL(string: pathLocal)!)
        
        playerViewController.player = player
        self.present(playerViewController, animated: true){
            playerViewController.player?.play()
        }
    }
    
    func openFile(_ url: URL, mediaName: String){
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory = paths[0]
        let masterPath = documentsDirectory.appending("/Media")
        let pathLocal = "file://" + masterPath + "/" + mediaName
        let pathLocalOpen = pathLocal.replacingOccurrences(of: " ", with: "")
        
        let localURL: URL! = URL(string: pathLocalOpen)
        
        // Open With iBook
        var docController: UIDocumentInteractionController?
        docController = UIDocumentInteractionController(url: localURL)
        let url = URL(string:"itms-books:");
        if UIApplication.shared.canOpenURL(url!) {
            docController!.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
            print("iBooks is installed")
        }else{
            print("iBooks is not installed")
            
            // Background Black
            self.navigationController?.isNavigationBarHidden = true
            self.viewClose = UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.size.width,height: self.view.frame.size.height))
            viewClose.backgroundColor = UIColor.black.withAlphaComponent(0.8)
            viewClose.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ChatController.closePdf(_:))))
            self.view.addSubview(viewClose)

            // Content PDF
            let data = try? Data(contentsOf: localURL)
            let baseURL = localURL.deletingLastPathComponent()
            self.webView = UIWebView(frame: CGRect(x: 0,y: 50,width: self.view.frame.size.width,height: self.view.frame.size.height-100))
            webView.load(data!, mimeType: "application/pdf", textEncodingName:"", baseURL: baseURL)
            self.view.addSubview(webView)
        }
        
    }
    
    var webView: UIWebView!
    var viewClose: UIView!
    func closePdf(_ sender: UITapGestureRecognizer){
        self.webView.removeFromSuperview()
        self.viewClose.removeFromSuperview()
        self.navigationController?.isNavigationBarHidden = false
    }
    
    var latSelected: Float!
    var longSelected: Float!
    var nameLocSelected = ""
    var detailLocSelected = ""
    func locationTapped(_ sender: UIGestureRecognizer){
        print("controller location")
        let cell = sender.view!.superview as! ChatCell
        let indexPath = self.collectionView!.indexPath(for: cell)
        let data = messages[indexPath!.row]
        self.latSelected = data.latitude
        self.longSelected = data.longitude
        self.nameLocSelected = data.nameAdrress!
        self.detailLocSelected = data.detailAdrress!
        
        print(data.latitude)
        print(data.longitude)
        print(self.nameLocSelected)
        print(self.detailLocSelected)
        
        self.performSegue(withIdentifier: "showLocation", sender: self)
    }
    
    //Long Press
    var paramsLongPress = 0
    var idChatReply = ""
    var selectedCellReply:ChatCell!
    func longPressCell(_ sender: UIGestureRecognizer){
        let view = sender.view!
        let cell = view as! ChatCell
        cell.chatController = self
        let indexPath = collectionView!.indexPath(for: cell)
        let message = messages[indexPath!.item]
        
        if paramsLongPress == 0 {
            cell.selectedView.isHidden = false
            cell.selectedView.alpha = 1.0
            self.idChatReply = message.idChat!
            self.paramsLongPress = 1
            self.selectedCellReply = cell
            
            //Reply
            let image = UIImage(named: "ico_reply")
            let attach = UIButton()
            attach.setImage(image, for: UIControlState())
            attach.addTarget(self, action: #selector(ChatController.replyTapped), for: UIControlEvents.touchDown)
            attach.frame=CGRect(x: 0, y: 0, width: 20, height: 20)
            let barAttach = UIBarButtonItem(customView: attach)
            //Space
            let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            fixedSpace.width = 20.0
            //Delete
            let imageDelete = UIImage(named: "ico_trash")
            let attachDelete = UIButton()
            attachDelete.setImage(imageDelete, for: UIControlState())
            attachDelete.addTarget(self, action: #selector(ChatController.deleteSelectedChat), for: UIControlEvents.touchDown)
            attachDelete.frame=CGRect(x: 0, y: 0, width: 20, height: 20)
            let barAttachDelete = UIBarButtonItem(customView: attachDelete)
            self.navigationItem.setRightBarButtonItems([barAttach, fixedSpace, barAttachDelete], animated: true)
        }
    }
    
    var paramsReply = 0
    var heightReply: CGFloat = 0.0
    func replyTapped(){
        self.heightReply = 118.0
        self.yInput = self.view.frame.height - self.heightReply
        let rect = CGRect(x: 0, y: self.yInput, width: self.view.frame.width, height: self.heightReply)
        self.newChatInputContainerView.frame = rect
        self.topAnchorInputText.isActive = false
        self.bottomAnchorInputText.isActive = true
        self.replyView.isHidden = false
        
        var nameReply = ""
        let idSelected = selectedCellReply.message!.fromId
        if idSelected != self.fromId {
            let id = self.idFriend.index(of: idSelected!)
            let data = self.nameFriend[id!]
            nameReply = data
        }else{
            nameReply = "You"
        }
        self.replyName.text = nameReply
        let type = selectedCellReply.message?.mediaType
        var content = ""
        if type == "" || type == "Reply" {
            content = selectedCellReply.message!.text!
            self.replyMedia.image = nil
        }else
        if type == "Image" {
            if selectedCellReply.message?.text != "" {
                content = selectedCellReply.message!.text!
            }else{
                content = "Photo"
            }
//            self.replyMedia.loadImageUsingCacheWithUrlString((selectedCellReply.message?.imageUrl)!)
            let imgUrl = URL(string: selectedCellReply.message!.imageUrl!)!
            self.replyMedia.setImage(withUrl: imgUrl)
        }else
        if type == "Video" {
            content = selectedCellReply.message!.mediaName!
            let imgUrl = URL(string: selectedCellReply.message!.imageUrl!)!
            self.replyMedia.setImage(withUrl: imgUrl)
        }else
        if type == "Audio"{
            content = selectedCellReply.message!.mediaName!
            self.replyMedia.image = UIImage(named: "ico_media_music")
        }else
        if type == "Voice" {
            content = "Voice Message"
            self.replyMedia.image = UIImage(named: "ico_record")
        }else
        if type == "Location" {
            if selectedCellReply.message?.nameAdrress != "" {
                content = selectedCellReply.message!.nameAdrress!
            }else{
                content = "Location"
            }
            self.replyMedia.image = UIImage(named: "ico_chat_location")
        }else{
            content = selectedCellReply.message!.mediaName!
            self.replyMedia.image = UIImage(named: "ico_media_file")
        }
        self.replyContent.text = content
        
        //Balik keadaan semula
        self.paramsLongPress = 0
        selectedCellReply.selectedView.isHidden = true
        
        let image = UIImage(named: "ico_attachment")
        let attach = UIButton()
        attach.setImage(image, for: UIControlState())
        attach.addTarget(self, action: #selector(ChatController.attachTapped), for: UIControlEvents.touchDown)
        attach.frame=CGRect(x: 0, y: 0, width: 26, height: 15)
        let barAttach = UIBarButtonItem(customView: attach)
        self.navigationItem.setRightBarButtonItems([barAttach], animated: true)
        
        self.paramsReply = 1
    }
    
    func cancelReply(){
        self.yInput = self.view.frame.height - self.heightInput
        let rect = CGRect(x: 0, y: self.yInput, width: self.view.frame.width, height: self.heightInput)
        self.newChatInputContainerView.frame = rect
        self.topAnchorInputText.isActive = true
        self.bottomAnchorInputText.isActive = false
        self.replyView.isHidden = true
        
        self.heightReply = 0
        self.idChatReply = ""
        self.paramsReply = 0
    }
    
    func tapOnReplyView(_ sender: UIGestureRecognizer){
        let view = sender.view!
        let cell = view.superview as! ChatCell
        cell.chatController = self
        let indexSelect = collectionView!.indexPath(for: cell)
        let keyMsg = messages[indexSelect!.item].keyReply
        
        if let indexTo = messages.index(where: {$0.idChat == keyMsg}) {
            let indexPath = IndexPath(item: indexTo, section: 0)
            collectionView?.scrollToItem(at: indexPath, at: .top, animated: true)
            
            DispatchQueue.main.async(execute: {
                if let cellIf = self.collectionView!.cellForItem(at: indexPath) {
                    let cellTo = cellIf as! ChatCell
                    cellTo.selectedView.isHidden = false
                    cellTo.selectedView.alpha = 1.0
                    UIView.animate(withDuration: 2.5, animations: {
                        cellTo.selectedView.alpha = 0.0
                    })
                }
            })
        }
    }
    
    //Delete chat
    func deleteSelectedChat(){
        let alert = UIAlertController(title: "", message: "Delete Message?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
            let idSelected = self.selectedCellReply.message!.idChat!
            let obj = try! Realm().objects(tb_chat.self).filter("id_chat == %@", idSelected)
            try! Realm().write(){
                try! Realm().delete(obj)
            }
            
            //Refresh
            self.observeMessages()
            
            //Bring Back
            self.paramsLongPress = 0
            self.selectedCellReply.selectedView.isHidden = true
            let image = UIImage(named: "ico_attachment")
            let attach = UIButton()
            attach.setImage(image, for: UIControlState())
            attach.addTarget(self, action: #selector(ChatController.attachTapped), for: UIControlEvents.touchDown)
            attach.frame=CGRect(x: 0, y: 0, width: 26, height: 15)
            let barAttach = UIBarButtonItem(customView: attach)
            self.navigationItem.setRightBarButtonItems([barAttach], animated: true)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // ------ KEYBOARD
//    override var inputAccessoryView: UIView? {
//        get {
//            return self.newChatInputContainerView
//        }
//    }
    
    override var canBecomeFirstResponder : Bool {
        return true
    }
    
    func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func handleKeyboardDidShow() {
        if messages.count > 0 {
            let indexPath = IndexPath(item: messages.count - 1, section: 0)
            collectionView?.scrollToItem(at: indexPath, at: .top, animated: true)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    func handleKeyboardWillShow(_ notification: Notification) {
        let keyboardFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
        let keyboardDuration = (notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
        
        var height: CGFloat!
        if self.paramsReply == 0 {
            height = self.heightInput
        }else{
            height = self.heightReply
        }
        self.yInput = self.view.frame.height - keyboardFrame!.height - height
        let rect = CGRect(x: 0, y: self.yInput, width: self.view.frame.width, height: height)
        self.newChatInputContainerView.frame = rect
        
        containerViewBottomAnchor?.constant = -keyboardFrame!.height
        UIView.animate(withDuration: keyboardDuration!, animations: {
            self.view.layoutIfNeeded()
        }) 
    }
    
    func handleKeyboardWillHide(_ notification: Notification) {
        let keyboardDuration = (notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
        
        var height: CGFloat!
        if self.paramsReply == 0 {
            height = self.heightInput
        }else{
            height = self.heightReply
        }
        self.yInput = self.view.frame.height - height
        let rect = CGRect(x: 0, y: self.yInput, width: self.view.frame.width, height: height)
        self.newChatInputContainerView.frame = rect
        
        containerViewBottomAnchor?.constant = 0
        UIView.animate(withDuration: keyboardDuration!, animations: {
            self.view.layoutIfNeeded()
        }) 
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // ------ ZOOM PICTURE
    
    var startingFrame: CGRect?
    var blackBackgroundView: UIView?
    var startingImageView: UIImageView?
    
    //my custom zooming logic
    func performZoomInForStartingImageView(_ startingImageView: UIImageView) {
        
        self.startingImageView = startingImageView
        self.startingImageView?.isHidden = true
        
        startingFrame = startingImageView.superview?.convert(startingImageView.frame, to: nil)
        
        let zoomingImageView = UIImageView(frame: startingFrame!)
        zoomingImageView.backgroundColor = UIColor.clear
        zoomingImageView.image = startingImageView.image
        zoomingImageView.isUserInteractionEnabled = true
        zoomingImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomOut)))
        
        if let keyWindow = UIApplication.shared.keyWindow {
            blackBackgroundView = UIView(frame: keyWindow.frame)
            blackBackgroundView?.backgroundColor = UIColor.black
            blackBackgroundView?.alpha = 0
//            blackBackgroundView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomOut)))
            keyWindow.addSubview(blackBackgroundView!)
            
            keyWindow.addSubview(zoomingImageView)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackBackgroundView?.alpha = 1
                self.newChatInputContainerView.alpha = 0
                
                let height = self.startingFrame!.height / self.startingFrame!.width * keyWindow.frame.width
                
                zoomingImageView.frame = CGRect(x: 0, y: 0, width: keyWindow.frame.width, height: height)
                
                zoomingImageView.center = keyWindow.center
                
                }, completion: { (completed) in
                    //                    do nothing
            })
            
        }
    }
    
    func handleZoomOut(_ tapGesture: UITapGestureRecognizer) {
        if let zoomOutImageView = tapGesture.view {
            //need to animate back out to controller
            zoomOutImageView.layer.cornerRadius = 16
            zoomOutImageView.clipsToBounds = true
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                zoomOutImageView.frame = self.startingFrame!
                self.blackBackgroundView?.alpha = 0
                self.newChatInputContainerView.alpha = 1
                
                }, completion: { (completed) in
                    zoomOutImageView.removeFromSuperview()
                    self.startingImageView?.isHidden = false
            })
        }
    }
    
    // ======== Share Media
    func handleShareMedia(_ data: UIImageView){
        let index = data.tag
        let msg = messages[index]
        let mediaName = msg.mediaName!
        
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory = paths[0]
        let masterPath = documentsDirectory.appending("/Media")
        let pathLocal = "file://" + masterPath + "/" + mediaName
        
        let VC : UIActivityViewController = UIActivityViewController(activityItems: [pathLocal], applicationActivities: nil)
        VC.popoverPresentationController?.sourceView = self.view
        if let presenter = VC.popoverPresentationController {
            presenter.sourceView = data
            presenter.sourceRect = data.bounds;
        }
        self.present(VC, animated: true, completion: nil)
    }
    
    // Offline Handle - Migrate server to local store
    func observeMessages(){
        if paramsKindChat == "personal" {
            let models = try! Realm().objects(tb_chat.self).filter("from = %@ AND to = %@ OR from = %@ AND to = %@",self.fromId, self.toId, self.toId, self.fromId)
            self.messages.removeAll()
            for data in models {
                self.messages.append(Chat(dictionary: data))
            }
        }else if paramsKindChat == "group"{
            let models = try! Realm().objects(tb_chat.self).filter("to = %@",self.toId)
            self.messages.removeAll()
            for data in models {
                self.messages.append(Chat(dictionary: data))
            }
        }
        DispatchQueue.main.async(execute: {
            if self.messages.count > 0 {
                self.collectionView?.reloadData()
                let indexPath = IndexPath(item: self.messages.count - 1, section: 0)
                self.collectionView?.scrollToItem(at: indexPath, at: .bottom, animated: false)
            }
        })
        
    }
    
    func checkingServer() {
        if paramsKindChat == "personal" {
            let messagesRef = FIRDatabase.database().reference().child("user-messages").child(fromId).child(toId)
            messagesRef.observe(.childAdded) { (snapshot: FIRDataSnapshot!) in
                
                let status = snapshot.value! as! Int
                let keyMessage = snapshot.key
                if status == 1 {
                    self.downloadData(keyMessage)
                }
            }
        }else if paramsKindChat == "group"{
            let messagesRef = FIRDatabase.database().reference().child("messages").queryOrdered(byChild: "toId").queryEqual(toValue: self.toId)
            messagesRef.observe(.childAdded) { (snapshot: FIRDataSnapshot!) in
                self.downloadDataGroup(snapshot.key)
            }
        }
    }

    func downloadData(_ key: String) {
        let messagesRef = FIRDatabase.database().reference().child("messages").queryOrderedByKey().queryEqual(toValue: key)
        messagesRef.observe(.childAdded) { (snapshot: FIRDataSnapshot!) in
            guard let dictionary = snapshot.value as? [String: AnyObject] else {
                return
            }
            print("download data to local")
            print(snapshot)
            
            //Update di firebase
            let userMessagesRef = FIRDatabase.database().reference().child("user-messages").child(self.fromId).child(self.toId)
            let messageId = key
            userMessagesRef.updateChildValues([messageId: 2])
            
            //Donwload
            self.migrateToLocal(dictionary, idChat: key)
        }
    }
    
    func downloadDataGroup(_ key: String) {
        let messagesRef = FIRDatabase.database().reference().child("messages").queryOrderedByKey().queryEqual(toValue: key)
        messagesRef.observe(.childAdded) { (snapshot: FIRDataSnapshot!) in
            guard let dictionary = snapshot.value as? [String: AnyObject] else {
                return
            }
            let idChat = snapshot.key
            self.migrateToLocal(dictionary, idChat: idChat)
        }
    }

    func migrateToLocal(_ chat: [String: AnyObject], idChat: String){
        //Data list chat personal
        if paramsKindChat == "personal" {
            if let modelUpdate = try! Realm().objects(listDataChatPersonal.self).filter("id_sender = %@", Int(self.toId)!).first {
                try! Realm().write({ 
                    modelUpdate.name = self.nameFriend[0]
                    modelUpdate.ava = self.avaFriend[0]
                    if let text = chat["text"] {modelUpdate.text = text as! String}
                    if let type = chat["mediaType"] {
                        if type as! String == "Image" {modelUpdate.text = "Photo"}
                        if type as! String == "Audio" {modelUpdate.text = "Audio"}
                        if type as! String == "Video" {modelUpdate.text = "Video"}
                        if type as! String == "File" {modelUpdate.text = "File"}
                        if type as! String == "Location" {modelUpdate.text = "Location"}
                        if type as! String == "Voice" {modelUpdate.text = "Voice"}
                    }
                    modelUpdate.dateLastChat = Int(Date().timeIntervalSince1970)
                })
            }else{
                let modelChat = listDataChatPersonal()
                modelChat.id_sender = Int(self.toId)!
                modelChat.name = self.nameFriend[0]
                modelChat.ava = self.avaFriend[0]
                if let text = chat["text"] {modelChat.text = text as! String}
                if let type = chat["mediaType"] {
                    if type as! String == "Photo" {modelChat.text = "Photo"}
                    if type as! String == "Audio" {modelChat.text = "Audio"}
                    if type as! String == "File" {modelChat.text = "File"}
                    if type as! String == "Location" {modelChat.text = "Location"}
                    if type as! String == "Voice" {modelChat.text = "Voice"}
                }
                modelChat.dateLastChat = Int(Date().timeIntervalSince1970)
                DBHelper.insert(modelChat)
            }
        }
        //-----------------------
        var fromIdForMedia = ""
        let model = tb_chat()
        model.id_chat = idChat
        if let fromId = chat["fromId"] {model.from = fromId as! String; fromIdForMedia = fromId as! String}
        print("data chat")
        print(chat)
        if let toId = chat["toId"] {model.to = toId as! String}
        if let text = chat["text"] {model.text = text as! String}
        if let time = chat["timestamp"] {model.time = time as! Double}
        if let imageUrl = chat["imageUrl"] {model.image_url = imageUrl as! String}
        if let imageWidth = chat["imageWidth"] {model.image_width = Int(imageWidth as! NSNumber)}
        if let imageHeight = chat["imageHeight"] {model.image_height = Int(imageHeight as! NSNumber)}
        if let videoUrl = chat["videoUrl"] {model.video_url = videoUrl as! String}
        if let audioUrl = chat["audioUrl"] {model.audio_url = audioUrl as! String}
        if let fileUrl = chat["fileUrl"] {model.file_url = fileUrl as! String}
        if let mediaName = chat["mediaName"] {model.mediaName = mediaName as! String}
        if let mediaSize = chat["mediaSize"] {model.mediaSize = mediaSize as! Int}
        if let mediaType = chat["mediaType"] {model.mediaType = mediaType as! String}
        if let lat = chat["latitude"] {model.latitude = Float(lat as! NSNumber)}
        if let long = chat["longitude"] {model.longitude = Float(long as! NSNumber)}
        if let address = chat["nameAddress"] {model.nameAdrress = address as! String}
        if let address = chat["detailAddress"] {model.detailAdrress = address as! String}
        if let keyReply = chat["keyReply"] {model.keyReply = keyReply as! String}
        model.is_read = 1
        
        let category = try! Realm().object(ofType: tb_chat.self, forPrimaryKey: idChat)
        if (category != nil) {
            print("Error Primary Key")
            //return
        }else{
            DBHelper.insert(model)
            //observeMessages()
        }
        
        var mediaExt: String! = ""
        var mediaUrl: String!
        var mediaType: String! = ""
        if let type = chat["mediaType"] { mediaType = type as! String }
        
        switch mediaType {
        case "Image" :
            if let url = chat["imageUrl"]{
                mediaUrl = url as! String
                mediaExt = ".jpg"
            }
        case "Video" :
            if let url = chat["videoUrl"]{
                mediaUrl = url as! String
                mediaExt = ".mov"
            }
        case "Audio" :
            if let url = chat["audioUrl"]{
                mediaUrl = url as! String
                mediaExt = ".mp3"
            }
        case "File" :
            if let url = chat["fileUrl"]{
                mediaUrl = url as! String
                mediaExt = ""
            }
        case "Voice" :
            if let url = chat["audioUrl"]{
                mediaUrl = url as! String
                //mediaExt = ".m4a"
            }
        default:
            print("Media yg lainnya")
            if let url = chat["fileUrl"]{
                mediaUrl = url as! String
                mediaExt = ""
            }
        }
        
        if let mediaName = chat["mediaName"]{
            self.dataMedia = chat
            downloadMediaToLocal(mediaUrl, mediaNames: mediaName as! String, mediaExts: mediaExt, mediaTypes: mediaType, fromIdForSaveMedias: fromIdForMedia)
        }else{
            observeMessages()
        }
        
        //observeMessages()
    }

    func downloadMediaToLocal(_ mediaUrl: String, mediaNames: String, mediaExts: String, mediaTypes: String, fromIdForSaveMedias: String){
        print("downloading media to local")
        // Declar
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory = paths[0]
        
        // Create Folder Media
        let dataPathFile = documentsDirectory.appending("/Media")
        do {
            try FileManager.default.createDirectory(atPath: dataPathFile, withIntermediateDirectories: false, attributes: nil)
        } catch {}
        
        // Save to Local
        let pathSave = documentsDirectory.appending("/Media")
        let pathLocal = "file://" + pathSave + "/\(mediaNames)\(mediaExts)"
        let pathLocalSave = pathLocal.replacingOccurrences(of: " ", with: "")
        
        let starsRef = FIRStorage.storage().reference(forURL: mediaUrl)
        let localURL: URL! = URL(string: pathLocalSave)
        let downloadTask = starsRef.write(toFile: localURL) { (URL, error) -> Void in
            if (error != nil) {
                print("Error : \(error)")
            } else {
                print("Downloaded : " + mediaNames + mediaExts)
                if self.paramsKindChat == "group"{
                    self.fromIdForSaveMedia = fromIdForSaveMedias
                    self.nameForSaveMedia = mediaNames
                    self.typeForSaveMedia = mediaTypes
                    self.makeSerialize(mediaName: mediaNames, mediaType: mediaTypes)
                }else{
                    self.observeMessages()
                }
            }
        }
        
    }
    
    func makeSerialize(mediaName: String, mediaType: String){
        let sess = try! Realm().objects(session.self).first
        let idGroup = sess!.id_group
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.fromId as AnyObject,
            "id_group" : idGroup as AnyObject,
            "name" : mediaName as AnyObject,
            "type" : mediaType as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postData(base64Encoded)
        }
    }
    
    func postData(_ message: String){
        let sess = try! Realm().objects(session.self).first
        let tokenSes = sess!.token
        
        let urlString = Config().urlMedia + "add_media"
        let token = tokenSes
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                let code = jsonData["code"].intValue
                if code == 1 || code == 3 {
                    let data = jsonData["msg"]
                    self.idMedia = String(data.intValue)
                    let id = String(data.intValue)
                    print("Save data media to server")
                    self.migrateDataMediaToLocal(id)
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in

        }
    }
    
    var dataMedia = [String: AnyObject]()
    var idMedia = ""
    var fromIdForSaveMedia = ""
    var nameForSaveMedia = ""
    var typeForSaveMedia = ""
    func migrateDataMediaToLocal(_ idMedia: String){
        let model = tb_media()
        
        model.id_media = self.idMedia
        let sess = try! Realm().objects(session.self).first!
        let idGroup = String(sess.id_group)
        model.id_group = idGroup
        model.id_from_user = self.fromIdForSaveMedia
        model.type = self.typeForSaveMedia
        let name = self.nameForSaveMedia
        model.name = name
        
        model.date_create = String(describing: Date())
        model.date_update = String(describing: Date())
        
        let category = try! Realm().object(ofType: tb_media.self, forPrimaryKey: name)
        if (category != nil) {
            print("Error Media Name Primary Key")
            //return
        }else{
            print("Saved media to local")
            DBHelper.insert(model)
            
            //checkingServer()
            observeMessages()
        }
    }
    
    func sendNotif(_ data: [String: AnyObject], key: String){
        print(data)
        var from_id = ""
        var varText = ""
        var timestamp = 0
        var to_id = ""
        var image_url = ""
        var image_height = 0
        var image_width = 0
        var video_url = ""
        var audio_url = ""
        var file_url = ""
        var media_name = ""
        var media_size = 0
        var media_type = ""
        var latitude = 0
        var longitude = 0
        var name_address = ""
        var detail_address = ""
        var key_reply = ""
        
        if let fromId = data["fromId"] {from_id = fromId as! String}
        if let toId = data["toId"] {to_id = toId as! String}
        if let text = data["text"] {varText = text as! String}
        if let time = data["timestamp"] {timestamp = Int(time as! NSNumber)}
        if let imageUrl = data["imageUrl"] {image_url = imageUrl as! String}
        if let imageWidth = data["imageWidth"] {image_width = Int(imageWidth as! NSNumber)}
        if let imageHeight = data["imageHeight"] {image_height = Int(imageHeight as! NSNumber)}
        if let videoUrl = data["videoUrl"] {video_url = videoUrl as! String}
        if let audioUrl = data["audioUrl"] {audio_url = audioUrl as! String}
        if let fileUrl = data["fileUrl"] {file_url = fileUrl as! String}
        if let mediaName = data["mediaName"] {media_name = mediaName as! String}
        if let mediaSize = data["mediaSize"] {media_size = mediaSize as! Int}
        if let mediaType = data["mediaType"] {media_type = mediaType as! String}
        if let lat = data["latitude"] {latitude = Int(lat as! NSNumber)}
        if let long = data["longitude"] {longitude = Int(long as! NSNumber)}
        if let address = data["nameAddress"] {name_address = address as! String}
        if let address = data["detailAddress"] {detail_address = address as! String}
        if let keyReply = data["keyReply"] {key_reply = keyReply as! String}
        
        let arrayData = [
            "key" : key,
            "fromId" : from_id,
            "isRead" : "0",
            "text" : varText,
            "timestamp" : timestamp,
            "toId" : to_id,
            "imageUrl" : image_url,
            "imageHeight" : image_height,
            "imageWidth" : image_width,
            "videoUrl" : video_url,
            "audioUrl" : audio_url,
            "fileUrl" : file_url,
            "mediaName" : media_name,
            "mediaSize" : media_size,
            "mediaType" : media_type,
            "latitude" : latitude,
            "longitude" : longitude,
            "nameAddress" : name_address,
            "detailAddress" : detail_address,
            "keyReply" : key_reply
        ] as [String : Any]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Notif(jsonString!)
    }
    
    func makeBase64Notif(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postDataNotif(base64Encoded)
        }
    }
    
    func postDataNotif(_ message: String){
        let sess = try! Realm().objects(session.self).first
        let tokenSes = sess!.token
        
        let urlString = Config().urlUpdate + "sendMessageToPersonal"
        let token = tokenSes
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                let code = jsonData["code"].intValue
                if code > 0 {
                    print("send notif chat success")
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                
        }
    }
    
    @IBAction func backToChat(_ segue: UIStoryboardSegue){}
}
