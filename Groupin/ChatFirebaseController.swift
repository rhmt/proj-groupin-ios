//
//  ChatController.swift
//  Groupin
//
//  Created by Macbook pro on 8/19/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
//import Firebase
import JSQMessagesViewController
//import RealmSwift
//
class ChatFirebaseController: JSQMessagesViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
//
//    var messages = [JSQMessage]()
//    var outgoingBubbleImageView: JSQMessagesBubbleImage!
//    var incomingBubbleImageView: JSQMessagesBubbleImage!
//    var avatars = [String: JSQMessagesAvatarImage]()
//    var senderImageUrl: Data!
//    var rootRef = FIRDatabase.database().reference()
//    var messageRef: FIRDatabaseReference!
//    var usersTypingQuery: FIRDatabaseQuery!
//    
//    var imgPicker = UIImagePickerController()
//    var imgData = Data()
//    let storageRef = FIRStorage.storage().reference()
//    
//    var sendImageUrl = ""
//    var mediaType = ""
//    var pickedImage: UIImage!
//    var pickedVideo: URL!
//    let fileName = ""
//    
//    var userIsTypingRef: FIRDatabaseReference!
//    fileprivate var localTyping = false
//    var isTyping: Bool {
//        get {
//            return localTyping
//        }
//        set {
////            localTyping = newValue
//            userIsTypingRef.setValue(newValue)
//        }
//    }
//    
//    var toId = ""
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        imgPicker.delegate = self
//        
////        let idSession = try! Realm().objects(session).first
////        let phone = "1"//idSession!.phone
////        let models = try! Realm().objects(tb_user).filter("phone == %@", phone).first!
////        let imageData = models.avatar
////        
////        if let data:NSData = imageData {
////            setupAvatarImage(toId, imageData: data, incoming: false)
////            senderImageUrl = data as NSData
////        } else {
////            setupAvatarColor(senderId, incoming: false)
////        }
//        
//        setupBubbles()
//        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
//        messageRef = rootRef.child("messages")
//        
//        // Link Profile
//        let button =  UIButton(type: .custom)
//        button.frame = CGRect(x: 0, y: 0, width: 100, height: 40) as CGRect
//        button.setTitle("Chat", for: UIControlState())
//        button.addTarget(self, action: #selector(ChatFirebaseController.profileTapped), for: UIControlEvents.touchUpInside)
//        self.navigationItem.titleView = button
//    }
//    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "showFriendProfile" {
//            let conn = segue.destination as! FriendProfileController
//            conn.phone = self.senderId
//        }
//    }
//    
//    func profileTapped(_ button: UIButton) {
//        self.performSegue(withIdentifier: "showFriendProfile", sender: self)
//    }
//    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        messages.removeAll()
//        observeMessages()
//        observeTyping()
//    }
//    
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//    }
//    
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
//        return messages[indexPath.item]
//    }
//    
//    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return messages.count
//    }
//    
//    
//    // Display Name
//    override func collectionView(_ collectionView: JSQMessagesCollectionView,attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
//        let message = messages[indexPath.item]
//        
//        if message.senderId == self.toId + "1" {
//            return nil
//        }
//        if indexPath.item > 0 {
//            let previousMessage = messages[indexPath.item - 1];
//            if previousMessage.senderId == self.toId {
//                return nil;
//            }
//        }
//        return NSAttributedString(string: message.senderDisplayName)
//    }
//    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForMessageBubbleTopLabelAt indexPath: IndexPath) -> CGFloat {
//        let message = messages[indexPath.item]
//        // Sent by me, skip
//        if message.senderId == self.toId + "1" {
//            return CGFloat(0.0);
//        }
//        // Same as previous sender, skip
//        if indexPath.item > 0 {
//            let previousMessage = messages[indexPath.item - 1];
//            if previousMessage.senderId == self.toId {
//                return CGFloat(0.0);
//            }
//        }
//        return kJSQMessagesCollectionViewCellLabelHeightDefault
//    }
//    
//    // Display Time (Belum Fungsi)
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAt indexPath: IndexPath!) -> NSAttributedString! {
//        let message = messages[indexPath.item]
//        let time:String = DateFormatter().string(from: message.date)
////        return NSAttributedString(string: time)
//        return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
//    }
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
//        return kJSQMessagesCollectionViewCellLabelHeightDefault
//    }
//    
//    // Display Date In Top
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
//        let message = messages[indexPath.item]
//        if indexPath.row == 0 {
//            return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
//        }
//        if (indexPath.item - 1 > 0) {
//            let previousMessage = messages[indexPath.item - 1]
//            if message.date.timeIntervalSince(previousMessage.date) / 60 > 1 {
//                return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
//            }
//        }
//        
//        return nil
//    }
//   
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
//        if indexPath.row == 0 {
//            return kJSQMessagesCollectionViewCellLabelHeightDefault
//        }
//        if indexPath.row - 1 > 0 {
//            let previousMessage = messages[indexPath.item - 1]
//            let message = messages[indexPath.item]
//            if message.date.timeIntervalSince(previousMessage.date) / 60 > 1 {
//                return kJSQMessagesCollectionViewCellLabelHeightDefault
//            }
//        }
//        return 0.0
//    }
//    
//    // Main
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
//        
//        let message = messages[indexPath.item]
//        if message.senderId == self.toId + "1" {
//            return outgoingBubbleImageView
//        }
//        if message.senderId == self.toId {
//            return incomingBubbleImageView
//        }
//        return nil
//    }
//    
//    
//    // Avatar
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
//        let message = messages[indexPath.item]
//        if let avatar = avatars[self.toId + "1"] {
////            return UIImageView(image: avatar)
//            return avatar
//        } else {
////            setupAvatarImage(message.senderId, imageData: message, incoming: true)
////            return UIImageView(image:avatars[message.senderId])
//            return avatars[message.senderId]
//        }
////        return avatars[message.senderId]
//    }
//    
//    // Main
//    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = super.collectionView(collectionView, cellForItemAt: indexPath)
//            as! JSQMessagesCollectionViewCell
//        
//        let message = messages[indexPath.item]
//        
//        if message.senderId == self.toId + "1" {
//            cell.textView!.textColor = UIColor.black
//        } else{
//            cell.textView!.textColor = UIColor.white
//        }
//        
//        return cell
//    }
//    
//    fileprivate func setupBubbles() {
//        let factory = JSQMessagesBubbleImageFactory()
//        outgoingBubbleImageView = factory?.outgoingMessagesBubbleImage(
//            with: UIColor.jsq_messageBubbleLightGray())
//        incomingBubbleImageView = factory?.incomingMessagesBubbleImage(
//            with: UIColor.jsq_messageBubbleBlue())
//    }
//    
//    // Message
//    func addMessage(_ id: String, name: String, text: String) {
//        let message = JSQMessage(senderId: id, displayName: name, text: text)
//        messages.append(message!)
//        finishReceivingMessage()
//    }
//    
//    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!,
//                                     senderDisplayName: String!, date: Date!) {
//        let format = DateFormatter()
//        format.dateFormat = "d MMMM YYYY 'at' hh:mm a"
//        let date:NSString = format.string(from: date) as NSString
//        // Data Message
//        let dataRef = messageRef.childByAutoId()
//        let dataMessage = [
//            "id": self.senderId,
//            "name": "Test1",
//            "senderId": self.toId + "1",
//            "text": text,
//            "date": date,
//            "mediaType": "",
//            "media": ""
//        ] as [String : Any]
//        dataRef.setValue(dataMessage)
//        
//        JSQSystemSoundPlayer.jsq_playMessageSentSound()
//        
//        finishSendingMessage()
//        isTyping = false
//    }
//    
//    fileprivate func observeMessages() {
//        let messageQuery = messageRef.queryLimited(toLast: 25)
//        messageQuery.observe(.childAdded) { (snapshot: FIRDataSnapshot!) in
//            let phone = snapshot.value!["id"] as! String
//            if phone == self.senderId {
//                let id = snapshot.value!["senderId"] as! String
//                let name = snapshot.value!["name"] as! String
//                let text = snapshot.value!["text"] as! String
////                let dateString = snapshot.value!["date"] as! String
////                let dateFormatter = NSDateFormatter()
////                let date = dateFormatter.dateFromString(dateString)
//                let type = snapshot.value!["mediaType"] as! String
//                
//                if type == "" {
//                    self.addMessage(id, name: name, text: text)
//                }
//                if type == "image" {
//                    let urlString = snapshot.value!["media"] as! String
//                    let url = URL(string: urlString)
//                    URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
//                        if error != nil {
//                            print(error)
//                            return
//                        }
//                        DispatchQueue.main.async(execute: {
//                            
//                            if let downloadedImage = UIImage(data: data!) {
//                                let photoItem = JSQPhotoMediaItem(image: UIImage(data: data!))
//                                self.addMedia(photoItem)
//                            }
//                        })
//                        
//                    }).resume()
//                
//                }
//                
//                self.finishReceivingMessage()
//            }
//        }
//    }
//    
//    override func textViewDidChange(_ textView: UITextView) {
//        super.textViewDidChange(textView)
//        isTyping = textView.text != ""
//    }
//    
//    fileprivate func observeTyping() {
//        let typingIndicatorRef = rootRef.child("typingIndicator")
//        let typingId = senderId + self.toId
//        userIsTypingRef = typingIndicatorRef.child(typingId)
//        userIsTypingRef.onDisconnectRemoveValue()
//        
//        usersTypingQuery = typingIndicatorRef.queryOrderedByValue().queryEqual(toValue: true)
//        
//        usersTypingQuery.observe(.value) { (data: FIRDataSnapshot!) in
//            
//            if data.childrenCount == 1 && self.isTyping {
//                return
//            }
//            
//            self.showTypingIndicator = data.childrenCount > 0
//            self.scrollToBottom(animated: true)
//        }
//    }
//    
//    func addMedia(_ media:JSQMediaItem) {
//        let message = JSQMessage(senderId: self.senderId, displayName: "", media: media)
//        self.messages.append(message!)
//        
//        self.finishSendingMessage(animated: true)
//    }
//    
//    // Avatar
//    func setupAvatarImage(_ name: String, imageData: Data?, incoming: Bool) {
//        if let data = imageData {
//            let image = UIImage(data: data)
//            let diameter = incoming ? UInt(collectionView.collectionViewLayout.incomingAvatarViewSize.width) : UInt(collectionView.collectionViewLayout.outgoingAvatarViewSize.width)
//            let avatarImage = JSQMessagesAvatarImageFactory.avatarImage(with: image, diameter: diameter)
//            avatars[name] = avatarImage
//            return
//        }
//        
//        // At some point, we failed at getting the image (probably broken URL), so default to avatarColor
//        setupAvatarColor(name, incoming: incoming)
//    }
//    func setupAvatarColor(_ name: String, incoming: Bool) {
//        let diameter = incoming ? UInt(collectionView.collectionViewLayout.incomingAvatarViewSize.width) : UInt(collectionView.collectionViewLayout.outgoingAvatarViewSize.width)
//        
//        let rgbValue = name.hash
//        let r = CGFloat(Float((rgbValue & 0xFF0000) >> 16)/255.0)
//        let g = CGFloat(Float((rgbValue & 0xFF00) >> 8)/255.0)
//        let b = CGFloat(Float(rgbValue & 0xFF)/255.0)
//        let color = UIColor(red: r, green: g, blue: b, alpha: 0.5)
//        
//        let nameLength = name.characters.count
//        let initials : String? = name.substring(to: senderId.index(senderId.startIndex, offsetBy: min(3, nameLength)))
//        let userImage = JSQMessagesAvatarImageFactory.avatarImage(withUserInitials: initials, backgroundColor: color, textColor: UIColor.black, font: UIFont.systemFont(ofSize: CGFloat(13)), diameter: diameter)
//        
//        avatars[name] = userImage
//    }
//    
//    // Attachment
//    override func didPressAccessoryButton(_ sender: UIButton) {
//        self.inputToolbar.contentView!.textView!.resignFirstResponder()
//        
//        let sheet = UIAlertController(title: "Media messages", message: nil, preferredStyle: .actionSheet)
//        
//        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
//            self.cameraTapped()
//        }
//        
//        let photoAction = UIAlertAction(title: "Send Photo", style: .default) { (action) in
//            self.libraryTapped()
//        }
//        
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//        
//        sheet.addAction(cameraAction)
//        sheet.addAction(photoAction)
//        sheet.addAction(cancelAction)
//        
//        self.present(sheet, animated: true, completion: nil)
//    }
//    
//    // Camera
//    func cameraTapped(){
//        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
//        {
//            self.imgPicker.delegate = self;
//            
//            self.imgPicker.sourceType = UIImagePickerControllerSourceType.camera
//            self.imgPicker.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
//            self.imgPicker.allowsEditing = true
//            self.imgPicker.videoMaximumDuration = 15
//            self.present(self.imgPicker, animated: true, completion: nil)
//        }
//        else
//        {
//            libraryTapped()
//        }
//    }
//    
//    // Library Image
//    func libraryTapped(){
//        self.imgPicker.allowsEditing = true
//        self.imgPicker.sourceType = .photoLibrary
//        present(imgPicker, animated: true, completion: nil)
//    }
//    
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        let picked = info[UIImagePickerControllerMediaType] as? String
//        
//        if picked == "public.image" {
//            print("Upload Image")
//            self.mediaType = "image"
//            self.pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
//            self.uploadImage()
//        }else{
//            self.mediaType = "video"
//            self.pickedVideo = info[UIImagePickerControllerMediaURL] as? URL
//        }
//        
//        dismiss(animated: true, completion: nil)
//    }
//    
//    func uploadImage(){
//        // Upload Image
//        let fileName = "\(UUID().uuidString).png"
//        let saveImageRef = storageRef.child("Media_Photo").child(fileName)
//        if let uploadData = UIImagePNGRepresentation(pickedImage!) {
//            saveImageRef.put(uploadData, metadata: nil, completion: { (metadata, error) in
//                if error != nil {
//                    print(error)
//                    return
//                }
//                self.sendImageUrl = (metadata?.downloadURL()?.absoluteString)!
//                
//                self.saveMedia()
//                let photoItem = JSQPhotoMediaItem(image: UIImage(named: fileName))
//                self.addMedia(photoItem!)
//            })
//        }
//    }
//    
//    func uploadVideo() {
//        // Upload Video
//        let fileName = "\(UUID().uuidString).mk4"
//        let saveImageRef = storageRef.child("Media_Video").child(fileName)
//        if let uploadData = self.pickedVideo {
//            saveImageRef.putFile(uploadData, metadata: nil, completion: { (metadata, error) in
//                if error != nil {
//                    print(error)
//                    return
//                }
//                self.sendImageUrl = (metadata?.downloadURL()?.absoluteString)!
//                self.saveMedia()
//                let videoItem = JSQVideoMediaItem.init(fileURL: self.pickedVideo, isReadyToPlay: true)
//                self.addMedia(videoItem!)
//            })
//        }
//    }
//    
//    func saveMedia(){
//        let todaysDate:Date = Date()
//        let dateFormatter:DateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "d MMMM YYYY 'at' hh:mm a"
//        let DateInFormat:String = dateFormatter.string(from: todaysDate)
//        print(DateInFormat)
//        
//        // Data Message
//        let dataRef = messageRef.childByAutoId()
//        let dataMessage = [
//            "id": self.senderId,
//            "name": "Test1",
//            "senderId": self.toId + "1",
//            "text": "",
//            "date": DateInFormat,
//            "mediaType": self.mediaType,
//            "media": self.sendImageUrl
//        ]
//        dataRef.setValue(dataMessage)
//        
//        JSQSystemSoundPlayer.jsq_playMessageSentSound()
//        
//        finishSendingMessage()
//        isTyping = false
//    }
//    
//    func getDocumentsDirectory() -> NSString {
//        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
//        let documentsDirectory = paths[0]
//        return documentsDirectory as NSString
//    }
//    
//    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//        dismiss(animated: true, completion: nil)
//    }
}
