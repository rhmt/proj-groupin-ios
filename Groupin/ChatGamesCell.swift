//
//  ChatGamesCell.swift
//  Pitung
//
//  Created by Macbook pro on 2/24/17.
//  Copyright © 2017 Macbook pro. All rights reserved.
//

import Foundation
import UIKit
import UICircularProgressRing

class ChatGamesCell: UITableViewCell {
    
    //Semua chat cell nya di sini
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var conHeightDate: NSLayoutConstraint!
    @IBOutlet weak var conWidthDate: NSLayoutConstraint!
    @IBOutlet weak var conSpaceIncomingDate: NSLayoutConstraint!
    @IBOutlet weak var conSpaceOutgoingDate: NSLayoutConstraint!
    
    @IBOutlet weak var viewIncomingMsg: UIView!
    @IBOutlet weak var lblIncomingName: UILabel!
    @IBOutlet weak var conHeightLblIncomingName: NSLayoutConstraint!
    @IBOutlet weak var imgIncomingAva: UIImageView!
    @IBOutlet weak var viewIncomingContainer: UIView!
    @IBOutlet weak var lblIncomingText: UITextView!
    @IBOutlet weak var lblIncomingTime: UILabel!
    @IBOutlet weak var conIncomingWidht: NSLayoutConstraint!
    @IBOutlet weak var viewIncomingMedia: UIView!
    @IBOutlet weak var imgIncomingMedia: UIImageView!
    @IBOutlet weak var lblIncomingMedia: UILabel!
    @IBOutlet weak var conHeightLblIncomingMedia: NSLayoutConstraint!
    @IBOutlet weak var btnIncomingShare: UIButton!
    @IBOutlet weak var imgRecoredIncomingMedia: UIImageView!
    @IBOutlet weak var viewIncomingVoice: UIView!
    @IBOutlet weak var imgIncomingPlayVoice: UIImageView!
    @IBOutlet weak var sldrIncomingVoice: UISlider!
    @IBOutlet weak var lblIncomingDurationVoice: UILabel!
    @IBOutlet weak var viewIncomingReply: UIView!
    @IBOutlet weak var conHeightViewIncomingReply: NSLayoutConstraint!
    @IBOutlet weak var viewIncomingContainerReply: UIView!
    @IBOutlet weak var viewIncomingIndicatorReply: UIView!
    @IBOutlet weak var lblIncomingNameReply: UILabel!
    @IBOutlet weak var lblIncomingContentReply: UILabel!
    @IBOutlet weak var imgIncomingContentReply: UIImageView!
    @IBOutlet weak var progBarIncoming: UICircularProgressRingView!
    @IBOutlet weak var cancelProgBarIncoming: UIButton!
    
    
    @IBOutlet weak var viewOutgoingMsg: UIView!
    @IBOutlet weak var viewOutgoingContainer: UIView!
    @IBOutlet weak var lblOutgoingText: UITextView!
    @IBOutlet weak var imgOutgoingIndicatorSend: UIImageView!
    @IBOutlet weak var lblOutgoingTime: UILabel!
    @IBOutlet weak var conOutgoingWidth: NSLayoutConstraint!
    @IBOutlet weak var viewOutgoingMedia: UIView!
    @IBOutlet weak var imgOutgoingMedia: UIImageView!
    @IBOutlet weak var lblOutgoingMedia: UILabel!
    @IBOutlet weak var conHeightLblOutgoingMedia: NSLayoutConstraint!
    @IBOutlet weak var btnOutgoingShare: UIButton!
    @IBOutlet weak var imgRecoredOutgoingMedia: UIImageView!
    @IBOutlet weak var viewOutgoingVoice: UIView!
    @IBOutlet weak var imgOutgoingPlayVoice: UIImageView!
    @IBOutlet weak var sldrOutgoingVoice: UISlider!
    @IBOutlet weak var lblOutgoingDurationVoice: UILabel!
    @IBOutlet weak var viewOutgoingReply: UIView!
    @IBOutlet weak var conHeightViewOutgoingReply: NSLayoutConstraint!
    @IBOutlet weak var viewOutgoingContainerReply: UIView!
    @IBOutlet weak var viewOutgoingIndicatorReply: UIView!
    @IBOutlet weak var lblOutgoingNameReply: UILabel!
    @IBOutlet weak var lblOutgoingContentReply: UILabel!
    @IBOutlet weak var imgOutgoingContentReply: UIImageView!
    @IBOutlet weak var progBarOutgoing: UICircularProgressRingView!
    @IBOutlet weak var cancelProgBarOutgoing: UIButton!
    
    @IBOutlet weak var viewIndicatorSelectedCell: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
