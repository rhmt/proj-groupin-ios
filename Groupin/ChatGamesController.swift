//
//  ChatGamesController.swift
//  Pitung
//
//  Created by Macbook pro on 2/22/17.
//  Copyright © 2017 Macbook pro. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import Firebase
import SwiftyJSON
import Alamofire
import AGEmojiKeyboard

class ChatGamesController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, AGEmojiKeyboardViewDataSource, AGEmojiKeyboardViewDelegate {
    
    @IBOutlet weak var tbChat: UITableView!
    @IBOutlet weak var viewInput: UIView!
    @IBOutlet weak var conHeightViewInput: NSLayoutConstraint!
    @IBOutlet weak var conBottomViewInput: NSLayoutConstraint!
    @IBOutlet weak var txtInput: UITextView!
    @IBOutlet weak var imgSend: UIImageView!
    @IBOutlet weak var imgEmot: UIImageView!
    
    var messages = [Chat]()
    var fromId = ""
    var toId = ""
    //Data user friend
    var idFriend = [String]()
    var nameFriend = [String]()
    var avaFriend = [String]()
    var colorFriend = [UIColor]()
    
    //Self Variable
    var statusSend = 0
    var statusEmot = 0
    var minusIndexMessage = 20
    var countMessage = 0
    var lastIndexMessage = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Preferences
        self.navigationItem.title = "Games"
        self.txtInput.delegate = self
        self.txtInput.textColor = UIColor.gray
        
        //Load data local
        let sess = try! Realm().objects(session.self).first!
        self.fromId = String(sess.id)
        self.toId = "Games_" + String(sess.id_group)
        
        //KeyBoard
        self.setupKeyboardObservers()
        
        // Background Image
        let backgroundImage = UIImage(named: "bg_chat")
        let imageView = UIImageView(image: backgroundImage)
        imageView.contentMode = .scaleAspectFill//.scaleAspectFit
        self.tbChat.backgroundView = imageView
        self.tbChat.keyboardDismissMode = .interactive
        
        //Add tapped on button
        let tapEmot = UITapGestureRecognizer(target: self, action: #selector(ChatGamesController.handleEmotTap(_:)))
        tapEmot.numberOfTapsRequired = 1
        imgEmot.isUserInteractionEnabled = true
        imgEmot.addGestureRecognizer(tapEmot)
        let tapSend = UITapGestureRecognizer(target: self, action: #selector(ChatGamesController.handleSend(_:)))
        tapSend.numberOfTapsRequired = 1
        imgSend.isUserInteractionEnabled = true
        imgSend.addGestureRecognizer(tapSend)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //Get Chat
        self.observeMessages()
        self.scrollToLast()
        self.checkingServer()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatCell", for: indexPath) as! ChatGamesCell
        
        //Awalnya semua view (incoming dan outgoing) di hidden
        cell.viewIncomingMsg.isHidden = true
        cell.viewOutgoingMsg.isHidden = true
        
        let data = self.messages[indexPath.row]
        
        //Time
        var timeShow = ""
        let seconds = data.timestamp
        let sec = seconds! / 1000
        let timestampDate = Date(timeIntervalSince1970: sec)
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        dateFormatter.dateStyle = .none
        dateFormatter.dateFormat = "HH:mm"
        timeShow = dateFormatter.string(from: timestampDate)
        
        //Incoming or Outgoing
        if data.fromId == self.fromId {
            //Outgoing
            cell.viewOutgoingMsg.isHidden = false
            cell.lblOutgoingText.text = data.text
            cell.lblOutgoingTime.text = timeShow
        }else{
            //Incoming
            cell.viewIncomingMsg.isHidden = false
            cell.lblIncomingText.text = data.text
            cell.lblIncomingTime.text = timeShow
            
            let idMember = self.idFriend.index(of: data.fromId!)
            let nameMember = self.nameFriend[idMember!]
            cell.lblIncomingName.text = nameMember
            if self.avaFriend[idMember!] != "" {
                let imgUrl = URL(string: self.avaFriend[idMember!])!
                cell.imgIncomingAva.setImage(withUrl: imgUrl)
            }else{
                cell.imgIncomingAva.image = UIImage(named: "dafault-ava-group")
            }
            cell.lblIncomingName.textColor = self.colorFriend[idMember!]
        }
        
        //Design
        cell.backgroundColor = UIColor.clear
        cell.viewIncomingMsg.backgroundColor = UIColor.clear
        cell.viewOutgoingMsg.backgroundColor = UIColor.clear
        var width = Config().estimateFrameForText(data.text!, width: 200).width + 32
        if width < 70 {
            width = 70
        }
        if width > 200 {
            width = 200
        }
        
        //Preference
        cell.conIncomingWidht.constant = width
        cell.conOutgoingWidth.constant = width
        cell.viewOutgoingContainer.layer.cornerRadius = 5
        cell.viewIncomingContainer.layer.cornerRadius = 5
        cell.viewOutgoingContainer.layer.shadowColor = UIColor.black.cgColor
        cell.viewIncomingContainer.layer.shadowColor = UIColor.black.cgColor
        cell.viewOutgoingContainer.layer.shadowRadius = 5
        cell.viewIncomingContainer.layer.shadowRadius = 5
        cell.viewOutgoingContainer.layer.shadowOffset = CGSize.zero
        cell.viewIncomingContainer.layer.shadowOffset = CGSize.zero
        cell.viewOutgoingContainer.layer.shadowOpacity = 0.3
        cell.viewIncomingContainer.layer.shadowOpacity = 0.3
        cell.imgIncomingAva.clipsToBounds = true
        cell.imgIncomingAva.layer.cornerRadius = cell.imgIncomingAva.frame.width / 2
        cell.lblIncomingText.isScrollEnabled = false
        cell.lblOutgoingText.isScrollEnabled = false
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height: CGFloat = 80
        let message = messages[indexPath.item]
        
        let text = message.text
        height = Config().estimateFrameForText(text!, width: 200).height + 50
        
        if height > 70 {
            height += 6
        }
        
        if message.fromId == self.fromId{
            return height
        }else{
            height += 10
            return height
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY: CGFloat = scrollView.contentOffset.y
        if (offsetY < 10){
            if self.countMessage > self.minusIndexMessage {
                let lastIndex = self.lastIndexMessage
                self.minusIndexMessage += 20
                self.observeMessages()
                
                DispatchQueue.main.async(execute: {
                    if self.messages.count > 0 {
                        self.tbChat.reloadData()
                        let indexPath = IndexPath(item: lastIndex, section: 0)
                        self.tbChat.scrollToRow(at: indexPath, at: .top, animated: false)
                    }
                })
            }
        }
    }
    
    //Chat
    func observeMessages(){
        let models = try! Realm().objects(tb_chat.self).filter("to = %@",self.toId)
        if models.count > 0 {
            self.countMessage = models.count - 1
            var lastModels = self.countMessage - self.minusIndexMessage
            if lastModels < 0 {
                lastModels = 0
            }
            self.lastIndexMessage = lastModels
            self.messages.removeAll()
            for i in lastModels...self.countMessage {
                self.messages.append(Chat(dictionary: models[i]))
            }
        }
    }
    
    func scrollToLast(){
        //Scroll to last chat
        DispatchQueue.main.async(execute: {
            if self.messages.count > 0 {
                self.tbChat.reloadData()
                let indexPath = IndexPath(item: self.messages.count - 1, section: 0)
                self.tbChat.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }
        })
    }
    
    func checkingServer(){
        let messagesRef = FIRDatabase.database().reference().child("messages").queryOrdered(byChild: "toId").queryEqual(toValue: self.toId)
        messagesRef.observe(.childAdded) { (snapshot: FIRDataSnapshot!) in
            let idChat = snapshot.key
            let primary = try! Realm().object(ofType: tb_chat.self, forPrimaryKey: idChat)
            if primary == nil {
                self.downloadData(idChat)
            }
        }
    }
    
    func downloadData(_ key: String) {
        let messagesRef = FIRDatabase.database().reference().child("messages").queryOrderedByKey().queryEqual(toValue: key)
        messagesRef.observe(.childAdded) { (snapshot: FIRDataSnapshot!) in
            guard let dictionary = snapshot.value as? [String: AnyObject] else {
                return
            }
            let idChat = snapshot.key
            self.migrateToLocal(dictionary, idChat: idChat)
        }
    }
    
    func migrateToLocal(_ chat: [String: AnyObject], idChat: String){
        //-----------------------
        let model = tb_chat()
        model.id_chat = idChat
        if let fromId = chat["fromId"] {model.from = fromId as! String}
        if let toId = chat["toId"] {model.to = toId as! String}
        if let text = chat["text"] {model.text = text as! String}
        if let time = chat["timestamp"] {model.time = Double(Int(time as! NSNumber))}
        if let imageUrl = chat["imageUrl"] {model.image_url = imageUrl as! String}
        if let imageWidth = chat["imageWidth"] {model.image_width = Int(imageWidth as! NSNumber)}
        if let imageHeight = chat["imageHeight"] {model.image_height = Int(imageHeight as! NSNumber)}
        if let videoUrl = chat["videoUrl"] {model.video_url = videoUrl as! String}
        if let audioUrl = chat["audioUrl"] {model.audio_url = audioUrl as! String}
        if let fileUrl = chat["fileUrl"] {model.file_url = fileUrl as! String}
        if let mediaName = chat["mediaName"] {model.mediaName = mediaName as! String}
        if let mediaSize = chat["mediaSize"] {model.mediaSize = mediaSize as! Int}
        if let mediaType = chat["mediaType"] {model.mediaType = mediaType as! String}
        if let lat = chat["latitude"] {model.latitude = Float(lat as! NSNumber)}
        if let long = chat["longitude"] {model.longitude = Float(long as! NSNumber)}
        if let address = chat["nameAddress"] {model.nameAdrress = address as! String}
        if let address = chat["detailAddress"] {model.detailAdrress = address as! String}
        if let keyReply = chat["keyReply"] {model.keyReply = keyReply as! String}
        model.is_read = 14
        
        let primary = try! Realm().object(ofType: tb_chat.self, forPrimaryKey: idChat)
        if (primary != nil) {
            print("Error Primary Key")
            return
        }else{
            DBHelper.insert(model)
        }
        
        self.observeMessages()
        self.scrollToLast()
    }
    
    // Handle Send
    func handleSend(_ sender: UITapGestureRecognizer){
        if statusSend > 0 {
            let properties: [String: AnyObject]!
            properties = ["text": self.txtInput.text as AnyObject]
            sendMessageWithProperties(properties)
            
            //Change tampilan send
            self.txtInput.text = ""
            self.txtInput.textColor = UIColor.black
            self.txtInput.becomeFirstResponder()
            self.statusSend = 0
            self.imgSend.image = UIImage(named: "ico_send_disable")
        }
    }
    
    func sendMessageWithProperties(_ properties: [String: AnyObject]) {
        let ref = FIRDatabase.database().reference().child("messages")
        let childRef = ref.childByAutoId()
        let timestamp: NSNumber = NSNumber.init(value: Date().timeIntervalSince1970 * 1000)
        var values: [String: AnyObject] = ["toId": self.toId as AnyObject, "fromId": self.fromId as AnyObject, "timestamp": timestamp, "isRead": 0 as AnyObject]
        
        properties.forEach({values[$0] = $1})
        
        childRef.updateChildValues(values) { (error, ref) in
            if error != nil {
                return
            }
            
            let userMessagesRef = FIRDatabase.database().reference().child("user-messages").child(self.fromId).child(self.toId)
            
            let messageId = childRef.key
            userMessagesRef.updateChildValues([messageId: 1])
            
            let recipientUserMessagesRef = FIRDatabase.database().reference().child("user-messages").child(self.toId).child(self.fromId)
            recipientUserMessagesRef.updateChildValues([messageId: 1])
        }
        
        //self.sendNotif(values, key: childRef.key)
        
        self.migrateToLocal(values, idChat: childRef.key)
    }
    
    // Emoji
    func handleEmotTap(_ sender: UITapGestureRecognizer){
        self.txtInput.resignFirstResponder()
        if statusEmot == 0 {
            // Show Emot keyboard
            let keyboardRect = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.view.frame.size.width), height: CGFloat(216))
            let emojiKeyboardView = AGEmojiKeyboardView(frame: keyboardRect, dataSource: self)
            emojiKeyboardView?.autoresizingMask = .flexibleHeight
            emojiKeyboardView?.delegate = self
            emojiKeyboardView?.tintColor = UIColor.lightGray
            self.txtInput.inputView = emojiKeyboardView
            self.statusEmot = 1
            self.imgEmot.image = UIImage(named: "ico_keyboard")
        }else{
            // Show standard keyboard
            self.txtInput.inputView = nil
            self.txtInput.reloadInputViews()
            self.statusEmot = 0
            self.imgEmot.image = UIImage(named: "ico_insert_emot")
        }
        self.txtInput.becomeFirstResponder()
    }
    
    //AGEmojiKeyboard
    func emojiKeyBoardView(_ emojiKeyBoardView: AGEmojiKeyboardView!, didUseEmoji emoji: String!) {
        if let range = self.txtInput.selectedTextRange {
            if range.start == range.end {
                self.txtInput.replace(range, withText: emoji)
            }
        }
        // Change Send Button
        self.imgSend.image = UIImage(named: "ico_send_selected")
    }
    
    func emojiKeyBoardViewDidPressBackSpace(_ emojiKeyBoardView: AGEmojiKeyboardView!) {
        let currentText = self.txtInput.text!
        if currentText != "" {
            self.txtInput.text = currentText.substring(to: currentText.index(before: currentText.endIndex))
        }
        let totalChar = currentText.characters.count
        if totalChar == 1 {
            // Change Send Button
            if self.txtInput.text?.isEmpty ?? true {
                self.imgSend.image = UIImage(named: "ico_send_disable")
            }
        }
    }
    
    func emojiKeyboardView(_ emojiKeyboardView: AGEmojiKeyboardView!, imageForNonSelectedCategory category: AGEmojiKeyboardViewCategoryImage) -> UIImage! {
        let img: UIImage? = self.storeImageEmot().withRenderingMode(.alwaysOriginal)
        img?.withRenderingMode(.alwaysOriginal)
        return img
    }
    
    func emojiKeyboardView(_ emojiKeyboardView: AGEmojiKeyboardView!, imageForSelectedCategory category: AGEmojiKeyboardViewCategoryImage) -> UIImage! {
        let img: UIImage? = self.storeImageEmot().withRenderingMode(.alwaysOriginal)
        img?.withRenderingMode(.alwaysOriginal)
        emojiKeyboardView.contentMode = .scaleToFill
        return img?.withRenderingMode(.alwaysOriginal)
    }
    
    public func backSpaceButtonImage(for emojiKeyboardView: AGEmojiKeyboardView!) -> UIImage! {
        let image = UIImage(named: "ico_emot_backspace")
        emojiKeyboardView.contentMode = .scaleToFill
        return image
    }
    
    //Image Keyboard store
    var imgIndex = 0
    func storeImageEmot() -> UIImage {
        self.imgIndex += 1
        let strImage = "ico_emot_cat_" + String(self.imgIndex)
        if let img: UIImage = UIImage(named: strImage) {
            img.withRenderingMode(.alwaysOriginal)
            return img
        }else{
            let img = UIImage()
            return img
        }
    }
    
    //TextView
    func textViewDidBeginEditing(_ textView: UITextView) {
        if self.txtInput.textColor == UIColor.gray {
            self.txtInput.text = nil
            self.txtInput.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if self.txtInput.text!.isEmpty {
            self.txtInput.text = "Write something here..."
            self.txtInput.textColor = UIColor.gray
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.imgSend.image = UIImage(named: "ico_send_selected")
        self.statusSend = 1
        if self.txtInput.text?.isEmpty ?? true {
            self.imgSend.image = UIImage(named: "ico_send_disable")
            self.statusSend = 0
        }
    }
    
    //Handle Keyboard
    override var canBecomeFirstResponder : Bool {
        return true
    }
    
    func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    func handleKeyboardDidShow() {
        if messages.count > 0 {
            let indexPath = IndexPath(item: messages.count - 1, section: 0)
            self.tbChat.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    func handleKeyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if view.frame.origin.y == 0 {
            self.conBottomViewInput.constant = keyboardSize!.height
        }
    }
    
    func handleKeyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y < 0 {
                self.conBottomViewInput.constant = 0
            }
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
