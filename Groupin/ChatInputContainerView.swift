//
//  ChatInputContainerView.swift
//  Groupin
//
//  Created by Macbook pro on 9/20/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class ChatInputContainerView: UIView, UITextViewDelegate, UITextFieldDelegate {
    
    weak var chatController: ChatController? {
        didSet {
            sendButton.addTarget(chatController, action: #selector(ChatController.handleSend), for: .touchUpInside)
            sendButton.isEnabled = false
            sendButton.isHidden = true
            sendButton.setBackgroundImage(UIImage(named: "ico_send_disable"), for: UIControlState())
            lblDurationRecord.isHidden = true
            
            sendVoiceButton.addTarget(self, action: #selector(self.holdRecord), for: .touchDown)
            sendVoiceButton.addTarget(self, action: #selector(self.endRecord), for: .touchUpInside)
            sendVoiceButton.isHidden = false
            sendVoiceButton.setBackgroundImage(UIImage(named: "ico_mic"), for: UIControlState())
            
            uploadImageView.addGestureRecognizer(UITapGestureRecognizer(target: chatController, action: #selector(ChatController.handleUploadTap)))
            emoticonView.addGestureRecognizer(UITapGestureRecognizer(target: chatController, action: #selector(ChatController.handleEmotTap)))
            
            self.xPosSend = self.sendVoiceButton.center.x
            self.yPosSend = self.sendVoiceButton.center.y
            self.xPosLbl = self.closeRecord.center.x
            self.yPosLbl = self.closeRecord.center.y
            
            slideToClose.addTarget(self, action: #selector(self.slideCloseRecord(_:)))
            sendVoiceButton.isUserInteractionEnabled = true
            sendVoiceButton.addGestureRecognizer(slideToClose)
            
            self.imgCancelReply.addGestureRecognizer(UITapGestureRecognizer(target: chatController, action: #selector(ChatController.cancelReply)))
        }
    }
    
    lazy var inputTextField: UITextView = {
        let textField = UITextView()
        textField.text = "Write something here"
        textField.textColor = UIColor.gray
        textField.font = textField.font?.withSize(16)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.delegate = self
        return textField
    }()
    
    let slideToClose = UIPanGestureRecognizer()
    let closeRecord: UILabel = {
        let tv = UILabel()
        tv.font = UIFont.systemFont(ofSize: 16)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = UIColor.clear
        tv.textColor = .gray
        tv.text = "< slide to close"
        tv.textAlignment = .right
        tv.isHidden = true
        return tv
    }()
    
    let lblDurationRecord: UILabel = {
        let lbl = UILabel()
        lbl.text = "00:00"
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = lbl.font?.withSize(16)
        return lbl
    }()
    
    let emoticonView: UIImageView = {
        let emoticonView = UIImageView()
        emoticonView.isUserInteractionEnabled = true
        emoticonView.image = UIImage(named: "ico_insert_emot")
        emoticonView.contentMode = UIViewContentMode.scaleAspectFit
        emoticonView.translatesAutoresizingMaskIntoConstraints = false
        return emoticonView
    }()
    
    let uploadImageView: UIImageView = {
        let uploadImageView = UIImageView()
        uploadImageView.isUserInteractionEnabled = true
        uploadImageView.image = UIImage(named: "ico_camare_nobg")
        uploadImageView.contentMode = UIViewContentMode.scaleAspectFit
        uploadImageView.translatesAutoresizingMaskIntoConstraints = false
        return uploadImageView
    }()
    
    let sendButton = UIButton(type: .system)
    let sendVoiceButton = UIButton(type: .system)
    
    //Reply
    let viewReply: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear
        view.isHidden = true
        return view
    }()
    
    let viewIndicatorReply: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
        return view
    }()
    
    let nameReply: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor(red: 63, green: 157, blue: 247)
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    let contentReply: UITextView = {
        let label = UITextView()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 14)
        label.backgroundColor = UIColor.clear
        label.isEditable = false
        label.isSelectable = false
        label.isScrollEnabled = false
        return label
    }()
    
    let imgCancelReply: UIImageView = {
        let img = UIImageView()
        img.isUserInteractionEnabled = true
        img.image = UIImage(named: "ico_close")
        img.contentMode = UIViewContentMode.scaleAspectFit
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let mediaReply: UIImageView = {
        let img = UIImageView()
        img.isUserInteractionEnabled = true
        img.contentMode = UIViewContentMode.scaleAspectFit
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    var widthInputTextAnchor: NSLayoutConstraint?
    var topInputTextAnchor: NSLayoutConstraint?
    var bottomInputTextAnchor: NSLayoutConstraint?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        //Separator
        let separatorLineView = UIView()
        //        separatorLineView.backgroundColor = UIColor(r: 220, g: 220, b: 220)
        separatorLineView.backgroundColor = UIColor.lightGray
        separatorLineView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(separatorLineView)
        separatorLineView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        separatorLineView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        separatorLineView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        separatorLineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        //Send Button
//        sendButton.setTitle("Send", forState: .Normal)
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(sendButton)
        sendButton.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        sendButton.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
//        sendButton.centerYAnchor.constraintEqualToAnchor(centerYAnchor).active = true
        sendButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        sendButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //Emot
        addSubview(emoticonView)
        emoticonView.leftAnchor.constraint(equalTo: leftAnchor, constant: 5).isActive = true
        emoticonView.centerYAnchor.constraint(equalTo: sendButton.centerYAnchor).isActive = true
        emoticonView.widthAnchor.constraint(equalToConstant: 28).isActive = true
        emoticonView.heightAnchor.constraint(equalToConstant: 28).isActive = true
        
        //Upload Image
        addSubview(uploadImageView)
        uploadImageView.rightAnchor.constraint(equalTo: sendButton.leftAnchor, constant: -4).isActive = true
        uploadImageView.centerYAnchor.constraint(equalTo: sendButton.centerYAnchor).isActive = true
        uploadImageView.widthAnchor.constraint(equalToConstant: 28).isActive = true
        uploadImageView.heightAnchor.constraint(equalToConstant: 28).isActive = true
        
        //Voice
        sendVoiceButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(sendVoiceButton)
        sendVoiceButton.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
//        sendVoiceButton.centerYAnchor.constraintEqualToAnchor(centerYAnchor).active = true
        sendVoiceButton.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        sendVoiceButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        sendVoiceButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //Slide to close
        addSubview(closeRecord)
        closeRecord.rightAnchor.constraint(equalTo: sendVoiceButton.leftAnchor, constant: -4).isActive = true
        closeRecord.centerYAnchor.constraint(equalTo: sendButton.centerYAnchor).isActive = true
        closeRecord.widthAnchor.constraint(equalToConstant: 200).isActive = true
        closeRecord.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        addSubview(viewReply)
        //Input Text
        addSubview(self.inputTextField)
        self.widthInputTextAnchor = self.inputTextField.rightAnchor.constraint(equalTo: uploadImageView.leftAnchor, constant: -8)
        self.widthInputTextAnchor?.isActive = true
        self.inputTextField.leftAnchor.constraint(equalTo: emoticonView.rightAnchor, constant: 5).isActive = true
        self.inputTextField.centerYAnchor.constraint(equalTo: sendButton.centerYAnchor).isActive = true
        self.topInputTextAnchor = self.inputTextField.topAnchor.constraint(equalTo: topAnchor, constant: 8)
        self.topInputTextAnchor!.isActive = true
        self.bottomInputTextAnchor = self.inputTextField.topAnchor.constraint(equalTo: viewReply.bottomAnchor , constant: 16)
        self.inputTextField.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        self.inputTextField.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        addSubview(self.lblDurationRecord)
        self.lblDurationRecord.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        self.lblDurationRecord.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        self.lblDurationRecord.leftAnchor.constraint(equalTo: emoticonView.rightAnchor, constant: 5).isActive = true
        self.lblDurationRecord.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        
        self.viewReply.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true
//        self.viewReply.bottomAnchor.constraintEqualToAnchor(self.inputTextField.topAnchor, constant: -16).active = true
        self.viewReply.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        self.viewReply.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        self.viewReply.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        viewReply.addSubview(viewIndicatorReply)
        self.viewIndicatorReply.widthAnchor.constraint(equalToConstant: 3).isActive = true
        self.viewIndicatorReply.leftAnchor.constraint(equalTo: viewReply.leftAnchor).isActive = true
        self.viewIndicatorReply.topAnchor.constraint(equalTo: viewReply.topAnchor).isActive = true
        self.viewIndicatorReply.bottomAnchor.constraint(equalTo: viewReply.bottomAnchor).isActive = true
        
        viewReply.addSubview(nameReply)
        self.nameReply.topAnchor.constraint(equalTo: viewReply.topAnchor).isActive = true
        self.nameReply.leftAnchor.constraint(equalTo: viewIndicatorReply.rightAnchor, constant: 8).isActive = true
        self.nameReply.rightAnchor.constraint(equalTo: viewReply.rightAnchor, constant: -8).isActive = true
        self.nameReply.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        viewReply.addSubview(contentReply)
        self.contentReply.topAnchor.constraint(equalTo: nameReply.bottomAnchor).isActive = true
        self.contentReply.bottomAnchor.constraint(equalTo: viewReply.bottomAnchor, constant: -8).isActive = true
        self.contentReply.leftAnchor.constraint(equalTo: viewIndicatorReply.rightAnchor, constant: 4).isActive = true
        self.contentReply.rightAnchor.constraint(equalTo: viewReply.rightAnchor, constant: -8).isActive = true
        
        viewReply.addSubview(mediaReply)
        self.mediaReply.topAnchor.constraint(equalTo: viewReply.topAnchor).isActive = true
//        self.mediaReply.bottomAnchor.constraintEqualToAnchor(viewReply.bottomAnchor).active = true
        self.mediaReply.rightAnchor.constraint(equalTo: viewReply.rightAnchor).isActive = true
        self.mediaReply.widthAnchor.constraint(equalToConstant: 50).isActive = true
        self.mediaReply.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        viewReply.addSubview(imgCancelReply)
        self.imgCancelReply.topAnchor.constraint(equalTo: mediaReply.topAnchor).isActive = true
        self.imgCancelReply.rightAnchor.constraint(equalTo: mediaReply.rightAnchor).isActive = true
        self.imgCancelReply.widthAnchor.constraint(equalToConstant: 18).isActive = true
        self.imgCancelReply.heightAnchor.constraint(equalToConstant: 18).isActive = true
    }
    
    //TextView
    func textViewDidBeginEditing(_ textView: UITextView) {
        if inputTextField.textColor == UIColor.gray {
            inputTextField.text = nil
            inputTextField.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if inputTextField.text!.isEmpty {
            inputTextField.text = "Write something here..."
            inputTextField.textColor = UIColor.gray
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        chatController?.textChange()
        changeSendButton()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func changeSendButton(){
        sendButton.setBackgroundImage(UIImage(named: "ico_send_selected"), for: UIControlState())
        sendButton.isEnabled = true
        sendButton.isHidden = false
        sendVoiceButton.isHidden = true
        uploadImageView.isHidden = true
        if inputTextField.text?.isEmpty ?? true {
            sendButton.setBackgroundImage(UIImage(named: "ico_send_disable"), for: UIControlState())
            sendButton.isEnabled = false
            sendButton.isHidden = true
            sendVoiceButton.isHidden = false
            uploadImageView.isHidden = false
        }
    }
    
    func holdRecord(){
        self.inputTextField.isHidden = true
        self.lblDurationRecord.isHidden = false
        self.lblDurationRecord.text = "00:00"
        self.uploadImageView.isHidden = true
        self.emoticonView.image = UIImage(named: "ico_record")
        self.closeRecord.isHidden = false
        
        self.widthInputTextAnchor?.isActive = false
        
        chatController?.handleRecord()
    }
    
    var xPosSend: CGFloat!
    var yPosSend: CGFloat!
    var xPosLbl: CGFloat!
    var yPosLbl: CGFloat!
    func endRecord(){
        self.doneRecord()
        
        chatController?.handleSendRecord()
    }
    
    func slideCloseRecord(_ sender: UIPanGestureRecognizer){
        chatController?.view.bringSubview(toFront: sender.view!)
        let velocity = sender.velocity(in: sender.view)
        if velocity.x > 0 || velocity.x < 0{
            let translation = sender.translation(in: chatController!.view)
            sender.view!.center = CGPoint(x: sender.view!.center.x + translation.x, y: sender.view!.center.y)
            sender.setTranslation(CGPoint.zero, in: chatController!.view)
            
            let lbl = self.closeRecord
            let posX = lbl.center.x + translation.x
            let posY = lbl.center.y
            lbl.layer.position = CGPoint(x: posX, y: posY)
            
            let posEnd = (chatController!.view.frame.width / 2) - 50
            
            if posX < posEnd {
                sender.isEnabled = false
                sender.isEnabled = true
                self.doneRecord()
                chatController?.stopRecord()
            }
        }
        
        if sender.state == UIGestureRecognizerState.ended {
            self.doneRecord()
            chatController?.handleSendRecord()
        }
    }
    
    func doneRecord(){
        self.inputTextField.isHidden = false
        self.lblDurationRecord.isHidden = true
        self.uploadImageView.isHidden = false
        self.emoticonView.image = UIImage(named: "ico_insert_emot")
        self.closeRecord.isHidden = true
        self.widthInputTextAnchor?.isActive = true
        self.closeRecord.layer.position = CGPoint(x: self.xPosLbl, y: self.yPosLbl)
        self.sendVoiceButton.layer.position = CGPoint(x: self.xPosSend, y: self.yPosSend)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
