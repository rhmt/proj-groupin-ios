//
//  ChatListController.swift
//  Groupin
//
//  Created by Macbook pro on 1/30/17.
//  Copyright © 2017 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift

class ChatListController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tbChat: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var viewNoData: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tbChat.tableFooterView = UIView()
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.items?[3].badgeValue = nil
        
        self.tbChat.delegate = self
        self.tbChat.dataSource = self
        
        self.colorNav()
        self.rightMenu()
        self.checkFromNotif()
    }
    
    func checkFromNotif(){
        if let data = try! Realm().objects(session_notif.self).first {
            if data.fromNotif == 1 {
                self.idFriend = data.id
                self.nameBarFriend = data.name
                self.avaFriend = data.avatar
                
                try! Realm().write({
                    data.fromNotif = 0
                    data.id = 0
                    data.name = ""
                    data.avatar = ""
                })
                
                self.performSegue(withIdentifier: "showChating", sender: self)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    
        self.tabBarController?.tabBar.isHidden = false
        
        self.loadData()
    }
    
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(true)
//        self.tabBarController?.tabBar.isHidden = true
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var dataData = [listDataChatPersonal]()
    func loadData(){
        self.dataData = DBHelper.getAllDataChatPersonal()
        
        if self.dataData.count > 0 {
            self.viewNoData.isHidden = true
            self.tbChat.isHidden = false
            self.tbChat.reloadData()
        }else{
            self.viewNoData.isHidden = false
            self.tbChat.isHidden = true
        }
        
        self.tbChat.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatCell", for: indexPath) as! ContactCell
        
        let data = dataData[indexPath.row]
        
        //Get indicator data
        let from = String(data.id_sender)
        let indi = try! Realm().objects(tb_chat.self).filter("from = %@ OR to = %@", from, from).filter("is_read = %@", 0)
        let totalIndi = indi.count
        if totalIndi > 0 {
            cell.conHeightLblWaktu.constant = cell.conHeightLblName.constant
            cell.lblWaktu.textColor = self.blue
            cell.viewIndicator.isHidden = false
            cell.viewIndicator.layer.cornerRadius = cell.viewIndicator.frame.width / 2
            cell.lblIndicator.text = String(totalIndi)
        }else{
            cell.conHeightLblWaktu.constant = cell.conHeightLblName.constant
            cell.lblWaktu.textColor = UIColor.black
            cell.viewIndicator.isHidden = true
        }
        //-----------------
        
        if let imgUrl = URL(string: data.ava) {
            cell.imgAva.setImage(withUrl: imgUrl)
        }else{
            cell.imgAva.image = UIImage(named: "default-avatar")
        }
        var nameSender = data.name
        if nameSender == "" {
            nameSender = "No Name"
        }
        cell.lblName.text = nameSender
        cell.lblKet.text = data.text
        let sec = Double(data.dateLastChat)
        let strSec = String(format: "%3.1f", sec)
        let lengSec = strSec.characters.count
        var seconds = sec
        if lengSec > 12 {
           seconds = sec / 1000
        }
        let timestampDate = Date(timeIntervalSince1970: TimeInterval(sec))
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let dateData = dateFormatter.string(from: timestampDate)
        let dateNow = dateFormatter.string(from: Date())
        if dateNow == dateData {
            dateFormatter.dateFormat = "HH:mm"
        }else{
            dateFormatter.dateFormat = "dd/MM/YY"
        }
        cell.lblWaktu.isHidden = false
        cell.lblWaktu.text = dateFormatter.string(from: timestampDate)
        cell.lblWaktu.textAlignment = .center
        
        cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
        cell.imgAva.clipsToBounds = true
        //Click untuk Ava
        cell.imgAva.tag = indexPath.row
        let tapPhoto = UITapGestureRecognizer(target: self, action: #selector(ChatListController.fotoTapped(_:)))
        tapPhoto.numberOfTapsRequired = 1
        cell.imgAva.isUserInteractionEnabled = true
        cell.imgAva.addGestureRecognizer(tapPhoto)
        
        return cell
    }
    
    var idFriend = 0
    var nameBarFriend = ""
    var avaFriend = ""
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = dataData[indexPath.row]
        self.idFriend = data.id_sender
        self.nameBarFriend = data.name
        self.avaFriend = data.ava
        
        let from = String(data.id_sender)
        let indi = try! Realm().objects(tb_chat.self).filter("from = %@ OR to = %@", from, from).filter("is_read = %@", 0)
        let totalIndi = indi.count
        if totalIndi > 0 {
            for item in indi {
                try! Realm().write {
                    item.is_read = 1
                }
            }
        }
        
        if UIApplication.shared.applicationIconBadgeNumber >= totalIndi {
            UIApplication.shared.applicationIconBadgeNumber -= totalIndi
        }
        
        self.performSegue(withIdentifier: "showChating", sender: self)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showChating" {
            let chatController = segue.destination as! ChatingController
            let id = String(self.idFriend)
            chatController.titleNav = self.nameBarFriend
            chatController.toId = id
            chatController.idFriend.append(id)
            chatController.nameFriend.append(self.nameBarFriend)
            chatController.avaFriend.append(self.avaFriend)
            chatController.colorFriend.append(UIColor(red: 63, green: 157, blue: 247))
            chatController.paramsKindChat = "personal"
            chatController.tabBarController?.tabBar.isHidden = true
            self.tabBarController?.tabBar.isHidden = true
        }
        if segue.identifier == "showContact" {
            let conn = segue.destination as! ContactController
            conn.tabBarController?.tabBar.isHidden = true
        }
    }
    
    func fotoTapped(_ sender: UITapGestureRecognizer){
        let indexPath = sender.view?.tag
        let data = dataData[indexPath!]
        
        let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showMember") as! PopupMemberController
        self.addChildViewController(Popover)
        Popover.view.frame = self.view.frame
        Popover.idFriend = String(data.id_sender)
        self.view.addSubview(Popover.view)
        Popover.didMove(toParentViewController: self)
    }
    
    // METHODE NAV
    let blue = UIColor(red: 63, green: 157, blue: 247)
    func colorNav(){
        navigationController!.navigationBar.barTintColor = self.blue
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationController!.navigationBar.tintColor = UIColor.white
    }
    
    func rightMenu(){
        let image = UIImage(named: "ico_tab_contact")
        let attach = UIButton()
        attach.setImage(image, for: UIControlState())
        attach.addTarget(self, action: #selector(self.contactTapped), for: UIControlEvents.touchDown)
        attach.frame=CGRect(x: 0, y: 0, width: 25, height: 25)
        let barAttach = UIBarButtonItem(customView: attach)
        self.navigationItem.setRightBarButtonItems([barAttach], animated: true)
    }
    
    func contactTapped(_ sender:UIButton){
        self.performSegue(withIdentifier: "showContact", sender: self)
    }
    
}
