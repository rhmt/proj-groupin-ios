//
//  ChatSendImagePreview.swift
//  Groupin
//
//  Created by Macbook pro on 11/25/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import Firebase

class ChatSendImagePreview: UIViewController, UITextViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var colView: UICollectionView!
    @IBOutlet weak var viewSend: UIView!
    @IBOutlet weak var imgSend: UIImageView!
    @IBOutlet weak var txtInput: UITextView!
    
    @IBOutlet weak var conHeightImgView: NSLayoutConstraint!
    
    // create swipe gesture
    let swipeGestureLeft = UISwipeGestureRecognizer()
    let swipeGestureRight = UISwipeGestureRecognizer()
    
    var fromId = ""
    var toId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Send Images"
        
        imgPicker.delegate = self
        txtInput.delegate = self
        viewSend.backgroundColor = UIColor.clear
        txtInput.backgroundColor = UIColor.clear
        txtInput.textColor = UIColor.gray
        
        let imgData = arrayImgData[0]
        imgView.image = UIImage(data: imgData)
        
        let tapPhoto = UITapGestureRecognizer(target: self, action: #selector(ChatSendImagePreview.sendTapped(_:)))
        tapPhoto.numberOfTapsRequired = 1
        imgSend.isUserInteractionEnabled = true
        imgSend.addGestureRecognizer(tapPhoto)
        imgSend.layer.cornerRadius = 15
        
        // set gesture direction
        self.swipeGestureLeft.direction = UISwipeGestureRecognizerDirection.left
        self.swipeGestureRight.direction = UISwipeGestureRecognizerDirection.right
        
        // add gesture target
        self.swipeGestureLeft.addTarget(self, action: #selector(ChatSendImagePreview.handleSwipeLeft(_:)))
        self.swipeGestureRight.addTarget(self, action: #selector(ChatSendImagePreview.handleSwipeRight(_:)))
        
        // add gesture in to view
        self.view.addGestureRecognizer(self.swipeGestureLeft)
        self.view.addGestureRecognizer(self.swipeGestureRight)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatSendImagePreview.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatSendImagePreview.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        if UIScreen.main.bounds.size.height == 480 {
            self.conHeightImgView.constant -= 100
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    var indexImageForCaption = 0
    var arrayImageForCaption = [""]
    var arrayNameImage = [String]()
    var numberOfImage = 0
    //CollectionView
    func numberOfSectionsInCollectionView(_ collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return numberOfImage + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgSelectedCell", for: indexPath) as! LikeLessThreadCell
        
        if indexPath.row < numberOfImage {
            let imgData = arrayImgData[indexPath.row]
            cell.imgLiker.image = UIImage(data: imgData)
            cell.imgLiker.layer.borderColor = UIColor.lightGray.cgColor
            cell.imgLiker.layer.borderWidth = 0.5
            cell.imgLiker.tag = indexPath.row
        }else{
            cell.imgLiker.image = UIImage(named: "ico_plus_gray_nobg")
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath)
    {
        if indexPath.row < numberOfImage {
            let imgData = arrayImgData[indexPath.row]
            imgView.image = UIImage(data: imgData)
            indexImageForCaption = indexPath.row
            var txtInput = "Write caption here..."
            self.txtInput.textColor = UIColor.gray
            if arrayImageForCaption[indexPath.row] != "" {
                txtInput = arrayImageForCaption[indexPath.row]
                self.txtInput.textColor = UIColor.black
            }
            self.txtInput.text = txtInput
            self.txtInput.resignFirstResponder()
        }else{
            if 10 > numberOfImage {
                self.imgPicker.allowsEditing = false
                self.imgPicker.sourceType = .photoLibrary
                present(imgPicker, animated: true, completion: nil)
            }
        }
    }
    
    //TextView
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtInput.text == "Write caption here..." {
            txtInput.text = nil
            txtInput.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtInput.text!.isEmpty {
            txtInput.text = "Write caption here..."
            txtInput.textColor = UIColor.gray
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        arrayImageForCaption[indexImageForCaption] = self.txtInput.text
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showBackToChating" {
            let conn = segue.destination as! ChatingController
            conn.indicatorFromSendImage = 1
            conn.arraySendImage = self.arrayImgData
            conn.arraySendCaptionImage = self.arrayImageForCaption
            conn.arrayNameImage = self.arrayNameImage
        }
    }
    
    var alertLoading: UIAlertController!
    var i = 0
    func sendTapped(_ sender: UITapGestureRecognizer){
        print("send image")
        print(self.arrayImgData)
        print(self.arrayImageForCaption)
        print(self.arrayNameImage)
        print("------------------")
        self.performSegue(withIdentifier: "showBackToChating", sender: self)
        
//        self.alertLoading = UIAlertController(title: "Sending", message: "Uploading files ... ", preferredStyle: UIAlertControllerStyle.alert)
//        self.present(self.alertLoading, animated: true, completion: nil)
//        
//        let imgData = self.arrayImgData[self.i]
//        let selectedImage = UIImage(data: imgData)
//        let sendCaption = arrayImageForCaption[self.i]
//        self.i += 1
//        uploadToFirebaseStorageUsingImage(selectedImage!, completion: { (imageUrl, metadata) in
//            self.sendMessageWithImageUrl(imageUrl, image: selectedImage!, metadata: metadata, caption: sendCaption) 
//        })
    }
    
    fileprivate func uploadToFirebaseStorageUsingImage(_ image: UIImage, completion: @escaping (_ imageUrl: String, _ metadata: FIRStorageMetadata) -> ()) {
        let imageName = UUID().uuidString
        let ref = FIRStorage.storage().reference().child("message_images").child(imageName)
        
        if let uploadData = UIImageJPEGRepresentation(image, 0.2) {
            let uploadTask = ref.put(uploadData, metadata: nil, completion: { (metadata, error) in
                
                if error != nil {
                    print("Failed to upload image:")
                    return
                }
                
                if let imageUrl = metadata?.downloadURL()?.absoluteString {
                    completion(imageUrl, metadata!)
                }
                
            })
            
            uploadTask.observe(.progress) { (snapshot) in
                
            }
            
            uploadTask.observe(.success) { (snapshot) in
                if self.numberOfImage > self.i {
                    let imgData = self.arrayImgData[self.i]
                    let selectedImage = UIImage(data: imgData)
                    let sendCaption = self.arrayImageForCaption[self.i]
                    self.uploadToFirebaseStorageUsingImage(selectedImage!, completion: { (imageUrl, metadata) in
                        self.sendMessageWithImageUrl(imageUrl, image: selectedImage!, metadata: metadata, caption: sendCaption)
                    })
                    self.i += 1
                }else{
                    self.alertLoading.dismiss(animated: true, completion: nil)
                    self.performSegue(withIdentifier: "showBackToChat", sender: self)
                }
            }
        }
    }
    
    fileprivate func sendMessageWithImageUrl(_ imageUrl: String, image: UIImage, metadata: FIRStorageMetadata, caption: String) {
        let mediaName = Config().nameMediaFile(metadata.name!, idUser: self.fromId)
        let properties: [String: AnyObject] = ["imageUrl": imageUrl as AnyObject, "imageWidth": image.size.width as AnyObject, "imageHeight": image.size.height as AnyObject, "text": caption as AnyObject, "mediaName": mediaName as AnyObject, "mediaType": "Image" as AnyObject, "mediaSize": Int(metadata.size) as AnyObject]
        sendMessageWithProperties(properties)
    }
    
    fileprivate func sendMessageWithProperties(_ properties: [String: AnyObject]) {
        let ref = FIRDatabase.database().reference().child("messages")
        let childRef = ref.childByAutoId()
        let timestamp = NSNumber.init(value: Date().timeIntervalSince1970 * 1000)
        var values: [String: AnyObject] = ["toId": toId as AnyObject, "fromId": fromId as AnyObject, "timestamp": timestamp, "isRead": 0 as AnyObject]
        
        //append properties dictionary onto values somehow??
        //key $0, value $1
        properties.forEach({values[$0] = $1})
        
        childRef.updateChildValues(values) { (error, ref) in
            if error != nil {
                return
            }
            
            self.txtInput.text = nil
            
            let userMessagesRef = FIRDatabase.database().reference().child("user-messages").child(self.fromId).child(self.toId)
            
            let messageId = childRef.key
            userMessagesRef.updateChildValues([messageId: 1])
            
            let recipientUserMessagesRef = FIRDatabase.database().reference().child("user-messages").child(self.toId).child(self.fromId)
            recipientUserMessagesRef.updateChildValues([messageId: 1])
        }
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    var imgPicker = UIImagePickerController()
    var arrayImgData = [Data]()
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var pickedImage: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            pickedImage = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            pickedImage = originalImage
        }
        
        imgView.contentMode = .scaleToFill
        imgView.image = pickedImage
        
        let imageURL = info[UIImagePickerControllerReferenceURL] as! NSURL
        let realName = imageURL.lastPathComponent
        let idName = UUID().uuidString
        let saveName = idName + realName!
        let imagePath = getDocumentsDirectory().appendingPathComponent("/Media/"+saveName)
        let jpegData = UIImageJPEGRepresentation(pickedImage!, 1)
        try? jpegData!.write(to: URL(fileURLWithPath: imagePath), options: [.atomic])
        
        self.arrayNameImage.append(saveName)
        self.numberOfImage += 1
        self.arrayImageForCaption.append("")
        self.indexImageForCaption += 1
        self.arrayImgData.append(jpegData!)
        self.colView.reloadData()
        self.txtInput.text = "Write caption here..."
        self.txtInput.textColor = UIColor.gray
        
        dismiss(animated: true, completion: nil)
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    //==============================================
    
    //Animations Slide
    var totalPage = 0
    func handleSwipeLeft(_ gesture: UISwipeGestureRecognizer){
        let params = self.indexImageForCaption + 1
        self.totalPage = self.numberOfImage
        if params < self.totalPage {
            self.indexImageForCaption += 1
            self.currentPageChange(1)
        }
    }
    func handleSwipeRight(_ gesture: UISwipeGestureRecognizer){
        if self.indexImageForCaption != 0 {
            self.indexImageForCaption -= 1
            self.currentPageChange(2)
        }
    }
    
    func currentPageChange(_ from: Int){
        animated(from)
        
        let currentPage = indexImageForCaption
        let imgData = arrayImgData[currentPage]
        imgView.image = UIImage(data: imgData)
        var txtInput = "Write caption here..."
        self.txtInput.textColor = UIColor.gray
        if arrayImageForCaption[currentPage] != "" {
            txtInput = arrayImageForCaption[currentPage]
            self.txtInput.textColor = UIColor.black
        }
        self.txtInput.text = txtInput
        self.txtInput.resignFirstResponder()
    }
    
    func animated(_ from: Int){
        let posXOrigin = self.imgView.frame.origin.x
        let posYOrigin = self.imgView.frame.origin.y
        var posX = 0
        if from == 1 {
            posX = Int(posXOrigin) + Int(self.imgView.frame.width)
        }else{
            posX = Int(posXOrigin) - Int(self.imgView.frame.width)
        }
//        let posY = Int(posYOrigin)
        let posXPrint = CGFloat(posX)
        self.imgView.layer.position = CGPoint(x: posXPrint, y: posYOrigin)
        UIView.animate(withDuration: 0.4, animations: {
            self.imgView.layer.position = CGPoint(x: posXOrigin, y: posYOrigin)
        })
        print(posXOrigin)
        print(posYOrigin)
        print(posX)
        print(posXPrint)
    }
    
    var activeField: UITextView?
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        activeField = textView
        return true
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        activeField = nil
        return true
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            let possTextField = self.viewSend.frame.origin.y + self.viewSend.frame.height
            let possKeyboard = self.view.frame.height - keyboardSize!.height - self.viewSend.frame.height
            if possTextField > possKeyboard {
                if view.frame.origin.y == 0{
                    self.view.frame.origin.y -= keyboardSize!.height
                }
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}
