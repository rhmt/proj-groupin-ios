//
//  ChatingController.swift
//  Pitung
//
//  Created by Macbook pro on 3/9/17.
//  Copyright © 2017 Macbook pro. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON
import Firebase
import AGEmojiKeyboard
import Photos
import FileBrowser
import CoreLocation
import GooglePlaces
import GooglePlacePicker
import AVKit
import AVFoundation
import MapleBacon
import UICircularProgressRing
import DropDown

class ChatingController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, AGEmojiKeyboardViewDataSource, AGEmojiKeyboardViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate,AVAudioRecorderDelegate, UIDocumentPickerDelegate {
    
    @IBOutlet weak var tbChat: UITableView!
    @IBOutlet weak var viewInput: UIView!
    @IBOutlet weak var viewSeparator: UIView!
    @IBOutlet weak var conHeightViewInput: NSLayoutConstraint!
    @IBOutlet weak var conBottomViewInput: NSLayoutConstraint!
    @IBOutlet weak var imgEmot: UIImageView!
    @IBOutlet weak var txtInput: UITextView!
    @IBOutlet weak var imgCamera: UIImageView!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnVoice: UIButton!
    @IBOutlet weak var lblVoiceDuration: UILabel!
    @IBOutlet weak var lblVoiceSliceToClose: UILabel!
    
    @IBOutlet weak var viewReply: UIView!
    @IBOutlet weak var conHeightViewReply: NSLayoutConstraint!
    @IBOutlet weak var viewSeparatorReply: UIView!
    @IBOutlet weak var viewContainerReply: UIView!
    @IBOutlet weak var viewIndicatorReply: UIView!
    @IBOutlet weak var lblNameReply: UILabel!
    @IBOutlet weak var lblContentReply: UILabel!
    @IBOutlet weak var imgContentReply: UIImageView!
    @IBOutlet weak var imgCloseReply: UIImageView!
    
    var messages = [Chat]()
    var fromId = ""
    var toId = ""
    var titleNav = ""
    //Data user friend
    var idFriend = [String]()
    var nameFriend = [String]()
    var avaFriend = [String]()
    var colorFriend = [UIColor]()
    
    //Self Variable
    var statusEmot = 0
    var minusIndexMessage = 20
    var countMessage = 0
    var lastIndexMessage = 0
    var paramsKindChat = ""
    var selectedImage: UIImage!
    var locationManager:CLLocationManager!
    var currentLat: CLLocationDegrees!
    var currentLong: CLLocationDegrees!
    var latSelected: Float!
    var longSelected: Float!
    var nameLocSelected = ""
    var detailLocSelected = ""
    var player: AVPlayer?
    var updater : CADisplayLink! = nil
    var normalizedTimeAudioSeek:Float! = 0
    var timer: Timer!
    var secon = 0
    var minute = 0
    var mainCell: ChatGamesCell!
    var xPosBtnSend: CGFloat!
    var yPosBtnSend: CGFloat!
    var xPosLblVoiceClose: CGFloat!
    var yPosLblVoiceClose: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Preferences
        //self.tabBarController?.tabBar.isHidden = true
        //self.tabBarController?.tabBar.layer.zPosition = -1
        self.tbChat.delegate = self
        self.tbChat.dataSource = self
        self.txtInput.delegate = self
        self.txtInput.resolveHashTags()
        self.txtInput.textColor = UIColor.gray
        self.lblVoiceSliceToClose.isHidden = true
        self.imgEmot.contentMode = .scaleAspectFit
        self.conHeightViewReply.constant = 0
        self.tbChat.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.tbChat.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
        
        //Load data local
        let sess = try! Realm().objects(session.self).first!
        self.fromId = String(sess.id)
        
        //KeyBoard
        self.setupKeyboardObservers()
        
        //Location
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
        self.locationManager.requestAlwaysAuthorization()
        
        // Background Image
        let backgroundImage = UIImage(named: "bg_chat")
        let imageView = UIImageView(image: backgroundImage)
        imageView.contentMode = .scaleAspectFill//.scaleAspectFit
        self.tbChat.backgroundView = imageView
        self.tbChat.keyboardDismissMode = .interactive
        
        //Add tapped on button
        //Emot
        let tapEmot = UITapGestureRecognizer(target: self, action: #selector(self.handleEmotTap(_:)))
        tapEmot.numberOfTapsRequired = 1
        imgEmot.isUserInteractionEnabled = true
        imgEmot.addGestureRecognizer(tapEmot)
        //Camera
        let tapCamera = UITapGestureRecognizer(target: self, action: #selector(self.handleCameraTap(_:)))
        tapCamera.numberOfTapsRequired = 1
        imgCamera.isUserInteractionEnabled = true
        imgCamera.addGestureRecognizer(tapCamera)
        
        self.navTitleDefault()
        //Attachment
        let image = UIImage(named: "ico_attachment")
        let attach = UIButton()
        attach.setImage(image, for: UIControlState())
        attach.addTarget(self, action: #selector(self.attachTapped), for: UIControlEvents.touchDown)
        attach.frame=CGRect(x: 0, y: 0, width: 26, height: 15)
        let barAttach = UIBarButtonItem(customView: attach)
        self.navigationItem.setRightBarButtonItems([barAttach], animated: true)
        //Record Voice
        let slideToClose = UIPanGestureRecognizer(target: self, action: #selector(self.slideCloseRecord(_:)))
        self.btnVoice.isUserInteractionEnabled = true
        self.btnVoice.addGestureRecognizer(slideToClose)
        //Cancel Reply
        let tapCancelReply = UITapGestureRecognizer(target: self, action: #selector(self.cancelReply(_:)))
        tapCancelReply.numberOfTapsRequired = 1
        self.imgCloseReply.isUserInteractionEnabled = true
        self.imgCloseReply.addGestureRecognizer(tapCancelReply)
        
        // get poss send voice
        self.xPosBtnSend = self.view.frame.height - self.btnSend.frame.height
        self.yPosBtnSend = self.view.frame.width - self.btnSend.frame.width
        self.xPosLblVoiceClose = self.lblVoiceSliceToClose.center.x
        self.yPosLblVoiceClose = self.lblVoiceSliceToClose.center.y
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // get poss send voice
        self.xPosBtnSend = self.btnSend.center.x
        self.yPosBtnSend = self.btnSend.center.y
        self.xPosLblVoiceClose = self.lblVoiceSliceToClose.center.x
        self.yPosLblVoiceClose = self.lblVoiceSliceToClose.center.y
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //self.txtInput.addObserver(self, forKeyPath: "contentSize", options: (connect as? NSKeyValueObservingOptionNew), nil)
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        
        //Get Chat
        print("get chat")
        self.observeMessages()
        self.scrollToLast()
        self.checkingServer()
        self.checkingTyping()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        //self.txtInput.removeObserver(self, forKeyPath: "contentSize")
    }
    
//    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//        let tv: UITextView? = object as? UITextView
//        var topCorrect: CGFloat? = ((tv?.bounds.size.height)! - (tv?.contentSize.height)! * (tv?.zoomScale)!) / 2.0
//        topCorrect = (topCorrect! < CGFloat(0.0) ? 0.0 : topCorrect)
//        tv?.contentOffset = CGPoint()
//        tv?.contentOffset.x = 0
//        tv?.contentOffset.y = -topCorrect!
//    }
    
    func navTitleDefault(){
        //Nav
        let button =  UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 100, height: 40) as CGRect
        button.setTitle(self.titleNav, for: UIControlState())
        button.addTarget(self, action: #selector(self.profileTapped), for: UIControlEvents.touchUpInside)
        self.navigationItem.titleView = button
    }
    
    var idFriendMentionSelected = 0
    func profileTapped(_ sender: UITapGestureRecognizer){
        if self.paramsKindChat == "personal" {
            self.idFriendMentionSelected = Int(self.toId)!
            self.performSegue(withIdentifier: "showFriendProfile", sender: self)
        }else{
            self.performSegue(withIdentifier: "showAboutGroup", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showFriendProfile" {
            let conn = segue.destination as! FriendProfileController
            conn.tabBarController?.tabBar.isHidden = false
            conn.idFriend = String(idFriendMentionSelected)
        }
        if segue.identifier == "showSendImage" {
            let conn = segue.destination as! ChatSendImagePreview
            let imgData = UIImageJPEGRepresentation(self.selectedImage, 1)
            conn.arrayImgData.append(imgData!)
            conn.arrayNameImage = self.arrayNameImage
            conn.numberOfImage += 1
            conn.fromId = self.fromId
            conn.toId = self.toId
        }
        if segue.identifier == "showLocation" {
            let conn = segue.destination as! EventViewMapsController
            conn.lat = Double(self.latSelected)
            conn.long = Double(self.longSelected)
            conn.chat = 1
            conn.chatLoc = self.nameLocSelected
            conn.chatLocDetail = self.detailLocSelected
            conn.navigationItem.title = "View Map"
        }
    }
    
    //Handle table
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatCell", for: indexPath) as! ChatGamesCell
        self.mainCell = cell
        
        let data = self.messages[indexPath.row]
        var width: CGFloat = 200
        
        //Awalnya semua view (incoming dan outgoing) di hidden
        cell.viewIndicatorSelectedCell.isHidden = true
        cell.viewIndicatorSelectedCell.backgroundColor = UIColor.clear
        cell.viewIndicatorSelectedCell.alpha = 1.0
        cell.viewOutgoingMsg.isHidden = true
        cell.viewOutgoingMedia.isHidden = true
        cell.imgRecoredOutgoingMedia.isHidden = true
        cell.viewOutgoingVoice.isHidden = true
        cell.lblOutgoingDurationVoice.isHidden = true
        cell.viewOutgoingReply.isHidden = true
        cell.progBarOutgoing.isHidden = true
        cell.btnOutgoingShare.isHidden = true
        cell.cancelProgBarOutgoing.isHidden = true
        cell.conHeightViewOutgoingReply.constant = 0
        cell.progBarOutgoing.tag = indexPath.row
        cell.conHeightLblOutgoingMedia.constant = Config().estimateFrameForText(data.text!, width: 200).height + 6
        cell.lblOutgoingMedia.textColor = UIColor.black
        //------------------------------------------
        cell.viewIncomingMsg.isHidden = true
        cell.viewIncomingMedia.isHidden = true
        cell.imgRecoredIncomingMedia.isHidden = true
        cell.viewIncomingVoice.isHidden = true
        cell.lblIncomingDurationVoice.isHidden = true
        cell.viewIncomingReply.isHidden = true
        cell.progBarIncoming.isHidden = true
        cell.btnIncomingShare.isHidden = true
        cell.cancelProgBarIncoming.isHidden = true
        cell.conHeightViewIncomingReply.constant = 0
        cell.progBarIncoming.tag = indexPath.row
        cell.conHeightLblIncomingMedia.constant = Config().estimateFrameForText(data.text!, width: 200).height + 6
        cell.lblIncomingMedia.textColor = UIColor.black
        
        //Time
        var timeShow = ""
        let seconds = Double(data.timestamp!)
        let strSeconds = String(format: "%3.1f", seconds)
        let lengSeconds = strSeconds.characters.count
        var sec: Double! = seconds
        if lengSeconds > 12 {
            sec = seconds / 1000
        }
        let timestampDate = Date(timeIntervalSince1970: TimeInterval(sec))
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        dateFormatter.dateStyle = .none
        dateFormatter.dateFormat = "HH:mm"
        timeShow = dateFormatter.string(from: timestampDate)
        
        //Date
        dateFormatter.dateFormat = "MMMM dd, YYYY"
        var dateShow = dateFormatter.string(from: timestampDate)
        
        cell.viewDate.isHidden = false
        cell.viewDate.layer.cornerRadius = 5
        cell.conHeightDate.constant = 21
        if indexPath.row > 0 {
            let dataPre = self.messages[indexPath.row - 1]
            let secPre = Double(dataPre.timestamp!)
            let strSeconds = String(format: "%3.1f", secPre)
            let lengSeconds = strSeconds.characters.count
            var dateSecPre = secPre
            if lengSeconds > 12 {
                dateSecPre = secPre / 1000
            }
            let timestampPre = Date(timeIntervalSince1970: TimeInterval(dateSecPre))
            
            let datePre = dateFormatter.string(from: timestampPre)
            
            if dateShow == datePre {
                cell.viewDate.isHidden = true
                cell.conHeightDate.constant = 0
            }else{
                cell.viewDate.isHidden = false
                cell.conHeightDate.constant = 21
                let widhtDate = Config().estimateFrameForText(dateShow, width: 500).width
                print(widhtDate)
                cell.conWidthDate.constant = widhtDate + 24
            }
        }
        //Cek today
        let today = dateFormatter.string(from: Date())
        if dateShow == today {
            dateShow = "Today"
        }
        cell.lblDate.text = dateShow
        let widhtDate = Config().estimateFrameForText(dateShow, width: 500).width
        cell.conWidthDate.constant = widhtDate + 24
        
        //Incoming or Outgoing
        let tapMedia = UITapGestureRecognizer(target: self, action: #selector(self.mediaTapped(_:)))
        tapMedia.numberOfTapsRequired = 1
        if data.fromId == self.fromId {
            //Outgoing
            cell.viewOutgoingMsg.isHidden = false
            cell.lblOutgoingText.text = data.text
            cell.lblOutgoingText.delegate = self
            cell.lblOutgoingText.resolveHashTags()
            cell.lblOutgoingTime.text = timeShow
            cell.imgOutgoingMedia.isUserInteractionEnabled = true
            cell.imgOutgoingMedia.addGestureRecognizer(tapMedia)
        }else{
            //Incoming
            cell.viewIncomingMsg.isHidden = false
            cell.lblIncomingText.text = data.text
            cell.lblIncomingText.delegate = self
            cell.lblIncomingText.resolveHashTags()
            cell.lblIncomingTime.text = timeShow
            cell.imgIncomingMedia.isUserInteractionEnabled = true
            cell.imgIncomingMedia.addGestureRecognizer(tapMedia)
            
            
            let idMember = self.idFriend.index(of: data.fromId!)
            let nameMember = self.nameFriend[idMember!]
            cell.lblIncomingName.text = nameMember
            if self.avaFriend[idMember!] != "" {
                let imgUrl = URL(string: self.avaFriend[idMember!])!
                cell.imgIncomingAva.setImage(withUrl: imgUrl)
            }else{
                cell.imgIncomingAva.image = UIImage(named: "dafault-ava-group")
            }
            cell.lblIncomingName.textColor = self.colorFriend[idMember!]
            
        }
        
        //Width Text
        if data.text != "" {
            width = Config().estimateFrameForText(data.text!, width: 200).width + 32
            if width < 70 {
                width = 100
            }
            if width > 200 {
                width = 200
            }
        }
        
        //Media
        if data.imageUrl != "" {
            width = 200
            cell.viewOutgoingMedia.isHidden = false
            cell.viewIncomingMedia.isHidden = false
            let nameImage = data.mediaName
            let nameImageLocal = nameImage!.replacingOccurrences(of: " ", with: "")
            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
            let getImagePath = paths.appendingPathComponent("/Media/\(nameImageLocal).jpg")
            let imgImage = UIImage(contentsOfFile: getImagePath)
            if imgImage != nil {
                cell.imgOutgoingMedia.image = imgImage
                cell.imgIncomingMedia.image = imgImage
            }else{
                cell.imgOutgoingMedia.image = UIImage(named: "dafault-ava-group")
                cell.imgIncomingMedia.image = UIImage(named: "dafault-ava-group")
            }
            //Kalau ada caption
            if data.text != "" {
                cell.lblOutgoingMedia.text = data.text
                cell.conHeightLblOutgoingMedia.constant = Config().estimateFrameForText(data.text!, width: 200).height + 6
                //-------------------------
                cell.lblIncomingMedia.text = data.text
                cell.conHeightLblIncomingMedia.constant = Config().estimateFrameForText(data.text!, width: 200).height + 6
            }else{
                cell.lblOutgoingMedia.text = ""
                cell.conHeightLblOutgoingMedia.constant = 4
                //-------------------------
                cell.lblIncomingMedia.text = ""
                cell.conHeightLblIncomingMedia.constant = 4
            }
            //Jika video
            if data.mediaType == "Video" {
                if data.fromId == self.fromId {
                    if let imgUrl = URL(string: data.imageUrl!) {
                        cell.imgOutgoingMedia.setImage(withUrl: imgUrl)
                    }
                    cell.conHeightLblOutgoingMedia.constant = 26
                    cell.lblOutgoingMedia.text = data.mediaName
                    cell.conHeightLblOutgoingMedia.constant = Config().estimateFrameForText(data.mediaName!, width: 200).height + 6
                    cell.lblOutgoingMedia.textColor = UIColor(red: 63, green: 157, blue: 247)
                    cell.imgRecoredOutgoingMedia.isHidden = false
                }else{
                    if let imgUrl = URL(string: data.imageUrl!) {
                        cell.imgIncomingMedia.setImage(withUrl: imgUrl)
                    }
                    
                    cell.conHeightLblIncomingMedia.constant = 26
                    cell.lblIncomingMedia.text = data.mediaName
                    cell.conHeightLblIncomingMedia.constant = Config().estimateFrameForText(data.mediaName!, width: 200).height + 6
                    cell.lblIncomingMedia.textColor = UIColor(red: 63, green: 157, blue: 247)
                    cell.imgRecoredIncomingMedia.isHidden = false
                }
            }
        }
        if data.audioUrl != "" {
            width = 150
            
            if data.fromId == self.fromId {
                cell.viewOutgoingMedia.isHidden = false
                cell.imgOutgoingMedia.image = UIImage(named: "ico_media_music")
                cell.lblOutgoingMedia.text = data.mediaName
                cell.conHeightLblOutgoingMedia.constant = Config().estimateFrameForText(data.mediaName!, width: 150).height + 6
                cell.lblOutgoingMedia.textColor = UIColor(red: 63, green: 157, blue: 247)
            }else{
                cell.viewIncomingMedia.isHidden = false
                cell.viewIncomingMedia.isHidden = false
                cell.imgIncomingMedia.image = UIImage(named: "ico_media_music")
                cell.lblIncomingMedia.text = data.mediaName
                cell.conHeightLblIncomingMedia.constant = Config().estimateFrameForText(data.mediaName!, width: 150).height + 6
                cell.lblIncomingMedia.textColor = UIColor(red: 63, green: 157, blue: 247)
            }
            
            //If Voice
            if data.mediaType == "Voice"{
                width = 200
                //Gesture
                let tapPlay = UITapGestureRecognizer(target: self, action: #selector(self.playVoiceTapped(_:)))
                tapPlay.numberOfTapsRequired = 1
                
                let masterPath = self.getDocumentsDirectory().appending("/Media")
                let audioNameLocal = data.mediaName
                let pathString = "file://" + masterPath + "/" + audioNameLocal!
                
                let localPlayer = AVPlayer(url: URL(string: pathString)!)
                self.player = AVPlayer(url: URL(string: pathString)!)
                
                let duration = localPlayer.currentItem?.asset.duration
                let sec = CMTimeGetSeconds(duration!)
                
                //Total waktu
                let devide = Int(sec) / 60
                let showMin = devide
                let showSec = Int(sec) - (devide * 60)
                var nulSec = ""
                var nulMin = ""
                if showSec < 10 {
                    nulSec = "0"
                }
                if showMin < 10 {
                    nulMin = "0"
                }
                
                let showTime = nulMin + String(showMin) + ":" + nulSec + String(showSec)
                
                if data.fromId == self.fromId {
                    cell.viewOutgoingMedia.isHidden = true
                    cell.viewOutgoingVoice.isHidden = false
                    cell.sldrOutgoingVoice.value = 0
                    cell.lblOutgoingDurationVoice.isHidden = false
                    cell.imgOutgoingPlayVoice.isUserInteractionEnabled = true
                    cell.imgOutgoingPlayVoice.addGestureRecognizer(tapPlay)
                    cell.lblOutgoingDurationVoice.text = showTime
                    cell.sldrOutgoingVoice.maximumValue = Float(sec)
                    cell.sldrOutgoingVoice.isContinuous = true
                    cell.sldrOutgoingVoice.addTarget(self, action: #selector(self.seekAction(_:)), for: .valueChanged)
                }else{
                    cell.viewIncomingMedia.isHidden = true
                    cell.viewIncomingVoice.isHidden = false
                    cell.sldrIncomingVoice.value = 0
                    cell.lblIncomingDurationVoice.isHidden = false
                    cell.imgIncomingPlayVoice.isUserInteractionEnabled = true
                    cell.imgIncomingPlayVoice.addGestureRecognizer(tapPlay)
                    cell.lblIncomingDurationVoice.text = showTime
                    cell.sldrIncomingVoice.maximumValue = Float(sec)
                    cell.sldrIncomingVoice.isContinuous = true
                    cell.sldrIncomingVoice.addTarget(self, action: #selector(self.seekAction(_:)), for: .valueChanged)
                }
            }
        }
        if data.fileUrl != "" {
            width = 150
            
            if data.fromId == self.fromId {
                cell.viewOutgoingMedia.isHidden = false
                cell.imgOutgoingMedia.image = UIImage(named: "ico_media_file")
                cell.lblOutgoingMedia.text = data.mediaName
                cell.conHeightLblOutgoingMedia.constant = Config().estimateFrameForText(data.mediaName!, width: 150).height + 6
                cell.lblOutgoingMedia.textColor = UIColor(red: 63, green: 157, blue: 247)
            }else{
                cell.viewIncomingMedia.isHidden = false
                cell.imgIncomingMedia.image = UIImage(named: "ico_media_file")
                cell.lblIncomingMedia.text = data.mediaName
                cell.conHeightLblIncomingMedia.constant = Config().estimateFrameForText(data.mediaName!, width: 150).height + 6
                cell.lblIncomingMedia.textColor = UIColor(red: 63, green: 157, blue: 247)
            }
        }
        if data.latitude != 0 {
            //Layout
            width = 200
            cell.viewOutgoingMedia.isHidden = false
            cell.viewIncomingMedia.isHidden = false
            
            //Data
            let staticMapUrl: String = "http://maps.google.com/maps/api/staticmap?markers=color:red|\(data.latitude!),\(data.longitude!)&\("zoom=17&size=400x400")&sensor=true"
            let mapUrl: URL = URL(string: staticMapUrl.addingPercentEscapes(using: String.Encoding.utf8)!)!
            if let imgData = try? Data(contentsOf: mapUrl) {
                let image: UIImage = UIImage(data: imgData)!
                cell.imgOutgoingMedia.image = image
                cell.imgIncomingMedia.image = image
            }else{
                cell.imgOutgoingMedia.image = UIImage(named: "ico_chat_location_no_connect")
                cell.imgIncomingMedia.image = UIImage(named: "ico_chat_location_no_connect")
            }
            let address = data.nameAdrress! + "\n" + data.detailAdrress!
            let lengNameAdrress = data.nameAdrress!.characters.count
            let lengDetailAdrress = data.detailAdrress!.characters.count
            let myMutableString = NSMutableAttributedString(string: address)
            myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 63, green: 157, blue: 247), range: NSRange(location:0,length:lengNameAdrress))
            myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location:lengNameAdrress,length:lengDetailAdrress))
            cell.lblOutgoingMedia.attributedText = myMutableString
            cell.lblIncomingMedia.attributedText = myMutableString
            if Config().estimateFrameForText(address, width: 200).height > 15 {
                cell.conHeightLblOutgoingMedia.constant = 60
                cell.conHeightLblIncomingMedia.constant = 60
            }else{
                cell.conHeightLblOutgoingMedia.constant = 46
                cell.conHeightLblIncomingMedia.constant = 46
            }
        }
        
        //Ada Reply
        if data.keyReply != "" {
            //Text
            if let datas = try! Realm().objects(tb_chat.self).filter("id_chat = %@", data.keyReply!).first {
                cell.viewIncomingReply.isHidden = false
                cell.viewOutgoingReply.isHidden = false
                
                var nameReply = ""
                let idSelected = datas.from
                if idSelected != self.fromId {
                    let id = self.idFriend.index(of: idSelected)
                    nameReply = self.nameFriend[id!]
                }else{
                    nameReply = "You"
                }
                cell.lblIncomingNameReply.text = nameReply
                cell.lblOutgoingNameReply.text = nameReply
                
                let heightReply = Config().estimateFrameForText(datas.text, width: 200).height + 38
                cell.conHeightViewIncomingReply.constant = heightReply
                cell.conHeightViewOutgoingReply.constant = heightReply
                
                let widthText = Config().estimateFrameForText(data.text!, width: 200).width
                let widthReply = Config().estimateFrameForText(datas.text, width: 200).width
                let widthNameReply = Config().estimateFrameForText(nameReply, width: 200).width
                
                if widthReply > widthText {
                    width = widthReply + 42
                }else{
                    width = widthText + 42
                }
                if widthNameReply > widthReply {
                    width = widthNameReply + 42
                }
                
                let type = datas.mediaType
                var content  = ""
                // if type == "File" {
                content = datas.mediaName
                cell.imgIncomingContentReply.image = UIImage(named: "ico_media_file")
                cell.imgOutgoingContentReply.image = UIImage(named: "ico_media_file")
                width += Config().estimateFrameForText(datas.mediaName, width: 200).width + 50
                //}
                if type == "" || type == "Reply" {
                    content = datas.text
                    cell.imgIncomingContentReply.image = nil
                    cell.imgOutgoingContentReply.image = nil
                }
                if type == "Image" {
                    if datas.text != "" {
                        content = datas.text
                    }else{
                        content = "Photo"
                    }
                    if let imgUrl = URL(string: datas.image_url) {
                        cell.imgIncomingContentReply.setImage(withUrl: imgUrl)
                        cell.imgOutgoingContentReply.setImage(withUrl: imgUrl)
                    }else{
                        cell.imgIncomingContentReply.image = UIImage(named: "default-avatar")
                        cell.imgOutgoingContentReply.image = UIImage(named: "default-avatar")
                    }
                    width += 50
                }
                if type == "Video" {
                    content = datas.mediaName
                    if let imgUrl = URL(string: datas.image_url) {
                        cell.imgIncomingContentReply.setImage(withUrl: imgUrl)
                        cell.imgOutgoingContentReply.setImage(withUrl: imgUrl)
                    }else{
                        cell.imgIncomingContentReply.image = UIImage(named: "default-avatar")
                        cell.imgOutgoingContentReply.image = UIImage(named: "default-avatar")
                    }
                    width += Config().estimateFrameForText(datas.mediaName, width: 200).width + 50
                }
                if type == "Audio"{
                    content = datas.mediaName
                    cell.imgIncomingContentReply.image = UIImage(named: "ico_media_music")
                    cell.imgOutgoingContentReply.image = UIImage(named: "ico_media_music")
                    width += Config().estimateFrameForText(datas.mediaName, width: 200).width + 50
                }
                if type == "Voice" {
                    content = "Voice Message"
                    cell.imgIncomingContentReply.image = UIImage(named: "ico_record")
                    cell.imgOutgoingContentReply.image = UIImage(named: "ico_record")
                    width += Config().estimateFrameForText(content, width: 200).width + 50
                }
                if type == "Location" {
                    if datas.nameAdrress != "" {
                        content = datas.nameAdrress
                        width += Config().estimateFrameForText(datas.nameAdrress, width: 200).width
                    }else{
                        content = "Location"
                    }
                    cell.imgIncomingContentReply.image = UIImage(named: "ico_chat_location")
                    cell.imgOutgoingContentReply.image = UIImage(named: "ico_chat_location")
                    width += 50
                }
                
                cell.lblIncomingContentReply.text = content
                cell.lblOutgoingContentReply.text = content
                
                if width > 200 {
                    width = 200
                }
                
                let tapReply = UITapGestureRecognizer(target: self, action: #selector(self.tapOnReplyView(_:)))
                tapReply.numberOfTapsRequired = 1
                if data.fromId == self.fromId {
                    cell.viewOutgoingReply.isUserInteractionEnabled = true
                    cell.viewOutgoingReply.addGestureRecognizer(tapReply)
                }else{
                    cell.viewIncomingReply.isUserInteractionEnabled = true
                    cell.viewIncomingReply.addGestureRecognizer(tapReply)
                }
            }
        }
        
        //Loading Upload or download
        //kesinii
        if data.mediaType == "Progress" {
            print("add progress download gan")
            if data.fromId == self.fromId {
                //self.circularProgressView.append(cell.progBarOutgoing)
                self.circularProgressView = cell.progBarOutgoing
                cell.progBarOutgoing.isHidden = false
                cell.cancelProgBarOutgoing.isHidden = false
            }else{
                self.circularProgressView = cell.progBarIncoming
                cell.progBarIncoming.isHidden = false
                cell.cancelProgBarIncoming.isHidden = false
            }
            width = 150
        }
        
        //Share
        if data.mediaType != "" && data.mediaType != "Voice" && data.mediaType != "Location" && data.mediaType != "Reply" {
            if data.fromId == self.fromId {
                cell.btnOutgoingShare.isHidden = false
            }else{
                cell.btnIncomingShare.isHidden = false
            }
        }
        
        //Long Press
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressCell(_:)))
        longPressRecognizer.minimumPressDuration = 1.00
        cell.addGestureRecognizer(longPressRecognizer)
        
        //Design dan Preference
        cell.backgroundColor = UIColor.clear
        cell.viewIncomingMsg.backgroundColor = UIColor.clear
        cell.viewOutgoingMsg.backgroundColor = UIColor.clear
        cell.conIncomingWidht.constant = width
        cell.conOutgoingWidth.constant = width
        cell.viewOutgoingContainer.layer.cornerRadius = 5
        cell.viewIncomingContainer.layer.cornerRadius = 5
        cell.viewOutgoingContainer.layer.shadowColor = UIColor.black.cgColor
        cell.viewIncomingContainer.layer.shadowColor = UIColor.black.cgColor
        cell.viewOutgoingContainer.layer.shadowRadius = 5
        cell.viewIncomingContainer.layer.shadowRadius = 5
        cell.viewOutgoingContainer.layer.shadowOffset = CGSize.zero
        cell.viewIncomingContainer.layer.shadowOffset = CGSize.zero
        cell.viewOutgoingContainer.layer.shadowOpacity = 0.3
        cell.viewIncomingContainer.layer.shadowOpacity = 0.3
        cell.viewOutgoingMedia.clipsToBounds = true
        cell.viewIncomingMedia.clipsToBounds = true
        cell.viewOutgoingVoice.clipsToBounds = true
        cell.viewIncomingVoice.clipsToBounds = true
        cell.imgIncomingAva.clipsToBounds = true
        cell.imgIncomingAva.layer.cornerRadius = cell.imgIncomingAva.frame.width / 2
        cell.lblIncomingText.isScrollEnabled = false
        cell.lblOutgoingText.isScrollEnabled = false
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 80
        let message = messages[indexPath.item]
        
        if message.text != "" {
            let text = message.text
            height = Config().estimateFrameForText(text!, width: 200).height + 50
            if height > 70 {
                height += 8
            }
        }
        if message.imageUrl != "" {
            //image dengan size
            if message.imageWidth != 0 && message.imageHeight != 0 {
                let imageWidth = message.imageWidth?.floatValue
                let imageHeight = message.imageHeight?.floatValue
                height = CGFloat(imageHeight! / imageWidth! * 200) + 24
                //image with caption
                if message.text != "" {
                    let text = message.text
                    let heightPlus = Config().estimateFrameForText(text!, width: 200).height + 50
                    height = CGFloat((imageHeight! / imageWidth! * 200) + Float(heightPlus)) + 24
                }
            //without size
            }else{
                height = 224
            }
            //Video
            if message.videoUrl != "" {
                height = 200
                height += Config().estimateFrameForText(message.mediaName!, width: 200).height
            }
            //Kalau ada caption
            if message.text != "" {
                height += Config().estimateFrameForText(message.text!, width: 200).height
            }
        }
        if message.audioUrl != "" {
            if message.mediaType == "Voice" {
                height = 84
            }else{
                height = 150
                height += Config().estimateFrameForText(message.mediaName!, width: 150).height
            }
        }
        if message.fileUrl != "" {
            height = 150
            height += Config().estimateFrameForText(message.mediaName!, width: 150).height
        }
        if message.latitude != 0 {
            height = 215
            let text = message.nameAdrress! + "\n" + message.detailAdrress!
            if message.nameAdrress != "" {
                height += 28
            }else{
                height += 4
            }
            
            if Config().estimateFrameForText(text, width: 200).height > 15 {
                height += 40
            }else{
                height += 25
            }
        }
        if message.keyReply != "" {
            if let data = try! Realm().objects(tb_chat.self).filter("id_chat = %@", message.keyReply!).first {
                height += Config().estimateFrameForText(data.text, width: 200).height + 8
                height += Config().estimateFrameForText(message.text!, width: 200).height  + 16
            }
        }
        
        if indexPath.row > 0 {
            let secons = Double(message.timestamp!)
            let timestamp = Date(timeIntervalSince1970: TimeInterval(secons))
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, YYYY"
            let dateShow = dateFormatter.string(from: timestamp)
            
            let dataPre = self.messages[indexPath.row - 1]
            let dateSecPre = Double(dataPre.timestamp!)
            let timestampPre = Date(timeIntervalSince1970: TimeInterval(dateSecPre))
            let datePre = dateFormatter.string(from: timestampPre)
            
            if dateShow != datePre {
                height += 21
            }
        }else{
            height += 21
        }
        
        if message.mediaType == "Progress" {
            height = 150
        }
        
        if message.fromId == self.fromId{
            return height
        }else{
            height += 10
            return height
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY: CGFloat = scrollView.contentOffset.y
        if (offsetY < 10){
            if self.countMessage > self.minusIndexMessage {
                let lastIndex = self.lastIndexMessage
                self.minusIndexMessage += 20
                self.observeMessages()
                
                DispatchQueue.main.async(execute: {
                    if self.messages.count > 0 {
                        self.tbChat.reloadData()
                        let indexPath = IndexPath(item: lastIndex, section: 0)
                        self.tbChat.scrollToRow(at: indexPath, at: .top, animated: false)
                    }
                })
            }
        }
    }
    
    //Handle get chat
    func observeMessages(){
        print("observe Messages")
        var models = [tb_chat]()
        if self.paramsKindChat == "personal" {
            let data = try! Realm().objects(tb_chat.self).filter("from = %@ AND to = %@ OR from = %@ AND to = %@",self.fromId, self.toId, self.toId, self.fromId)//.sorted(byKeyPath: "time", ascending: true)
            for item in data {
                models.append(item)
            }
        }
        if self.paramsKindChat == "group"{
            let data = try! Realm().objects(tb_chat.self).filter("to = %@",self.toId)//.sorted(byKeyPath: "time", ascending: true)
            for item in data {
                models.append(item)
            }
        }
        if models.count > 0 {
            self.countMessage = models.count - 1
            
            var lastModels = self.countMessage - self.minusIndexMessage
            if lastModels < 0 {
                lastModels = 0
            }
            self.lastIndexMessage = lastModels
            self.messages.removeAll()
            for i in lastModels...self.countMessage {
                self.messages.append(Chat(dictionary: models[i]))
            }
        }
    }
    
    func scrollToLast(){
        //Scroll to last chat
        DispatchQueue.main.async(execute: {
            if self.messages.count > 0 {
                self.tbChat.reloadData()
                let indexPath = IndexPath(item: self.messages.count - 1, section: 0)
                self.tbChat.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }
        })
    }
    
    func checkingServer(){
        print("checkingServer")
        if paramsKindChat == "personal" {
            let messagesRef = FIRDatabase.database().reference().child("user-messages").child(fromId).child(toId)
            messagesRef.observe(.childAdded) { (snapshot: FIRDataSnapshot!) in
                
                let status = snapshot.value! as! Int
                let keyMessage = snapshot.key
                if status == 1 {
                    //Update di firebase
                    let userMessagesRef = FIRDatabase.database().reference().child("user-messages").child(self.fromId).child(self.toId)
                    userMessagesRef.updateChildValues([keyMessage: 2])
                    
                    self.downloadData(keyMessage)
                }
            }
        }else if paramsKindChat == "group"{
            let messagesRef = FIRDatabase.database().reference().child("messages").queryOrdered(byChild: "toId").queryEqual(toValue: self.toId)
            messagesRef.observe(.childAdded) { (snapshot: FIRDataSnapshot!) in
                print("1")
                self.downloadData(snapshot.key)
            }
        }
    }
    
    func downloadData(_ key: String) {
        print("2")
        let messagesRef = FIRDatabase.database().reference().child("messages").queryOrderedByKey().queryEqual(toValue: key)
        messagesRef.observe(.childAdded) { (snapshot: FIRDataSnapshot!) in
            guard let dictionary = snapshot.value as? [String: AnyObject] else {
                return
            }
            let idChat = snapshot.key
            self.migrateToLocal(dictionary, idChat: idChat, sendStat: 0)
        }
        
    }
    
    func migrateToLocal(_ chat: [String: AnyObject], idChat: String, sendStat: Int){
        print("3")
        //Data list chat personal
        if paramsKindChat == "personal" {
            if let modelUpdate = try! Realm().objects(listDataChatPersonal.self).filter("id_sender = %@", Int(self.toId)!).first {
                try! Realm().write({
                    modelUpdate.name = self.nameFriend[0]
                    modelUpdate.ava = self.avaFriend[0]
                    if let text = chat["text"] {modelUpdate.text = text as! String}
                    if let type = chat["mediaType"] {
                        if type as! String == "Image" {modelUpdate.text = "Photo"}
                        if type as! String == "Audio" {modelUpdate.text = "Audio"}
                        if type as! String == "Video" {modelUpdate.text = "Video"}
                        if type as! String == "File" {modelUpdate.text = "File"}
                        if type as! String == "Location" {modelUpdate.text = "Location"}
                        if type as! String == "Voice" {modelUpdate.text = "Voice"}
                    }
                    modelUpdate.dateLastChat = Int(Date().timeIntervalSince1970)
                })
            }else{
                let modelChat = listDataChatPersonal()
                modelChat.id_sender = Int(self.toId)!
                modelChat.name = self.nameFriend[0]
                modelChat.ava = self.avaFriend[0]
                if let text = chat["text"] {modelChat.text = text as! String}
                if let type = chat["mediaType"] {
                    if type as! String == "Photo" {modelChat.text = "Photo"}
                    if type as! String == "Audio" {modelChat.text = "Audio"}
                    if type as! String == "File" {modelChat.text = "File"}
                    if type as! String == "Location" {modelChat.text = "Location"}
                    if type as! String == "Voice" {modelChat.text = "Voice"}
                }
                modelChat.dateLastChat = Int(Date().timeIntervalSince1970)
                DBHelper.insert(modelChat)
            }
        }
        //-----------------------
        var fromIdForMedia = ""
        let model = tb_chat()
        model.id_chat = idChat
        if let fromId = chat["fromId"] {model.from = fromId as! String; fromIdForMedia = fromId as! String}
        if let toId = chat["toId"] {model.to = toId as! String}
        if let text = chat["text"] {model.text = text as! String}
        if let time = chat["timestamp"]!.doubleValue {model.time = time}
        if let imageUrl = chat["imageUrl"] {model.image_url = imageUrl as! String}
        if let imageWidth = chat["imageWidth"] {model.image_width = Int(imageWidth as! NSNumber)}
        if let imageHeight = chat["imageHeight"] {model.image_height = Int(imageHeight as! NSNumber)}
        if let videoUrl = chat["videoUrl"] {model.video_url = videoUrl as! String}
        if let audioUrl = chat["audioUrl"] {model.audio_url = audioUrl as! String}
        if let fileUrl = chat["fileUrl"] {model.file_url = fileUrl as! String}
        if let mediaName = chat["mediaName"] {model.mediaName = mediaName as! String}
        if let mediaSize = chat["mediaSize"] {model.mediaSize = mediaSize as! Int}
        if let mediaType = chat["mediaType"] {model.mediaType = mediaType as! String}
        if let lat = chat["latitude"] {model.latitude = Float(lat as! NSNumber)}
        if let long = chat["longitude"] {model.longitude = Float(long as! NSNumber)}
        if let address = chat["nameAddress"] {model.nameAdrress = address as! String}
        if let address = chat["detailAddress"] {model.detailAdrress = address as! String}
        if let keyReply = chat["keyReply"] {model.keyReply = keyReply as! String}
        model.is_read = 1
        
        let primary = try! Realm().object(ofType: tb_chat.self, forPrimaryKey: idChat)
        if (primary != nil) {
            print("Error Primary Key")
            return
        }else{
            DBHelper.insert(model)
        }
        
        //sendStat = 0:incoming(download) 1:outgoing(migrate)
        switch sendStat {
        case 0:
            var mediaName: String! = ""
            if let name = chat["mediaName"] { mediaName = name as! String }
            var mediaUrl: String! = ""
            if let url = chat["imageUrl"] { mediaUrl = url as! String }
            var mediaType: String! = ""
            if let type = chat["mediaType"] { mediaType = type as! String }
            
            if let id = chat["idMedia"]{
                self.idMediaForSendToFirebase = id as! String
                self.fromIdForSaveMedia = fromIdForMedia
                self.nameForSaveMedia = mediaName
                self.typeForSaveMedia = mediaType
                self.statusSendMedia = 0
                
                self.migrateDataMediaToLocal()
                downloadMediaToLocal(mediaUrl)
            }else{
                self.observeMessages()
                self.scrollToLast()
            }
        case 1 :
            print("outgoing media (migrate)")
            self.observeMessages()
            self.scrollToLast()
        default:
            //Langsung refresh
            self.observeMessages()
            self.scrollToLast()
        }
    }
    
    func downloadMediaToLocal(_ mediaUrl: String){
        print("downloading media to local")
        // Create Folder Media
        let dataPathFile = self.getDocumentsDirectory().appending("/Media")
        do {
            try FileManager.default.createDirectory(atPath: dataPathFile, withIntermediateDirectories: false, attributes: nil)
        } catch {}
        
        // Save to Local
        let pathLocal = "file://" + dataPathFile + "/\(self.nameForSaveMedia)"
        let pathLocalSave = pathLocal.replacingOccurrences(of: " ", with: "")
        
        let starsRef = FIRStorage.storage().reference(forURL: mediaUrl)
        let localURL: URL! = URL(string: pathLocalSave)
        let downloadTask = starsRef.write(toFile: localURL) { (URL, error) -> Void in
            if (error != nil) {
                print("Error : \(String(describing: error))")
            } else {
                print("Downloading : " + self.nameForSaveMedia)
                
            }
        }
        downloadTask.observe(.success) { (snapshot) in
            print("Download success : " + self.nameForSaveMedia)
            if self.paramsKindChat == "group"{
                self.migrateDataMediaToLocal()
            }else{
                self.observeMessages()
                self.scrollToLast()
            }
        }
    }
    
    func makeSerializeSaveMedia(){
        let sess = try! Realm().objects(session.self).first
        let idGroup = sess!.id_group
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.fromId as AnyObject,
            "id_group" : idGroup as AnyObject,
            "name" : self.nameForSaveMedia as AnyObject,
            "type" : self.typeForSaveMedia as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64SaveMedia(jsonString!)
    }
    
    func makeBase64SaveMedia(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postDataSaveMedia(base64Encoded)
        }
    }
    
    func postDataSaveMedia(_ message: String){
        let sess = try! Realm().objects(session.self).first
        let tokenSes = sess!.token
        
        let urlString = Config().urlMedia + "add_media"
        let token = tokenSes
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                let code = jsonData["code"].intValue
                if code == 1 || code == 3 {
                    let data = jsonData["msg"]
                    let id = String(data.intValue)
                    print("Save data media to server")
                    self.idMediaForSendToFirebase = id
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.migrateDataMediaToLocal()
        }
    }
    
    var fromIdForSaveMedia = ""
    var nameForSaveMedia = ""
    var typeForSaveMedia = ""
    var idMediaForSendToFirebase = ""
    var statusSendMedia = 0
    //sinaa
    func migrateDataMediaToLocal(){
        let model = tb_media()
        
        model.id_media = self.idMediaForSendToFirebase
        let sess = try! Realm().objects(session.self).first!
        let idGroup = String(sess.id_group)
        model.id_group = idGroup
        model.id_from_user = self.fromIdForSaveMedia
        var type = ""
        switch self.typeForSaveMedia {
        case "1":
            type = "Image"
        case "2":
            type = "Video"
        case "3":
            type = "Audio"
        case "4":
            type = "Document"
        default:
            type = self.typeForSaveMedia
        }
        model.type = type
        let name = self.nameForSaveMedia
        model.name = name
        
        model.date_create = String(describing: Date())
        model.date_update = String(describing: Date())
        
        let category = try! Realm().object(ofType: tb_media.self, forPrimaryKey: name)
        if (category != nil) {
            print("Error Media Name Primary Key")
            //return
        }else{
            print("Saved media to local")
            DBHelper.insert(model)
            
            //sendStat = 0:incoming(download) 1:outgoing(migrate)
            if self.statusSendMedia == 0 {
                self.observeMessages()
                self.scrollToLast()
            }
        }
    }
    
    func sendNotif(_ data: [String: AnyObject], key: String){
        print(data)
        var from_id = ""
        var varText = ""
        var timestamp = 0
        var to_id = ""
        var image_url = ""
        var image_height = 0
        var image_width = 0
        var video_url = ""
        var audio_url = ""
        var file_url = ""
        var media_name = ""
        var media_size = 0
        var media_type = ""
        var latitude = 0
        var longitude = 0
        var name_address = ""
        var detail_address = ""
        var key_reply = ""
        var id_media = ""
        
        if let fromId = data["fromId"] {from_id = fromId as! String}
        if let toId = data["toId"] {to_id = toId as! String}
        if let text = data["text"] {varText = text as! String}
        if let time = data["timestamp"] {timestamp = Int(time as! NSNumber)}
        if let imageUrl = data["imageUrl"] {image_url = imageUrl as! String}
        if let imageWidth = data["imageWidth"] {image_width = Int(imageWidth as! NSNumber)}
        if let imageHeight = data["imageHeight"] {image_height = Int(imageHeight as! NSNumber)}
        if let videoUrl = data["videoUrl"] {video_url = videoUrl as! String}
        if let audioUrl = data["audioUrl"] {audio_url = audioUrl as! String}
        if let fileUrl = data["fileUrl"] {file_url = fileUrl as! String}
        if let mediaName = data["mediaName"] {media_name = mediaName as! String}
        if let mediaSize = data["mediaSize"] {media_size = mediaSize as! Int}
        if let mediaType = data["mediaType"] {media_type = mediaType as! String}
        if let lat = data["latitude"] {latitude = Int(lat as! NSNumber)}
        if let long = data["longitude"] {longitude = Int(long as! NSNumber)}
        if let address = data["nameAddress"] {name_address = address as! String}
        if let address = data["detailAddress"] {detail_address = address as! String}
        if let keyReply = data["keyReply"] {key_reply = keyReply as! String}
        if let idMedia = data["idMedia"] {id_media = idMedia as! String}
        let idSession = try! Realm().objects(session.self).first!
        let idGroup = idSession.id_group
        let topic = "G" + String(idGroup)
        
        let arrayData = [
            "key" : key,
            "fromId" : from_id,
            "isRead" : "0",
            "text" : varText,
            "timestamp" : timestamp,
            "toId" : to_id,
            "imageUrl" : image_url,
            "imageHeight" : image_height,
            "imageWidth" : image_width,
            "videoUrl" : video_url,
            "audioUrl" : audio_url,
            "fileUrl" : file_url,
            "mediaName" : media_name,
            "mediaSize" : media_size,
            "mediaType" : media_type,
            "latitude" : latitude,
            "longitude" : longitude,
            "nameAddress" : name_address,
            "detailAddress" : detail_address,
            "keyReply" : key_reply,
            "topic" : topic,
            "id_group" : idGroup,
            "idMedia" : id_media
            ] as [String : Any]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Notif(jsonString!)
    }
    
    func makeBase64Notif(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postDataNotif(base64Encoded)
        }
    }
    
    func postDataNotif(_ message: String){
        let sess = try! Realm().objects(session.self).first
        let tokenSes = sess!.token
        
        var urlString = ""
        if paramsKindChat == "personal" {
            urlString = Config().urlUpdate + "sendMessageToPersonal"
        }else{
            urlString = Config().urlUpdate + "sendMessageToGroup"
        }
        let token = tokenSes
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                let code = jsonData["code"].intValue
                if code > 0 {
                    print("send notif chat success")
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                
        }
    }
    
    // Handle Send
    @IBAction func btnSendTapped(_ sender: Any) {
        let properties: [String: AnyObject]!
        if self.paramsReply == 0 {
            properties = ["text": self.txtInput.text as AnyObject]
        }else{
            properties = ["text": self.txtInput.text as AnyObject, "mediaType": "Reply" as AnyObject, "keyReply": self.idChatReply as AnyObject]
            
            //Change text input to default
            self.viewReply.isHidden = true
            self.conHeightViewReply.constant = 0
            self.viewSeparator.isHidden = false
            self.idChatReply = ""
            self.paramsReply = 0
            
        }
        sendMessageWithProperties(properties)
        
        //Change tampilan send
        self.txtInput.text = ""
        self.txtInput.textColor = UIColor.black
        self.btnSend.setBackgroundImage(UIImage(named: "ico_send_disable"), for: .normal)
        self.btnSend.isHidden = true
        self.btnVoice.isHidden = false
        self.imgCamera.isHidden = false
    }
    
    func sendMessageWithProperties(_ properties: [String: AnyObject]) {
        let ref = FIRDatabase.database().reference().child("messages")
        let childRef = ref.childByAutoId()
        let timestamp = (Date().timeIntervalSince1970 * 1000.0).rounded()
        //let timestamp = Date().timeIntervalSince1970
        var values: [String: AnyObject] = ["toId": self.toId as AnyObject, "fromId": self.fromId as AnyObject, "timestamp": timestamp as AnyObject, "isRead": 0 as AnyObject]
        
        properties.forEach({values[$0] = $1})
        
        childRef.updateChildValues(values) { (error, ref) in
            if error != nil {
                return
            }
            
            let userMessagesRef = FIRDatabase.database().reference().child("user-messages").child(self.fromId).child(self.toId)
            
            let messageId = childRef.key
            userMessagesRef.updateChildValues([messageId: 1])
            
            let recipientUserMessagesRef = FIRDatabase.database().reference().child("user-messages").child(self.toId).child(self.fromId)
            recipientUserMessagesRef.updateChildValues([messageId: 1])
        }
        
        self.sendNotif(values, key: childRef.key)
        
        self.migrateToLocal(values, idChat: childRef.key, sendStat: 1)
    }
    
    func sendMessageWithImageUrl(_ imageUrl: String, image: UIImage, metadata: FIRStorageMetadata, caption: String, imageName: String) {
        //let mediaName = Config().nameMediaFile(metadata.name!, idUser: self.fromId)
        let mediaName = imageName
        let properties: [String: AnyObject] = ["imageUrl": imageUrl as AnyObject, "imageWidth": image.size.width as AnyObject, "imageHeight": image.size.height as AnyObject, "text": caption as AnyObject, "mediaName": mediaName as AnyObject, "mediaType": "Image" as AnyObject, "mediaSize": Int(metadata.size) as AnyObject, "idMedia": self.idMediaForSendToFirebase as AnyObject]
        self.sendMessageWithProperties(properties)
    }
    
    func uploadToFirebaseStorageUsingImage(_ image: UIImage, completion: @escaping (_ imageUrl: String, _ metadata: FIRStorageMetadata) -> ()) {
        let imageName = UUID().uuidString
        let ref = FIRStorage.storage().reference().child("message_images").child(imageName)
        
        if let uploadData = UIImageJPEGRepresentation(image, 0.2) {
            self.uploadTask = ref.put(uploadData, metadata: nil, completion: { (metadata, error) in
                
                if error != nil {
                    print("Failed to upload image:")//, error)
                    return
                }
                
                if let imageUrl = metadata?.downloadURL()?.absoluteString {
                    completion(imageUrl, metadata!)
                    print("upload")
                }
                
            })
            
            if self.circularProgressView != nil {
                self.circularProgressView.value = 0
            }
            let intRandom = arc4random_uniform(9999)
            let idChatRandom = String(intRandom)
            if self.indicatorFromSendImage == 1 {
                self.bubbleTmpForUploadLoading(100, height: 100, idChat: idChatRandom)
            }
            
            self.uploadTask.observe(.progress) { (snapshot) in
                if self.indicatorFromSendImage == 1 {
                    if let completedUnitCount = snapshot.progress?.completedUnitCount {
                        let percentComplete = 100.0 * Double(completedUnitCount)
                            / Double(snapshot.progress!.totalUnitCount)
                        print("percent gan", percentComplete)
                        if percentComplete.isNaN {
                            return
                        }
                        if self.circularProgressView != nil {
                            self.circularProgressView.setProgress(value: CGFloat(percentComplete), animationDuration: 0.5, completion: {
                            })
                        }
                    }
                }
            }
            
            self.uploadTask.observe(.success) { (snapshot) in
                self.circularProgressView.value = 0
                if self.indicatorFromSendImage == 1 {
                    if self.arraySendImage.count > self.iFromSendImage {
                        let imgData = self.arraySendImage[self.iFromSendImage]
                        let selectedImage = UIImage(data: imgData)
                        let nameImage = self.arrayNameImage[self.iFromSendImage]
                        let sendCaption = self.arraySendCaptionImage[self.iFromSendImage]
                        //Send media data to server
                        if self.paramsKindChat == "group"{
                            self.nameForSaveMedia = nameImage
                            self.typeForSaveMedia = "1" //1:image 2:video 3:audio 4:document
                            self.fromIdForSaveMedia = self.fromId
                            self.makeSerializeSaveMedia()
                        }
                        self.uploadToFirebaseStorageUsingImage(selectedImage!, completion: { (imageUrl, metadata) in
                            self.sendMessageWithImageUrl(imageUrl, image: selectedImage!, metadata: metadata, caption: sendCaption, imageName: nameImage)
                        })
                        self.iFromSendImage += 1
                    }else{
                        self.indicatorFromSendImage = 0
                    }
                }
                self.removeBubbleTmpForUploadLoading(idChatRandom)
            }
        }
    }
    
    // Handle send image
    func handleCameraTap(_ sender: UITapGestureRecognizer){
        self.txtInput.resignFirstResponder()
        self.conBottomViewInput.constant = 0
        BrowseFoto(self.btnSend)
    }
    func BrowseFoto(_ sender: UIButton) {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.modalPresentationStyle = .popover
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.cameraTapped()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.libraryTapped()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
            self.conBottomViewInput.constant = 0
        }
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds;
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // Camera
    func cameraTapped(){
        if Config().checkAccessCamera() == false {
            print("access denied")
            let msg = "Please allow the app to access your camera through the Settings."
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                if granted == true {
                    // User granted
                    Config().alertForAllowAccess(self, msg: msg)
                }else{
                    // User Rejected
                    Config().alertForAllowAccess(self, msg: msg)
                }
            });
        }else{
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
            {
                let imagePickerController = UIImagePickerController()
                imagePickerController.delegate = self
                
                imagePickerController.sourceType = UIImagePickerControllerSourceType.camera
                imagePickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
                imagePickerController.allowsEditing = false
                imagePickerController.videoMaximumDuration = 15
                self.present(imagePickerController, animated: true, completion: nil)
            }
            else
            {
                libraryTapped()
            }
        }
    }
    
    // Library Image
    func libraryTapped(){
        if Config().checkAccessPhotos() == false {
            print("access denied")
            let msg = "Please allow the app to access your photos through the Settings."
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.denied {
                    print("denied")
                    Config().alertForAllowAccess(self, msg: msg)
                }else{
                    print("another")
                    Config().alertForAllowAccess(self, msg: msg)
                }
            })
        }else{
            let imagePickerController = UIImagePickerController()
            
            imagePickerController.allowsEditing = false
            imagePickerController.sourceType = .photoLibrary
            imagePickerController.delegate = self
            imagePickerController.mediaTypes = [kUTTypeImage as String]
            
            present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    //handle image picker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        //copy to folder media
        var pickedImage: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            pickedImage = editedImage
        }
        if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            pickedImage = originalImage
        }
    
        
        if let videoUrl = info[UIImagePickerControllerMediaURL] as? URL {
            let realName = videoUrl.lastPathComponent
            let idName = UUID().uuidString
            let saveName = idName + realName
            let mediaPath = "file://" + self.getDocumentsDirectory().appending("/Media/"+saveName)
            let toPath = URL(string: mediaPath)
            let fileManager = FileManager.default
            do {
                print("copy video")
                try fileManager.copyItem(at: videoUrl as URL, to: toPath!)
            }
            catch let error as NSError {
                print("Ooops! Something went wrong: \(error)")
            }
            
            //sina
            if paramsKindChat == "group"{
                self.nameForSaveMedia = saveName
                self.typeForSaveMedia = "2" //1:image 2:video 3:audio 4:document
                self.fromIdForSaveMedia = self.fromId
                self.makeSerializeSaveMedia()
            }
            self.handleVideoSelectedForUrl(videoUrl, fileName: saveName)
        } else {
            var realName = ""
            if let imageURL = info[UIImagePickerControllerReferenceURL] as? URL {
                realName = imageURL.lastPathComponent
            }
            let idName = UUID().uuidString
            let saveName = idName + realName
            let imagePath = getDocumentsDirectory().appendingPathComponent("/Media/"+saveName)
            
            self.arrayNameImage.removeAll()
            self.arrayNameImage.append(saveName)
            
            let jpegData = UIImageJPEGRepresentation(pickedImage!, 1)
            try? jpegData!.write(to: URL(fileURLWithPath: imagePath), options: [.atomic])
            
            handleImageSelectedForInfo(info as [String : AnyObject])
        }
        
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    func handleImageSelectedForInfo(_ info: [String: AnyObject]) {
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            self.selectedImage = selectedImage
            self.performSegue(withIdentifier: "showSendImage", sender: self)
        }
    }
    
    var uploadTask: FIRStorageUploadTask!
    func handleVideoSelectedForUrl(_ url: URL, fileName: String) {
        self.uploadTask = FIRStorage.storage().reference().child("message_movies").child(fileName).putFile(url, metadata: nil, completion: { (metadata, error) in
            
            if error != nil {
                print("Failed upload of video:")
                return
            }
            
            if let videoUrl = metadata?.downloadURL()?.absoluteString {
                if let thumbnailImage = Config().thumbnailImageForFileUrl(url) {
                    self.uploadToFirebaseStorageUsingImage(thumbnailImage, completion: { (imageUrl, metadata) in
                        //let mediaName = Config().nameMediaFile(metadata.name!, idUser: self.fromId) + ".mov"
                        let properties: [String: AnyObject] = ["imageUrl": imageUrl as AnyObject, "imageWidth": thumbnailImage.size.width as AnyObject, "imageHeight": thumbnailImage.size.height as AnyObject, "videoUrl": videoUrl as AnyObject, "mediaName": fileName as AnyObject, "mediaType": "Video" as AnyObject, "mediaSize": Int(metadata.size) as AnyObject, "idMedia": self.idMediaForSendToFirebase as AnyObject]
                        
                        self.sendMessageWithProperties(properties)
                    })
                }
            }
        })
        
        
        if self.circularProgressView != nil {
            self.circularProgressView.value = 0
        }
        let intRandom = arc4random_uniform(9999)
        let idChatRandom = String(intRandom)
        self.bubbleTmpForUploadLoading(100, height: 100, idChat: idChatRandom)
        
        self.uploadTask.observe(.progress) { (snapshot) in
            print("upload progress")
            if let completedUnitCount = snapshot.progress?.completedUnitCount {
                print("lagi upload gan")
                let percentComplete = 100.0 * Double(completedUnitCount)
                    / Double(snapshot.progress!.totalUnitCount)
                if percentComplete.isNaN {
                    return
                }
                //kesini
                if self.circularProgressView != nil {
                    self.circularProgressView.setProgress(value: CGFloat(percentComplete), animationDuration: 0.5, completion: {
                        print("beres upload")
                    })
                }
            }
        }
        
        self.uploadTask.observe(.success) { (snapshot) in
            print("upload success")
            self.removeBubbleTmpForUploadLoading(idChatRandom)
        }
    }
    
    //Data For Loading Progress upload or download
    //var circularProgressView = [UICircularProgressRingView]()
    var circularProgressView: UICircularProgressRingView!
    
    func bubbleTmpForUploadLoading(_ width: Int, height: Int, idChat: String){
        
        print("add new row")
        let model = tb_chat()
        model.id_chat = idChat
        model.from = self.fromId
        model.to = self.toId
        model.image_width = width
        model.image_height = height
        model.mediaType = "Progress"
        model.time = (Date().timeIntervalSince1970 * 1000.0).rounded() + 50000
        let category = try! Realm().object(ofType: tb_chat.self, forPrimaryKey: "000")
        if (category != nil) {
            print("Error Primary Key")
            return
        }else{
            print("Succes")
            DBHelper.insert(model)
            self.observeMessages()
        }
        self.tbChat.reloadData()
    }
    
    func removeBubbleTmpForUploadLoading(_ idChat: String) {
        DBHelper.deleteByIdChat(idChat)
        print("Deleted")
    }
    
    //Handle Cancel Progress
    @IBAction func cancelProgBarIncomingTapped(_ sender: UIButton) {
        print("cancel upload")
        if self.uploadTask != nil {
            self.uploadTask.cancel()
            //self.uploadTask.removeAllObservers()
            
            //Get id message
            let cell = sender.superview!.superview!.superview!.superview as! ChatGamesCell
            let indexPath = self.tbChat!.indexPath(for: cell)
            let data = messages[indexPath!.row]
            let idChat = data.idChat
            //Remove chat
            self.removeBubbleTmpForUploadLoading(idChat!)
            self.observeMessages()
            self.scrollToLast()
            self.tbChat.reloadData()
        }
    }
    
    @IBAction func cancelProgBarOutgoingTapped(_ sender: UIButton) {
        print("cancel upload")
        if self.uploadTask != nil {
            self.uploadTask.cancel()
            //self.uploadTask.removeAllObservers()
            
            //Get id message
            let cell = sender.superview!.superview!.superview!.superview as! ChatGamesCell
            let indexPath = self.tbChat!.indexPath(for: cell)
            let data = messages[indexPath!.row]
            let idChat = data.idChat
            //Remove chat
            self.removeBubbleTmpForUploadLoading(idChat!)
            self.observeMessages()
            self.scrollToLast()
            self.tbChat.reloadData()
        }
    }
    
    //Handle record voice
    var audioRecorder: AVAudioRecorder!
    @IBAction func btnVoiceTapDown(_ sender: Any) {
        self.btnSend.isHidden = true
        self.txtInput.isHidden = true
        self.lblVoiceDuration.isHidden = false
        self.lblVoiceDuration.text = "00:00"
        self.view.bringSubview(toFront: lblVoiceDuration)
        self.imgCamera.isHidden = true
        self.imgEmot.image = UIImage(named: "ico_record")
        self.lblVoiceSliceToClose.isHidden = false
        
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        self.secon = -1
        self.timer = nil
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerRecord), userInfo: nil, repeats: true)
        self.timer.fire()
        
        if audioRecorder == nil {
            print("Start Record")
            self.startRecording()
        }
    }
    
    var voiceName = ""
    var voiceUrlFile: URL!
    func startRecording() {
        //Unique File Name
        let date = Int(Date().timeIntervalSince1970)
        voiceName = "Record_Pitung_" + self.fromId + "_" + String(date) + ".m4a"
        //-------
        
        let audioFilename = self.getDocumentsDirectory().appendingPathComponent("/Media/\(voiceName)")
        voiceUrlFile = URL(fileURLWithPath: audioFilename)
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: voiceUrlFile, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
        } catch {
            finishRecording(false)
        }
    }
    
    func finishRecording(_ success: Bool) {
        self.stopRecord()
        
        if success {
            self.uploadRecord(voiceName, filePath: voiceUrlFile)
        } else {
            // Failed
            self.alertRecord("Failed to record, please check your settings")
        }
    }
    
    func stopRecord(){
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        if audioRecorder != nil {
            print("stop record")
            audioRecorder.stop()
            audioRecorder = nil
        }
        if timer != nil {
            print("stop timer")
            timer.invalidate()
            timer = nil
        }
        secon = 0
        minute = 0
    }
    func timerRecord(){
        if secon < 60-1 {
            secon += 1
        }else{
            secon = 0
            minute += 1
        }
        var nulSec = ""
        var nulMin = ""
        if secon < 10 {
            nulSec = "0"
        }
        if minute < 10 {
            nulMin = "0"
        }
        
        self.lblVoiceDuration.text = nulMin + String(minute) + ":" + nulSec + String(secon)
        print(self.lblVoiceDuration.text!)
        
        if minute == 10 {
            self.stopRecord()
            self.alertRecord("Maximum duration is 10 minutes")
        }
    }
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(false)
        }
    }
    func alertRecord(_ msg: String){
        let alert = UIAlertController(title: "", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
            print("Alert Record")
        }))
    }
    
    func uploadRecord(_ fileName: String, filePath: URL){
        self.uploadTask = FIRStorage.storage().reference().child("message_audio").child(fileName).putFile(filePath, metadata: nil, completion: { (metadata, error) in
            
            if error != nil {
                print("Failed upload of audio: ")//, error)
                return
            }
            
            if let audioUrl = metadata?.downloadURL()?.absoluteString {
                let mediaName = metadata!.name!
                let properties: [String: AnyObject] = ["audioUrl": audioUrl as AnyObject, "mediaName": mediaName as AnyObject, "mediaType": "Voice" as AnyObject, "mediaSize": Int((metadata?.size)!) as AnyObject]
                self.sendMessageWithProperties(properties)
            }
        })
        
        if self.circularProgressView != nil {
            self.circularProgressView.value = 0
        }
        let intRandom = arc4random_uniform(9999)
        let idChatRandom = String(intRandom)
        self.bubbleTmpForUploadLoading(100, height: 100, idChat: idChatRandom)
        
        self.uploadTask.observe(.progress) { (snapshot) in
            if let completedUnitCount = snapshot.progress?.completedUnitCount {
                let percentComplete = 100.0 * Double(completedUnitCount)
                    / Double(snapshot.progress!.totalUnitCount)
                if percentComplete.isNaN {
                    return
                }
                if self.circularProgressView != nil {
                        self.circularProgressView.setProgress(value: CGFloat(percentComplete), animationDuration: 0.5, completion: {
                        })
                    
                }
            }
        }
        self.uploadTask.observe(.success) { (snapshot) in
            self.removeBubbleTmpForUploadLoading(idChatRandom)
        }
    }
    
    @IBAction func btnVoiceTapUp(_ sender: Any) {
        self.doneRecord()
        
        self.handleSendRecord()
    }
    
    func handleSendRecord(){
        if audioRecorder != nil {
            self.finishRecording(true)
        }else {
            self.finishRecording(false)
        }
    }
    
    func doneRecord(){
        self.txtInput.isHidden = false
        self.lblVoiceDuration.isHidden = true
        self.imgCamera.isHidden = false
        self.imgEmot.image = UIImage(named: "ico_insert_emot")
        self.lblVoiceSliceToClose.isHidden = true
        self.lblVoiceSliceToClose.layer.position = CGPoint(x: self.xPosLblVoiceClose, y: self.yPosLblVoiceClose)
        self.btnVoice.layer.position = CGPoint(x: self.xPosBtnSend, y: self.yPosBtnSend)
        self.btnSend.isHidden = true
        self.btnVoice.isHidden = false
    }
    
    func slideCloseRecord(_ sender: UIPanGestureRecognizer){
        let velocity = sender.velocity(in: sender.view)
        if velocity.x > 0 || velocity.x < 0{
            let translation = sender.translation(in: self.view)
            sender.view!.center = CGPoint(x: sender.view!.center.x + translation.x, y: sender.view!.center.y)
            sender.setTranslation(CGPoint.zero, in: self.view)
            
            let lbl = self.lblVoiceSliceToClose
            let posX = lbl!.center.x + translation.x
            let posY = lbl!.center.y
            lbl!.layer.position = CGPoint(x: posX, y: posY)
            
            let posEnd = (self.view.frame.width / 2) - 50 //Posisi tengah layar(dikurang 50 karna buttonnya ukuran 50)
            
            //beres di slide sampe tujuan
            if posX < posEnd {
                sender.isEnabled = false
                sender.isEnabled = true
                self.doneRecord()
                self.stopRecord()
            }
        }
        
        //beres di pan gesture
        if sender.state == UIGestureRecognizerState.ended {
            self.doneRecord()
            self.handleSendRecord()
        }
    }
    
    // Handle Attachment
    func attachTapped(_ sender: UIButton){
        self.txtInput.resignFirstResponder()
        self.conBottomViewInput.constant = 0
        popupAttach(sender)
    }
    
    func popupAttach(_ sender: UIButton) {
        let alert:UIAlertController=UIAlertController(title: "Attachment", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.modalPresentationStyle = .popover
        
        let audioAction = UIAlertAction(title: "Audio", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.audioAttachTapped()
        }
        let videoAction = UIAlertAction(title: "Video", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.videoAttachTapped()
        }
        let fileAction = UIAlertAction(title: "File", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.fileAttachTapped()
        }
        let locationAction = UIAlertAction(title: "Location", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.locationAttachTapped()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
            self.conBottomViewInput.constant = 0
        }
        
        alert.addAction(audioAction)
        alert.addAction(videoAction)
        alert.addAction(fileAction)
        alert.addAction(locationAction)
        alert.addAction(cancelAction)
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds;
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func audioAttachTapped(){
        self.documentPickerType = 1
        let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypeMP3)], in: .import)
        documentPickerController.delegate = self
        present(documentPickerController, animated: true, completion: nil)
    }
    
    func videoAttachTapped(){
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
        imagePickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
        
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func fileAttachTapped(){
        self.documentPickerType = 2
        let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypePlainText)], in: .import)
        documentPickerController.delegate = self
        present(documentPickerController, animated: true, completion: nil)
    }
    
    var documentPickerType = 0 //1:Audio, 2:File
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let documentURL = url as URL
        let fileName = documentURL.lastPathComponent
        
        //Copy to folder media
        let fileManager = FileManager.default
        let mediaPath = "file://" + self.getDocumentsDirectory().appending("/Media/"+fileName)
        let toPath = URL(string: mediaPath)
        do {
            print("copy media")
            try fileManager.copyItem(at: documentURL, to: toPath!)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
        
        switch self.documentPickerType {
        case 1:
            if paramsKindChat == "group"{
                self.nameForSaveMedia = fileName
                self.typeForSaveMedia = "3" //1:image 2:video 3:audio 4:document
                self.fromIdForSaveMedia = self.fromId
                self.makeSerializeSaveMedia()
            }
            
            self.uploadAudio(fileName, filePath: documentURL)
        case 2:
            if paramsKindChat == "group"{
                self.nameForSaveMedia = fileName
                self.typeForSaveMedia = "4" //1:image 2:video 3:audio 4:document
                self.fromIdForSaveMedia = self.fromId
                self.makeSerializeSaveMedia()
            }
            
            self.uploadFile(fileName, filePath: documentURL)
        default:
            print("Other Type")
        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func locationAttachTapped(){
        let statusAccess = CLLocationManager.authorizationStatus()
        if statusAccess == .denied {
            let msg = "Please allow the app to access your location through the Settings."
            Config().alertForAllowAccess(self, msg: msg)
        }else if statusAccess == .restricted {
            let msg = "Please allow the app to access your location through the Settings."
            Config().alertForAllowAccess(self, msg: msg)
        }else if statusAccess == .notDetermined {
            let msg = "Please allow the app to access your location through the Settings."
            Config().alertForAllowAccess(self, msg: msg)
        }else{
            let center = CLLocationCoordinate2DMake(currentLat, currentLong)
            let northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001)
            let southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001)
            let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
            let config = GMSPlacePickerConfig(viewport: viewport)
            let placePicker = GMSPlacePicker(config: config)
            
            placePicker.pickPlace { (place, error) in
                if let error = error {
                    print("Pick Place error: \(error.localizedDescription)")
                    return
                }
                
                if let place = place {
                    let name = place.name
                    let address = place.formattedAddress
                    let lat = place.coordinate.latitude
                    let long = place.coordinate.longitude
                    print("Place name \(name)")
                    print("Place address \(address!)")
                    print(lat)
                    print(long)
                    
                    self.uploadLocation(lat, long: long, nameAddress: name, detailAddress: address!)
                } else {
                    print("No place selected")
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.currentLat = manager.location?.coordinate.latitude
        self.currentLong = manager.location?.coordinate.longitude
        self.locationManager.stopUpdatingLocation()
    }
    
    func uploadAudio(_ fileName: String, filePath: URL){
        self.uploadTask = FIRStorage.storage().reference().child("message_audio").child(fileName).putFile(filePath, metadata: nil, completion: { (metadata, error) in
            
            if error != nil {
                print("Failed upload of audio: ", error!)
                return
            }
            
            if let audioUrl = metadata!.downloadURL()?.absoluteString {
                let mediaName = fileName//Config().nameMediaFile(metadata!.name!, idUser: self.fromId)
                let properties: [String: AnyObject] = ["audioUrl": audioUrl as AnyObject, "mediaName": mediaName as AnyObject, "mediaType": "Audio" as AnyObject, "mediaSize": Int(metadata!.size) as AnyObject]
                self.sendMessageWithProperties(properties)
            }
        })
        
        if self.circularProgressView != nil {
            self.circularProgressView.value = 0
        }
        let intRandom = arc4random_uniform(9999)
        let idChatRandom = String(intRandom)
        self.bubbleTmpForUploadLoading(100, height: 100, idChat: idChatRandom)
        
        self.uploadTask.observe(.progress) { (snapshot) in
            if let completedUnitCount = snapshot.progress?.completedUnitCount {
                let percentComplete = 100.0 * Double(completedUnitCount)
                    / Double(snapshot.progress!.totalUnitCount)
                if percentComplete.isNaN {
                    return
                }
                if self.circularProgressView != nil {
                    self.circularProgressView.setProgress(value: CGFloat(percentComplete), animationDuration: 0.5, completion: {
                    })
                }
            }
        }
        self.uploadTask.observe(.success) { (snapshot) in
            self.removeBubbleTmpForUploadLoading(idChatRandom)
        }
    }
    
    func uploadFile(_ fileName: String, filePath: URL){
        self.uploadTask = FIRStorage.storage().reference().child("message_file").child(fileName).putFile(filePath, metadata: nil, completion: { (metadata, error) in
            
            if error != nil {
                print("Failed upload of file: ")//, error)
                return
            }
            
            if let fileUrl = metadata?.downloadURL()?.absoluteString {
                let mediaName = fileName//Config().nameMediaFile(metadata!.name!, idUser: self.fromId)
                let properties: [String: AnyObject] = ["fileUrl": fileUrl as AnyObject, "mediaName": mediaName as AnyObject, "mediaType": (metadata?.contentType)! as AnyObject, "mediaSize": Int((metadata?.size)!) as AnyObject]
                self.sendMessageWithProperties(properties)
            }
        })
        
        if self.circularProgressView != nil {
            self.circularProgressView.value = 0
        }
        let intRandom = arc4random_uniform(9999)
        let idChatRandom = String(intRandom)
        self.bubbleTmpForUploadLoading(100, height: 100, idChat: idChatRandom)
        
        self.uploadTask.observe(.progress) { (snapshot) in
            if let completedUnitCount = snapshot.progress?.completedUnitCount {
                let percentComplete = 100.0 * Double(completedUnitCount)
                    / Double(snapshot.progress!.totalUnitCount)
                if percentComplete.isNaN {
                    return
                }
                if self.circularProgressView != nil {
                    self.circularProgressView.setProgress(value: CGFloat(percentComplete), animationDuration: 0.5, completion: {
                    })
                }
            }
        }
        self.uploadTask.observe(.success) { (snapshot) in
            self.removeBubbleTmpForUploadLoading(idChatRandom)
        }
    }
    
    func uploadLocation(_ lat: Double, long: Double, nameAddress: String, detailAddress: String){
        let properties: [String: AnyObject] = ["latitude": lat as AnyObject, "longitude": long as AnyObject, "nameAddress": nameAddress as AnyObject, "detailAddress": detailAddress as AnyObject, "mediaType": "Location" as AnyObject]
        self.sendMessageWithProperties(properties)
    }
    
    //Open Media (Image, Audio, Video, Location, Document)
    func mediaTapped(_ sender: UIGestureRecognizer){
        let cell = sender.view!.superview!.superview!.superview!.superview!.superview as! ChatGamesCell
        let indexPath = self.tbChat!.indexPath(for: cell)
        let data = messages[indexPath!.row]
        
        if data.imageUrl != "" {
            if data.videoUrl != "" {
                self.videoTap(data)
            }else{
                self.imageTap(data)
            }
        }
        if data.audioUrl != "" {
            self.audioTap(data)
        }
        if data.fileUrl != "" {
            self.fileTap(data)
        }
        if data.latitude != 0 {
            self.locationTap(data)
        }
    }
    
    func imageTap(_ data: Chat){
        let nameImage = data.mediaName
        let nameImageLocal = nameImage!.replacingOccurrences(of: " ", with: "")
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let getImagePath = paths.appendingPathComponent("/Media/\(nameImageLocal).jpg")
        let imgImage = UIImage(contentsOfFile: getImagePath)
        
        let height = CGFloat(data.imageHeight!) / CGFloat(data.imageWidth!) * self.view.frame.width
        
        let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showUpdateDetailPhoto") as! UpdateDetailPhotoController
        self.addChildViewController(Popover)
        Popover.view.frame = self.view.frame
        Popover.valueConHeightViewImg = height - 100
        Popover.valueConWidthViewImg = self.view.frame.width - 100
        if imgImage != nil {
            Popover.imgPhoto.image = imgImage
        }else{
            Popover.imgPhoto.image = UIImage(named: "dafault-ava-group")
        }
        self.view.addSubview(Popover.view)
        Popover.didMove(toParentViewController: self)
    }
    
    func videoTap(_ data: Chat){
        let videoName = data.mediaName
        
        var localPlayer: AVPlayer?
        let playerViewController = AVPlayerViewController()
        
        let masterPath = self.getDocumentsDirectory().appending("/Media")
        let videoNameLocal = videoName!.replacingOccurrences(of: " ", with: "")
        let pathLocal = "file://" + masterPath + "/" + videoNameLocal// + ".mov"
        
        localPlayer = AVPlayer(url: URL(string: pathLocal)!)
        playerViewController.player = localPlayer
        self.present(playerViewController, animated: true){
            playerViewController.player?.play()
        }
    }
    
    func audioTap(_ data: Chat){
        let audioName = data.mediaName
        
        var localPlayer: AVPlayer?
        let playerViewController = AVPlayerViewController()
        
        //Local Path
        let masterPath = self.getDocumentsDirectory().appending("/Media")
        let audioNameLocal = audioName!.replacingOccurrences(of: " ", with: "")
        let pathLocal = "file://" + masterPath + "/" + audioNameLocal + ".mp3"
        
        localPlayer = AVPlayer(url: URL(string: pathLocal)!)
        
        playerViewController.player = localPlayer
        self.present(playerViewController, animated: true){
            playerViewController.player?.play()
        }
    }
    
    func fileTap(_ data: Chat){
        let mediaName = data.mediaName
        let masterPath = self.getDocumentsDirectory().appending("/Media")
        let pathLocal = "file://" + masterPath + "/" + mediaName!
        let pathLocalOpen = pathLocal.replacingOccurrences(of: " ", with: "")
        
        let localURL: URL! = URL(string: pathLocalOpen)
        
        // Open With iBook
        var docController: UIDocumentInteractionController?
        docController = UIDocumentInteractionController(url: localURL)
        let url = URL(string:"itms-books:");
        if UIApplication.shared.canOpenURL(url!) {
            docController!.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
            print("iBooks is installed")
        }else{
            print("iBooks is not installed")
            
            // Background Black
            self.navigationController?.isNavigationBarHidden = true
            self.viewClose = UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.size.width,height: self.view.frame.size.height))
            viewClose.backgroundColor = UIColor.black.withAlphaComponent(0.8)
            viewClose.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ChatingController.closePdf(_:))))
            self.view.addSubview(viewClose)
            
            // Content PDF
            let data = try? Data(contentsOf: localURL)
            let baseURL = localURL.deletingLastPathComponent()
            self.webView = UIWebView(frame: CGRect(x: 0,y: 50,width: self.view.frame.size.width,height: self.view.frame.size.height-100))
            //webView.load(data!, mimeType: "application/pdf", textEncodingName:"", baseURL: baseURL)
            webView.loadRequest(NSMutableURLRequest(url: URL(fileURLWithPath: pathLocal)) as URLRequest)
            self.view.addSubview(webView)
//            NSURLCacheStorageAllowedInMemoryOnly
        }
        
    }
    var webView: UIWebView!
    var viewClose: UIView!
    func closePdf(_ sender: UITapGestureRecognizer){
        self.webView.removeFromSuperview()
        self.viewClose.removeFromSuperview()
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func locationTap(_ data: Chat){
        self.latSelected = data.latitude
        self.longSelected = data.longitude
        self.nameLocSelected = data.nameAdrress!
        self.detailLocSelected = data.detailAdrress!
        
        self.performSegue(withIdentifier: "showLocation", sender: self)
    }
    
    //play Voice
    var imgIncomingPlayVoice: UIImageView!
    var imgOutgoingPlayVoice: UIImageView!
    var lblIncomingDurationVoice: UILabel!
    var lblOutgoingDurationVoice: UILabel!
    var sldrIncomingVoice: UISlider!
    var sldrOutgoingVoice: UISlider!
    func playVoiceTapped(_ sender: UITapGestureRecognizer){
        let cell = sender.view!.superview!.superview!.superview!.superview!.superview as! ChatGamesCell
        let indexPath = self.tbChat!.indexPath(for: cell)
        let data = messages[indexPath!.row]
        
        self.imgIncomingPlayVoice = cell.imgIncomingPlayVoice
        self.imgOutgoingPlayVoice = cell.imgOutgoingPlayVoice
        self.lblIncomingDurationVoice = cell.lblIncomingDurationVoice
        self.lblOutgoingDurationVoice = cell.lblOutgoingDurationVoice
        self.sldrIncomingVoice = cell.sldrIncomingVoice
        self.sldrOutgoingVoice = cell.sldrOutgoingVoice
        
        if cell.lblIncomingDurationVoice.text != "00:00" || cell.lblOutgoingDurationVoice.text != "00:00" {
            if cell.imgIncomingPlayVoice.image == UIImage(named: "ico_voice_play") ||  cell.imgOutgoingPlayVoice.image == UIImage(named: "ico_voice_play") {
                
                let masterPath = self.getDocumentsDirectory().appending("/Media")
                let audioNameLocal = data.mediaName
                let pathString = "file://" + masterPath + "/" + audioNameLocal!
                let newPlayer = AVPlayer(url: URL(string: pathString)!)
                
                let assetPlayer = self.player?.currentItem?.asset
                let urlPlayer = (assetPlayer as? AVURLAsset)?.url
                let assetNewPlayer = newPlayer.currentItem?.asset
                var urlNewPlayer: URL! = nil
                if assetNewPlayer is AVURLAsset {
                    urlNewPlayer = (assetNewPlayer as? AVURLAsset)?.url
                }
                
                if urlPlayer != urlNewPlayer {
                    self.player = AVPlayer(url: URL(string: pathString)!)
                    self.player?.volume = 1.0
                }
                
                self.updater = CADisplayLink(target: self, selector: #selector(self.trackAudio))
                self.updater.frameInterval = 1
                self.updater.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
                
                self.player!.play()
                cell.imgIncomingPlayVoice.image = UIImage(named: "ico_voice_pause")
                cell.imgOutgoingPlayVoice.image = UIImage(named: "ico_voice_pause")
                
                timer = nil
                timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerDurationVoice), userInfo: nil, repeats: true)
                timer.fire()
            } else {
                self.player!.pause()
                cell.imgIncomingPlayVoice.image = UIImage(named: "ico_voice_play")
                cell.imgOutgoingPlayVoice.image = UIImage(named: "ico_voice_play")
                
                if timer != nil {
                    timer.invalidate()
                    timer = nil
                }
            }
        }
    }
    func timerDurationVoice(){
        let devide = Int(normalizedTimeAudioSeek) / 60
        let showMin = devide
        let showSec = Int(normalizedTimeAudioSeek) - (devide * 60)
        minute = showMin
        secon = showSec
        
        if secon < 60-1 {
            secon += 1
        }else{
            secon = 0
            minute += 1
        }
        var nulSec = ""
        var nulMin = ""
        if secon < 10 {
            nulSec = "0"
        }
        if minute < 10 {
            nulMin = "0"
        }
        
        let countTime = nulMin + String(minute) + ":" + nulSec + String(secon)
        
        self.lblOutgoingDurationVoice.text = countTime
        self.lblIncomingDurationVoice.text = countTime
    }
    func trackAudio(){
        let currTime = CMTimeGetSeconds(self.player!.currentTime())
        let durr = CMTimeGetSeconds(self.player!.currentItem!.asset.duration)
        normalizedTimeAudioSeek = Float(currTime * durr / durr)
        self.sldrIncomingVoice.value = normalizedTimeAudioSeek
        self.sldrOutgoingVoice.value = normalizedTimeAudioSeek
        
        if currTime >= durr {
            //Total waktu
            let devide = Int(durr) / 60
            let showMin = devide
            let showSec = Int(durr) - (devide * 60)
            var nulSec = ""
            var nulMin = ""
            if showSec < 10 {
                nulSec = "0"
            }
            if showMin < 10 {
                nulMin = "0"
            }
            let showTime = nulMin + String(showMin) + ":" + nulSec + String(showSec)
            self.lblIncomingDurationVoice.text = showTime
            self.lblOutgoingDurationVoice.text = showTime
            self.secon = 0
            self.minute = 0
            self.player?.pause()
            let timeNull = CMTimeMake(Int64(0), 1)
            self.player?.seek(to: timeNull)
            if timer != nil {
                timer.invalidate()
                timer = nil
            }
        }
        if self.player?.rate == 0{
            self.imgIncomingPlayVoice.image = UIImage(named: "ico_voice_play")
            self.imgOutgoingPlayVoice.image = UIImage(named: "ico_voice_play")
        }else{
            self.imgIncomingPlayVoice.image = UIImage(named: "ico_voice_pause")
            self.imgOutgoingPlayVoice.image = UIImage(named: "ico_voice_pause")
        }
    }
    func seekAction(_ playbackSlider: UISlider){
        let seconds : Int64 = Int64(playbackSlider.value)
        let targetTime:CMTime = CMTimeMake(seconds, 1)
        
        self.player!.seek(to: targetTime)
    }
    
    //Handle share
    @IBAction func btnIncomingShareTapped(_ sender: UIButton) {
        self.hanldeShare(sender)
    }
    
    @IBAction func btnOutgoingShareTapped(_ sender: UIButton) {
        self.hanldeShare(sender)
    }
    
    func hanldeShare(_ data: UIButton){
        let cell = data.superview!.superview!.superview! as! ChatGamesCell
        let indexPath = self.tbChat!.indexPath(for: cell)
        let msg = messages[indexPath!.row]
        let mediaType = msg.mediaType!
        let mediaName = msg.mediaName!
        
        let masterPath = self.getDocumentsDirectory().appending("/Media")
        let pathLocal = "file://" + masterPath + "/" + mediaName
        var dataToShare: AnyObject! = "Cant share this media" as AnyObject!
        
        switch mediaType {
        case "Image":
            let dataImage = UIImage(contentsOfFile: pathLocal)
            print("image1 ", dataImage)
            dataToShare = dataImage
            if dataImage == nil {
                let pathImage = "file://" + masterPath + "/" + mediaName + ".jpg"
                let dataImageWithJPG = UIImage(contentsOfFile: pathImage)
                print("image2 ", dataImageWithJPG)
                dataToShare = dataImageWithJPG
                if dataImageWithJPG == nil {
                    dataToShare = "Cant share this image" as AnyObject!
                }
            }
        case "Video":
            let videoLink = URL(fileURLWithPath: pathLocal)
            dataToShare = videoLink as AnyObject!
            if videoLink == nil {
                let pathVideo = "file://" + masterPath + "/" + mediaName + ".mov"
                let dataImageWithMOV = URL(fileURLWithPath: pathVideo)
                dataToShare = dataImageWithMOV as AnyObject!
            }
        case "Audio":
            let audioLink = URL(fileURLWithPath: pathLocal)
            dataToShare = audioLink as AnyObject!
            if audioLink == nil {
                let pathAudio = "file://" + masterPath + "/" + mediaName + ".mav"
                let dataImageWithWAV = URL(fileURLWithPath: pathAudio)
                dataToShare = dataImageWithWAV as AnyObject!
            }
        default:
            dataToShare = "Cant share this media" as AnyObject!
        }
        
        let VC : UIActivityViewController = UIActivityViewController(activityItems: [dataToShare], applicationActivities: nil)
        VC.popoverPresentationController?.sourceView = self.view
        if let presenter = VC.popoverPresentationController {
            presenter.sourceView = data
            presenter.sourceRect = data.bounds;
        }
        self.present(VC, animated: true, completion: nil)
    }
    
    //Handle Long Press Cell
    var paramsLongPress = 0
    var idChatReply = ""
    var selectedDataReply:Chat!
    var selectedCellReply:ChatGamesCell!
    func longPressCell(_ sender: UIGestureRecognizer){
        print("reply")
        let cell = sender.view! as! ChatGamesCell
        let indexPath = self.tbChat.indexPath(for: cell)
        let data = messages[indexPath!.item]
        
        if paramsLongPress == 0 {
            cell.viewIndicatorSelectedCell.isHidden = false
            cell.viewIndicatorSelectedCell.backgroundColor = UIColor(red: 63, green: 157, blue: 247).withAlphaComponent(0.7)
            self.idChatReply = data.idChat!
            self.paramsLongPress = 1
            self.selectedDataReply = data
            self.selectedCellReply = cell
            
            //Reply
            let image = UIImage(named: "ico_reply")
            let attach = UIButton()
            attach.setImage(image, for: UIControlState())
            attach.addTarget(self, action: #selector(self.replyTapped), for: UIControlEvents.touchDown)
            attach.frame=CGRect(x: 0, y: 0, width: 20, height: 20)
            let barAttach = UIBarButtonItem(customView: attach)
            //Space
            let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            fixedSpace.width = 20.0
            //Delete
            let imageDelete = UIImage(named: "ico_trash")
            let attachDelete = UIButton()
            attachDelete.setImage(imageDelete, for: UIControlState())
            attachDelete.addTarget(self, action: #selector(ChatController.deleteSelectedChat), for: UIControlEvents.touchDown)
            attachDelete.frame=CGRect(x: 0, y: 0, width: 20, height: 20)
            let barAttachDelete = UIBarButtonItem(customView: attachDelete)
            self.navigationItem.setRightBarButtonItems([barAttach, fixedSpace, barAttachDelete], animated: true)
        }
    }
    
    var paramsReply = 0
    func replyTapped(){
        self.viewReply.isHidden = false
        self.conHeightViewReply.constant = 55
        self.viewSeparator.isHidden = true
        
        var nameReply = ""
        let idSelected = selectedDataReply.fromId
        if idSelected != self.fromId {
            let id = self.idFriend.index(of: idSelected!)
            let data = self.nameFriend[id!]
            nameReply = data
        }else{
            nameReply = "You"
        }
        self.lblNameReply.text = nameReply
        let type = selectedDataReply.mediaType
        var content = ""
        //if type == "File" {
        content = selectedDataReply.mediaName!
        self.imgContentReply.image = UIImage(named: "ico_media_file")
        //}
        if type == "" || type == "Reply" {
            content = selectedDataReply.text!
            self.imgContentReply.image = nil
        }
        if type == "Image" {
            if selectedDataReply.text != "" {
                content = selectedDataReply.text!
            }else{
                content = "Photo"
            }
            let imgUrl = URL(string: selectedDataReply.imageUrl!)!
            self.imgContentReply.setImage(withUrl: imgUrl)
        }
        if type == "Video" {
            content = selectedDataReply.mediaName!
            let imgUrl = URL(string: selectedDataReply.imageUrl!)!
            self.imgContentReply.setImage(withUrl: imgUrl)
        }
        if type == "Audio"{
            print("file")
            content = selectedDataReply.mediaName!
            self.imgContentReply.image = UIImage(named: "ico_media_music")
        }
        if type == "Voice" {
            content = "Voice Message"
            self.imgContentReply.image = UIImage(named: "ico_record")
        }
        if type == "Location" {
            if selectedDataReply.nameAdrress != "" {
                content = selectedDataReply.nameAdrress!
            }else{
                content = "Location"
            }
            self.imgContentReply.image = UIImage(named: "ico_chat_location")
        }
        self.lblContentReply.text = content
        
        //Balik keadaan semula
        self.paramsLongPress = 0
        selectedCellReply.viewIndicatorSelectedCell.isHidden = true
        
        let image = UIImage(named: "ico_attachment")
        let attach = UIButton()
        attach.setImage(image, for: UIControlState())
        attach.addTarget(self, action: #selector(self.attachTapped), for: UIControlEvents.touchDown)
        attach.frame=CGRect(x: 0, y: 0, width: 26, height: 15)
        let barAttach = UIBarButtonItem(customView: attach)
        self.navigationItem.setRightBarButtonItems([barAttach], animated: true)
        
        self.paramsReply = 1
    }
    
    func cancelReply(_ sender: UIGestureRecognizer){
        self.viewReply.isHidden = true
        self.conHeightViewReply.constant = 0
        self.viewSeparator.isHidden = false
        
        self.idChatReply = ""
        self.paramsReply = 0
    }
    
    func tapOnReplyView(_ sender: UIGestureRecognizer){
        let cell = sender.view!.superview!.superview!.superview!.superview as! ChatGamesCell
        let indexPath = self.tbChat!.indexPath(for: cell)
        let data = messages[indexPath!.row]
        let keyMsg = data.keyReply
        
        if let indexTo = messages.index(where: {$0.idChat == keyMsg}) {
            let indexPath = IndexPath(item: indexTo, section: 0)
            self.tbChat!.scrollToRow(at: indexPath, at: .top, animated: true)
            
            DispatchQueue.main.async(execute: {
                if let cellIf = self.tbChat!.cellForRow(at: indexPath) {
                    let cellTo = cellIf as! ChatGamesCell
                    cellTo.viewIndicatorSelectedCell.isHidden = false
                    cellTo.viewIndicatorSelectedCell.backgroundColor = UIColor(red: 63, green: 157, blue: 247).withAlphaComponent(0.7)
                    UIView.animate(withDuration: 3.5, animations: {
                        cellTo.viewIndicatorSelectedCell.alpha = 0.0
                    })
                }
            })
        }
    }
    
    // Handle Emoji
    func handleEmotTap(_ sender: UITapGestureRecognizer){
        print("ga bisa?")
        self.txtInput.resignFirstResponder()
        if statusEmot == 0 {
            // Show Emot keyboard
            let keyboardRect = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.view.frame.size.width), height: CGFloat(216))
            let emojiKeyboardView = AGEmojiKeyboardView(frame: keyboardRect, dataSource: self)
            emojiKeyboardView?.autoresizingMask = .flexibleHeight
            emojiKeyboardView?.delegate = self
            emojiKeyboardView?.tintColor = UIColor.lightGray
            self.txtInput.inputView = emojiKeyboardView
            self.statusEmot = 1
            self.imgEmot.image = UIImage(named: "ico_keyboard")
        }else{
            // Show standard keyboard
            self.txtInput.inputView = nil
            self.txtInput.reloadInputViews()
            self.statusEmot = 0
            self.imgEmot.image = UIImage(named: "ico_insert_emot")
        }
        self.txtInput.becomeFirstResponder()
    }
    
    //AGEmojiKeyboard
    func emojiKeyBoardView(_ emojiKeyBoardView: AGEmojiKeyboardView!, didUseEmoji emoji: String!) {
        if let range = self.txtInput.selectedTextRange {
            if range.start == range.end {
                self.txtInput.replace(range, withText: emoji)
            }
        }
        // Change Send Button
        self.btnSend.setBackgroundImage(UIImage(named: "ico_send_selected"), for: .normal)
    }
    
    func emojiKeyBoardViewDidPressBackSpace(_ emojiKeyBoardView: AGEmojiKeyboardView!) {
        let currentText = self.txtInput.text!
        if currentText != "" {
            self.txtInput.text = currentText.substring(to: currentText.index(before: currentText.endIndex))
        }
        let totalChar = currentText.characters.count
        if totalChar == 1 {
            // Change Send Button
            if self.txtInput.text?.isEmpty ?? true {
                self.btnSend.setBackgroundImage(UIImage(named: "ico_send_disable"), for: .normal)
            }
        }
    }
    
    func emojiKeyboardView(_ emojiKeyboardView: AGEmojiKeyboardView!, imageForNonSelectedCategory category: AGEmojiKeyboardViewCategoryImage) -> UIImage! {
        let img: UIImage? = self.storeImageEmot().withRenderingMode(.alwaysOriginal)
        img?.withRenderingMode(.alwaysOriginal)
        return img
    }
    
    func emojiKeyboardView(_ emojiKeyboardView: AGEmojiKeyboardView!, imageForSelectedCategory category: AGEmojiKeyboardViewCategoryImage) -> UIImage! {
        let img: UIImage? = self.storeImageEmot().withRenderingMode(.alwaysOriginal)
        img?.withRenderingMode(.alwaysOriginal)
        emojiKeyboardView.contentMode = .scaleToFill
        return img?.withRenderingMode(.alwaysOriginal)
    }
    
    public func backSpaceButtonImage(for emojiKeyboardView: AGEmojiKeyboardView!) -> UIImage! {
        let image = UIImage(named: "ico_emot_backspace")
        emojiKeyboardView.contentMode = .scaleToFill
        return image
    }
    
    //Image Keyboard store
    var imgIndex = 0
    func storeImageEmot() -> UIImage {
        self.imgIndex += 1
        let strImage = "ico_emot_cat_" + String(self.imgIndex)
        if let img: UIImage = UIImage(named: strImage) {
            img.withRenderingMode(.alwaysOriginal)
            return img
        }else{
            let img = UIImage()
            return img
        }
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        // Mention
        let scheme = URL.scheme!
        print(URL)
        
        switch scheme {
        case "mention" :
            let nsText = textView.text!
            let words = nsText.components(separatedBy: CharacterSet.whitespacesAndNewlines)
            var strMention = ""
            var wordArray = [String]()
            for word in words {
                wordArray.append(word)
                if word.hasPrefix("@") {
                    strMention = word as String
                }
            }
            let indexNameMention = wordArray.index(of: strMention)
            let nameMention = wordArray[indexNameMention!]
            let nameOne = nameMention.replacingOccurrences(of: "@", with: "")
            let countTextView = wordArray.count - 1
            let sess = try! Realm().objects(session.self).first!
            let idGroup = sess.id_group
            if indexNameMention! < countTextView {
                let searchName = try! Realm().objects(listMember.self).filter("name CONTAINS[c] %@ AND id_group = %@", nameOne, idGroup)
                if searchName.count == 1 {
                    idFriendMentionSelected = searchName[0].id_member
                    self.performSegue(withIdentifier: "showFriendProfile", sender: self)
                }else{
                    let afterName = wordArray[indexNameMention! + 1]
                    let nameTwo = nameOne + " " + afterName
                    let searchNameTwo = try! Realm().objects(listMember.self).filter("name CONTAINS[c] %@ AND id_group = %@", nameTwo, idGroup)
                    if searchNameTwo.count == 1 {
                        idFriendMentionSelected = searchNameTwo[0].id_member
                        self.performSegue(withIdentifier: "showFriendProfile", sender: self)
                    }else{
                        let afterNameThree = wordArray[indexNameMention! + 2]
                        let nameThree = nameOne + " " + afterName + " " + afterNameThree
                        let searchNameThree = try! Realm().objects(listMember.self).filter("name CONTAINS[c] %@ AND id_group = %@", nameThree, idGroup)
                        idFriendMentionSelected = searchNameThree[0].id_member
                        self.performSegue(withIdentifier: "showFriendProfile", sender: self)
                    }
                }
            }
        default:
            print("just a regular url")
        }
        
        return true
    }
    
    //TextView
    func textViewDidBeginEditing(_ textView: UITextView) {
        if self.txtInput.textColor == UIColor.gray {
            self.txtInput.text = nil
            self.txtInput.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.statusTyping(status: 0) // not typing
        
        if self.txtInput.text!.isEmpty {
            self.txtInput.text = "Write something here..."
            self.txtInput.textColor = UIColor.gray
        }
    }
    
    //typing
    func textViewDidChange(_ textView: UITextView) {
        self.statusTyping(status: 1) // is typing
        
        self.btnSend.setBackgroundImage(UIImage(named: "ico_send_selected"), for: .normal)
        self.btnSend.isHidden = false
        self.btnVoice.isHidden = true
        self.imgCamera.isHidden = true
        if self.txtInput.text?.isEmpty ?? true {
            self.btnSend.setBackgroundImage(UIImage(named: "ico_send_disable"), for: .normal)
            self.btnSend.isHidden = true
            self.btnVoice.isHidden = false
            self.imgCamera.isHidden = false
        }
        
        //Mention
        let nsText:NSString = self.txtInput.text as NSString
        let words:[String] = nsText.components(separatedBy: CharacterSet.whitespacesAndNewlines)
        let wordCount = words.count
        let wordParam = wordCount - 1
        let word = words[wordParam]
        var wordSearch: String! = ""
        if word.hasPrefix("@") {
            wordSearch = word.replacingOccurrences(of: "@", with: "")
            
            //Data Mention
            let sess = try! Realm().objects(session.self).first!
            let idGroup = sess.id_group
            let modelsMention = try! Realm().objects(listMember.self).filter("name CONTAINS[c] %@ AND id_group = %@", wordSearch, idGroup)
            dataMention.removeAll()
            dataMentionAva.removeAll()
            
            //Show Mention
            for model in modelsMention {
                dataMention.append(model.name)
                dataMentionAva.append(model.avatar)
            }
            if dataMention.isEmpty == true {
                list.hide()
            }else{
                setupDropDowns()
                list.show()
            }
        }else{
            list.hide()
        }
    }
    
    func statusTyping(status: Int){
        let userMessagesRef = FIRDatabase.database().reference().child("activity").child(self.toId).child(self.fromId)
        
        userMessagesRef.removeValue()
        if status == 1 {
            userMessagesRef.updateChildValues(["typing": status])
        }else{
            userMessagesRef.updateChildValues(["typing": status])
        }
    }
    
    func checkingTyping(){
        let activityRef = FIRDatabase.database().reference().child("activity").child(self.fromId).child(self.toId)
        activityRef.observe(.childAdded) { (snapshot: FIRDataSnapshot!) in
            let status = snapshot.value! as! Int
            if status == 1 {
                print("is typing")
                self.navigationItem.titleView = Config().setTitleNavBar(self.titleNav, subtitle: "Is Typing ...")
            }else{
                print("not typing")
                self.navigationItem.titleView = nil
                self.navTitleDefault()
            }
        }
    }
    
    // DropDown Mention
    let list = DropDown()
    lazy var dropDowns: [DropDown] = { return [ self.list ] }()
    var dataMention = [""]
    var dataMentionAva = [""]
    func setupDropDowns() {
        let widthLayout: CGFloat = self.view.frame.width - 66
        list.anchorView = self.viewInput
        list.width = widthLayout
        list.cornerRadius = 0
        list.direction = .top
        list.topOffset = CGPoint(x: 16, y: 0 - self.viewInput.bounds.height)
        list.dataSource = dataMention
        
        let nsText:NSString = self.txtInput.text as NSString
        let words = nsText.components(separatedBy: CharacterSet.whitespacesAndNewlines)
        let wordCount = words.count
        let wordParam = wordCount - 1
        let word = words[wordParam]
        var input = ""
        if word as String == "@" {
            input = String(self.txtInput.text.characters.dropLast())
            print(input)
        }else{
            input = self.txtInput.text.replacingOccurrences(of: word as String, with: "")
        }
        list.selectionAction = { [unowned self] (index, item) in
            print(input)
            print(item)
            self.txtInput.text = input + "@" + item
        }
        
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().textFont = UIFont.systemFont(ofSize: 14)
        DropDown.appearance().cellHeight = 45
        list.cellNib = UINib(nibName: "CellDropDown", bundle: nil)
        list.customCellConfiguration = { (index , item , cell) -> Void in
            guard let cell = cell as? CellDropDown else { return }
            cell.imgAva.loadImageUsingCacheWithUrlString(self.dataMentionAva[index])
            cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
            cell.imgAva.clipsToBounds = true
        }
    }
    
    //Handle Keyboard
    override var canBecomeFirstResponder : Bool {
        return true
    }
    
    func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        //self.statusTyping(status: 0) // not typing
        NotificationCenter.default.removeObserver(self)
    }
    
    func handleKeyboardDidShow() {
        if messages.count > 0 {
            let indexPath = IndexPath(item: messages.count - 1, section: 0)
            self.tbChat.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    func handleKeyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if view.frame.origin.y == 0 {
            self.conBottomViewInput.constant = keyboardSize!.height
        }
    }
    
    func handleKeyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y < 0 {
                self.conBottomViewInput.constant = 0
            }
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    var arraySendImage = [Data]()
    var arraySendCaptionImage = [String]()
    var arrayNameImage = [String]()
    var iFromSendImage = 0
    var indicatorFromSendImage = 0
    @IBAction func backToChating(_ segue: UIStoryboardSegue){
        self.iFromSendImage = 0
        self.conBottomViewInput.constant = 0
        if indicatorFromSendImage == 1 {
            let imgData = self.arraySendImage[self.iFromSendImage]
            let selectedImage = UIImage(data: imgData)
            let nameImage = arrayNameImage[self.iFromSendImage]
            let sendCaption = arraySendCaptionImage[self.iFromSendImage]
            self.iFromSendImage += 1
            //Send media data to server
            if paramsKindChat == "group"{
                self.nameForSaveMedia = nameImage
                self.typeForSaveMedia = "1" //1:image 2:video 3:audio 4:document
                self.fromIdForSaveMedia = self.fromId
                self.makeSerializeSaveMedia()
            }
            uploadToFirebaseStorageUsingImage(selectedImage!, completion: { (imageUrl, metadata) in
                self.sendMessageWithImageUrl(imageUrl, image: selectedImage!, metadata: metadata, caption: sendCaption, imageName: nameImage)
            })
        }
    }
}
