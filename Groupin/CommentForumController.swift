//
//  CommentForumController.swift
//  Groupin
//
//  Created by Macbook pro on 9/8/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class CommentForumController: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tbComment: UITableView!
    @IBOutlet weak var txtComment: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    
    @IBOutlet weak var conSpaceBotTxtComment: NSLayoutConstraint!
    
    var idUser = 0
    var token = ""
    var idThread = ""
    
    var idThreadReply = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtComment.delegate = self
        self.tbComment.delegate = self
        self.tbComment.dataSource = self
        self.navigationItem.title = "Comments"
        
//        self.tbComment.rowHeight = UITableViewAutomaticDimension
//        self.tbComment.estimatedRowHeight = 30.0
        
        
        
        self.loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.loadData()
    }
    
    func loadData(){
        //Session
        let dataSession = try! Realm().objects(session.self).first
        self.idUser = dataSession!.id
        self.token = dataSession!.token
        
        makeSerialize()
        
        NotificationCenter.default.addObserver(self, selector: #selector(SetProfileController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SetProfileController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_thread" : self.idThread as AnyObject,
            "id_user" : self.idUser as AnyObject,
            "comment" : self.txtComment.text! as AnyObject,
            "id_thread_reply" : self.idThreadReply as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            if self.dari == "" {
                getData(base64Encoded)
            }else if self.dari == "send" {
                postData(base64Encoded)
            }else if self.dari == "reply" {
                getDataReply(base64Encoded)
            }else if self.dari == "sendReply" {
                postDataReply(base64Encoded)
            }
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    func getData(_ message: String){
        let urlString = Config().urlThread + "list_comment/0/50"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)["msg"]
                print(self.jsonData)
                if self.jsonData.count > 0 {
                    self.paramsJson = 1
                    self.idComment.removeAll()
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.tbComment.reloadData()
        }
    }
    
    func getDataReply(_ message: String){
        let urlString = Config().urlThread + "list_comment_reply/0/50"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)["msg"]
                print(self.jsonData)
                if self.jsonData.count > 0 {
                    self.paramsJson = 1
                    self.idComment.removeAll()
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.tbComment.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.paramsJson == 1 {
            return self.jsonData.count
        }
        return 0
    }
    
    var idComment = [String]()
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCommentCell", for: indexPath) as! ListGeneralCell
        
        if self.paramsJson == 1 {
            for (key, _) in self.jsonData {
                self.idComment.append(key)
            }
            let data = jsonData[indexPath.row]
            let nameCommenter = data["user"]["name"].string!
            let dataComment = data["comment"].string!
            let date = Config().timeElapsed(data["date"].string!)
            
            let strComment = nameCommenter + " : " + dataComment + " (" + date + ")"
            let lengStrName = nameCommenter.characters.count
            let lengStrIsi = dataComment.characters.count
            let lengStrDate = date.characters.count
            
            let myMutableString = NSMutableAttributedString(string: strComment)
            myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 63, green: 157, blue: 247), range: NSRange(location:0,length:lengStrName))
            myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location:lengStrName + lengStrIsi + 4,length:lengStrDate+2))
            cell.lblTitle.attributedText = myMutableString
            
            let width = cell.lblTitle.frame.width
            let heightTitle = Config().estimateFrameForText(strComment, width: Int(width))
            print(heightTitle.height + 19)
            cell.heightTitle.constant = heightTitle.height + 19
        }
        
        return cell
    }
    
    var idComentSelected = ""
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        idComentSelected = idComment[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let data = jsonData[indexPath.row]
        let nameCommenter = data["user"]["name"].string!
        let dataComment = data["comment"].string!
        let date = Config().timeElapsed(data["date"].string!)
        
        let strComment = nameCommenter + " : " + dataComment + " (" + date + ")"
        
        let width = self.tbComment.frame.width - 8
        let heightTitle = Config().estimateFrameForText(strComment, width: Int(width))
        let height = heightTitle.height + 8
        
        return CGFloat(height)
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
    
    
    var dari = ""
    @IBAction func btnSendTapped(_ sender: AnyObject) {
        if self.dari == "reply" {
            self.dari = "sendReply"
        }else{
            self.dari = "send"
        }
        makeSerialize()
    }
    
    func postData(_ message: String){
        let urlString = Config().urlThread + "add_comment"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)["msg"]
                print(self.jsonData)
                if self.jsonData.count > 0 {
                    self.paramsJson = 1
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.txtComment.text = ""
                self.dari = ""
                self.makeSerialize()
        }
    }
    
    func postDataReply(_ message: String){
        let urlString = Config().urlThread + "add_comment_reply"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)["msg"]
                print(self.jsonData)
                if self.jsonData.count > 0 {
                    self.paramsJson = 1
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.txtComment.text = ""
                self.dari = "reply"
                self.makeSerialize()
        }
    }
    
    var activeField: UITextField?
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            let possKeyboard = self.view.frame.height - keyboardSize!.height - activeField!.frame.height
            if activeField?.frame.origin.y > possKeyboard {
                self.conSpaceBotTxtComment.constant = keyboardSize!.height + 8
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.conSpaceBotTxtComment.constant = 8
    }
    
}
