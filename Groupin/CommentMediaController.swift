//
//  CommentMediaController.swift
//  Groupin
//
//  Created by Macbook pro on 9/8/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class CommentMediaController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var tbComment: UITableView!
    @IBOutlet weak var txtComment: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    
    @IBOutlet weak var botViewContent: NSLayoutConstraint!
    
    var idUser = 0
    var token = ""
    var idMedia = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showAnimate()
        let sess = try! Realm().objects(session.self).first
        idUser = sess!.id
        token = sess!.token
        
        self.txtComment.delegate = self
        self.tbComment.delegate = self
        self.tbComment.dataSource = self
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        viewMain.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        viewMain.layer.cornerRadius = 5
        
        //Click to close
        let tapBack = UITapGestureRecognizer(target: self, action: #selector(CommentMediaController.closePopup(_:)))
        tapBack.numberOfTapsRequired = 1
        viewMain.isUserInteractionEnabled = true
        viewMain.addGestureRecognizer(tapBack)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CommentMediaController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CommentMediaController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //self.makeSerialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.makeSerialize()
    }
    
    func scrollToLast(){
        //Scroll to last chat
        print("kesini dulu")
        DispatchQueue.main.async(execute: {
            if self.paramsJson == 1 {
                print("terus ini")
                if self.jsonData.count > 0 {
                    print("harusnya sih kesini")
                    self.tbComment.reloadData()
                    let indexPath = IndexPath(item: self.jsonData.count - 1, section: 0)
                    self.tbComment.scrollToRow(at: indexPath, at: .bottom, animated: false)
                }
            }
        })
    }
    
    @IBAction func btnCloaseTapped(_ sender: AnyObject) {
        self.removeAnimate()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_media" : self.idMedia as AnyObject,
            "id_user" : self.idUser as AnyObject,
            "comment" : self.txtComment.text! as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        print(arrayData)
        makeBase64(jsonString!)
    }
    
    var dari = ""
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            if dari == "" {
                getData(base64Encoded)
            }else{
                postData(base64Encoded)
            }
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    func getData(_ message: String){
        let urlString = Config().urlMedia + "list_comment/0/50"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)["msg"]
                print(self.jsonData)
                if self.jsonData.count > 0 {
                    self.paramsJson = 1
                    self.idComment.removeAll()
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.tbComment.reloadData()
                self.scrollToLast()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.paramsJson == 1 {
            return self.jsonData.count
        }
        return 0
    }
    
    var idComment = [String]()
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCommentMediaCell", for: indexPath) as! ListGeneralCell
        
        if self.paramsJson == 1 {
            for (key, _) in self.jsonData {
                self.idComment.append(key)
            }
            let data = jsonData[indexPath.row]
            
            if let imgUrl = data["user"]["thumbnail"].string {
                cell.imgAva.loadImageUsingCacheWithUrlString(imgUrl)
            }else{
                cell.imgAva.image = UIImage(named: "default-avatar")
            }
            cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
            cell.imgAva.clipsToBounds = true
            
            cell.lblTitle.text = data["user"]["name"].string!
            cell.lblSubTitle.text = data["comment"].string!
            let date = Config().timeElapsed(data["date_create"].string!)
            cell.lblTime.text = date
        }
        
        return cell
    }
    
    var idComentSelected = ""
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        idComentSelected = idComment[indexPath.row]
    }
    
    @IBAction func btnSendTapped(_ sender: AnyObject) {
        self.dari = "sent"
        makeSerialize()
    }
    
    func postData(_ message: String){
        let urlString = Config().urlMedia + "add_comment"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)["msg"]
                print(self.jsonData)
                if self.jsonData.count > 0 {
                    self.paramsJson = 1
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.txtComment.text = ""
                self.dari = ""
                self.makeSerialize()
                self.scrollToLast()
        }
    }
    
    func closePopup(_ sender: UITapGestureRecognizer){
        self.removeAnimate()
    }
    
    func showAnimate(){
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0.0
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            }, completion: {(finished: Bool) in
                if(finished){
                    self.view.removeFromSuperview()
                }
        })
        
        self.updateDataParent()
    }
    
    func updateDataParent(){
        let vc = self.parent as! ViewMediaController
        vc.makeSerializeComment()
    }
    
    var activeField: UITextField?
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            let possKeyboard = self.view.frame.height - keyboardSize!.height - activeField!.frame.height
            if activeField?.frame.origin.y > possKeyboard {
//                if view.frame.origin.y == 0{
//                    self.view.frame.origin.y -= keyboardSize!.height
//                }
                self.botViewContent.constant = keyboardSize!.height
                //self.heightViewContent.constant -= keyboardSize!.height / 2
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if view.frame.origin.y != 0 {
//                self.view.frame.origin.y += keyboardSize.height
//            }
            if self.botViewContent.constant > 50 {
                self.botViewContent.constant = 50
                //self.heightViewContent.constant += keyboardSize.height
            }
        }
    }
}
