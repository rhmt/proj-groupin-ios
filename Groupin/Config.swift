//
//  Config.swift
//  Groupin
//
//  Created by Macbook pro on 10/15/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import RealmSwift
import Photos

class Config: UIViewController {
    var urlMain:String = "http://cyberlabs.asia/portofolio/groupinstorage/gate/"
    var urlImage:String = "http://cyberlabs.asia/portofolio/groupinstorage/uploads/avatar/"
    var urlUser:String = "http://cyberlabs.asia/portofolio/groupinstorage/gate/user/"
    var urlGroup:String = "http://cyberlabs.asia/portofolio/groupinstorage/gate/group/"
    var urlRoom:String = "http://cyberlabs.asia/portofolio/groupinstorage/gate/room/"
    var urlUpdate:String = "http://cyberlabs.asia/portofolio/groupinstorage/gate/update/"
    var urlPages:String = "http://cyberlabs.asia/portofolio/groupinstorage/gate/pages/"
    var urlEvent:String = "http://cyberlabs.asia/portofolio/groupinstorage/gate/event/"
    var urlThread:String = "http://cyberlabs.asia/portofolio/groupinstorage/gate/thread/"
    var urlMedia:String = "http://cyberlabs.asia/portofolio/groupinstorage/gate/media/"
    var urlPayment:String = "http://cyberlabs.asia/portofolio/groupinstorage/gate/payment/"
    
    func md5(string: String) -> String {
        var digest = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
        if let data = string.data(using: String.Encoding.utf8) {
            CC_MD5((data as NSData).bytes, CC_LONG(data.count), &digest)
        }
        
        var digestHex = ""
        for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
            digestHex += String(format: "%02x", digest[index])
        }
        
        return digestHex
    }
    
    func estimateFrameForText(_ text: String, width: Int) -> CGRect {
        let size = CGSize(width: width, height: 10000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)], context: nil)
    }
    
    func nameMediaFile(_ nameFile: String, idUser: String) -> String {
        var dataNameFile = nameFile
        
        let currentDate = String(Int(Date().timeIntervalSince1970))
        let nameFileSave = nameFile.replacingOccurrences(of: " ", with: "")
        dataNameFile = currentDate + "_" + idUser + "_" + nameFileSave
        
        return dataNameFile
    }
    
    func getMediaPath() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory = paths[0]
        let masterPath = documentsDirectory.appending("/Media")
        
        return masterPath as NSString
    }
    
    func ResizeImage(_ image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func isValidEmail(_ text:String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: text)
    }
    
    func timeElapsed(_ time: String) -> String {
        let userCalendar = Calendar.current
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let getCalStr = dateFormatter.date(from: time)
        
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let dateNow = dateFormatter.string(from: Date())
        let dateCal = dateFormatter.string(from: getCalStr!)
        
        dateFormatter.dateFormat = "HH:mm:ss"
        let getTimeStr = dateFormatter.string(from: getCalStr!)
        let startTime = dateFormatter.date(from: getTimeStr)
        let timeNowStr = dateFormatter.string(from: Date())
        let endDate = dateFormatter.date(from: timeNowStr)
        
        var returnValue = ""
        if dateNow == dateCal {
            let hourMinuteComponents: NSCalendar.Unit = [.hour, .minute]
            let timeDifference = (userCalendar as NSCalendar).components(
                hourMinuteComponents,
                from: startTime!,
                to: endDate!,
                options: [])
            
            let diffTime = timeDifference.hour!
            if diffTime > 0 {
                returnValue = String(describing: diffTime) + " hour ago"
            }else{
                if timeDifference.minute! > 0 {
                    returnValue = String(describing: timeDifference.minute!) + " minutes ago"
                }else{
                    returnValue = "Just Now"
                }
            }
        }else{
            dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
            let timeStr = dateFormatter.date(from: time)
            dateFormatter.dateFormat = "MMM dd, YYYY hh:mm a"
            returnValue = dateFormatter.string(from: timeStr!)
        }
        
        return returnValue
    }
    
    func convertDate(_ data: String, from: String, to: String) -> String{
        let formatFrom = DateFormatter()
        formatFrom.dateFormat = from
        let formatTo = DateFormatter()
        formatTo.dateFormat = to
        
        let fromToDate = formatFrom.date(from: data)
        let result = formatTo.string(from: fromToDate!)
        
        return result
    }
    
    func CekLogin(_ view: UIViewController){
        let id = try! Realm().objects(session.self).first!
        let idUser = id.id
        let token = id.token
        let uid = UIDevice.current.identifierForVendor!.uuidString
        print(uid)
        print(idUser)
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : idUser as AnyObject,
            "device_id" : uid as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        let utf8str = jsonString!.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            let urlString = Config().urlUser + "cek_login_other_device"
            let token = token
            var parameters = [String:AnyObject]()
            parameters = ["token" : token as AnyObject, "msg": base64Encoded as AnyObject]
            Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
                if let value = response.result.value {
                    let jsonData = JSON(value)
                    if jsonData["msg"].string! == "2" {
                        let alert = UIAlertController(title: "Info", message: "Your account has been login in another device", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in self.BackToLogin(view)
                            
                        }))
                        view.present(alert, animated: true, completion: nil)
                    }
                }else{
                    print("Something Went Wrong..")
                }
                }.responseData { Response in
                    
            }
        }
    }
    
    //Back to Login
    func BackToLogin(_ view: UIViewController){
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        let demo = intro()
        demo.intro = 1
        DBHelper.insert(demo)
        
        let login = view.storyboard?.instantiateViewController(withIdentifier: "showLogin") as! LoginController
        view.present(login, animated: true, completion: nil)
    }
    
    //Get posisi character
    func getPos(_ data: String, str: String) -> Int{
        let nsData:NSString = data as NSString
        let wordsData = nsData.components(separatedBy: CharacterSet.whitespacesAndNewlines)
        var arrayData = [String]()
        for word in wordsData {
            arrayData.append(word as String)
        }
        
        let nsStr:NSString = str as NSString
        let wordsStr = nsStr.components(separatedBy: CharacterSet.whitespacesAndNewlines)
        var arrayStr = [String]()
        for word in wordsStr {
            arrayStr.append(word as String)
        }
        
        let posData = arrayData.index(of: arrayStr[0])
        let lastData = Int(posData!)
        
        var dataStr = ""
        if lastData > 0 {
            var i = 0
            repeat{
                dataStr += arrayData[i] + " "
                i += 1
            }while lastData > i
        }
        
        let finalPoss = dataStr.characters.count
        return finalPoss
    }
    
    //Checking access photos
    func checkAccessPhotos() -> Bool{
        print("photos")
        let status = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
            //  Permission already Authorized
            print("authorized")
            return true
        }else{
            // Permission Denied
            return false
        }
    }
    
    //Checking access camera
    func checkAccessCamera() -> Bool{
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized{
            // Already Authorized
            return true
        }else{
            return false
        }
        
    }
    
    //Alert untuk allow access privacy permission
    func alertForAllowAccess(_ view: UIViewController, msg: String){
        let alertController = UIAlertController (title: "", message: msg, preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    // Fallback on earlier versions
                    UIApplication.shared.openURL(settingsUrl)
                }
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        view.present(alertController, animated: true, completion: nil)
    }
    
    func randomColor() -> UIColor{
        let red = CGFloat(drand48())
        let green = CGFloat(drand48())
        let blue = CGFloat(drand48())
        return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
    func thumbnailImageForFileUrl(_ fileUrl: URL) -> UIImage? {
        let asset = AVAsset(url: fileUrl)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        
        do {
            let thumbnailCGImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60), actualTime: nil)
            return UIImage(cgImage: thumbnailCGImage)
            
        } catch let err {
            print(err)
        }
        
        return nil
    }
    
    //Indikator Notif
    func notifUpdates(_ view: UIViewController){
        let id = try! Realm().objects(session.self).first!
        let idUser = id.id
        let token = id.token
        
        var lastUpdate = ""
        let last = try! Realm().objects(tb_updates.self)
        if last.count > 0 {
            lastUpdate = last.last!.date_update
        }else{
            let dateJoin = try! Realm().objects(tb_user.self).first!
            lastUpdate = dateJoin.tgl_join
        }
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : idUser as AnyObject,
            "datetime" : lastUpdate as AnyObject
        ]
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        var message = ""
        let utf8str = jsonString!.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        { message = base64Encoded }
        
        let urlString = Config().urlUpdate + "get_update"
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)["msg"]
                print("Notif updates")
                print(jsonData)
                if jsonData.count > 0 {
                    let tabArray = view.tabBarController?.tabBar.items as NSArray!
                    let tabItem = tabArray?.object(at: 1) as! UITabBarItem
                    tabItem.badgeValue = "*"
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                
        }
    }
    
    func notifNotification(_ view: UIViewController){
        let data = try! Realm().objects(tb_notif.self).filter("is_read = %@", 0)
        if data.count > 0 {
            let tabArray = view.tabBarController?.tabBar.items as NSArray!
            let tabItem = tabArray?.object(at: 2) as! UITabBarItem
            tabItem.badgeValue = String(data.count)
            
            UIApplication.shared.applicationIconBadgeNumber += data.count
        }
    }
    
    func notifChatPersonal(_ view: UIViewController){
        let dataId = try! Realm().objects(tb_chat.self)
        for item in dataId {
            let str = item.from
            let charset = CharacterSet(charactersIn: "g")
            if str.lowercased().rangeOfCharacter(from: charset) == nil {
                let dataUnread = try! Realm().objects(tb_chat.self).filter("from = %@ AND is_read = %@", str, 0)
                let count = dataUnread.count
                if count > 0 {
                    let tabArray = view.tabBarController?.tabBar.items as NSArray!
                    let tabItem = tabArray?.object(at: 3) as! UITabBarItem
                    tabItem.badgeValue = "*"
                    return
                }
            }
        }
    }
    
    func setTitleNavBar(_ title:String, subtitle:String) -> UIView {
        let titleLabel = UILabel(frame: CGRect(x: 0, y: -2, width: 0, height: 0))
        
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
        titleLabel.text = title
        titleLabel.sizeToFit()
        
        let subtitleLabel = UILabel(frame: CGRect(x: 0, y: 18, width: 0, height: 0))
        subtitleLabel.backgroundColor = UIColor.clear
        subtitleLabel.textColor = UIColor.white
        subtitleLabel.font = UIFont.systemFont(ofSize: 12)
        subtitleLabel.text = subtitle
        subtitleLabel.sizeToFit()
        
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: max(titleLabel.frame.size.width, subtitleLabel.frame.size.width), height: 30))
        titleView.backgroundColor = UIColor.clear
        titleView.addSubview(titleLabel)
        titleView.addSubview(subtitleLabel)
        
        let widthDiff = subtitleLabel.frame.size.width - titleLabel.frame.size.width
        
        if widthDiff > 0 { // jika sub title lebih panjang
            var frame = titleLabel.frame
            frame.origin.x = widthDiff / 2
            titleLabel.frame = frame.integral
        } else { // jika title lebih panjang
            var frame = subtitleLabel.frame
            frame.origin.x = abs(widthDiff) / 2
            subtitleLabel.frame = frame.integral
        }
        
        return titleView
    }
}
