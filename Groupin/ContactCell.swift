//
//  ContactCell.swift
//  Groupin
//
//  Created by Macbook pro on 8/11/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class ContactCell: UITableViewCell {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblKet: UILabel!
    @IBOutlet weak var lblWaktu: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var btnReinvite: UIButton!
    @IBOutlet weak var viewIndicator: UIView!
    @IBOutlet weak var lblIndicator: UILabel!
    
    @IBOutlet weak var conHeightImgAva: NSLayoutConstraint!
    @IBOutlet weak var conHeightLblName: NSLayoutConstraint!
    @IBOutlet weak var conHeightLblWaktu: NSLayoutConstraint!
    
}
