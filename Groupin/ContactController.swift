//
//  ContactController.swift
//  Groupin
//
//  Created by Macbook pro on 8/11/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import Contacts
import SwiftyJSON
import Alamofire
import MessageUI
import ContactsUI
import MapleBacon
import XMSegmentedControl
import PhoneNumberKit

class ContactController: UIViewController, UITableViewDataSource, UITableViewDelegate, MFMessageComposeViewControllerDelegate, CNContactViewControllerDelegate, XMSegmentedControlDelegate, UISearchBarDelegate {

    @IBOutlet weak var viewSegment: XMSegmentedControl!
    @IBOutlet weak var tbFriends: UITableView!
    @IBOutlet weak var tbNotFriends: UITableView!
    @IBOutlet weak var tbContact: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var conViewSearch: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!
    
    //Friend
    var dataData = [listFriend]()
    var dataNumberFriend = [String]()
    var dataNumberFriendInContact = [String]()
    var filteredListFriend = [listFriend]()
    //------
    
    //Not Friend
    var dataIdNotFriend = [String]()
    var dataNameNotFriend = [String]()
    var dataPhoneNotFriend = [String]()
    var dataAvatarNotFriend = [String]()
    var filteredIdNotFriend = [String]()
    var filteredNameNotFriend = [String]()
    var filteredPhoneNotFriend = [String]()
    var filteredAvatarNotFriend = [String]()
    //----------
    
    //Contact
    var name = [String]()
    var number = [String]()
    var filteredNameContact = [String]()
    var filteredNumberContact = [String]()
    //-------
    
    var statusTab = 1
    
    var idUser = 0
    var token = ""
    let blue = UIColor(red: 63, green: 157, blue: 247)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = false
        
        //self.navigationController?.navigationBar.isTranslucent = false
        
        self.title = "Contacts"
        
        self.viewNoData.isHidden = true
        
//        self.tbFriends.rowHeight = UITableViewAutomaticDimension
//        self.tbFriends.estimatedRowHeight = 55.0
//        self.tbNotFriends.rowHeight = UITableViewAutomaticDimension
//        self.tbNotFriends.estimatedRowHeight = 55.0
//        self.tbContact.rowHeight = UITableViewAutomaticDimension
//        self.tbContact.estimatedRowHeight = 55.0
        
        //Default Table View
        self.tbFriends.isHidden = false
        self.tbNotFriends.isHidden = true
        self.tbContact.isHidden = true
        self.tbFriends.tableFooterView = UIView()
        self.tbNotFriends.tableFooterView = UIView()
        self.tbContact.tableFooterView = UIView()
        
        //Segment (Tabbar on top)
        self.viewSegment.delegate = self
        self.viewSegment.segmentTitle = ["Friends", "Not Friends", "Not Joined"]
        self.viewSegment.selectedItemHighlightStyle = .bottomEdge
        self.viewSegment.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
        self.viewSegment.highlightColor = UIColor.white
        self.viewSegment.tint = UIColor(red: 34, green: 112, blue: 186)
        self.viewSegment.highlightTint = UIColor.white
        
        //Search
        self.viewSearch.isHidden = true
        self.conViewSearch.constant = 1
        self.searchBar.delegate = self
        self.searchBar.placeholder = "Input search here..."
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        
        rightMenu()
        colorNav()
        
        // Contact
        tbContact.delegate = self
        tbContact.dataSource = self
        
        //Get Data from Session
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.token = idSession!.token
        
        //Get Phone Login then get country code
        let user = try! Realm().objects(tb_user.self).filter("id = %@", self.idUser).first!
        let phoneUser = user.phone
        do {
            let getPhoneNumber = try phoneNumberKit.parse(phoneUser)
            let getNumberCountryCode = getPhoneNumber.countryCode
            if let getRegionCode = phoneNumberKit.mainCountry(forCode:getNumberCountryCode) {
                self.regionCode = getRegionCode
                print(getRegionCode)
            }else{
                print("Invalid Code Phone User Number")
            }
        } catch {
            print("Invalid Phone User Number")
        }
        
        //contact device
        self.requestForAccess { (accessGranted) -> Void in
            print("aksess")
            self.findContacts()
        }
        self.makeSerializeSync()
        
        self.loadLocal()
        self.makeSerialize()
        
        // Search Bar Color
        UISearchBar.appearance().barTintColor = blue
        UISearchBar.appearance().tintColor = UIColor.white
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = blue
    }
    
    func loadLocal(){
        dataData = DBHelper.getAllFriend()
        
        //Inset data phone number to variable
        for data in dataData{
            self.dataNumberFriend.append(data.phone)
        }
        
        if self.paramsJson == 1 {
            self.paramsJson = 0
            self.tbFriends.reloadData()
            self.tbNotFriends.reloadData()
            self.tbContact.reloadData()
        }
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewDidDisappear(true)
//        self.tabBarController?.tabBar.isHidden = false
//    }
    
    func xmSegmentedControl(_ xmSegmentedControl: XMSegmentedControl, selectedSegment: Int) {
        if xmSegmentedControl == viewSegment {
            if selectedSegment == 0 {
                self.statusTab = 1
                self.tbFriends.isHidden = false
                self.tbNotFriends.isHidden = true
                self.tbContact.isHidden = true
                if self.dataData.count > 0{
                    self.tbFriends.isHidden = false
                    self.viewNoData.isHidden = true
                }else{
                    self.tbFriends.isHidden = true
                    self.viewNoData.isHidden = false
                }
                self.tbFriends.reloadData()
            }else if selectedSegment == 1 {
                self.statusTab = 2
                self.tbFriends.isHidden = true
                self.tbNotFriends.isHidden = false
                self.tbContact.isHidden = true
                if self.dataNameNotFriend.count > 0{
                    self.tbNotFriends.isHidden = false
                    self.viewNoData.isHidden = true
                }else{
                    self.tbNotFriends.isHidden = true
                    self.viewNoData.isHidden = false
                }
                self.tbNotFriends.reloadData()
            }else if selectedSegment == 2 {
                self.statusTab = 3
                self.tbFriends.isHidden = true
                self.tbNotFriends.isHidden = true
                self.tbContact.isHidden = false
                if self.name.count > 0{
                    self.tbContact.isHidden = false
                    self.viewNoData.isHidden = true
                }else{
                    self.tbContact.isHidden = true
                    self.viewNoData.isHidden = false
                }
                self.tbContact.reloadData()
            }
        }
    }
    
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idUser as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    var paramsJson = 0
    func getData(_ message: String){
        let urlString = Config().urlUser + "list_member_friend"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                
                let msg = jsonData["msg"]
                if msg.count > 0 {
                    self.paramsJson = 1
                    
                    for (key, _) in msg {
                        let modelUpdate = try! Realm().objects(listFriend.self).filter("id_friend = %@", Int(key)!).first
                        
                        if modelUpdate == nil {
                            let model = listFriend()
                            model.id_friend = Int(key)!
                            model.name = msg[key]["name"].string!
                            if let img = msg[key]["avatar"].string {
                                model.avatar = img
                            }else{
                                model.avatar = ""
                            }
                            model.msg_status = msg[key]["msg_status"].string!
                            model.phone = msg[key]["phone"].string!
                            DBHelper.insert(model)
                        }else{
                            try! Realm().write({
                                modelUpdate!.name = msg[key]["name"].string!
                                if let img = msg[key]["avatar"].string {
                                    modelUpdate!.avatar = img
                                }
                                modelUpdate!.msg_status = msg[key]["msg_status"].string!
                                modelUpdate!.phone = msg[key]["phone"].string!
                            })
                        }
                        
                        let phone = msg[key]["phone"].string!
                        if let inxNumber = self.number.index(of: phone) {
                            self.dataNumberFriendInContact.append(phone)
                            self.number.remove(at: inxNumber)
                            self.name.remove(at: inxNumber)
                            
                            //Insert to database
                            if let modelUpdateFriend = try! Realm().objects(listFriend.self).filter("phone = %@", phone).first {
                                try! Realm().write {
                                    modelUpdateFriend.is_contact = 1
                                }
                            }
                        }
                    }
                    self.loadLocal()
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showChating" {
            let chatController = segue.destination as! ChatingController
            let id = String(self.idFriend)
            chatController.titleNav = self.nameBarFriend
            chatController.toId = id
            chatController.idFriend.append(id)
            chatController.nameFriend.append(self.nameBarFriend)
            chatController.avaFriend.append(self.avaFriend)
            chatController.colorFriend.append(UIColor(red: 63, green: 157, blue: 247))
            chatController.paramsKindChat = "personal"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var ret = 0
        if tableView == tbFriends {
            if self.searchBar.text != "" {
                ret = self.filteredListFriend.count
            }else{
                ret = self.dataData.count
            }
        }
    
        if tableView == tbNotFriends {
            if self.searchBar.text != "" {
                ret = self.filteredNameNotFriend.count
            }else{
                ret = self.dataNameNotFriend.count
            }
        }
    
        if tableView == tbContact{
            if self.searchBar.text != "" {
                print("kesini")
                print(self.filteredNameContact.count)
                ret = self.filteredNameContact.count
            }else{
                ret = self.name.count
            }
        }
        return ret
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactCell
        
        if tableView == tbFriends {
            self.tbFriends.contentInset = UIEdgeInsetsMake(0,0,0,0)
            
            var data = listFriend()
            if self.searchBar.text != "" {
                data = self.filteredListFriend[indexPath.row]
            }else{
                data = self.dataData[indexPath.row]
            }
            
            
            if let imgUrl = URL(string: data.avatar) {
                cell.imgAva.setImage(withUrl: imgUrl)
            }else{
                cell.imgAva.image = UIImage(named: "default-avatar")
            }
            cell.lblName.text = data.name
            cell.lblKet.text = data.msg_status
            
            //Button
            cell.btnAdd.isHidden = false
            let phone = data.phone
            cell.btnAdd.isHidden = true
            if let _ = self.dataNumberFriendInContact.index(of: phone) {
                cell.btnAdd.isHidden = true
            }else{
                cell.btnAdd.isHidden = false
            }
            //Click untuk Ava
            cell.imgAva.tag = indexPath.row
            let tapPhoto = UITapGestureRecognizer(target: self, action: #selector(ContactController.fotoTapped(_:)))
            tapPhoto.numberOfTapsRequired = 1
            cell.imgAva.isUserInteractionEnabled = true
            cell.imgAva.addGestureRecognizer(tapPhoto)
        }else if tableView == tbNotFriends {
            self.tbNotFriends.contentInset = UIEdgeInsetsMake(0,0,0,0)
            let inx = indexPath.row
            var showAva = ""
            var showName = ""
            var showNumber = ""
            if self.searchBar.text != "" {
                showAva = self.filteredAvatarNotFriend[inx]
                showName = self.filteredNameNotFriend[inx]
                showNumber = self.filteredPhoneNotFriend[inx]
            }else{
                showAva = self.dataAvatarNotFriend[inx]
                showName = self.dataNameNotFriend[inx]
                showNumber = self.dataPhoneNotFriend[inx]
            }
            
            if showAva != "" {
                cell.imgAva.loadImageUsingCacheWithUrlString(showAva)
            }else{
                cell.imgAva.image = UIImage(named: "default-avatar")
            }
            
            cell.lblName.text = showName
            cell.lblKet.text = showNumber
            cell.btnAdd.isHidden = false
        }else if tableView == tbContact {
            self.tbContact.contentInset = UIEdgeInsetsMake(0,0,0,0)
//            let number = self.number[indexPath.row - countDataFriend]
//            let dataLocal = try! Realm().objects(tb_invite_contact.self)
//            if dataLocal.count > 0 {
//                let dataFilter = try! Realm().objects(tb_invite_contact.self).filter("phone == \(number)")
//                if dataFilter.count > 0 {
//                    cell.btnReinvite.isHidden = false
//                }
//            }else{
//                cell.btnInvite.isHidden = false
//            }
            cell.imgAva.image = UIImage(named: "default-avatar")
            let inx = indexPath.row
            var showName = ""
            var showNumber = ""
            if self.searchBar.text != "" {
                print("kesana")
                showName = self.filteredNameContact[inx]
                showNumber = self.filteredNumberContact[inx]
            }else{
                showName = self.name[inx]
                showNumber = self.number[inx]
            }
            cell.lblName.text = showName
            cell.lblKet.text = showNumber
            
            //Rounded Button
            cell.btnInvite.isHidden = false
            cell.btnInvite.backgroundColor = UIColor(red: 34, green: 112, blue: 186)
            cell.btnInvite.layer.cornerRadius = 2
            cell.btnInvite.layer.borderWidth = 0.5
            cell.btnInvite.layer.borderColor = UIColor.lightGray.cgColor
            cell.btnReinvite.setTitleColor(UIColor(red: 63, green: 157, blue: 247), for: UIControlState())
            cell.btnReinvite.layer.cornerRadius = 2
            cell.btnReinvite.layer.borderWidth = 1
            cell.btnReinvite.layer.borderColor = UIColor(red: 63, green: 157, blue: 247).cgColor
        }
        
        //Rounded Ava
        cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
        cell.imgAva.clipsToBounds = true
        
        return cell
    }
    
    var idFriend = 0
    var nameBarFriend = ""
    var avaFriend = ""
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if tableView == tbFriends {
            self.tabBarController?.tabBar.isHidden = true
            
            var data = listFriend()
            if self.searchBar.text != "" {
                data = self.filteredListFriend[indexPath.row]
            }else{
                data = self.dataData[indexPath.row]
            }
            
            self.idFriend = data.id_friend
            self.nameBarFriend = data.name
            self.avaFriend = data.avatar
            self.performSegue(withIdentifier: "showChating", sender: self)
        }else if tableView == tbNotFriends {
            print("Not friends")
        }else{
            print("Contact")
        }
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
    func fotoTapped(_ sender: UITapGestureRecognizer){
        let indexPath = sender.view?.tag
        let data = dataData[indexPath!]
        
        let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showMember") as! PopupMemberController
        self.addChildViewController(Popover)
        Popover.view.frame = self.view.frame
        Popover.idFriend = String(data.id_friend)
        self.view.addSubview(Popover.view)
        Popover.didMove(toParentViewController: self)
    }
    
    @IBAction func btnAddTapped(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview as! ContactCell
        let indexPath = tbFriends.indexPath(for: cell)
        let data = dataData[indexPath!.row]
        
        let alert = UIAlertController(title: "Confirmation", message: "Add " + data.name + " to your contact", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.jsonSelected = data
            print(self.jsonSelected)
            switch CNContactStore.authorizationStatus(for: .contacts){
            case .authorized:
                self.savePhoneNumber()
            case .notDetermined:
                self.store.requestAccess(for: .contacts){succeeded, err in
                    guard err == nil && succeeded else{
                        return
                    }
                    self.savePhoneNumber()
                }
            default:
                print("Cant save contact")
            }
            cell.btnAdd.isHidden = true
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    let store = CNContactStore()
    var jsonSelected = listFriend()
    func savePhoneNumber(){
        let contactData = CNMutableContact()
        
        //Image
        let dataImg = self.jsonSelected.avatar
        let imgUrl = URL(string: dataImg)
        let urlData = try? Data(contentsOf: imgUrl!)
        let imgUi = UIImage(data: urlData!)
        let imgData = UIImageJPEGRepresentation(imgUi!, 1.0)
        contactData.imageData = imgData!
        
        //name
        contactData.givenName = self.jsonSelected.name
        
        //phone
        let homePhone = CNLabeledValue(label: CNLabelPhoneNumberMobile,value: CNPhoneNumber(stringValue: self.jsonSelected.phone))
        contactData.phoneNumbers = [homePhone]
        
        //Save
        let request = CNSaveRequest()
        request.add(contactData, toContainerWithIdentifier: nil)
        do{
            try store.execute(request)
            print("Successfully added the contact")
            self.msgError = "Save to contact success"
            self.makeSerialize()
        } catch let err{
            print("Failed to save the contact. \(err)")
            self.msgError = "Failed to save the contact"
        }
        
        //change icon add to hidden
        self.dataNumberFriendInContact.append(self.jsonSelected.phone)
        self.tbFriends.reloadData()
        
        self.alertStatus()
    }
    
    var msgError = ""
    func alertStatus(){
        if msgError != "" {
            let alert = UIAlertController(title: "Info", message: msgError, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnAddFriendTapped(_ sender: Any) {
        print("add friend")
        
        let button = sender
        let view = (button as AnyObject).superview!
        let cell = view!.superview as! ContactCell
        let indexPath = tbNotFriends.indexPath(for: cell)
        self.notifIdNotFriend = self.dataIdNotFriend[indexPath!.row]
        self.notifNameNotFriend = self.dataNameNotFriend[indexPath!.row]
        self.notifAvatarNotFriend = self.dataAvatarNotFriend[indexPath!.row]
        
        // Loading
        alertLoading = UIAlertController(title: "Sending Request Friend", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        self.present(alertLoading, animated: true, completion: nil)
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.notifIdNotFriend as AnyObject,
            "id_login" : self.idUser as AnyObject
        ]
        
        let dataJson = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: dataJson, encoding: String.Encoding.utf8.rawValue)
        
        let utf8str = jsonString?.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            self.postDataRequestFriend(base64Encoded)
        }
    }
    
    var alertLoading = UIAlertController()
    func postDataRequestFriend(_ message: String){
        let urlString = Config().urlUpdate + "request_as_friend"
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                self.sendNotifForSendingRequest()
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
        }
    }
    
    var notifIdNotFriend = ""
    var notifNameNotFriend = ""
    var notifAvatarNotFriend = ""
    func sendNotifForSendingRequest(){
        print("tambah notif")
        let model = tb_notif()
        model.id_inviter = Int(self.notifIdNotFriend)!
        if self.notifAvatarNotFriend != "" {
            model.icon = self.notifAvatarNotFriend
        }else{
            model.icon = ""
        }
        model.title = "Sending Request Friend"
        
        let name = self.notifNameNotFriend
        model.body = "You have sent a friend request to " + name
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let date = dateFormatter.string(from: Date())
        model.date = date
        model.type = 6
        DBHelper.insert(model)
    }
    
    @IBAction func btnInviteTapped(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview as! ContactCell
        let indexPath = tbContact.indexPath(for: cell)
        let number = self.number[indexPath!.row - dataData.count]
        print(number)
//        self.jsonSelected = dataData[indexPath!.row]
        
        if !MFMessageComposeViewController.canSendText() {
            print("SMS services are not available")
        }else{
            let composeVC = MFMessageComposeViewController()
            composeVC.messageComposeDelegate = self
            
            composeVC.recipients = [number]
            composeVC.body = "Install Groupin, please!"
            
            self.present(composeVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnReinviteTapped(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview as! ContactCell
        let indexPath = tbContact.indexPath(for: cell)
        let number = self.number[indexPath!.row - dataData.count]
        print(number)
//        self.jsonSelected = dataData[indexPath!.row]
        
        if !MFMessageComposeViewController.canSendText() {
            print("SMS services are not available")
        }else{
            let composeVC = MFMessageComposeViewController()
            composeVC.messageComposeDelegate = self
            
            composeVC.recipients = [number]
            composeVC.body = "Install Groupin, please!"
            
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController,
                                      didFinishWith result: MessageComposeResult) {
        // Check the result or perform other tasks.
        let model = tb_invite_contact()
//        model.phone = self.jsonSelected.phone
//        model.name = self.jsonSelected.name
        DBHelper.insert(model)
        
        // Dismiss the message compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    // List Contact HP
    func requestForAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        // Get authorization
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        
        // Find out what access level we have currently
        switch authorizationStatus {
        case .authorized:
            completionHandler(true)
        case .denied, .notDetermined:
            CNContactStore().requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                if access {
                    completionHandler(access)
                }
                else {
                    if authorizationStatus == CNAuthorizationStatus.denied {
                        DispatchQueue.main.async(execute: {
                            let msg = "Please allow the app to access your contact through the Settings."
                            Config().alertForAllowAccess(self, msg: msg)
                        })
                    }
                }
            })
            
        default:
            completionHandler(false)
        }
    }
    
    var getContactNumber = ""
    var getContactName = ""
    func findContacts() -> [CNContact] {
        let keysToFetch = [CNContactGivenNameKey,
                           CNContactFamilyNameKey, CNContactEmailAddressesKey,
                           CNContactPhoneNumbersKey
        ]
        let fetchRequest = CNContactFetchRequest(keysToFetch: keysToFetch as [CNKeyDescriptor])
        var contacts = [CNContact]()
        self.name.removeAll()
        self.number.removeAll()
        do{
            try! store.enumerateContacts(with: fetchRequest, usingBlock: { (contact, stop) -> Void in
                if contact.phoneNumbers.count > 0 {
                    let get = (contact.phoneNumbers[0].value as CNPhoneNumber).value(forKey: "digits") as! String
                    
                    let countNumber = get.characters.count
                    if countNumber > 9 {
                        
                        self.getContactNumber = get
                        var contName = ""
                        if contact.givenName != "" {
                            contName += contact.givenName
                        }
                        if contact.familyName != "" {
                            contName += " " + contact.familyName
                        }
                        self.getContactName = contName
                        
                        self.allCountries = self.phoneNumberKit.allCountries()
                        do{
                            let phoneNumber = try self.phoneNumberKit.parse(get, withRegion: self.regionCode, ignoreType: true)
                            let phoneValid = self.phoneNumberKit.format(phoneNumber, toType: .e164)
                            
                            //check available data on number friend
                            if let _ = self.dataNumberFriend.index(of: phoneValid) {
                                self.dataNumberFriendInContact.append(phoneValid)
                            }else{
                                contacts.append(contact)
                                self.number.append(phoneValid)
                                self.name.append(self.getContactName)
                            }
                        }catch{
                            print("Invalid Phone Number")
                            self.parseNumber(self.allCountries[0])
                        }
                    }
                }
                }
            )
            return contacts
        }
    }
    
    //Convert Phone number to valid number data by server
    let phoneNumberKit = PhoneNumberKit()
    var regionCode = "ID"
    var allCountries = [String]()
    var loopDataGetNumber = 0
    func parseNumber(_ region: String){
        do {
            let phoneNumber = try phoneNumberKit.parse(self.getContactNumber, withRegion: region, ignoreType: true)
            let phoneValid = phoneNumberKit.format(phoneNumber, toType: .e164)
            
            //check available data on number friend
            if let _ = self.dataNumberFriend.index(of: phoneValid) {
                self.dataNumberFriendInContact.append(phoneValid)
            }else{
                self.number.append(phoneValid)
                self.name.append(self.getContactName)
            }
        } catch  {
            print("Invalid Phone Number")
            self.loopDataGetNumber += 1
            if self.allCountries.count > self.loopDataGetNumber {
                self.parseNumber(self.allCountries[self.loopDataGetNumber])
            }
        }
    }
    
    //Sync Phone Contact with Data in server (to get data not friend)
    func makeSerializeSync(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "contact" : self.number as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Sync(jsonString!)
    }
    
    func makeBase64Sync(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getDataSync(base64Encoded)
        }
    }
    
    func getDataSync(_ message: String){
        let urlString = Config().urlUser + "sync_contact"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                
                let msg = jsonData["msg"]
                if msg.count > 0 {
                    self.paramsJson = 1
                    for(key, _) in msg{
                        let data = msg[key]
                        //check available data on number friend
                        if let _ = self.dataNumberFriend.index(of: data["phone"].string!) {
                        }else{
                            self.dataIdNotFriend.append(data["id_user"].string!)
                            self.dataNameNotFriend.append(data["name"].string!)
                            if let ava = data["avatar"].string {
                                self.dataAvatarNotFriend.append(ava)
                            }else{
                                self.dataAvatarNotFriend.append("")
                            }
                            self.dataPhoneNotFriend.append(data["phone"].string!)
                            
                            //Delete Data contact
                            if let inxNumber = self.number.index(of: data["phone"].string!) {
                                self.number.remove(at: inxNumber)
                                self.name.remove(at: inxNumber)
                            }
                        }
                    }
                }else{
                    print("sync not available")
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.loadLocal()
        }
    }
    
    // METHODE NAV
    func colorNav(){
        navigationController!.navigationBar.barTintColor = self.blue
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationController!.navigationBar.tintColor = UIColor.white
    }
    
    func rightMenu(){
        let rightMenuButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(ContactController.searchTapped))
        self.navigationItem.setRightBarButtonItems([rightMenuButtonItem], animated: true)
    }
    
    //Search
    func searchTapped(_ sender:UIButton){
        self.viewSearch.isHidden = false
        self.conViewSearch.constant = 44
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        self.searchBar.showsCancelButton = true
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        switch self.statusTab {
        case 1:
            let data = try! Realm().objects(listFriend.self).filter("name CONTAINS[c] %@", searchText.lowercased())
            self.filteredListFriend.removeAll()
            self.filteredListFriend.append(contentsOf: data)
            self.tbFriends.reloadData()
        case 2:
            self.filteredNameNotFriend.removeAll()
            for item in self.dataNameNotFriend {
                if item.lowercased().contains(searchText.lowercased()) {
                    self.filteredNameNotFriend.append(item)
                    
                    let inx = self.filteredNameNotFriend.index(of: item)
                    self.filteredIdNotFriend.append(self.filteredIdNotFriend[inx!])
                    self.filteredNameNotFriend.append(self.filteredNameNotFriend[inx!])
                    self.filteredPhoneNotFriend.append(self.filteredPhoneNotFriend[inx!])
                    self.filteredAvatarNotFriend.append(self.filteredAvatarNotFriend[inx!])
                }
            }
            self.tbNotFriends.reloadData()
        case 3:
            self.filteredNameContact.removeAll()
            for item in self.name {
                if item.lowercased().contains(searchText.lowercased()) {
                    self.filteredNameContact.append(item)
                    
                    let inx = self.name.index(of: item)
                    let dataNumber = self.number[inx!]
                    self.filteredNumberContact.append(dataNumber)
                }
            }
            self.tbContact.reloadData()
        default:
            print("Tidak ada tab yg di select")
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("cancel")
        searchBar.resignFirstResponder()
        self.viewSearch.isHidden = true
        self.conViewSearch.constant = 1
        self.searchBar.showsCancelButton = false
    }

}
