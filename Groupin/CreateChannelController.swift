//
//  CreateChannelController.swift
//  Groupin
//
//  Created by Macbook pro on 7/26/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class CreateChannelController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var lblPublic: UILabel!
    @IBOutlet weak var swPublic: UISwitch!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var tbListMember: UITableView!
    
    let blue = UIColor(red: 63, green: 157, blue: 247)
    var stPublic = "1"
    var admin = [""]
    var models = tb_group()
    var idRoom = 0
    var token = ""
    var idAdminSelect = [String]()
    var nameAdminSelect = [String]()
    var avaAdminSelect = [String]()
    
    //Edit
    var statusEdit = 0
    var idChannel = 0
    var nameChat = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tbListMember.tableFooterView = UIView()
        self.txtName.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        //Rounded Button
        btnContinue.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
        btnContinue.layer.cornerRadius = 5
        btnContinue.layer.borderWidth = 1
        btnContinue.layer.borderColor = UIColor.lightGray.cgColor
        
        lblPublic.textColor = UIColor(red: 83, green: 186, blue: 32)
        
        navigationController!.navigationBar.barTintColor = self.blue
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationController!.navigationBar.tintColor = UIColor.white
        
        let idSession = try! Realm().objects(session.self).first
//        self.idRoom = (idSession?.id_group)!
        self.token = (idSession?.token)!
        
        if statusEdit == 1 {
            readChannel()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSelectMember" {
            let conn = segue.destination as! SelectMemberController
            conn.navigationItem.title = "Select Member"
            conn.name = txtName.text!
            conn.statusPublic = self.stPublic
            conn.admin = self.idAdminSelect
            
            //Edit
            conn.statusEdit = self.statusEdit
            conn.idChannel = self.idChannel
            conn.idRoom = self.idRoom
        }
        if segue.identifier == "showAddAdmin" {
            let conn = segue.destination as! SetAdminController
            conn.navigationItem.title = "Select Admin"
            conn.dari = "room"
            conn.phoneSelect = self.idAdminSelect
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if statusEdit == 1{
            if self.paramsJson == 1 {
                return self.jsonData["msg"].count + 1
            }else{
                return 1
            }
        }else{
            return self.idAdminSelect.count + 1
        }
    }
    
    var count = 0
    var id = [String]()
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addAdminCell", for: indexPath) as! AddAdminCell
        
        var data: JSON!
        if self.count == 0 {
            if self.paramsJson == 1 {
                id.removeAll()
                for (key, _) in jsonData["msg"] {
                    id.append(key)
                }
                data = jsonData["msg"][id[indexPath.row]]
                count = jsonData["msg"].count
            }
        }else{
            if indexPath.row < count{
                if self.paramsJson == 1 {
                    id.removeAll()
                    for (key, _) in jsonData["msg"] {
                        id.append(key)
                    }
                    data = jsonData["msg"][id[indexPath.row]]
                    count = jsonData["msg"].count
                }
            }else{self.paramsJson = 0}
        }
        
        if self.statusEdit == 1 {
            if indexPath.row < count  {
                let imgUrl = data["avatar"].string
                cell.imgAva.loadImageUsingCacheWithUrlString(imgUrl!)
                cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
                cell.imgAva.clipsToBounds = true
                cell.lblName.text = data["name"].string
            }else{
                cell.imgAva.image = UIImage(named: "ico_add_user")
                cell.lblName.text = "Add user(s) as admin"
            }
        }else{
            if indexPath.row < self.idAdminSelect.count{
                let imgUrl = self.avaAdminSelect[indexPath.row]
                cell.imgAva.loadImageUsingCacheWithUrlString(imgUrl)
                cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
                cell.imgAva.clipsToBounds = true
                cell.lblName.text = self.nameAdminSelect[indexPath.row]
            }else{
                cell.imgAva.image = UIImage(named: "ico_add_user")
                cell.lblName.text = "Add user(s) as admin"
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        if self.paramsJson == 1 {
            let count = jsonData["msg"].count
            if indexPath.row < count  {
                print("Admin Tapped")
            }else{
                self.performSegue(withIdentifier: "showAddAdmin", sender: self)
            }
        }else {
            if indexPath.row < self.idAdminSelect.count{
                print("Admin Tapped")
            }else{
                print("kirim")
                print(self.idAdminSelect)
                self.performSegue(withIdentifier: "showAddAdmin", sender: self)
            }
        }
    }
    
    @IBAction func switchPublicTapped(_ sender: UISwitch) {
        if sender.isOn {
            lblPublic.text = "Public"
            lblPublic.textColor = UIColor(red: 83, green: 186, blue: 32)
            self.stPublic = "1"
        }else{
            lblPublic.text = "Private"
            lblPublic.textColor = UIColor(red: 232, green: 76, blue: 61)
            self.stPublic = "2"
        }
    }
    
    @IBAction func btnContinueTapped(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showSelectMember", sender: self)
    }
    
    @IBAction func backToCreateRoom(_ segue: UIStoryboardSegue){
        self.tbListMember.reloadData()
        print(self.idAdminSelect)
    }
    
    func readChannel(){
//        let data = try! Realm().objects(tb_channel).filter("id_channel == %@", self.idChannel).first!
//        txtName.text = data.name_channel
//        if data.status_public == "1" {
//            lblPublic.text = "Public"
//            lblPublic.textColor = UIColor.greenColor()
//            swPublic.on = true
//        }else{
//            lblPublic.text = "Private"
//            lblPublic.textColor = UIColor.redColor()
//            swPublic.on = false
//        }
        self.navigationItem.title = "Edit Chat Room"
        txtName.text = self.nameChat
        
        getDataMember()
    }
    
    func getDataMember(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idRoom as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            print(base64Encoded)
            getData(base64Encoded)
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    func getData(_ message: String){
        let urlString = Config().urlRoom + "list_user_room"
        let token = self.token
        print("Token : \(token)")
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)
                self.paramsJson = 1
                
                print(self.jsonData)
                
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.tbListMember.reloadData()
        }
    }
    
    //Keyboard
    var activeField: UITextField?
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            let possKeyboard = self.view.frame.height - keyboardSize!.height - activeField!.frame.height
            if activeField!.frame.origin.y > possKeyboard {
                if view.frame.origin.y == 0{
                    self.view.frame.origin.y -= keyboardSize!.height
                }
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}
