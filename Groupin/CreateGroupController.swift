//
//  CreateGroupController.swift
//  Groupin
//
//  Created by Macbook pro on 7/26/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import Realm
import DropDown
import SwiftyJSON
import Alamofire
import Photos
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class CreateGroupController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate, UITextInputTraits, UITextFieldDelegate, UIScrollViewDelegate {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtDesc: UITextView!
    @IBOutlet weak var lblDescLeft: UILabel!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtCategori: UIButton!
    @IBOutlet weak var btnRadio1: UIButton!
    @IBOutlet weak var imgRadio1: UIImageView!
    @IBOutlet weak var btnRadio2: UIButton!
    @IBOutlet weak var imgRadio2: UIImageView!
    @IBOutlet weak var txtJlmMember: UITextField!{
        didSet{
            txtJlmMember.keyboardType = UIKeyboardType.numberPad
        }
    }
    var keyboardType: UIKeyboardType {
        get{
            return txtJlmMember.keyboardType
        }
        set{
            if newValue != UIKeyboardType.numberPad{
                self.keyboardType = UIKeyboardType.numberPad
            }
        } 
    }
    @IBOutlet weak var switchPublic: UISwitch!
    @IBOutlet weak var switchPromot: UISwitch!
    @IBOutlet weak var lblPublic: UILabel!
    @IBOutlet weak var lblPromot: UILabel!
    @IBOutlet weak var btnContinue: UIButton!
    
    @IBOutlet weak var conSpaceBotView: NSLayoutConstraint!
    
    var imgPicker = UIImagePickerController()
    var imgData = Data()
    
    var btnRadio1Status = "false"
    var btnRadio2Status = "true"
    var idToSave = 0
    var countId = 0
    var idUser = 0
    var token = ""
    
    var models = [tb_group]()
    
    let listCountry = DropDown()
    let listCity = DropDown()
    var listCat = DropDown()
    lazy var dropDowns: [DropDown] = { return [self.listCat, self.listCountry, self.listCity] }() 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtDesc.delegate = self
        self.txtName.delegate = self
        self.txtCountry.delegate = self
        self.txtJlmMember.delegate = self
        self.txtJlmMember.isEnabled = true
        self.scrollView.delegate = self
        
        //Get Data from Session
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.token = (idSession?.token)!
        
        imgPicker.delegate = self
        
        //Desc Rounded
        txtDesc.layer.cornerRadius = 5
        txtDesc.layer.borderColor = UIColor.lightGray.cgColor
        txtDesc.layer.borderWidth = 0.5
        
        //Img Ava
        let tapPhoto = UITapGestureRecognizer(target: self, action: #selector(CreateGroupController.takePhotoTapped(_:)))
        tapPhoto.numberOfTapsRequired = 1
        imgAva.isUserInteractionEnabled = true
        imgAva.addGestureRecognizer(tapPhoto)
        imgAva.layer.cornerRadius = 15
        
        //Rounded Button
        btnContinue.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
        btnContinue.layer.cornerRadius = 5
        btnContinue.layer.borderWidth = 1
        btnContinue.layer.borderColor = UIColor.lightGray.cgColor
        
        NotificationCenter.default.addObserver(self, selector: #selector(CreateGroupController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateGroupController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtDesc.text == "Description" {//.textColor == UIColor.lightGrayColor() {
            txtDesc.text = nil
            txtDesc.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtDesc.text!.isEmpty {
            txtDesc.text = "Description"
            txtDesc.textColor = UIColor.gray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count
        self.lblDescLeft.text = String(500 - numberOfChars)
        return 500 > numberOfChars
    }
    
    func takePhotoTapped(_ sender: UITapGestureRecognizer) {
        BrowseFoto(UIButton.init())
    }
    func BrowseFoto(_ sender: UIButton) {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.modalPresentationStyle = .popover
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.cameraTapped()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.libraryTapped()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds;
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    // Camera
    func cameraTapped(){
        if Config().checkAccessCamera() == false {
            print("access denied")
            let msg = "Please allow the app to access your camera through the Settings."
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                if granted == true {
                    // User granted
                    Config().alertForAllowAccess(self, msg: msg)
                }else{
                    // User Rejected
                    Config().alertForAllowAccess(self, msg: msg)
                }
            });
        }else{
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
                self.imgPicker.delegate = self;
                
                self.imgPicker.sourceType = UIImagePickerControllerSourceType.camera
                self.imgPicker.mediaTypes = [kUTTypeImage as String]
                self.imgPicker.allowsEditing = true
                self.present(self.imgPicker, animated: true, completion: nil)
            }else{
                libraryTapped()
            }
        }
    }
    
    // Library Image
    func libraryTapped(){
        if Config().checkAccessPhotos() == false {
            print("access denied")
            let msg = "Please allow the app to access your photos through the Settings."
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.denied {
                    print("denied")
                    Config().alertForAllowAccess(self, msg: msg)
                }else{
                    print("another")
                    Config().alertForAllowAccess(self, msg: msg)
                }
            })
        }else{
            self.imgPicker.allowsEditing = true
            self.imgPicker.sourceType = .photoLibrary
            self.present(self.imgPicker, animated: true, completion: nil)
        }
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    var postImageName: String! = ""
    var postImageUrl: String! = ""
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var pickedImage: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            pickedImage = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            pickedImage = originalImage
        }
        
        imgAva.contentMode = .scaleToFill
        imgAva.image = pickedImage
        
        let imageName = UUID().uuidString
        postImageName = imageName
        let imagePath = getDocumentsDirectory().appendingPathComponent(imageName+".jpg")
        postImageUrl = imagePath
        
        let jpegData = UIImageJPEGRepresentation(pickedImage!, 0.8)
        try? jpegData!.write(to: URL(fileURLWithPath: imagePath), options: [.atomic])
        
        self.imgData = jpegData!
        
        dismiss(animated: true, completion: nil)
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    //==============================================
    
    var idCountrySelected = 0
    var nameCountrySelected = ""
    var listDataCountry = [""]
    @IBAction func txtCountryTyping(_ sender: AnyObject) {
        let urlString = Config().urlMain + "get_country"
        let word = txtCountry.text
        Alamofire.request(urlString, method: .post, parameters: ["like" : word!]).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                self.listDataCountry.removeAll()
                for (key, _) in jsonData{
                    let dataCountry = jsonData[key]["country"]
                    self.listDataCountry.append(String(describing: dataCountry))
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                if self.txtCountry.text != "" {
                    self.setupCountry()
                    self.listCountry.show()
                }else{
                    self.listCountry.hide()
                }
        }
    }
    
    var idCitySelected = 0
    var nameCitySelected = ""
    var listDataCity = [""]
    @IBAction func txtCityTyping(_ sender: AnyObject) {
        let urlString = Config().urlMain + "get_city/" + String(idCountrySelected)
        let word = txtCity.text
        Alamofire.request(urlString, method: .post, parameters: ["like" : word!]).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                self.listDataCity.removeAll()
                for (key, value) in jsonData{
                    self.idCitySelected = Int(key)!
                    self.listDataCity.append(String(describing: value))
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                if self.txtCity.text != "" {
                    self.setupCity()
                    self.listCity.show()
                }else{
                    self.listCity.hide()
                }
        }
    }
    
    func getIdCountryOrCity(_ params: String){
        var urlString = ""
        var like = ""
        var keySave = ""
        
        if params == "country" {
            urlString = Config().urlMain + "get_country"
            like = self.nameCountrySelected
        }else{
            urlString = Config().urlMain + "get_city/" + String(self.idCountrySelected)
            like = self.nameCitySelected
        }
        Alamofire.request(urlString, method: .post, parameters: ["like" : like]).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                for (key, _) in jsonData{
                    keySave = key
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                if params == "country" {
                    self.idCountrySelected = Int(keySave)!
                    print(self.idCountrySelected)
                }else{
                    self.idCitySelected = Int(keySave)!
                    print(self.idCitySelected)
                }
        }
    }
    
    //Dropdown Country and City
    func setupCountry() {
        listCountry.anchorView = txtCountry
        listCountry.bottomOffset = CGPoint(x: 0, y: txtCountry.bounds.height)
        listCountry.direction = .bottom
        listCountry.dataSource = listDataCountry
        
        listCountry.selectionAction = { [unowned self] (index, item) in
            self.txtCountry.text = item
            self.txtCountry.resignFirstResponder()
            self.nameCountrySelected = item
            self.getIdCountryOrCity("country")
        }
    }
    func setupCity() {
        listCity.anchorView = txtCity
        listCity.bottomOffset = CGPoint(x: 0, y: txtCity.bounds.height)
        listCity.direction = .bottom
        listCity.dataSource = listDataCity
        
        listCity.selectionAction = { [unowned self] (index, item) in
            self.txtCity.text = item
            self.txtCity.resignFirstResponder()
            self.nameCitySelected = item
            self.getIdCountryOrCity("city")
        }
    }
    
    var listDataCatagory = [""]
    var listDataIdCatagory = [""]
    var catagorySelected = ""
    @IBAction func btnCatTapped(_ sender: AnyObject) {
        let urlString = Config().urlGroup + "get_group_type"
        Alamofire.request(urlString, method: .post).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                self.listDataCatagory.removeAll()
                self.listDataIdCatagory.removeAll()
                for (key, value) in jsonData{
                    self.listDataIdCatagory.append(key)
                    self.listDataCatagory.append(value.string!)
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.listCat.anchorView = self.txtCategori
                self.listCat.bottomOffset = CGPoint(x: 0, y: self.txtCategori.bounds.height)
                self.listCat.dataSource = self.listDataCatagory
                
                self.listCat.selectionAction = { [unowned self] (index, item) in
                    self.txtCategori.setTitle(item, for: .normal)
                    let catSelect = self.listDataCatagory.index(of: item)
                    let saveCat = self.listDataIdCatagory[catSelect!]
                    self.catagorySelected = saveCat
                    if item == "" {
                        self.txtCategori.setTitleColor(UIColor.gray, for: UIControlState.normal)
                    }else{
                        self.txtCategori.setTitleColor(UIColor.black, for: UIControlState.normal)
                    }
                }
                
                self.listCat.show()
        }
    }
    
    @IBAction func btnRadio1Tapped(_ sender: AnyObject) {
        self.btnRadio1Status = "true"
        self.btnRadio2Status = "false"
        
        imgRadio1.image = UIImage(named: "ico_radio_on")
        imgRadio2.image = UIImage(named: "ico_radio_off")
        txtJlmMember.isEnabled = false
    }
    
    @IBAction func btnRadio2Tapped(_ sender: AnyObject) {
        self.btnRadio1Status = "false"
        self.btnRadio2Status = "true"
        
        imgRadio1.image = UIImage(named: "ico_radio_off")
        imgRadio2.image = UIImage(named: "ico_radio_on")
        txtJlmMember.isEnabled = true
        txtJlmMember.becomeFirstResponder()
    }
    
    @IBAction func switchPublicTapped(_ sender: UISwitch) {
        if sender.isOn {
            lblPublic.text = "Public"
            lblPublic.textColor = UIColor(red: 83, green: 186, blue: 32)
        }else{
            lblPublic.text = "Private"
            lblPublic.textColor = UIColor(red: 232, green: 76, blue: 61)
        }
    }
    
    @IBAction func switchPromotTapped(_ sender: UISwitch) {
        if sender.isOn {
            lblPromot.text = "Promoted"
            lblPromot.textColor = UIColor(red: 83, green: 186, blue: 32)
        }else{
            lblPromot.text = "Not Promoted"
            lblPromot.textColor = UIColor(red: 232, green: 76, blue: 61)
        }
    }
    
    var alertLoading: UIAlertController!
    @IBAction func btnContinueTapped(_ sender: AnyObject) {
        if txtName.text == "" || txtDesc.text == "" || txtCountry.text == "" || txtCity.text == ""{
            self.alertLoading = UIAlertController(title: "Error", message: "Please complete all data", preferredStyle: UIAlertControllerStyle.alert)
            self.alertLoading.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            DispatchQueue.main.async(execute: {
                self.present(self.alertLoading, animated: true, completion: nil)
            })
        }else if txtJlmMember.isEnabled == true && txtJlmMember.text == "" {
            self.alertLoading = UIAlertController(title: "Error", message: "Please complete all data", preferredStyle: UIAlertControllerStyle.alert)
            self.alertLoading.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            DispatchQueue.main.async(execute: {
                self.present(self.alertLoading, animated: true, completion: nil)
            })
        }else{
            self.createGroup()
        }
    }
    
    func createGroup(){
        self.alertLoading = UIAlertController(title: "Processing", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        DispatchQueue.main.async(execute: {
            self.present(self.alertLoading, animated: true, completion: nil)
        })
        
        makeSerialize()
    }
    
    func makeSerialize(){
        // If data pass
        var numberMemberAllowed = ""
        if self.btnRadio1Status == "true" {
            numberMemberAllowed = "0"
        }else if self.btnRadio2Status == "true"{
            numberMemberAllowed = txtJlmMember.text!
        }
        var isPublic = ""
        if lblPublic.text == "Public" {
            isPublic = "1"
        }else{
            isPublic = "0"
        }
        var isPromot = ""
        if lblPromot.text == "Promoted" {
            isPromot = "1"
        }else{
            isPromot = "0"
        }
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "name" : txtName.text! as AnyObject,
            "fk_country" : self.idCountrySelected as AnyObject,
            "fk_city" : self.idCitySelected as AnyObject,
            "fk_category" : self.catagorySelected as AnyObject,
            "description" : txtDesc.text! as AnyObject,
            "member_allowed" : numberMemberAllowed as AnyObject,
            "is_public" : isPublic as AnyObject,
            "is_promote" : isPromot as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            print("Encoded:  \(base64Encoded)")
            
            if let base64Decoded = Data(base64Encoded: base64Encoded, options:   NSData.Base64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: String.Encoding.utf8.rawValue) })
            {
                print("Decoded:  \(base64Decoded!)")
            }
            
            postData(base64Encoded)
        }
    }
    
    var msgError = ""
    func postData(_ message: String){
        let urlString = Config().urlGroup + "create_group"
        let token = self.token
        print(token)
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        
        //Upload Data with Image
        
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            
            if self.postImageUrl != "" {
                multipartFormData.append(URL(fileURLWithPath: self.postImageUrl), withName: "avatar")
            }
            
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: urlString,
        encodingCompletion: {
            encodingResult in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON {
                    response in
                    
                    if let value = response.result.value {
                        let jsonData = JSON(value)
                        print(jsonData)
                        if jsonData["code"] == "1"{
                            self.msgError = ""
                            self.idToSave = jsonData["msg"].intValue
                        }else{
                            self.msgError = jsonData["msg"].string!
                        }
                        self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        })
    }
    
    func alertStatus(){
        if msgError != "" {
            let alert = UIAlertController(title: "Info", message: msgError, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }else{
            self.makeSerializeData()
        }
    }
    
    func makeSerializeData(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idUser as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Data(jsonString!)
    }
    
    func makeBase64Data(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getDataGroup(base64Encoded)
        }
    }
    
    var jsonGroup: JSON!
    func getDataGroup(_ message: String){
        let urlString = Config().urlGroup + "list_own_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                self.jsonGroup = jsonData["msg"]
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.createSession()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSuccessGroup" {
            self.tabBarController?.tabBar.isHidden = true
            let conn = segue.destination as! SuccessCreateGroupController
            conn.nameGroup = txtName.text!
        }
    }
    
    func createSession(){
        //Session group
        let model = try! Realm().objects(session.self).first
        try! Realm().write(){
            model?.id_group = self.idToSave
        }
        
        //Group
        if jsonGroup != nil {
            let objGroup = try! Realm().objects(tb_group.self)
            try! Realm().write(){
                try! Realm().delete(objGroup)
            }
            for (key, _) in jsonGroup {
                let modelGroup = tb_group()
                modelGroup.id = Int(key)!
                modelGroup.name = jsonGroup[key]["group"].string!
                if let ava = jsonGroup[key]["avatar"].string {
                    modelGroup.avatar = ava
                }else{
                    modelGroup.avatar = ""
                }
                modelGroup.city = jsonGroup[key]["city"].string!
                modelGroup.country = jsonGroup[key]["country"].string!
                modelGroup.jlm_member = jsonGroup[key]["member"].string!
                modelGroup.category = jsonGroup[key]["category"].string!
                modelGroup.desc = jsonGroup[key]["desc"].string!
                modelGroup.date_create = jsonGroup[key]["date_create"].string!
                modelGroup.id_user_create = jsonGroup[key]["id_user_create"].string!
                modelGroup.user_create = jsonGroup[key]["name_user_create"].string!
                modelGroup.is_public = jsonGroup[key]["is_public"].string!
                modelGroup.is_promot = jsonGroup[key]["is_promote"].string!
                modelGroup.user_level = jsonGroup[key]["user_level"].string!
                DBHelper.insert(modelGroup)
            }
        }

        DispatchQueue.main.async(execute: {
            self.performSegue(withIdentifier: "showSuccessGroup", sender: self)
        })
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if activeField != nil{
//            activeField?.resignFirstResponder()
//        }else{
//            self.view.endEditing(true)
//        }
//    }
    var activeField: UITextField?
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            self.conSpaceBotView.constant = keyboardSize!.height
//            let possKeyboard = self.view.frame.height - keyboardSize!.height - activeField!.frame.height
//            if activeField?.frame.origin.y > possKeyboard {
//                if (!CGRectContainsPoint(aRect, activeField!.frame.origin)){
//                    if view.frame.origin.y == 0{
//                        self.view.frame.origin.y -= keyboardSize!.height
//                    }
//                }
//            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.conSpaceBotView.constant = 0
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}
