//
//  DBHelper.swift
//  Groupin
//
//  Created by Macbook pro on 7/22/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import RealmSwift

struct DBHelper {
    
    // INSERT
    static func insert(_ obj: Object){
        try! Realm().write(){
            try! Realm().add(obj)
        }
    }
    
    // READ
    static func getAllUser() -> [tb_user]{
        let objs: Results<tb_user> = {
            try! Realm().objects(tb_user.self)
        }()
        return Array(objs)
    }
    
    static func getAllGroup() -> [tb_group]{
        let objs: Results<tb_group> = {
            try! Realm().objects(tb_group.self).sorted(byKeyPath: "name", ascending: true)
        }()
        return Array(objs)
    }
    
    static func getAllRoom() -> [tb_room]{
        let objs: Results<tb_room> = {
            try! Realm().objects(tb_room.self)
        }()
        return Array(objs)
    }
    
    static func getAllSession() -> [session]{
        let objs: Results<session> = {
            try! Realm().objects(session.self)
        }()
        return Array(objs)
    }
    
    static func getAllStatus() -> [listStatus]{
        let objs: Results<listStatus> = {
            try! Realm().objects(listStatus.self).sorted(byKeyPath: "name", ascending: true)
        }()
        return Array(objs)
    }
    
    static func getAllFriend() -> [listFriend]{
        let objs: Results<listFriend> = {
            try! Realm().objects(listFriend.self).sorted(byKeyPath: "name", ascending: true)
        }()
        return Array(objs)
    }
    
    static func getAllBlocked() -> [listBlocked]{
        let objs: Results<listBlocked> = {
            try! Realm().objects(listBlocked.self).sorted(byKeyPath: "name", ascending: true)
        }()
        return Array(objs)
    }
    
    static func getAllTerm() -> [termOfUse]{
        let objs: Results<termOfUse> = {
            try! Realm().objects(termOfUse.self)
        }()
        return Array(objs)
    }
    
    static func getAllListForum() -> [listForum]{
        let objs: Results<listForum> = {
            try! Realm().objects(listForum.self).sorted(byKeyPath: "id_thread", ascending: false)
        }()
        return Array(objs)
    }
    
    static func getAllListEvent() -> [listEvent]{
        let objs: Results<listEvent> = {
            try! Realm().objects(listEvent.self).sorted(byKeyPath: "id_event", ascending: false)
        }()
        return Array(objs)
    }
    
    static func getAllDataChatPersonal() -> [listDataChatPersonal]{
        let objs: Results<listDataChatPersonal> = {
            try! Realm().objects(listDataChatPersonal.self).sorted(byKeyPath: "dateLastChat", ascending: false)
        }()
        return Array(objs)
    }
    
    static func getAllCatSearch() -> [listCatSearch]{
        let objs: Results<listCatSearch> = {
            try! Realm().objects(listCatSearch.self)
        }()
        return Array(objs)
    }
    
    // DELETE
    static func deleteByPhone(_ phone: String){
        let obj = try! Realm().objects(tb_user.self).filter("phone == %@", phone)
        try! Realm().write(){
            try! Realm().delete(obj)
        }
    }
    
    static func deleteByIdChat(_ idChat: String){
        let obj = try! Realm().objects(tb_chat.self).filter("id_chat == %@", idChat)
        try! Realm().write(){
            try! Realm().delete(obj)
        }
    }

}
