//
//  DBModel.swift
//  Groupin
//
//  Created by Macbook pro on 7/22/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import RealmSwift
import Realm

//Intro
class intro: Object{
    dynamic var intro = 0
}

//USER
class tb_user: Object {
    dynamic var id = 0
    dynamic var phone = ""
    dynamic var password = ""
    dynamic var avatar = ""
    dynamic var name = ""
    dynamic var gender = ""
    dynamic var tgl_lahir = ""
    dynamic var id_country = ""
    dynamic var country = ""
    dynamic var id_city = ""
    dynamic var city = ""
    dynamic var status = ""
    dynamic var tgl_join = ""
    dynamic var hanya_join_main_group = ""
    dynamic var valid_email = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

class interestUser: Object {
    dynamic var id_table = 0
    dynamic var id_category = 0
    dynamic var category = ""
    dynamic var detail = ""
}

class dataCountry: Object {
    dynamic var id_country = 0
    dynamic var country = ""
}

class dataCity: Object {
    dynamic var id_city = 0
    dynamic var city = ""
}

class dataInterest: Object {
    dynamic var id_interest = 0
    dynamic var interest = ""
}

class listFriend: Object {
    dynamic var id_friend = 0
    dynamic var phone = ""
    dynamic var name = ""
    dynamic var avatar = ""
    dynamic var msg_status = ""
    dynamic var date_friends = 0
    dynamic var is_contact = 0
}

//Session
class session: Object {
    dynamic var id = 0
    dynamic var token = ""
    dynamic var last_login = 0
    dynamic var id_group = 0
    dynamic var remember = 0
}

//Session Notif
class session_notif: Object {
    dynamic var fromNotif = 0
    dynamic var id = 0
    dynamic var name = ""
    dynamic var avatar = ""
}

//GROUP
class tb_group: Object {
    dynamic var id = 0
    dynamic var avatar = ""
    dynamic var name = ""
    dynamic var desc = ""
    dynamic var country = ""
    dynamic var city = ""
    dynamic var category = ""
    dynamic var date_create = ""
    dynamic var id_user_create = ""
    dynamic var user_create = ""
    dynamic var jlm_member = ""
    dynamic var is_public = ""
    dynamic var is_promot = ""
    dynamic var user_level = ""
    
    dynamic var id_parent = ""
    dynamic var name_parent = ""
    dynamic var avatar_parent = ""
    
    dynamic var is_notif = 1
    dynamic var is_sound = 1
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

class listMember: Object {
    dynamic var id_member = 0
    dynamic var id_group = 0
    dynamic var name = ""
    dynamic var avatar = ""
    dynamic var status_level = ""
    dynamic var from_group = ""
}

//class listParent: Object { // Ga kepake
//    dynamic var id_parent = 0
//    dynamic var id_group = 0
//    dynamic var name = ""
//    dynamic var avatar = ""
//}

class listChild: Object {
    dynamic var id_child = 0
    dynamic var id_group = 0
    dynamic var name = ""
    dynamic var avatar = ""
}

// Room
class listMemberRoom: Object {
    dynamic var id_room = 0
    dynamic var id = 0
    dynamic var name = ""
    dynamic var avatar = ""
    dynamic var status_level = ""
}

class tb_room: Object {
    dynamic var id_group = 0
    dynamic var id_room = 0
    dynamic var name = ""
    dynamic var status_public = ""
}

class tb_own_room: Object {
    dynamic var id_group = 0
    dynamic var id_room = 0
    dynamic var name = ""
    dynamic var user_level = ""
}

//Chat
class listDataChatPersonal: Object { // Untuk di halaman list chat personal
    dynamic var id_sender = 0
    dynamic var name = ""
    dynamic var ava = ""
    dynamic var text = ""
    dynamic var dateLastChat = 0
}

class tb_chat: Object {
    dynamic var id_chat = ""
    dynamic var from = ""
    dynamic var to = ""
    dynamic var text = ""
    dynamic var time:Double = 0.0
    
    dynamic var image_url = ""
    dynamic var image_height = 0
    dynamic var image_width = 0
    
    dynamic var video_url = ""
    dynamic var audio_url = ""
    dynamic var file_url = ""
    
    dynamic var mediaName = ""
    dynamic var mediaSize = 0
    dynamic var mediaType = ""
    
    dynamic var latitude: Float = 0
    dynamic var longitude: Float = 0
    dynamic var nameAdrress = ""
    dynamic var detailAdrress = ""
    
    dynamic var keyReply = ""
    
    dynamic var is_read = 0
    
    override static func primaryKey() -> String? {
        return "id_chat"
    }
}

//Media
class tb_media: Object {
    dynamic var id_media = ""
    dynamic var id_group = ""
    dynamic var id_from_user = ""
    dynamic var name = ""
    dynamic var type = ""
    dynamic var date_create = ""
    dynamic var date_update = ""
    dynamic var comments_statistic = ""
    dynamic var like_statistic = ""
    override static func primaryKey() -> String? {
        return "name"
    }
}

//Notifications
class tb_notif: Object {
    dynamic var id_inviter = 0
    dynamic var id_group = 0
    dynamic var id_group_parent = 0
    dynamic var id_room = 0
    dynamic var id_event = 0
    dynamic var icon = ""
    dynamic var title = ""
    dynamic var body = ""
    dynamic var date = ""
    dynamic var type = 0
    dynamic var tab = 0
    dynamic var is_read = 0
}

//Updates
class tb_updates: Object {
    dynamic var id_update = 0
    dynamic var thumbnail = ""
    dynamic var title = ""
    dynamic var content = ""
    dynamic var date_create = ""
    dynamic var date_update = ""
    dynamic var update_type = ""
    dynamic var data_id_user = ""
    dynamic var data_id_user_name = ""
    dynamic var data_id_group = ""
    dynamic var data_id_group_name = ""
    dynamic var data_id_thread = ""
    dynamic var data_id_thread_name = ""
    dynamic var data_id_event = ""
    dynamic var data_id_event_name = ""
}

//Contact Invited
class tb_invite_contact: Object {
    dynamic var phone = ""
    dynamic var name = ""
}

//Mention di Group
//class tb_mention: Object {
//    dynamic var id_user = 0
//    dynamic var name = ""
//    dynamic var ava = ""
//}

//Category Search Group
class listCatSearch: Object {
    dynamic var id_cat = ""
    dynamic var name = ""
}

//List Status
class listStatus: Object {
    dynamic var name = ""
}

//Term Of Use
class termOfUse: Object {
    dynamic var content = ""
}

//Blocked Contact
class listBlocked: Object {
    dynamic var id_user = 0
    dynamic var name = ""
    dynamic var ava = ""
}

//Forum
class listForum: Object {
    dynamic var id_thread = 0
    dynamic var id_group = 0
    dynamic var thumb = ""
    dynamic var subject = ""
    dynamic var content = ""
    dynamic var date = ""
    dynamic var total_view = ""
    dynamic var total_like = ""
    dynamic var total_comment = ""
}

//Event
class listEvent: Object {
    dynamic var id_event = 0
    dynamic var id_group = 0
    dynamic var image = ""
    dynamic var name = ""
    dynamic var range_price = ""
    dynamic var desc = ""
    dynamic var address = ""
    dynamic var latitude = ""
    dynamic var longitude = ""
    dynamic var date_from = ""
    dynamic var date_to = ""
    dynamic var date_submission = ""
    dynamic var id_country = 0
    dynamic var country = ""
    dynamic var id_city = 0
    dynamic var city = ""
    dynamic var id_create = 0
    dynamic var name_create = ""
    dynamic var id_attendance = ""
    dynamic var attendance_status = ""
    dynamic var ticket_name = ""
    dynamic var priceType = ""
    dynamic var price = ""
    dynamic var amount = ""
    dynamic var quota = ""
    dynamic var use_quota = ""
    dynamic var payment_status = ""
    dynamic var id_payment = "" // kode unik pembayaran
}

class listTicket: Object {
    dynamic var id_ticket = 0
    dynamic var id_event = 0
    dynamic var name = ""
    dynamic var total = ""
}

class listBuyTicket: Object {
    dynamic var id_buy = 0
    dynamic var id_ticket = 0
    dynamic var id_user = 0
    dynamic var name_user = ""
    dynamic var ava_user = ""
//    dynamic var status_attend = ""
    dynamic var total = ""
}
