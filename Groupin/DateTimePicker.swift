//
//  DateTimePicker.swift
//  Groupin
//
//  Created by Macbook pro on 12/28/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import Foundation
import UIKit

class DateTimePicker: UIViewController {
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var btnDone: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("DateTimePicker")
        self.showAnimate()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        viewMain.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        //Click to close
        let tapBack = UITapGestureRecognizer(target: self, action: #selector(DateTimePicker.closePopup(_:)))
        tapBack.numberOfTapsRequired = 1
        viewMain.isUserInteractionEnabled = true
        viewMain.addGestureRecognizer(tapBack)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    var dateFormatter: DateFormatter!
    var dari: Int! = 0 // 1: Add Event, 
    var untuk: String! = ""
    var value: Date!
    @IBAction func btnDoneTapped(_ sender: AnyObject) {
        print("done tapped")
        self.dateFormatter = DateFormatter()
        self.value = datePicker.date
        
        self.performSegue(withIdentifier: "backToAddEvent", sender: self)
        self.removeAnimate()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "backToAddEvent" {
            let conn = segue.destination as! AddEventController
            conn.wbFromTo = self.untuk
            conn.datePicked = self.value
        }
    }
    
    func closePopup(_ sender: UITapGestureRecognizer){
        self.removeAnimate()
    }
    
    func showAnimate(){
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0.0
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            }, completion: {(finished: Bool) in
                if(finished){
                    self.view.removeFromSuperview()
                }
        })
    }
}
