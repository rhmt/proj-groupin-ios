//
//  DeleteReasonAccounController.swift
//  Groupin
//
//  Created by Macbook pro on 8/18/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import DropDown
import RealmSwift
import SwiftyJSON
import Alamofire

class DeleteReasonAccounController: UIViewController, UITextFieldDelegate{
    
    var pass = ""
    var phone = ""
    var idUser = 0
    var token = ""
    
    @IBOutlet weak var btnList: UIButton!
    @IBOutlet weak var txtOther: UITextField!
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var btnDelete: UIButton!
    
    let list = DropDown()
    lazy var dropDowns: [DropDown] = { return [ self.list ] }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtOther.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        navigationItem.title = "Delete Account"
        
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.token = idSession!.token
        
        //Rounded Button
        btnDelete.layer.cornerRadius = 5
        btnDelete.layer.borderWidth = 1
        btnDelete.layer.borderColor = UIColor.lightGray.cgColor
        
        self.getDataFilter()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let numberOfChars = newText.characters.count
        if numberOfChars > 0 {
            self.btnDelete.isEnabled = true
            self.btnDelete.backgroundColor = UIColor.red
        }else{
            self.btnDelete.isEnabled = false
            self.btnDelete.backgroundColor = UIColor.lightGray
        }
        return true
    }
    
    var IdDataReason = [String]()
    var dataReason = [String]()
    var IdDataReasonSelected = ""
    func getDataFilter(){
        let urlString = Config().urlMain + "list_reason_delete"
        Alamofire.request(urlString, method: .post).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                self.dataReason.removeAll()
                self.IdDataReason.removeAll()
                for (key, value) in jsonData{
                    self.IdDataReason.append(key)
                    self.dataReason.append(value.string!)
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                print(self.dataReason)
                print(self.IdDataReason)
                self.setupDropDowns()
        }
    }
    
    @IBAction func btnListTapped(_ sender: AnyObject) {
        list.show()
    }
    
    // DropDown Function
    func setupDropDowns() {
        list.anchorView = btnList
        list.bottomOffset = CGPoint(x: 0, y: btnList.bounds.height)
        list.dataSource = dataReason
        
        list.selectionAction = { [unowned self] (index, item) in
            self.btnList.setTitle(item, for: UIControlState())
            let idReason = self.dataReason.index(of: item)
            let inputReason = self.IdDataReason[idReason!]
            self.IdDataReasonSelected = inputReason
            if item == "Other" {
                self.txtOther.isHidden = false
                self.viewLine.isHidden = false
                self.btnDelete.isEnabled = false
                self.btnDelete.backgroundColor = UIColor.gray
                self.txtOther.becomeFirstResponder()
            }else{
                self.txtOther.isHidden = true
                self.viewLine.isHidden = true
                self.btnDelete.isEnabled = true
                self.btnDelete.backgroundColor = UIColor(red: 232, green: 76, blue: 61)
            }

        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSuccessDelete" {
            let conn = segue.destination as! DeleteSuccController
            conn.navigationItem.title = "Account Disable"
            conn.navigationItem.hidesBackButton = true
        }
    }
    
    @IBAction func btnDeleteTapped(_ sender: AnyObject) {
        let alert = UIAlertController(title: "Attention", message: "Are You Sure Want to Delete Your Account?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.makeSerialize()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    var alertLoading: UIAlertController!
    func makeSerialize(){
        self.alertLoading = UIAlertController(title: "Processing", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        self.present(self.alertLoading, animated: true, completion: nil)
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "id_delete_reason" : self.IdDataReasonSelected as AnyObject,
            "reason_detail" : txtOther.text! as AnyObject,
        ]
        print(arrayData)
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    var msgError = ""
    var paramsError = 0
    func getData(_ message: String){
        let urlString = Config().urlUpdate + "request_delete_account"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(urlString)
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                self.paramsError = 1
                self.msgError = jsonData["msg"].string!
            }else{
                print("Something Went Wrong..")
                self.paramsError = 2
                self.msgError = "Failed connect to server"
            }
            }.responseData { Response in
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
        }
    }
    
    func alertStatus(){
        let alert = UIAlertController(title: "Info", message: msgError, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            if self.paramsError == 1 {
                self.performSegue(withIdentifier: "showSuccessDelete", sender: self)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}
