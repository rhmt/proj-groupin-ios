//
//  DeleteSuccController.swift
//  Groupin
//
//  Created by Macbook pro on 8/18/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class DeleteSuccController: UIViewController {
    
    @IBOutlet weak var btnBackLogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        
        //Rounded Button
        btnBackLogin.layer.cornerRadius = 5
        btnBackLogin.layer.borderWidth = 1
        btnBackLogin.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnBackLoginTapped(_ sender: AnyObject) {
        let login = storyboard?.instantiateViewController(withIdentifier: "showLogin") as! LoginController
        present(login, animated: true, completion: nil)
    }
}
