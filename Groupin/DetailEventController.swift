//
//  DetailEventController.swift
//  Groupin
//
//  Created by Macbook pro on 1/9/17.
//  Copyright © 2017 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire
import EventKit

class DetailEventController: UIViewController {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblCreated: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblAt: UILabel!
    @IBOutlet weak var conHeightLblAt: NSLayoutConstraint!
    @IBOutlet weak var tbCP: UITableView!
    @IBOutlet weak var lblKetOts: UILabel!
    @IBOutlet weak var btnSaveCalendar: UIButton!
    @IBOutlet weak var conWidthBtnSaveCalender: NSLayoutConstraint!
    @IBOutlet weak var btnViewMap: UIButton!
    @IBOutlet weak var imgQR: UIImageView!
    @IBOutlet weak var btnSetAttend: UIButton!
    
    var idEvent = ""
    var idUser = 0
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //session
        let sess = try! Realm().objects(session.self).first!
        self.idUser = sess.id
        self.token = sess.token
        
        self.loadData()
        self.designView()
        self.makeSerialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    func loadData(){
        let data = try! Realm().objects(listEvent.self).filter("id_event = %@", Int(self.idEvent)!).first!
        
        self.navigationItem.title = data.name
        
        if data.image != "" {
            self.imgAva.loadImageUsingCacheWithUrlString(data.image)
        }else{
            self.imgAva.image = UIImage(named: "dafault-ava-group")
        }
        self.imgAva.clipsToBounds = true
        self.lblTitle.text = data.name
        self.lblPrice.text = data.range_price
        self.lblDesc.text = data.desc
        self.lblCreated.text = data.name_create
        let strDateFrom = "YYYY-MM-dd HH:mm:ss"
        let strDateTo = "MMM dd, YYYY hh:mm a"
        let dateFrom = Config().convertDate(data.date_from, from: strDateFrom, to: strDateTo)
        let dateTo = Config().convertDate(data.date_to, from: strDateFrom, to: strDateTo)
        self.lblFrom.text = dateFrom
        self.lblTo.text = dateTo
        self.lblAt.text = data.address
        self.lblAt.textColor = UIColor(red: 63, green: 157, blue: 247)
        
        var attend = ""
        if data.attendance_status == "1" {
            attend = "WILL ATTEND ( " + data.priceType + " " + data.price + " )"
            self.btnSetAttend.setTitleColor(UIColor(red: 83, green: 186, blue: 32), for: UIControlState())
            self.btnSetAttend.isEnabled = false
        }else if data.attendance_status == "2"{
            attend = "WILL NOT ATTEND"
            self.btnSetAttend.setTitleColor(UIColor(red: 232, green: 76, blue: 61), for: UIControlState())
            self.btnSetAttend.isEnabled = false
        }else{
            attend = "SET ATTENDANCE"
            self.btnSetAttend.setTitleColor(UIColor.black, for: UIControlState())
            self.btnSetAttend.isEnabled = true
        }
        
        let dateGet = DateFormatter()
        dateGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let date = dateGet.date(from: data.date_from) {
            let dateCheckData = date.timeIntervalSince1970
            let dateCheckNow = Date().timeIntervalSince1970
            if dateCheckData < dateCheckNow {
                attend = "EVENT EXPIRED"
                self.btnSetAttend.setTitleColor(UIColor(red: 232, green: 76, blue: 61), for: UIControlState())
                self.btnSetAttend.isEnabled = false
            }
        }
        self.btnSetAttend.setTitle(attend, for: UIControlState())
        
        if data.id_create == self.idUser {
            self.rightMenu()
            self.btnSetAttend.isHidden = true
        }else{
            self.btnSetAttend.isHidden = false
        }
        
        btnSaveCalendar.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
        btnSaveCalendar.layer.cornerRadius = 5
        btnSaveCalendar.layer.borderWidth = 1
        btnSaveCalendar.layer.borderColor = UIColor.lightGray.cgColor
        btnViewMap.backgroundColor = UIColor(red: 232, green: 76, blue: 61)
        btnViewMap.layer.cornerRadius = 5
        btnViewMap.layer.borderWidth = 1
        btnViewMap.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func designView(){
        print("design")
        let data = try! Realm().objects(listEvent.self).filter("id_event = %@", Int(self.idEvent)!).first!
        let address = data.address
        
        let heightAddress = Config().estimateFrameForText(address, width: Int(self.lblAt.frame.width)).height
        self.conHeightLblAt.constant = heightAddress
        
        let widthBtn = (self.view.frame.width / 2) - 32
        self.conWidthBtnSaveCalender.constant = widthBtn
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idEvent as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            self.getData(base64Encoded)
        }
    }
    
    var paramsJson = 0
    var jsonData: JSON!
    func getData(_ message: String){
        let urlString = Config().urlEvent + "detail_event"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post , parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)["msg"]
                print(jsonData)
                self.jsonData = jsonData
                if jsonData.count > 0 {
                    self.paramsJson = 1
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
        }
    }
    
    @IBAction func btnSaveCalendarTapped(_ sender: AnyObject) {
        let data = try! Realm().objects(listEvent.self).filter("id_event = %@", Int(self.idEvent)!).first!
        let dateFrom = data.date_from
        let dateTo = data.date_to
        let titleEvent = data.name
        let noteEvent = data.desc
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let fromDate = dateFormatter.date(from: dateFrom)
        let toDate = dateFormatter.date(from: dateTo)
        
        let eventStore : EKEventStore = EKEventStore()
        eventStore.requestAccess(to: EKEntityType.event, completion: {
            (granted, error) in
            
            if (granted) && (error == nil) {
                print("granted \(granted)")
                print("error \(error)")
                
                let event:EKEvent = EKEvent(eventStore: eventStore)
                
                event.title = titleEvent
                event.startDate = fromDate!
                event.endDate = toDate!
                event.notes = noteEvent
                event.calendar = eventStore.defaultCalendarForNewEvents
                
                do {
                    try eventStore.save(event, span: .thisEvent)
                    print("Saved Event")
                    let alertLoading = UIAlertController(title: "", message: "Saved to your calendar", preferredStyle: UIAlertControllerStyle.alert)
                    self.present(alertLoading, animated: true, completion: nil)
                    UIView.animate(withDuration: 3.0, animations: {
                        alertLoading.dismiss(animated: true, completion: nil)
                    })
                } catch {
                    print("Bad things happened")
                    
                }
            }
        })
    }
    
    var latSelected = ""
    var longSelected = ""
    @IBAction func btnViewMapTapped(_ sender: AnyObject) {
        let data = try! Realm().objects(listEvent.self).filter("id_event = %@", Int(self.idEvent)!).first!
        self.latSelected = data.latitude
        self.longSelected = data.longitude
        
        self.performSegue(withIdentifier: "showLocation", sender: self)
    }
    
    var typeSelected = ""
    @IBAction func btnSetAttendTapped(_ sender: AnyObject) {
        if self.paramsJson == 1 {
            self.typeSelected = self.jsonData["ticket"][0]["idtb_ticket_event"].string!
        }
        let alert = UIAlertController(title: "", message: "Are you able to attend this event?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.performSegue(withIdentifier: "showAttend", sender: self)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { action in
            self.noAttendEvent()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func noAttendEvent(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "id_event" : self.idEvent as AnyObject,
            "id_ticket" : self.typeSelected as AnyObject,
            "jumlah" : "0" as AnyObject,
            "price" : "0" as AnyObject,
            "status" : "2" as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Attend(jsonString!)
    }
    
    func makeBase64Attend(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getDataAttend(base64Encoded)
        }
    }
    
    func getDataAttend(_ message: String){
        let urlString = Config().urlEvent + "set_attendance"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(urlString)
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                //                self.jsonData = JSON(value)
                print(JSON(value))
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.updateDataNoAttent()
        }
    }
    
    func updateDataNoAttent(){
        let data = try! Realm().objects(listEvent.self).filter("id_event = %@", Int(self.idEvent)!).first!
        try! Realm().write({
            data.attendance_status = "2"
        })
        
        self.btnSetAttend.setTitleColor(UIColor(red: 232, green: 76, blue: 61), for: UIControlState())
        self.btnSetAttend.isEnabled = false
        self.btnSetAttend.setTitle("WILL NOT ATTEND", for: .normal)
    }
    
    func rightMenu(){
        let button = UIButton()
        button.setImage(UIImage(named: "ico_more"), for: UIControlState())
        button.addTarget(self, action: #selector(DetailEventController.popUpEdit(_:)), for: UIControlEvents.touchDown)
        button.frame=CGRect(x: 0, y: 0, width: 24, height: 24)
        let rightMenuButtonItem:UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.setRightBarButtonItems([rightMenuButtonItem], animated: true)
    }
    
    var untuk = ""
    func popUpEdit(_ sender: UIButton){
        let alert:UIAlertController=UIAlertController(title: "", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.modalPresentationStyle = .popover
        
        let editAction = UIAlertAction(title: "Edit This Event", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.untuk = "edit"
            self.performSegue(withIdentifier: "showAddEvent", sender: self)
        }
        let staffAction = UIAlertAction(title: "Add Staff Event", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.performSegue(withIdentifier: "showChoiceMethodeStaff", sender: self)
        }
        let listAction = UIAlertAction(title: "Purchased Ticket", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.performSegue(withIdentifier: "showTicketList", sender: self)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel){ UIAlertAction in }
        
        alert.addAction(editAction)
        alert.addAction(staffAction)
        alert.addAction(listAction)
        alert.addAction(cancelAction)
        
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAddEvent" {
            let conn = segue.destination as! AddEventController
            conn.untuk = self.untuk
            conn.idEventEdit = self.idEvent
            conn.navigationItem.title = "Edit Event"
        }
        if segue.identifier == "showTicketList" {
            let conn = segue.destination as! ListTicketEventController
            conn.idEvent = self.idEvent
            conn.navigationItem.title = self.lblTitle.text!
        }
        if segue.identifier == "showLocation" {
            let conn = segue.destination as! EventViewMapsController
            conn.lat = Double(self.latSelected)!
            conn.long = Double(self.longSelected)!
            conn.navigationItem.title = "View Map"
        }
        if segue.identifier == "showAttend" {
            let conn = segue.destination as! EventAttendController
            conn.idEventSelected = self.idEvent
            conn.nameSelected = self.lblTitle.text!
            conn.createdSelected = self.lblCreated.text!
            conn.fromSelected = self.lblFrom.text!
            conn.toSelected = self.lblTo.text!
            conn.addressSelected = self.lblAt.text!
            if self.paramsJson == 1 {
                conn.ticketSelected = self.jsonData["ticket"]
            }
            conn.from = 1
        }
    }
    
    @IBAction func backToDetailEvent(_ sender: UIStoryboardSegue){
        print("wb")
        //self.makeSerialize()
        self.performSegue(withIdentifier: "backToListEvent", sender: self)
    }
}
