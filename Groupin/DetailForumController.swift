//
//  DetailForumController.swift
//  Groupin
//
//  Created by Macbook pro on 9/8/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class DetailForumController: UIViewController, UIPopoverPresentationControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgCup: UIImageView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblPlace: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var colImgContent: UICollectionView!
    @IBOutlet weak var viewComment: UIView!
    @IBOutlet weak var tbComment: UITableView!
    
    @IBOutlet weak var colLike: UICollectionView!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var viewSeparatorLike: UIView!
    
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var imgComment: UIImageView!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var imgShare: UIImageView!
    @IBOutlet weak var btnShare: UIButton!
    
    @IBOutlet weak var tbReply: UITableView!
    
    @IBOutlet weak var heightMainView: NSLayoutConstraint!
    @IBOutlet weak var widhtLblName: NSLayoutConstraint!
    @IBOutlet weak var heightContent: NSLayoutConstraint!
    @IBOutlet weak var heightImgContent: NSLayoutConstraint!
    @IBOutlet weak var heigthTbReply: NSLayoutConstraint!
    @IBOutlet weak var heightLblLike: NSLayoutConstraint!
    @IBOutlet weak var heightColLike: NSLayoutConstraint!
    @IBOutlet weak var heightViewComment: NSLayoutConstraint!
    
    var idThread = ""
    var idUser = 0
    var idGroup = 0
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tbComment.delegate = self
        self.tbReply.delegate = self
        
        self.loadData()
    }
    
//    override func willMoveToParentViewController(parent: UIViewController?) {
//        if parent == nil {
//            self.performSegueWithIdentifier("showBackToListForum", sender: self)
//            let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc = storyboard.instantiateViewControllerWithIdentifier("showGroupHome") as! GroupHomeController
//            vc.selectedIndex = 1
//            self.presentViewController(vc, animated: true, completion: nil)
//        }
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    func loadData(){
        //Session
        let dataSession = try! Realm().objects(session.self).first
        self.idUser = dataSession!.id
        self.idGroup = dataSession!.id_group
        self.token = dataSession!.token
        
        let rightMenuButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(DetailForumController.addReply))
        self.navigationItem.setRightBarButtonItems([rightMenuButtonItem], animated: true)
        
        self.imgAva.layer.cornerRadius = self.imgAva.frame.width / 2
        self.imgAva.clipsToBounds = true
        
        let tapListLikes = UITapGestureRecognizer(target: self, action: #selector(DetailForumController.listLikesTapped(_:)))
        tapListLikes.numberOfTapsRequired = 1
        lblLike.isUserInteractionEnabled = true
        lblLike.addGestureRecognizer(tapListLikes)
        
        makeSerialize()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_thread" : self.idThread as AnyObject,
            "id_user" : self.idUser as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            switch self.dari {
            case "like":
                self.postLike(base64Encoded)
            case "refreshLike":
                self.getDataLike(base64Encoded)
            default:
                self.getData(base64Encoded)
            }
        }
    }
    
    func getData(_ message: String){
        let urlString = Config().urlThread + "update_views"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let dataJason = JSON(value)["code"]
                print(dataJason)
                if dataJason.string! == "1" {
                    self.getDataThread(message)
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                
        }
    }
    
    func getDataThread(_ message: String){
        let urlString = Config().urlThread + "detail_thread"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        var paramsSuc = 0
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let dataJason = JSON(value)["msg"]
                print(dataJason)
                if dataJason.count > 0 {
                    paramsSuc = 1
                    
                    self.navigationItem.title = dataJason["subject"].string!
                    self.lblTitle.text = dataJason["subject"].string!
                    let content = dataJason["content"].string!
                    self.lblContent.text = content
                    let heightContent = Config().estimateFrameForText(content, width: Int(self.lblContent.frame.width)).height
                    self.heightContent.constant = heightContent
                    self.heightMainView.constant += heightContent
                    
                    let dateFormatterGet = DateFormatter()
                    dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let dateFormatterPrint = DateFormatter()
                    dateFormatterPrint.dateFormat = "d MMM YYYY, h:mm a"
                    let dataDateFrom = dataJason["date"].string!
                    if let date = dateFormatterGet.date(from: dataDateFrom) {
                        let dateView = dateFormatterPrint.string(from: date)
                        self.lblTime.text = dateView
                    }else{
                        self.lblTime.text = dataJason["date"].string! + " ago"
                    }
                    
                    let dataTS = dataJason["user"]
                    if let imgUrl = dataTS["thumbnail"].string {
                        self.imgAva.loadImageUsingCacheWithUrlString(imgUrl)
                    }else{
                        self.imgAva.image = UIImage(named: "default-avatar")
                    }
                    let nameTs = dataTS["name"].string!
                    self.lblName.text = nameTs
                    let widthName = Config().estimateFrameForText(nameTs, width: 180).width
                    self.widhtLblName.constant = widthName + 8
                    if let cup = dataTS["like_rank"].string {
                        let imgCup = "ico_cup" + cup
                        self.imgCup.image = UIImage(named: imgCup)
                    }
                    self.lblPlace.text = dataTS["city"].string! + ", " + dataTS["country"].string!
                    
                    self.dataImgContent = dataJason["img_content"]
                    if self.dataImgContent.count > 0 {
                        self.heightImgContent.constant = 50
                        self.heightMainView.constant += 50
                    }
                    
                    self.totalLikes = dataJason["total_like"].string!
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                if paramsSuc == 1{
                    self.paramsJson = 1
                    self.colImgContent.reloadData()
                    self.getDataComment(message)
                }
        }
    }
    
    var jsonDataComment: JSON!
    func getDataComment(_ message: String){
        let urlString = Config().urlThread + "list_comment/0/50"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)["msg"]
                print(jsonData)
                if jsonData.count > 0 {
                    self.paramsJsonReply = 1
                    self.jsonDataComment = jsonData
                    var plus = 0
                    if jsonData.count > 3 {
                        plus = 4
                    }else{
                        plus = jsonData.count
                    }
                    let plusHeight = plus * 35
                    self.heightViewComment.constant += CGFloat(plusHeight)
                    self.heightMainView.constant += CGFloat(plusHeight)
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.getDataLike(message)
                self.tbComment.reloadData()
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    var paramsJsonLike = 0
    func getDataLike(_ message: String){
        let urlString = Config().urlThread + "list_like/0/50"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let dataJason = JSON(value)["msg"]
                if dataJason.count > 0 {
                    self.jsonData = dataJason
                    self.paramsJsonLike = 1
                    self.viewSeparatorLike.isHidden = false
                    self.heightColLike.constant = 30
                    self.heightLblLike.constant = 21
                    self.heightMainView.constant += 51
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.colLike.reloadData()
                if self.dari == "" {
                    self.getDataReply(message)
                }
                self.dari = ""
        }
    }
    
    func numberOfSectionsInCollectionView(_ collectionView: UICollectionView) -> Int {
        return 2
    }
    
    var dataImgContent: JSON!
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if self.paramsJson == 1 {
            if collectionView == colImgContent {
                print("return dataImgContent")
                print(self.dataImgContent.count)
                return self.dataImgContent.count
            }else if collectionView == colLike {
                if self.paramsJsonLike == 1 {
                    print("return jsonData")
                    print(self.jsonData.count)
                    return self.jsonData.count
                }
            }
        }
        return 0
    }
    
    var idLiker = [String]()
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell = LikeLessThreadCell()
        
        if paramsJson == 1 {
            if collectionView == colImgContent {
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgSelectedThreadCell", for: indexPath) as! LikeLessThreadCell
                
                if let data = self.dataImgContent[indexPath.row].string {
                    cell.imgLiker.loadImageUsingCacheWithUrlString(data)
                }else{
                    cell.imgLiker.image = UIImage(named: "default-avatar")
                }
                cell.imgLiker.layer.borderColor = UIColor.lightGray.cgColor
                cell.imgLiker.layer.borderWidth = 0.5
            }else if collectionView == colLike{
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: "likeLessThreadCell", for: indexPath) as! LikeLessThreadCell
                
                for (key, _) in self.jsonData {
                    self.idLiker.append(key)
                }
                let data = jsonData[indexPath.row]
                if let imgUrl = data["user"]["thumbnail"].string {
                    cell.imgLiker.loadImageUsingCacheWithUrlString(imgUrl)
                }else{
                    cell.imgLiker.image = UIImage(named: "default-avatar")
                }
                cell.imgLiker.layer.cornerRadius = cell.imgLiker.frame.width / 2
                cell.imgLiker.clipsToBounds = true
                //Data Like
                let idLastLiker = self.jsonData.count - 1
                let lastLiker = jsonData[idLastLiker]["user"]["name"].string!
                let likeView = jsonData.count - 1
                var like = ""
                if likeView > 0 {
                    like = " and " + String(likeView) + " other"
                }
                self.lblLike.text = like
                self.lblLike.text = lastLiker + like + " like this."
            }
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath)
    {
        
    }
    
    var totalLikes = ""
    func listLikesTapped(_ sender: UITapGestureRecognizer){
        self.performSegue(withIdentifier: "showLikesThread", sender: self)
    }
    
    func addReply(){
        self.performSegue(withIdentifier: "showReplyForum", sender: self)
    }
    
    var dari = ""
    @IBAction func btnLikeTapped(_ sender: AnyObject) {
        self.dari = "like"
        makeSerialize()
    }
    
    func postLike(_ message: String){
        let urlString = Config().urlThread + "add_like"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let dataJason = JSON(value)["msg"]
                if dataJason.count > 0 {
                    print("Liked")
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.dari = "refreshLike"
                self.makeSerialize()
        }
    }
    
    @IBAction func btnCommentTapped(_ sender: AnyObject) {
        self.commentUntuk = ""
        self.performSegue(withIdentifier: "showCommentForum", sender: self)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    @IBAction func btnShareTapped(_ sender: AnyObject) {
        let VC : UIActivityViewController = UIActivityViewController(activityItems: [self.lblTitle.text!, self.lblContent.text!], applicationActivities: nil)
        
        VC.popoverPresentationController?.sourceView = self.view
        
        if let presenter = VC.popoverPresentationController {
            presenter.sourceView = sender as? UIView
            presenter.sourceRect = sender.bounds;
        }
        self.present(VC, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showLikesThread" {
            let conn = segue.destination as! LikesForumController
            conn.idThread = self.idThread
            conn.totalLikes = self.totalLikes
        }
        if segue.identifier == "showCommentForum" {
            let conn = segue.destination as! CommentForumController
            conn.idThread = self.idThread
            conn.dari = self.commentUntuk
            conn.idThreadReply = self.idThreadReply
        }
        if segue.identifier == "showReplyForum" {
            let conn = segue.destination as! AddReplyController
            conn.idThread = self.idThread
            conn.dari = "reply"
        }
    }
    
    var jsonDataReply: JSON!
    var paramsJsonReply = 0
    func getDataReply(_ message: String){
        let urlString = Config().urlThread + "list_thread_reply/0/50"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                self.jsonDataReply = JSON(value)["msg"]
                print(self.jsonDataReply)
                let count = self.jsonDataReply.count
                // tinggi awal
                let heightZero = self.heigthTbReply.constant
                self.heigthTbReply.constant = 0
                self.heightMainView.constant -= heightZero
                if count > 0 {
                    self.paramsJsonReply = 1
                    let plus = 120 * count
                    self.heigthTbReply.constant = CGFloat(plus)
                    self.heightMainView.constant += CGFloat(plus)
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.tbReply.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if paramsJsonReply == 1 {
            if tableView == tbComment {
                let count = self.jsonDataComment.count
                if count > 3 {
                    return 4
                }else{
                    return self.jsonDataComment.count
                }
            }else{
                return self.jsonDataReply.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tbComment {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listCommentCell", for: indexPath) as! ListGeneralCell
            
            let data: JSON!
            var name: String! = ""
            var comment: String! = ""
            var date: String! = ""
            let count = self.jsonDataComment.count
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MMM dd, YYYY hh:mm a"
            
            if count > 3 {
                if indexPath.row < 3 {
                    let inx = count - 3
                    data = self.jsonDataComment[inx + indexPath.row]
                    name = data["user"]["name"].string
                    comment = data["comment"].string!
                    let dataDateFrom = data["date"].string!
                    if let dateData = dateFormatterGet.date(from: dataDateFrom) {
                        let dateView = dateFormatterPrint.string(from: dateData)
                        date = dateView
                    }else{
                        date = data["date"].string! + " ago"
                    }
                }
            }else{
                data = self.jsonDataComment[indexPath.row]
                name = data["user"]["name"].string!
                comment = data["comment"].string!
                let dataDateFrom = data["date"].string!
                if let dateData = dateFormatterGet.date(from: dataDateFrom) {
                    let dateView = dateFormatterPrint.string(from: dateData)
                    date = dateView
                }else{
                    date = data["date"].string! + " ago"
                }
            }
            
            let strComment = name + " : " + comment + " (" + date + ")"
            let lengStrName = name.characters.count
            let lengStrIsi = comment.characters.count
            let lengStrDate = date.characters.count
            
            let myMutableString = NSMutableAttributedString(string: strComment)
            myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 63, green: 157, blue: 247), range: NSRange(location:0,length:lengStrName))
            myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location:lengStrName + lengStrIsi + 4,length:lengStrDate+2))
            
            if count > 3 {
                if indexPath.row < 3 {
                    cell.lblTitle.attributedText = myMutableString
                }else{
                    cell.lblTitle.text = "See All \(count) Comments"
                    cell.lblTitle.textColor = UIColor(red: 63, green: 157, blue: 247)
                    cell.lblTitle.textAlignment = .center
                }
            }else{
                cell.lblTitle.attributedText = myMutableString
            }
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "threadReplyCell", for: indexPath) as! ThreadReplyCell
            if paramsJsonReply == 1 {
                let data = jsonDataReply[indexPath.row]
                
                cell.lblName.text = data["name_user"].string!
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "d MMM YYYY, h:mm a"
                let dataDateFrom = data["date"].string!
                if let date = dateFormatterGet.date(from: dataDateFrom) {
                    let dateView = dateFormatterPrint.string(from: date)
                    cell.lblTime.text = dateView
                }else{
                    cell.lblTime.text = data["date"].string! + " ago"
                }
                
                let contentReply = data["reply"].string!
                cell.lblContent.text = contentReply
                let heightContent = Config().estimateFrameForText(contentReply, width: Int(cell.lblContent.frame.width)).height
                cell.heightContentReply.constant = heightContent
                
                let imgContent = data["img_content"]
                if imgContent.count > 0 {
                    cell.dataParams = 1
                    cell.dataImg = imgContent
                    self.heigthTbReply.constant += 50.0
                    self.heightMainView.constant += 50.0
                    cell.heightImgContentReply.constant = 50
                    cell.colImgContent.reloadData()
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tbComment {
            print("tap commend")
            let count = self.jsonDataComment.count
            if count > 3 {
                if indexPath.row > 2 {
                    self.commentUntuk = ""
                    self.performSegue(withIdentifier: "showCommentForum", sender: self)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height = 0
        
        if tableView == tbComment {
            let data: JSON!
            let count = self.jsonDataComment.count
            var comment = ""
            var name = ""
            var date = ""
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MMM dd, YYYY hh:mm a"
            
            if count > 3 {
                if indexPath.row < 3 {
                    let inx = count - 3
                    data = self.jsonDataComment[inx + indexPath.row]
                    comment = data["comment"].string!
                    name = data["user"]["name"].string!
                    let dataDateFrom = data["date"].string!
                    if let dateData = dateFormatterGet.date(from: dataDateFrom) {
                        let dateView = dateFormatterPrint.string(from: dateData)
                        date = dateView
                    }else{
                        date = data["date"].string! + " ago"
                    }
                }else{
                    comment = "See All 100 Comments"
                }
            }else{
                data = self.jsonDataComment[indexPath.row]
                name = data["user"]["name"].string!
                comment = data["comment"].string!
                let dataDateFrom = data["date"].string!
                if let dateData = dateFormatterGet.date(from: dataDateFrom) {
                    let dateView = dateFormatterPrint.string(from: dateData)
                    date = dateView
                }else{
                    date = data["date"].string! + " ago"
                }
            }
            let strComment = name + " : " + comment + " (" + date + ")"
            
            let widthLblComment = self.viewComment.frame.width
            let heightLblComment = Config().estimateFrameForText(strComment, width: Int(widthLblComment)).height
            height = Int(heightLblComment) + 19
            if height < 35 {
                height = 35
            }else{
                let plus = height - 35
                self.heightViewComment.constant += CGFloat(plus)
                self.heightMainView.constant += CGFloat(plus)
            }
        }else{
            height = 120
            let data = jsonDataReply[indexPath.row]
            let imgContent = data["img_content"]
            if imgContent.count > 0 {
                height += 50
            }
        }
        return CGFloat(height)
    }
    
    var commentUntuk = ""
    var idThreadReply = ""
    @IBAction func btnCommentThreadTapped(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview as! ThreadReplyCell
        let indexPath = tbReply.indexPath(for: cell)
        let data = jsonDataReply[indexPath!.row]
        idThreadReply = data["id_reply"].string!
        
        commentUntuk = "reply"
        self.performSegue(withIdentifier: "showCommentForum", sender: self)
    }
    
    @IBAction func backToDetailForum(_ segue: UIStoryboardSegue){
        print("wb to detail forum")
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_thread" : self.idThread as AnyObject,
            "id_user" : self.idUser as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        let utf8str = jsonString?.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            self.getDataReply(base64Encoded)
        }
    }
}
