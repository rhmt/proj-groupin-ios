//
//  DetailGroupController.swift
//  Groupin
//
//  Created by Macbook pro on 8/1/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class DetailGroupController: UIViewController {
    
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblDesk: UITextView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblMember: UILabel!
    @IBOutlet weak var lblGroupParent: UILabel!
    @IBOutlet weak var lblTitleGroupParent: UILabel!
    @IBOutlet weak var imgAvaParent: UIImageView!
    
    @IBOutlet weak var conHeightLblTitleGroup: NSLayoutConstraint!
    @IBOutlet weak var conHeightLblGroup: NSLayoutConstraint!
    @IBOutlet weak var conHeightAvaParent: NSLayoutConstraint!
    
    @IBOutlet weak var conHeightViewContainer: NSLayoutConstraint!
    
    var from = "" // 0 Welcome, 1 Search Groups
    var idUser = 0
    var idGroup = 0
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showAnimate()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        //Click to close
        let tapBack = UITapGestureRecognizer(target: self, action: #selector(DetailGroupController.closePopup(_:)))
        tapBack.numberOfTapsRequired = 1
        mainView.isUserInteractionEnabled = true
        mainView.addGestureRecognizer(tapBack)
        
        lblCategory.textColor = UIColor(red: 63, green: 157, blue: 247)
        lblLocation.textColor = UIColor(red: 63, green: 157, blue: 247)
        lblMember.textColor = UIColor(red: 63, green: 157, blue: 247)
        lblGroupParent.textColor = UIColor(red: 63, green: 157, blue: 247)
        
        //Get ID user
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.token = idSession!.token
        
        makeSerialize()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    @IBAction func btnCloseTapped(_ sender: AnyObject) {
        self.removeAnimate()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idGroup as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    func getData(_ message: String){
        let urlString = Config().urlGroup + "detail_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                let data = jsonData["msg"]
                print(data)
                let code = jsonData["code"].string
                if code == "1" {
                    
                    self.is_public = data["is_public"].intValue
                    
                    if let imgUrl = data["avatar"].string {
                        self.imgAva.image = UIImage(named: "dafault-ava-group")
                        self.imgAva.loadImageUsingCacheWithUrlString(imgUrl)
                    }else{
                        self.imgAva.image = UIImage(named: "dafault-ava-group")
                    }
                    self.imgAva.layer.cornerRadius = 5
                    self.imgAva.clipsToBounds = true
                    self.lblName.text = data["group_name"].string
                    self.nameGroupSelected = data["group_name"].string!
                    self.lblCategory.text = data["category"].string
                    self.lblDesk.text = data["desc"].string
                    self.lblLocation.text = data["city"].string! + ", " + data["country"].string!
                    var members = "1"
                    if let member = data["member"].string {members=member}
                    self.lblMember.text = members
                    let parent = data["parent"]
                    if parent.count > 0 {
                        self.lblGroupParent.isHidden = false
                        self.lblTitleGroupParent.isHidden = false
                        self.conHeightLblTitleGroup.constant = 21
                        self.conHeightAvaParent.constant = 45
                        self.conHeightLblGroup.constant = 21
                        self.conHeightViewContainer.constant = 425
                        for (key, _) in parent {
                            self.lblGroupParent.text = parent[key]["group_name"].string
                            if let imgParentUrl = parent[key]["avatar"].string {
                                self.imgAvaParent.image = UIImage(named: "dafault-ava-group")
                                self.imgAvaParent.loadImageUsingCacheWithUrlString(imgParentUrl)
                            }else{
                                self.imgAvaParent.image = UIImage(named: "dafault-ava-group")
                            }
                            self.imgAvaParent.layer.cornerRadius = 5
                            self.imgAvaParent.clipsToBounds = true
                        }
                    }else{
                        self.lblGroupParent.isHidden = true
                        self.lblTitleGroupParent.isHidden = true
                        self.conHeightLblTitleGroup.constant = 0
                        self.conHeightAvaParent.constant = 0
                        self.conHeightLblGroup.constant = 0
                        self.conHeightViewContainer.constant = 343
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var is_public = 0
    @IBAction func btnJoinTapped(_ sender: AnyObject) {
        if self.is_public > 0 {
            let dataValid = try! Realm().objects(tb_user.self).first!
            let isValid = dataValid.valid_email
            if isValid == "2" {
                self.msgError = "Your email has not been verified"
                self.alertStatus()
            }else{
                if let _ = try! Realm().objects(tb_group.self).filter("id = %@", self.idGroup).first {
                    print("Direct")
                    self.directToGroup()
                }else{
                    if self.is_public == 1 {
                        print("join - public")
                        self.makeSerializeJoin()
                    }else{
                        print("join - private")
                        let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showReasonJoinGroup") as! ReasonJoinGroupController
                        Popover.idGroup = self.idGroup
                        self.addChildViewController(Popover)
                        Popover.view.frame = self.view.frame
                        self.view.addSubview(Popover.view)
                        Popover.didMove(toParentViewController: self)
                    }
                }
            }
        }
    }
    
    func makeSerializeJoin(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_group" : self.idGroup as AnyObject,
            "id" : self.idUser as AnyObject,
            "is_private" : 1 as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Join(jsonString!)
    }
    
    func makeBase64Join(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getDataJoin(base64Encoded)
        }
    }
    
    func getDataJoin(_ message: String){
        let urlString = Config().urlGroup + "join_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(urlString)
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)["msg"]
                print(jsonData)
                self.msgError = "Join group success"
            }else{
                print("Something Went Wrong..")
                self.msgError = "Failed connet to server, try again"
            }
            }.responseData { Response in
               self.alertStatus()
        }
    }
    
    var msgError = ""
    func alertStatus(){
        if msgError != "" {
            let alert = UIAlertController(title: "", message: msgError, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.makeSerializeData()
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func makeSerializeData(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idUser as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Data(jsonString!)
    }
    
    func makeBase64Data(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getDataGroup(base64Encoded)
        }
    }
    
    func getDataGroup(_ message: String){
        let urlString = Config().urlGroup + "list_own_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                let jsonGroup = jsonData["msg"]
                //Group
                if jsonGroup.count > 0 {
                    let objGroup = try! Realm().objects(tb_group.self)
                    try! Realm().write(){
                        try! Realm().delete(objGroup)
                    }
                    for (key, _) in jsonGroup {
                        let modelGroup = tb_group()
                        modelGroup.id = Int(key)!
                        modelGroup.name = jsonGroup[key]["group"].string!
                        if let ava = jsonGroup[key]["avatar"].string {
                            modelGroup.avatar = ava
                        }else{
                            modelGroup.avatar = ""
                        }
                        modelGroup.city = jsonGroup[key]["city"].string!
                        modelGroup.country = jsonGroup[key]["country"].string!
                        modelGroup.jlm_member = jsonGroup[key]["member"].string!
                        modelGroup.category = jsonGroup[key]["category"].string!
                        modelGroup.desc = jsonGroup[key]["desc"].string!
                        modelGroup.date_create = jsonGroup[key]["date_create"].string!
                        modelGroup.id_user_create = jsonGroup[key]["id_user_create"].string!
                        modelGroup.user_create = jsonGroup[key]["name_user_create"].string!
                        modelGroup.is_public = jsonGroup[key]["is_public"].string!
                        modelGroup.is_promot = jsonGroup[key]["is_promote"].string!
                        modelGroup.user_level = jsonGroup[key]["user_level"].string!
                        DBHelper.insert(modelGroup)
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                //self.directToGroup()
        }
    }
    
    var nameGroupSelected = ""
    func directToGroup(){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "showGroupHome") as! GroupHomeController
        vc.idGroup = self.idGroup
        let model = try! Realm().objects(session.self).first
        try! Realm().write(){
            model?.id_group = self.idGroup
        }
        vc.navigationItem.title = self.nameGroupSelected
        vc.fromCreate = 1
        vc.tabBarController?.tabBar.isHidden = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func closePopup(_ sender: UITapGestureRecognizer){
        self.removeAnimate()
    }
    
    func showAnimate(){
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0.0
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            }, completion: {(finished: Bool) in
                if(finished){
                    self.view.removeFromSuperview()
                }
        })
    }
    
}
