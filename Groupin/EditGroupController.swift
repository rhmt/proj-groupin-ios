//
//  EditGroupController.swift
//  Groupin
//
//  Created by Macbook pro on 8/10/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import DropDown
import SwiftyJSON
import Alamofire
import Photos

class EditGroupController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate, UITextFieldDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var imgSetAva: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var lblLeft: UILabel!
    @IBOutlet weak var txtDesk: UITextView!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var btnCat: UIButton!
    @IBOutlet weak var imgRadio1: UIImageView!
    @IBOutlet weak var imgRadio2: UIImageView!
    @IBOutlet weak var btnUnlimited: UIButton!
    @IBOutlet weak var txtJlmMember: UITextField!{
        didSet{
            txtJlmMember.keyboardType = UIKeyboardType.numberPad
        }
    }
    var keyboardType: UIKeyboardType {
        get{
            return txtJlmMember.keyboardType
        }
        set{
            if newValue != UIKeyboardType.numberPad{
                self.keyboardType = UIKeyboardType.numberPad
            }
        }
    }
    @IBOutlet weak var btnLimited: UIButton!
    @IBOutlet weak var lblPublic: UILabel!
    @IBOutlet weak var swPublic: UISwitch!
    @IBOutlet weak var lblPromoted: UILabel!
    @IBOutlet weak var swPromoted: UISwitch!
    @IBOutlet weak var tbAdmin: UITableView!
    @IBOutlet weak var btnSave: UIButton!
    
    var listCat = DropDown()
    var listCountry = DropDown()
    var listCity = DropDown()
    lazy var dropDowns: [DropDown] = { return [self.listCat, self.listCountry, self.listCity] }() 
    
    var idUser = 0
    var idGroup = 0
    var token = ""
    
    var imgPicker = UIImagePickerController()
    var imgData = Data()
    
    var unlimitedStatus = "true"
    var limitedStatus = "false"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtDesk.delegate = self
        self.txtName.delegate = self
        self.txtCountry.delegate = self
        self.txtCity.delegate = self
        self.txtJlmMember.delegate = self
        self.scrollView.delegate = self
        
        imgPicker.delegate = self
        
        //Img Ava
        let tapPhoto = UITapGestureRecognizer(target: self, action: #selector(EditGroupController.btnSetPhotoTapped(_:)))
        tapPhoto.numberOfTapsRequired = 1
        imgAva.isUserInteractionEnabled = true
        imgAva.addGestureRecognizer(tapPhoto)
        
        NotificationCenter.default.addObserver(self, selector: #selector(EditGroupController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EditGroupController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //Rounded Button
        btnSave.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
        btnSave.layer.cornerRadius = 5
        btnSave.layer.borderWidth = 1
        btnSave.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //Get id_group form session
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.idGroup = idSession!.id_group
        self.token = idSession!.token
        
        //Desc Rounded
        imgAva.layer.cornerRadius = 10
        imgAva.clipsToBounds = true
        txtDesk.layer.cornerRadius = 5
        txtDesk.layer.borderColor = UIColor.lightGray.cgColor
        txtDesk.layer.borderWidth = 0.5
        btnSave.layer.cornerRadius = 5
        btnSave.layer.borderColor = UIColor.lightGray.cgColor
        btnSave.layer.borderWidth = 1
        
        makeSerialize()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idGroup as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            self.getData(base64Encoded)
        }
    }
    
    func getData(_ message: String){
        let urlString = Config().urlGroup + "detail_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                let data = jsonData["msg"]
                print(data)
                
                if let imgUrl = data["avatar"].string {
                    self.imgAva.loadImageUsingCacheWithUrlString(imgUrl)
                }else{
                    self.imgAva.image = UIImage(named: "dafault-ava-group")
                }
                self.imgAva.contentMode = .scaleToFill
                self.txtName.text = data["group_name"].string
                self.txtDesk.text = data["desc"].string
                let desc = data["desc"].string
                let countDesc = desc!.characters.count
                self.lblLeft.text = String(500 - countDesc)
                self.txtCountry.text = data["country"].string!
                self.txtCity.text = data["city"].string!
                
                self.btnCat.setTitle(data["category"].string, for: UIControlState.normal)
                if data["number_member_allowed"].string == "0" {
                    self.imgRadio1.image = UIImage(named: "ico_radio_on")
                    self.imgRadio2.image = UIImage(named: "ico_radio_off")
                    self.txtJlmMember.isEnabled = false
                }else{
                    // Isi sama yg buat limited
                    self.imgRadio1.image = UIImage(named: "ico_radio_off")
                    self.imgRadio2.image = UIImage(named: "ico_radio_on")
                    self.txtJlmMember.isEnabled = true
                    self.txtJlmMember.text = data["total_member"].string
                }
                if data["is_public"].string == "1" {
                    self.lblPublic.text = "Public"
                    self.lblPublic.textColor = UIColor.green
                    self.swPublic.isOn = true
                }else{
                    self.lblPublic.text = "Private"
                    self.lblPublic.textColor = UIColor.red
                    self.swPublic.isOn = false
                }
                if data["is_promote"].string == "1" {
                    self.lblPromoted.text = "Promoted"
                    self.lblPromoted.textColor = UIColor.green
                    self.swPromoted.isOn = true
                }else{
                    self.lblPromoted.text = "Not Promoted"
                    self.lblPromoted.textColor = UIColor.red
                    self.swPromoted.isOn = false
                }
                
                //Edit
                self.idCountrySelected = data["fk_country"].intValue
                self.idCitySelected = data["fk_city"].intValue
                self.catagorySelected = data["fk_category"].string!
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.getMember(message)
        }
    }
    
    var dataMember: JSON!
    var countAdmin = 0
    var paramsJson = 0
    func getMember(_ message: String){
        let urlString = Config().urlUser + "list_member_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                self.dataMember = jsonData["msg"]
                if self.dataMember.count > 0{
                    self.paramsJson = 1
                }
                
                //Data
                self.idMember.removeAll()
                self.countAdmin = 0
                if self.paramsJson == 1 {
                    for (key, _) in self.dataMember {
                        if self.dataMember[key]["user_level"].string! == "2"{
                            self.countAdmin += 1
                            self.idMember.append(key)
                        }
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.tbAdmin.reloadData()
        }
    }
    
    func btnSetPhotoTapped(_ sender: UITapGestureRecognizer) {
        BrowseFoto(UIButton.init())
    }
    
    func BrowseFoto(_ sender: UIButton) {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.modalPresentationStyle = .popover
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.cameraTapped()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.libraryTapped()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds;
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    // Camera
    func cameraTapped(){
        if Config().checkAccessCamera() == false {
            print("access denied")
            let msg = "Please allow the app to access your camera through the Settings."
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                if granted == true {
                    // User granted
                    Config().alertForAllowAccess(self, msg: msg)
                }else{
                    // User Rejected
                    Config().alertForAllowAccess(self, msg: msg)
                }
            });
        }else{
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
            {
                self.imgPicker.delegate = self;
                
                self.imgPicker.sourceType = UIImagePickerControllerSourceType.camera
                self.imgPicker.mediaTypes = [kUTTypeImage as String]
                self.imgPicker.allowsEditing = true
                self.imgPicker.videoMaximumDuration = 15
                self.present(self.imgPicker, animated: true, completion: nil)
            }
            else
            {
                libraryTapped()
            }
        }
    }
    // Library Image
    func libraryTapped(){
        if Config().checkAccessPhotos() == false {
            print("access denied")
            let msg = "Please allow the app to access your photos through the Settings."
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.denied {
                    print("denied")
                    Config().alertForAllowAccess(self, msg: msg)
                }else{
                    print("another")
                    Config().alertForAllowAccess(self, msg: msg)
                }
            })
        }else{
            self.imgPicker.allowsEditing = true
            self.imgPicker.sourceType = .photoLibrary
            present(imgPicker, animated: true, completion: nil)
        }
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    var postImageName: String!
    var postImageUrl: String!
    var paramsUploadAva = 0
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var pickedImage: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            pickedImage = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            pickedImage = originalImage
        }
        imgAva.contentMode = .scaleToFill
        imgAva.image = pickedImage
        
        let imageName = UUID().uuidString
        postImageName = imageName
        let imagePath = getDocumentsDirectory().appendingPathComponent(imageName+".jpg")
        postImageUrl = imagePath
        paramsUploadAva = 1
        
        let jpegData = UIImageJPEGRepresentation(pickedImage!, 0.8)
        try? jpegData!.write(to: URL(fileURLWithPath: imagePath), options: [.atomic])
        
        self.imgData = jpegData!
        
        dismiss(animated: true, completion: nil)
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    //==============================================
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtDesk.text == "Description" {//.textColor == UIColor.lightGrayColor() {
            txtDesk.text = nil
            txtDesk.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtDesk.text!.isEmpty {
            txtDesk.text = "Description"
            txtDesk.textColor = UIColor.gray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count
        self.lblLeft.text = String(500 - numberOfChars)
        return numberOfChars < 500;
    }
    
    var idCountrySelected = 0
    var nameCountrySelected = ""
    var listDataCountry = [""]
    @IBAction func txtCountryTyping(_ sender: AnyObject) {
        let urlString = Config().urlMain + "get_country"
        let word = txtCountry.text
        Alamofire.request(urlString, method: .post, parameters: ["like" : word!]).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                self.listDataCountry.removeAll()
                for (key, _) in jsonData{
                    let dataCountry = jsonData[key]["country"]
                    self.listDataCountry.append(String(describing: dataCountry))
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                if self.txtCountry.text != "" {
                    self.setupCountry()
                    self.listCountry.show()
                }else{
                    self.listCountry.hide()
                }
        }
    }
    
    var idCitySelected = 0
    var nameCitySelected = ""
    var listDataCity = [""]
    @IBAction func txtCityTyping(_ sender: AnyObject) {
        let urlString = Config().urlMain + "get_city/" + String(idCountrySelected)
        let word = txtCity.text
        Alamofire.request(urlString, method: .post, parameters: ["like" : word!]).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                self.listDataCity.removeAll()
                for (key, value) in jsonData{
                    self.idCitySelected = Int(key)!
                    self.listDataCity.append(String(describing: value))
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                if self.txtCity.text != "" {
                    self.setupCity()
                    self.listCity.show()
                }else{
                    self.listCity.hide()
                }
        }
    }
    
    func getIdCountryOrCity(_ params: String){
        var urlString = ""
        var like = ""
        var keySave = ""
        
        if params == "country" {
            urlString = Config().urlMain + "get_country"
            like = self.nameCountrySelected
        }else{
            urlString = Config().urlMain + "get_city/" + String(self.idCountrySelected)
            like = self.nameCitySelected
        }
        
        Alamofire.request(urlString, method: .post, parameters: ["like" : like]).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                for (key, _) in jsonData{
                    keySave = key
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                if params == "country" {
                    self.idCountrySelected = Int(keySave)!
                    print(self.idCountrySelected)
                }else{
                    self.idCitySelected = Int(keySave)!
                    print(self.idCitySelected)
                }
        }
    }
    
    //Dropdown Country and City
    func setupCountry() {
        listCountry.anchorView = txtCountry
        listCountry.bottomOffset = CGPoint(x: 0, y: txtCountry.bounds.height)
        listCountry.direction = .bottom
        listCountry.dataSource = listDataCountry
        
        listCountry.selectionAction = { [unowned self] (index, item) in
            self.txtCountry.text = item
            self.txtCountry.resignFirstResponder()
            self.nameCountrySelected = item
            self.getIdCountryOrCity("country")
        }
    }
    func setupCity() {
        listCity.anchorView = txtCity
        listCity.bottomOffset = CGPoint(x: 0, y: txtCity.bounds.height)
        listCity.direction = .bottom
        listCity.dataSource = listDataCity
        
        listCity.selectionAction = { [unowned self] (index, item) in
            self.txtCity.text = item
            self.txtCity.resignFirstResponder()
            self.nameCitySelected = item
            self.getIdCountryOrCity("city")
        }
    }
    
    @IBAction func btnCatTapped(_ sender: AnyObject) {
        setupListCat()
        listCat.show()
    }
    
    var listDataCatagory = [""]
    var listDataIdCatagory = [""]
    var catagorySelected = ""
    func setupListCat(){
        let urlString = Config().urlGroup + "get_group_type"
        Alamofire.request(urlString, method: .post).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                self.listDataCatagory.removeAll()
                self.listDataIdCatagory.removeAll()
                for (key, value) in jsonData{
                    self.listDataIdCatagory.append(key)
                    self.listDataCatagory.append(value.string!)
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.listCat.anchorView = self.btnCat
                self.listCat.bottomOffset = CGPoint(x: 0, y: self.btnCat.bounds.height)
                self.listCat.dataSource = self.listDataCatagory
                
                self.listCat.selectionAction = { [unowned self] (index, item) in
                    self.btnCat.setTitle(item, for: .normal)
                    let catSelect = self.listDataCatagory.index(of: item)
                    let saveCat = self.listDataIdCatagory[catSelect!]
                    self.catagorySelected = saveCat
                    if item == "" {
                        self.btnCat.setTitleColor(UIColor.gray, for: UIControlState.normal)
                    }else{
                        self.btnCat.setTitleColor(UIColor.black, for: UIControlState.normal)
                    }
                }
                
                self.listCat.show()
        }
    }
    
    @IBAction func btnUnlimitedTapped(_ sender: AnyObject) {
        self.unlimitedStatus = "true"
        self.limitedStatus = "false"
        
        imgRadio1.image = UIImage(named: "ico_radio_on")
        imgRadio2.image = UIImage(named: "ico_radio_off")
        txtJlmMember.isEnabled = false
    }
    
    @IBAction func btnLimitedTapped(_ sender: AnyObject) {
        self.unlimitedStatus = "false"
        self.limitedStatus = "true"
        
        imgRadio1.image = UIImage(named: "ico_radio_off")
        imgRadio2.image = UIImage(named: "ico_radio_on")
        txtJlmMember.isEnabled = true
        txtJlmMember.becomeFirstResponder()
    }
    
    @IBAction func swPublicTapped(_ sender: UISwitch) {
        if sender.isOn {
            lblPublic.text = "Public"
            lblPublic.textColor = UIColor(red: 83, green: 186, blue: 32)
        }else{
            lblPublic.text = "Private"
            lblPublic.textColor = UIColor(red: 232, green: 76, blue: 61)
        }
    }
    
    @IBAction func swPromotTapped(_ sender: UISwitch) {
        if sender.isOn {
            lblPromoted.text = "Promoted"
            lblPromoted.textColor = UIColor(red: 83, green: 186, blue: 32)
        }else{
            lblPromoted.text = "Not Promoted"
            lblPromoted.textColor = UIColor(red: 232, green: 76, blue: 61)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSetAdmin" {
            let conn = segue.destination as! SetAdminController
            conn.navigationItem.title = "Set Admin"
            conn.dari = "group"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.paramsJson == 1 {
            return self.countAdmin  + 1
        }else{
            return 1
        }
    }
    
    var idMember = [String]()
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SetAdminCell", for: indexPath) as! SetAdminCell
        
        if indexPath.row < countAdmin  {
            let imgUrl = self.dataMember[idMember[indexPath.row]]["avatar"].string!
            cell.imgAva.loadImageUsingCacheWithUrlString(imgUrl)
            cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
            cell.imgAva.clipsToBounds = true
            cell.lblName.text = self.dataMember[idMember[indexPath.row]]["name"].string
        }else{
            cell.imgAva.image = UIImage(named: "ico_add_user")
            cell.lblName.text = "Add User as Admin"
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath){
        if indexPath.row < countAdmin {
//            self.performSegueWithIdentifier("showGroupHome", sender: self)
        }else{
            self.performSegue(withIdentifier: "showSetAdmin", sender: self)
        }
    }
    
    @IBAction func backToAboutGroup(_ segue: UIStoryboardSegue){
        self.tbAdmin.reloadData()
        print(idMember)
        print(countAdmin)
    }
    
    var alertLoading: UIAlertController!
    @IBAction func btnSaveTapped(_ sender: AnyObject) {
        self.alertLoading = UIAlertController(title: "Processing", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        DispatchQueue.main.async(execute: {
            self.present(self.alertLoading, animated: true, completion: nil)
        })
        
        makeSerializeSave()
    }
    
    func makeSerializeSave(){
        // If data pass
        var numberMemberAllowed = ""
        if self.unlimitedStatus == "true" {
            numberMemberAllowed = "0"
        }else if self.limitedStatus == "true"{
            numberMemberAllowed = txtJlmMember.text!
        }
        var isPublic = ""
        if lblPublic.text == "Public" {
            isPublic = "1"
        }else{
            isPublic = "0"
        }
        var isPromot = ""
        if lblPromoted.text == "Promoted" {
            isPromot = "1"
        }else{
            isPromot = "0"
        }
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_group" : self.idGroup as AnyObject,
            "id_user" : self.idUser as AnyObject,
            "name" : txtName.text! as AnyObject,
            "fk_country" : self.idCountrySelected as AnyObject,
            "fk_city" : self.idCitySelected as AnyObject,
            "fk_category" : self.catagorySelected as AnyObject,
            "description" : txtDesk.text! as AnyObject,
            "member_allowed" : numberMemberAllowed as AnyObject,
            "is_public" : isPublic as AnyObject,
            "is_promote" : isPromot as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Save(jsonString!)
    }
    
    func makeBase64Save(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            print("Encoded:  \(base64Encoded)")
            
            if let base64Decoded = Data(base64Encoded: base64Encoded, options:   NSData.Base64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: String.Encoding.utf8.rawValue) })
            {
                print("Decoded:  \(base64Decoded!)")
            }
            
            postData(base64Encoded)
        }
    }
    
    var msgError = ""
    func postData(_ message: String){
        let urlString = Config().urlGroup + "edit_group"
        let token = self.token
        print(token)
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        
        //Upload Data with Image
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            
            if self.paramsUploadAva == 1 {
                multipartFormData.append(URL(fileURLWithPath: self.postImageUrl), withName: "avatar")
            }
            
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: urlString,
        encodingCompletion: {
            encodingResult in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON {
                    response in
                    
                    if let value = response.result.value {
                        let jsonData = JSON(value)
                        print(jsonData)
                        if jsonData["code"] == "1"{
                            self.msgError = ""
                        }else{
                            self.msgError = jsonData["msg"].string!
                        }
                        self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        })
    }
    
    func alertStatus(){
        if msgError != "" {
            let alert = UIAlertController(title: "Info", message: msgError, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }else{
            // jika sukses
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if activeField != nil{
            activeField?.resignFirstResponder()
        }
    }
    
    var activeField: UITextField?
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            let possKeyboard = self.view.frame.height - keyboardSize!.height - activeField!.frame.height
            if activeField!.frame.origin.y > possKeyboard {
//                if (!CGRectContainsPoint(aRect, activeField!.frame.origin)){
                    if view.frame.origin.y == 0{
                        self.view.frame.origin.y -= keyboardSize!.height
//                        scrollView.setContentOffset(CGPointMake(0, possKeyboard), animated: true)
                    }
//                }
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
}
