//
//  EditProfileController.swift
//  Groupin
//
//  Created by Macbook pro on 7/26/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import DropDown
import SwiftyJSON
import Alamofire
import Photos
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class EditProfileController: UIViewController, UITextFieldDelegate, UIPopoverPresentationControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewEdit: UIView!
    
    @IBOutlet weak var viewShow: UIView!
    @IBOutlet weak var showAva: UIImageView!
    @IBOutlet weak var showStatus: UILabel!
    @IBOutlet weak var showPoint: UILabel!
    @IBOutlet weak var showName: UILabel!
    @IBOutlet weak var showGender: UILabel!
    @IBOutlet weak var showBirthday: UILabel!
    @IBOutlet weak var showCountry: UILabel!
    @IBOutlet weak var showCity: UILabel!
    @IBOutlet weak var showInterest: UILabel!
    
    @IBOutlet weak var showTbInterest: UITableView!
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var imgSetAva: UIImageView!
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var btnGender: UIButton!
    @IBOutlet weak var btnDay: UIButton!
    @IBOutlet weak var btnMonth: UIButton!
    @IBOutlet weak var btnYear: UIButton!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    
    @IBOutlet weak var tbInterest: UITableView!
    @IBOutlet weak var heightView: NSLayoutConstraint!
    @IBOutlet weak var heightViewShow: NSLayoutConstraint!
    @IBOutlet weak var heightTable: NSLayoutConstraint!
    
    @IBOutlet weak var conSpaceBotView: NSLayoutConstraint!
    
    let imgPicker = UIImagePickerController()
    let listGender = DropDown()
    let listDay = DropDown()
    let listMonth = DropDown()
    let listYear = DropDown()
    let listCountry = DropDown()
    let listCity = DropDown()
    let listInterest = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.listGender,
            self.listDay,
            self.listMonth,
            self.listYear,
            self.listCountry,
            self.listCity,
            self.listInterest
        ] }() 
    
    var id = 0
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.showPoint.isHidden = true
        self.txtName.delegate = self
        self.txtCountry.delegate = self
        self.txtCity.delegate = self
        self.scrollView.delegate = self
        self.tbInterest.tableFooterView = UIView()
        self.showTbInterest.tableFooterView = UIView()
        editNav()
        setupDropDown()
        
        self.showPoint.isHidden = true
        
        self.loadLocal()
        self.loadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(EditProfileController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EditProfileController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func loadData(){
        self.imgPicker.delegate = self
        
        //Get Phone from Session
        let idSession = try! Realm().objects(session.self).first
        self.id = idSession!.id
        self.token = idSession!.token
        
        // Rounded
        imgAva.layer.cornerRadius = imgAva.frame.width / 2
        imgAva.clipsToBounds = true
        showAva.layer.cornerRadius = showAva.frame.width / 2
        showAva.clipsToBounds = true
        //Rounded Button
        btnSave.backgroundColor = UIColor(red: 83, green: 186, blue: 32)
        btnSave.layer.cornerRadius = 5
        btnSave.layer.borderWidth = 1
        btnSave.layer.borderColor = UIColor.lightGray.cgColor
        
        //Input Ava
        let tapPhoto = UITapGestureRecognizer(target: self, action: #selector(EditProfileController.btnSetAvaTapped(_:)))
        tapPhoto.numberOfTapsRequired = 1
        imgSetAva.isUserInteractionEnabled = true
        imgSetAva.addGestureRecognizer(tapPhoto)
        imgAva.isUserInteractionEnabled = true
        imgAva.addGestureRecognizer(tapPhoto)
    }
    
    var dataInterest = [interestUser]()
    var dataData = [tb_user]()
    func loadLocal(){
        dataData = DBHelper.getAllUser()
        
        let data = dataData[0]
        
        // Show
        if data.avatar != "" {
            let imgUrl = Config().urlImage + data.avatar
            self.showAva.loadImageUsingCacheWithUrlString(imgUrl)
        }else{
            self.showAva.image = UIImage(named: "default-avatar")
        }
        self.showStatus.text = data.status
        self.showName.text = data.name
        var gender = ""
        if data.gender == "1" {
            gender = "Male"
        }else{gender = "Female"}
        self.showGender.text = gender
        let dataDate = data.tgl_lahir
        let fromDate = "YYYY-MM-dd"
        let toDate = "MMM dd, YYYY"
        let showDate = Config().convertDate(dataDate, from: fromDate, to: toDate)
        self.showBirthday.text = showDate
        self.showCountry.text = data.country
        self.showCity.text = data.city
//        self.showInterest.text = data["interest"].string
        
        //Edit
        if data.avatar != "" {
            let imgUrl = Config().urlImage + data.avatar
            self.imgAva.loadImageUsingCacheWithUrlString(imgUrl)
        }else{
            self.imgAva.image = UIImage(named: "default-avatar")
        }
        self.btnStatus.setTitle(data.status, for: UIControlState())
        self.txtName.text = data.name
        self.btnGender.setTitle(gender, for: UIControlState())
        
        let tgl = data.tgl_lahir
        let day = tgl.substring(with: (tgl.characters.index(tgl.startIndex, offsetBy: 8) ..< tgl.characters.index(tgl.endIndex, offsetBy: 0)))
        let month = tgl.substring(with: (tgl.characters.index(tgl.startIndex, offsetBy: 5) ..< tgl.characters.index(tgl.endIndex, offsetBy: -3)))
        let year = tgl.substring(with: (tgl.characters.index(tgl.startIndex, offsetBy: 0) ..< tgl.characters.index(tgl.endIndex, offsetBy: -6)))
        self.btnDay.setTitle(day, for: UIControlState())
        self.btnMonth.setTitle(month, for: UIControlState())
        self.btnYear.setTitle(year, for: UIControlState())
        self.txtCountry.text = data.country
        self.txtCity.text = data.city
        
        //Data untuk save
        self.idCountrySelected = Int(data.id_country)!
        self.idCitySelected = Int(data.id_city)!
        self.idInterestSelected.removeAll()
        self.detailInterestSelected.removeAll()
        let dataIntCari = try! Realm().objects(interestUser.self)
        dataInterest.removeAll()
        for item in dataIntCari {
            dataInterest += [item]
        }
        print("Ini Interest")
        print(dataInterest)
        for dataInt in dataInterest {
            self.idtbInterest.append(dataInt.id_table)
            self.idInterestSelected.append(dataInt.id_category)
            self.detailInterestSelected.append(dataInt.detail)
        }
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.id as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            self.getData(base64Encoded)
        }
    }
    
    var paramsJason = 0
    var data: JSON!
    func getData(_ message: String){
        let urlString = Config().urlUser + "detail_user"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                self.paramsJason = 1
                self.data = jsonData["msg"]
                
                //Save to local
                let modelUser = try! Realm().objects(tb_user.self).first!
                try! Realm().write({ 
                    modelUser.phone = self.data["user"].string!
                    modelUser.name = self.data["name"].string!
                    modelUser.avatar = self.data["avatar"].string!
                    modelUser.status = self.data["msg_status"].string!
                    modelUser.gender = self.data["gender"].string!
                    modelUser.tgl_lahir = self.data["tgl_lahir"].string!
                    modelUser.id_country = self.data["id_country"].string!
                    modelUser.country = self.data["name_country"].string!
                    modelUser.id_city = self.data["id_city"].string!
                    modelUser.city = self.data["name_city"].string!
                })
                
                //Interest
                let objGroup = try! Realm().objects(interestUser.self)
                try! Realm().write(){
                    try! Realm().delete(objGroup)
                }
                let interest = self.data["interest"]
                for (key, _) in interest {
                    let modelInterest = interestUser()
                    let dataInt = self.data["interest"][key]
                    modelInterest.id_table = Int(key)!
                    modelInterest.id_category = dataInt["kode_interest"].intValue
                    modelInterest.category = dataInt["group_interest"].string!
                    modelInterest.detail = dataInt["detail_interest"].string!
                    DBHelper.insert(modelInterest)
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                if self.paramsJason == 1 {
                    self.showTbInterest.reloadData()
                    for _ in self.data["interest"] {
                        self.interestCount.append(0)
                        if self.data["interest"].count > 2 {
                            self.heightView.constant += 50
                            self.heightViewShow.constant += 50
                            self.heightTable.constant += 50
                        }
                    }
                    self.tbInterest.reloadData()
                }
                self.loadLocal()
        }
    }
    
    var interestCount = [Int]()
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == showTbInterest {
            return self.dataInterest.count
        }else{
            return self.idInterestSelected.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        if tableView == showTbInterest {
            let cell = tableView.dequeueReusableCell(withIdentifier: "showInterestCell", for: indexPath) as! ListGeneralCell
            
            cell.lblTitle.text = dataInterest[indexPath.row].category
            cell.lblSubTitle.text = dataInterest[indexPath.row].detail
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "editInterestCell", for: indexPath) as! listInterestCell
            
            if indexPath.row < self.dataInterest.count {
                cell.btnListInterest.setTitleColor(UIColor.black, for: UIControlState())
                cell.btnListInterest.setTitle(dataInterest[indexPath.row].category, for: UIControlState())
                cell.txtInterest.text = dataInterest[indexPath.row].detail
            }
            
            return cell
        }
    }
    
    var alertLoading: UIAlertController!
    @IBAction func btnSaveTapped(_ sender: AnyObject) {
        self.alertLoading = UIAlertController(title: "Processing", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        DispatchQueue.main.async(execute: {
            self.present(self.alertLoading, animated: true, completion: nil)
        })
        
        makeSerializeEdit()
    }
    
    var idtbInterest = [Int]()
    func makeSerializeEdit(){
        print(idtbInterest)
        let namePost = txtName.text!
        var gender = ""
        if btnGender.titleLabel?.text == "Male"{
            gender = "1"
        }else{gender = "2"}
        let tglLahir = (btnYear.titleLabel?.text)! + "-" + (btnMonth.titleLabel?.text)! + "-" + (btnDay.titleLabel?.text)!
        
        var arrayData = [String:AnyObject]()
        var listInterest = [String:AnyObject]()
        var arrayInterest = [NSString]()
        var i = 0
        for id in self.idInterestSelected {
            listInterest = [
                "fk_interest" : id as AnyObject,
                "interest_detail" : self.detailInterestSelected[i] as AnyObject
            ]
            let data = try! JSONSerialization.data(withJSONObject: listInterest, options: .prettyPrinted)
            let stringData = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            
            if i < self.idtbInterest.count {
                let strJason = String(stringData!)
                let jasonData = String(strJason.characters.dropLast())
                let idtbInterest = self.idtbInterest[i]
                let plusIdtbInterest = ("\(jasonData),\n\"idtb_interest\" : \(idtbInterest)\n}")
                arrayInterest += [NSString(string: plusIdtbInterest)]
            }else{
                arrayInterest += [stringData!]
            }
            i += 1
        }
        
        arrayData = [
            "id_user" : self.id as AnyObject,
            "fk_country" : self.idCountrySelected as AnyObject,
            "fk_city" : self.idCitySelected as AnyObject,
            "name" : namePost as AnyObject,
            "avatar" : self.postImageName as AnyObject,
            "gender" : gender as AnyObject,
            "birth_date" : tglLahir as AnyObject]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        let strJason = String(jsonString!)
        let jasonData = String(strJason.characters.dropLast())
        let passData = ("\(jasonData),\n\"interest\" : \(arrayInterest)\n}")
        
        makeBase64Edit(passData as NSString)
    }
    
    func makeBase64Edit(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            print("Encoded:  \(base64Encoded)")
            
            if let base64Decoded = Data(base64Encoded: base64Encoded, options:   NSData.Base64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: String.Encoding.utf8.rawValue) })
            {
                print("Decoded:  \(base64Decoded!)")
            }
            
            postDataEdit(base64Encoded)
        }
    }
    
    var msgError = ""
    func postDataEdit(_ message: String){
        let urlString = Config().urlUser + "update_profile"
        let token = Config().md5(string: message)
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        //Upload Image
        Alamofire.upload(multipartFormData: {
            multipartFormData in

            if self.paramsUploadAva == 1 {
                multipartFormData.append(URL.init(fileURLWithPath: self.postImageUrl), withName: "avatar")
            }
            
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: urlString,
        encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                        
                        if let value = response.result.value {
                            let jsonData = JSON(value)
                            print(jsonData)
                            if jsonData["code"] == "1"{
                                self.msgError = ""
                            }else{
                                self.msgError = jsonData["msg"].string!
                            }
                            self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
                        }
                    }
            case .failure(let encodingError):
                print(encodingError)
                self.msgError = "Failed connect to server, try again"
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
            }
        })
    }
    
    func alertStatus(){
        if msgError != "" {
            let alert = UIAlertController(title: "Info", message: msgError, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }else{
            self.makeSerialize()
            self.cancel()
        }
    }
    
    func btnSetAvaTapped(_ sender: UITapGestureRecognizer) {
        BrowseFoto(UIButton.init())
    }
    
    func BrowseFoto(_ sender: UIButton) {
        let alert:UIAlertController=UIAlertController(title: "Choose Image teteteteet", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.modalPresentationStyle = .popover
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.cameraTapped()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.libraryTapped()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds;
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // Camera
    func cameraTapped(){
        if Config().checkAccessCamera() == false {
            print("access denied")
            let msg = "Please allow the app to access your camera through the Settings."
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                if granted == true {
                    // User granted
                    Config().alertForAllowAccess(self, msg: msg)
                }else{
                    // User Rejected
                    Config().alertForAllowAccess(self, msg: msg)
                }
            });
        }else{
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
            {
                self.imgPicker.delegate = self;
                
                self.imgPicker.sourceType = UIImagePickerControllerSourceType.camera
                self.imgPicker.mediaTypes = [kUTTypeImage as String]
                self.imgPicker.allowsEditing = true
                self.present(self.imgPicker, animated: true, completion: nil)
            }
            else
            {
                libraryTapped()
            }
        }
    }
    
    // Library Image
    func libraryTapped(){
        if Config().checkAccessPhotos() == false {
            print("access denied")
            let msg = "Please allow the app to access your photos through the Settings."
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.denied {
                    print("denied")
                    Config().alertForAllowAccess(self, msg: msg)
                }else{
                    print("another")
                    Config().alertForAllowAccess(self, msg: msg)
                }
            })
        }else{
            self.imgPicker.allowsEditing = true
            self.imgPicker.sourceType = .photoLibrary
            present(imgPicker, animated: true, completion: nil)
        }
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    var postImageName = ""
    var postImageUrl = ""
    var paramsUploadAva = 0
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var pickedImage: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            pickedImage = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            pickedImage = originalImage
        }
        imgAva.contentMode = .scaleToFill
        imgAva.image = pickedImage
        
        let imageName = UUID().uuidString
        postImageName = imageName
        let imagePath = getDocumentsDirectory().appendingPathComponent(imageName+".jpg")
        postImageUrl = imagePath
        paramsUploadAva = 1
        
        let jpegData = UIImageJPEGRepresentation(pickedImage!, 0.8)
        try? jpegData!.write(to: URL(fileURLWithPath: imagePath), options: [.atomic])
        
//        self.imgData = jpegData!
        
        dismiss(animated: true, completion: nil)
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    //==============================================
    
    @IBAction func btnStatusTapped(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showSetStatus", sender: self)
    }
    
    @IBAction func btnGenderTapped(_ sender: AnyObject) {
        listGender.show()
    }
    
    @IBAction func btnDayTapped(_ sender: AnyObject) {
        listDay.show()
    }
    
    @IBAction func btnMonthTapped(_ sender: AnyObject) {
        listMonth.show()
    }
    
    @IBAction func btnYearTapped(_ sender: AnyObject) {
        listYear.show()
    }
    
    var idCountrySelected = 0
    var nameCountrySelected = ""
    var listDataCountry = [""]
    @IBAction func txtCountryTyping(_ sender: AnyObject) {
        let urlString = Config().urlMain + "get_country"
        let word = txtCountry.text
        Alamofire.request(urlString, method: .post, parameters: ["like" : word!]).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                self.listDataCountry.removeAll()
                for (key, _) in jsonData{
                    let dataCountry = jsonData[key]["country"]
                    self.listDataCountry.append(String(describing: dataCountry))
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                if self.txtCountry.text != "" {
                    self.setupCountry()
                    self.listCountry.show()
                }else{
                    self.listCountry.hide()
                }
        }
    }
    
    var idCitySelected = 0
    var nameCitySelected = ""
    var listDataCity = [""]
    @IBAction func txtCityTyping(_ sender: AnyObject) {
        let urlString = Config().urlMain + "get_city/" + String(idCountrySelected)
        let word = txtCity.text
        Alamofire.request(urlString, method: .post, parameters: ["like" : word!]).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                self.listDataCity.removeAll()
                for (key, value) in jsonData{
                    self.idCitySelected = Int(key)!
                    self.listDataCity.append(String(describing: value))
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                if self.txtCity.text != "" {
                    self.setupCity()
                    self.listCity.show()
                }else{
                    self.listCity.hide()
                }
        }
    }
    
    func getIdCountryOrCity(_ params: String){
        var urlString = ""
        var like = ""
        var keySave = ""
        
        if params == "country" {
            urlString = Config().urlMain + "get_country"
            like = self.nameCountrySelected
        }else{
            urlString = Config().urlMain + "get_city/" + String(self.idCountrySelected)
            like = self.nameCitySelected
        }
        
        Alamofire.request(urlString, method: .post, parameters: ["like" : like]).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                for (key, _) in jsonData{
                    keySave = key
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                if params == "country" {
                    self.idCountrySelected = Int(keySave)!
                    print(self.idCountrySelected)
                }else{
                    self.idCitySelected = Int(keySave)!
                    print(self.idCitySelected)
                }
        }
    }
    
    //Dropdown Country and City
    func setupCountry() {
        listCountry.anchorView = txtCountry
        listCountry.bottomOffset = CGPoint(x: 0, y: txtCountry.bounds.height)
        listCountry.direction = .bottom
        listCountry.dataSource = listDataCountry
        
        listCountry.selectionAction = { [unowned self] (index, item) in
            self.txtCountry.text = item
            self.txtCountry.resignFirstResponder()
            self.nameCountrySelected = item
            self.getIdCountryOrCity("country")
        }
    }
    func setupCity() {
        listCity.anchorView = txtCity
        listCity.bottomOffset = CGPoint(x: 0, y: txtCity.bounds.height)
        listCity.direction = .bottom
        listCity.dataSource = listDataCity
        
        listCity.selectionAction = { [unowned self] (index, item) in
            self.txtCity.text = item
            self.txtCity.resignFirstResponder()
            self.nameCitySelected = item
            self.getIdCountryOrCity("city")
        }
    }
    
    
    var idInterestSelected = [0,0]
    var listDataInterest = [""]
    var listDataIdInterest = [""]
    @IBAction func btnListInterestTapped(_ sender: AnyObject) {
        let urlString = Config().urlGroup + "get_group_type"
        Alamofire.request(urlString, method: .post).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                self.listDataInterest.removeAll()
                self.listDataIdInterest.removeAll()
                for (key, value) in jsonData{
                    self.listDataIdInterest.append(key)
                    self.listDataInterest.append(value.string!)
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                let button = sender
                let view = button.superview!
                let cell = view!.superview as! listInterestCell
                let indexPath = self.tbInterest.indexPath(for: cell)
                
                self.listInterest.anchorView = cell.btnListInterest
                self.listInterest.bottomOffset = CGPoint(x: 0, y: cell.btnListInterest.bounds.height)
                self.listInterest.dataSource = self.listDataInterest
                
                self.listInterest.selectionAction = { [unowned cell] (index, item) in
                    cell.btnListInterest.setTitle(item, for: .normal)
                    let idInterest = self.listDataInterest.index(of: item)
                    let inputInterest = self.listDataIdInterest[idInterest!]
                    self.idInterestSelected[(indexPath?.row)!] = Int(inputInterest)!
                    if item == "" {
                        cell.btnListInterest.setTitleColor(UIColor.gray, for: UIControlState.normal)
                        cell.txtInterest.isEnabled = true
                        cell.txtInterest.resignFirstResponder()
                    }else{
                        cell.btnListInterest.setTitleColor(UIColor.black, for: UIControlState.normal)
                        cell.txtInterest.isEnabled = true
                        cell.txtInterest.becomeFirstResponder()
                    }
                }
                
                self.listInterest.show()
        }
    }
    
    var detailInterestSelected = ["",""]
    @IBAction func txtDetailInterestChanged(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview as! listInterestCell
        let indexPath = self.tbInterest.indexPath(for: cell)
        
        detailInterestSelected[(indexPath?.row)!] = cell.txtInterest.text!
    }
    
    var interest = ["",""]
    @IBAction func btnAddInterestTapped(_ sender: AnyObject) {
        interest.append("")
        idInterestSelected.append(0)
        detailInterestSelected.append("")
        self.interestCount.append(0)
        tbInterest.reloadData()
        
        heightTable.constant += 50
        heightView.constant += 50
        heightViewShow.constant += 50
    }
    
    // DropDown Function
    func setupDropDown(){
        setupGender()
        setupDay()
        setupMonth()
        setupYear()
    }
    func setupGender() {
        listGender.anchorView = btnGender
        listGender.bottomOffset = CGPoint(x: 0, y: btnGender.bounds.height)
        listGender.dataSource = [
            "Male",
            "Female"
        ]
        listGender.selectionAction = { [unowned self] (index, item) in
            self.btnGender.setTitle(item, for: UIControlState())
        }
    }
    func setupDay() {
        listDay.anchorView = btnDay
        listDay.bottomOffset = CGPoint(x: 0, y: btnDay.bounds.height)
        for i in 1...31 {
            if i < 10 {
                listDay.dataSource.append("0\(i)")
            }else{
                listDay.dataSource.append("\(i)")
            }
        }
        
        listDay.selectionAction = { [unowned self] (index, item) in
            self.btnDay.setTitle(item, for: UIControlState())
        }
    }
    func setupMonth() {
        listMonth.anchorView = btnMonth
        listMonth.bottomOffset = CGPoint(x: 0, y: btnMonth.bounds.height)
        for i in 1...12 {
            if i < 10 {
                listMonth.dataSource.append("0\(i)")
            }else{
                listMonth.dataSource.append("\(i)")
            }
        }
        
        listMonth.selectionAction = { [unowned self] (index, item) in
            self.btnMonth.setTitle(item, for: UIControlState())
        }
    }
    func setupYear() {
        let currentYearInt = (Calendar.current as NSCalendar).component(NSCalendar.Unit.year, from: Date())
        let minYear = currentYearInt - 16
        let maxYear = minYear - 30
        
        listYear.anchorView = btnYear
        listYear.bottomOffset = CGPoint(x: 0, y: btnYear.bounds.height)
        for i in (maxYear...minYear).reversed() {
            listYear.dataSource.append("\(i)")
        }
        
        listYear.selectionAction = { [unowned self] (index, item) in
            self.btnYear.setTitle(item, for: UIControlState())
        }
    }
    
    // Methode Nav
    func editNav(){
        let rightMenuButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.edit, target: self, action: #selector(EditProfileController.editTapped))
        self.navigationItem.setRightBarButtonItems([rightMenuButtonItem], animated: true)
    }
    
    func editTapped(_ sender:UIButton){
        self.navigationItem.title = "Edit Profile"
        viewShow.isHidden = true
        viewEdit.isHidden = false
        
        cancelNav()
    }
    
    func cancelNav(){
        let rightMenuButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(EditProfileController.cancelTapped))
        self.navigationItem.setRightBarButtonItems([rightMenuButtonItem], animated: true)
    }
    
    func cancelTapped(_ sender:UIButton){
        self.navigationItem.title = "Profile"
        cancel()
    }
    
    func cancel(){
        viewShow.isHidden = false
        viewEdit.isHidden = true
        
        editNav()
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if activeField != nil{
//            activeField?.resignFirstResponder()
//        }
//    }
    
    var activeField: UITextField?
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            self.conSpaceBotView.constant = keyboardSize!.height
//            let possKeyboard = self.view.frame.height - keyboardSize!.height - activeField!.frame.height
//            if activeField?.frame.origin.y > possKeyboard {
//                if (!CGRectContainsPoint(aRect, activeField!.frame.origin)){
//                    if view.frame.origin.y == 0{
//                        self.view.frame.origin.y -= keyboardSize!.height
//                    }
//                }
//            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.conSpaceBotView.constant = 0
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}
