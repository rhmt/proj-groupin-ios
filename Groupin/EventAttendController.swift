//
//  EventAttendController.swift
//  Groupin
//
//  Created by Macbook pro on 8/30/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire
import DropDown

class EventAttendController: UIViewController, UITextFieldDelegate, UIScrollViewDelegate {
    
    let blue = UIColor(red: 63, green: 157, blue: 247)
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBy: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblAt: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var heightViewmain: NSLayoutConstraint!
    @IBOutlet weak var heightLblAt: NSLayoutConstraint!
    
    @IBOutlet weak var tbTicket: UITableView!
    @IBOutlet weak var heigthTable: NSLayoutConstraint!
    @IBOutlet weak var checkAgree: UIImageView!
    @IBOutlet weak var lblAgree: UILabel!
    @IBOutlet weak var heightLblAgree: NSLayoutConstraint!
    
    let listQty = DropDown()
    lazy var dropDowns: [DropDown] = { return [ self.listQty ] }() 
    
    var idEventSelected = ""
    var nameSelected = ""
    var createdSelected = ""
    var fromSelected = ""
    var toSelected = ""
    var addressSelected = ""
    var ticketSelected: JSON!
    var from = 0 //0:List Event, 1:Detail Event
    
    var idUser = 0
    var idGroup = 0
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colorNav()
        self.navigationItem.title = "Order Ticket"
        self.txtName.delegate = self
        self.scrollView.delegate = self
        
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.idGroup = idSession!.id_group
        self.token = idSession!.token
        
        btnContinue.backgroundColor = UIColor(red: 83, green: 186, blue: 32)
        btnContinue.layer.cornerRadius = 5
        btnContinue.layer.borderWidth = 1
        btnContinue.layer.borderColor = UIColor.lightGray.cgColor
        
        self.makeSerializeData()
        self.showData()
        
        let strAgree = self.lblAgree.text
        let strAdd = "\nAttendant's ID Card will be used for identity check"
        let strText = strAgree! + strAdd
        let attrAgree = NSMutableAttributedString(string: strText)
        attrAgree.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location:strAgree!.characters.count,length:strAdd.characters.count))
        attrAgree.addAttribute(NSFontAttributeName, value: UIFont.italicSystemFont(ofSize: 14), range: NSRange(location:strAgree!.characters.count,length:strAdd.characters.count))
        self.lblAgree.text = ""
        self.lblAgree.attributedText = attrAgree
        
        self.txtName.isEnabled = true
        self.loadAwal()
        
        NotificationCenter.default.addObserver(self, selector: #selector(EventAttendController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EventAttendController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    func loadAwal(){
        let strAgree = self.lblAgree.text! + "\nAttendant's ID Card will be used for identity check"
        
        let heightConAgree = Config().estimateFrameForText(strAgree, width: Int(self.lblAgree.frame.width)).height
        self.heightLblAgree.constant = heightConAgree - 30
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(EventAttendController.checkAgreeTapped(_:)))
        gesture.numberOfTapsRequired = 1
        self.lblAgree.isUserInteractionEnabled = true
        self.lblAgree.addGestureRecognizer(gesture)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(EventAttendController.checkAgreeTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        self.checkAgree.isUserInteractionEnabled = true
        self.checkAgree.addGestureRecognizer(tapGesture)
    }
    
    func makeSerializeData(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idEventSelected as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Data(jsonString!)
    }
    
    func makeBase64Data(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            self.getDataData(base64Encoded)
        }
    }
    
    var paramsJson = 0
    func getDataData(_ message: String){
        let urlString = Config().urlEvent + "detail_event"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post , parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)["msg"]
                print(jsonData)
                self.ticketSelected = jsonData["ticket"]
                if jsonData.count > 0 {
                    self.paramsJson = 1
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.tbTicket.reloadData()
                self.heightView()
        }
    }
    
    func heightView(){
        var height = 0
        if self.paramsJson == 1 {
            height = self.ticketSelected.count * 60
        }
        heigthTable.constant = 32 + CGFloat(height)
        heightViewmain.constant = heightViewmain.constant + CGFloat(height) - 100
        
        let heightAddress = Config().estimateFrameForText(self.addressSelected, width: Int(self.lblAt.frame.width)).height
        self.heightLblAt.constant = heightAddress
    }
    
    func showData(){
        lblTitle.text = self.nameSelected
        lblBy.text = self.createdSelected
        lblFrom.text = self.fromSelected
        lblTo.text = self.toSelected
        lblAt.text = self.addressSelected
    }
    
    // Table List Tickert
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.paramsJson == 1 {
            return ticketSelected.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ticketCell", for: indexPath) as! TicketAttendanceCell
        
        if self.paramsJson == 1 {
            let data = ticketSelected[indexPath.row]
            cell.lblTicketName.text = data["ticket_name"].string
            let mataUang = data["price_type"].string!
            let price = data["price"].string!
            if price == "0" {
                cell.lblPrice.text = "FREE"
            }else{
                cell.lblPrice.text = mataUang.uppercased() + " " + data["price"].string!
            }
            
            cell.btnQty.tag = indexPath.row
            if cell.btnQty.tag != self.tagBtnSelected {
                cell.btnQty.setTitle("0", for: UIControlState())
            }
        }
        
        return cell
    }
    
    var alertLoading: UIAlertController!
    @IBAction func btnContinueTapped(_ sender: AnyObject) {
        self.alertLoading = UIAlertController(title: "Processing", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        DispatchQueue.main.async(execute: {
            self.present(self.alertLoading, animated: true, completion: nil)
        })
        
        let imgButtonOn = UIImage(named: "ico_keep_on")
        if self.checkAgree.image == imgButtonOn && self.txtName.text != "" && self.qtySelected != "" {
            self.makeSerialize()
        }else{
            print("data not complete")
            self.alertLoading.dismiss(animated: true, completion: nil)
            self.msgError = "Please complete the data"
            self.success = 0
            self.alertStatus()
        }
    }
    
    var qtySelected = ""
    var typeSelected = ""
    var priceSelected = ""
    var tagBtnSelected = 0
    @IBAction func btnQtyTapped(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview as! TicketAttendanceCell
        let indexPath = self.tbTicket.indexPath(for: cell)
        tagBtnSelected = button.tag
        
        var quota = 0
        if self.paramsJson == 1 {
            quota = ticketSelected[indexPath!.row]["quota"].intValue - ticketSelected[indexPath!.row]["quota_use"].intValue
        }
        var listDataQty = ["0"]
        var i = 1
        repeat {
            listDataQty.append(String(i))
            i += 1
        }while i <= quota
        
        if self.paramsJson == 1 {
            typeSelected = ticketSelected[indexPath!.row]["idtb_ticket_event"].string!
            priceSelected = ticketSelected[indexPath!.row]["price"].string!
        }
        
        listQty.anchorView = cell.btnQty
        listQty.bottomOffset = CGPoint(x: 0, y: cell.btnQty.bounds.height)
        listQty.direction = .bottom
        listQty.dataSource = listDataQty
        
        listQty.selectionAction = { [unowned cell] (index, item) in
            cell.btnQty.setTitle(item, for: UIControlState())
            self.qtySelected = item
            //self.heigthTable.constant = 8
//            self.heightViewmain.constant = 8
            //self.heightView()
            //self.tbTicket.reloadData()
        }
        
        listQty.show()
    }
    
    @IBAction func btnMoreInfoTapped(_ sender: AnyObject) {
    }
    
    func checkAgreeTapped(_ sender: UITapGestureRecognizer){
        let imgButtonOff = UIImage(named: "ico_keep_off")
        let imgButtonOn = UIImage(named: "ico_keep_on")
        if self.checkAgree.image == imgButtonOff {
            checkAgree.image = imgButtonOn
        }else{
            checkAgree.image = imgButtonOff
        }
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "id_event" : self.idEventSelected as AnyObject,
            "id_ticket" : self.typeSelected as AnyObject,
            "jumlah" : self.qtySelected as AnyObject,
            "price" : self.priceSelected as AnyObject,
            "status" : "1" as AnyObject,
            "full_name" : self.txtName.text! as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    func getData(_ message: String){
        let urlString = Config().urlEvent + "set_attendance"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(urlString)
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                if jsonData["code"].string == "1" {
                    let data = jsonData["msg"]
                    self.msgError = data["message"].string!
                    self.success = 1
                    
                    let model = try! Realm().objects(listEvent.self).filter("id_event = %@", Int(self.idEventSelected)!).first!
                    try! Realm().write({
                        model.id_payment = data["code_uniq"].string!
                    })
                    
                }else{
                    self.msgError = "Failed connect to server, plaese try again later"
                    self.success = 0
                }
            }else{
                print("Something Went Wrong..")
                self.msgError = "Failed connect to server, plaese try again later"
                self.success = 0
            }
            
            }.responseData { Response in
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
        }
    }
    
    var msgError = ""
    var success = 0
    func alertStatus(){
        if msgError != "" {
            let alert = UIAlertController(title: "Info", message: msgError, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                if self.success == 1 {
                    if self.from == 0 {
                        self.performSegue(withIdentifier: "backToListEvent", sender: self)
                    }else{
                        self.performSegue(withIdentifier: "backToDetailEvent", sender: self)
                    }
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // METHODE NAV
    func colorNav(){
        navigationController!.navigationBar.barTintColor = self.blue
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationController!.navigationBar.tintColor = UIColor.white
    }
    
    //Keyboard Hendler
    var activeField: UITextField?
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            let possKeyboard = self.view.frame.height - keyboardSize!.height - activeField!.frame.height
            if activeField!.frame.origin.y > possKeyboard {
                //                if (!CGRectContainsPoint(aRect, activeField!.frame.origin)){
                if view.frame.origin.y == 0{
                    self.view.frame.origin.y -= keyboardSize!.height
                }
                //                }
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
            else {
                
            }
        }
    }
}
