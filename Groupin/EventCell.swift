//
//  EventCell.swift
//  Groupin
//
//  Created by Macbook pro on 8/30/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class EventCell: UICollectionViewCell {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblHarga: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblTempat: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnMap: UIButton!
    @IBOutlet weak var btnAttend: UIButton!
    @IBOutlet weak var imgQr: UIImageView!
    @IBOutlet weak var heightDesc: NSLayoutConstraint!
    @IBOutlet weak var widthDesc: NSLayoutConstraint!
    @IBOutlet weak var heightAddress: NSLayoutConstraint!
}