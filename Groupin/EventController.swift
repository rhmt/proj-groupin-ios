//
//  EventController.swift
//  Groupin
//
//  Created by Macbook pro on 8/30/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire
import EventKit

class EventController: UIViewController, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var tbEvent: UITableView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var viewNoData: UIView!
    
    @IBOutlet weak var conHeightBtnAdd: NSLayoutConstraint!
    
    var idUser = 0
    var idGroup = 0
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Get ID user
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.idGroup = idSession!.id_group
        self.token = idSession!.token
        
        //Rounded Button
        btnAdd.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
        btnAdd.layer.cornerRadius = 5
        btnAdd.layer.borderWidth = 1
        btnAdd.layer.borderColor = UIColor.lightGray.cgColor
        
        self.tbEvent.isHidden = true
        self.viewNoData.isHidden = false
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print("did appear")
        self.loadLocal()
        self.makeSerialize()
    }
    
    var dataData = [listEvent]()
    func loadLocal(){
        print("refresh more")
        self.dataData.removeAll()
        let data = try! Realm().objects(listEvent.self).filter("id_group =%@", self.idGroup).sorted(byKeyPath: "date_from", ascending: false)
        if data.count > 0 {
            self.tbEvent.isHidden = false
            self.viewNoData.isHidden = true
            for item in data {
                self.dataData.append(item)
            }
        }else{
            self.tbEvent.isHidden = true
            self.viewNoData.isHidden = false
        }
        
        if self.paramsJson == 1 {
            self.tbEvent.reloadData()
        }
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "id_group" : self.idGroup as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    func getData(_ message: String){
        let urlString = Config().urlEvent + "list_event"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)["msg"]
                self.jsonData = jsonData
                print(self.jsonData)
                if jsonData.count > 0 {
                    self.paramsJson = 1
                    
                    var i = 0
                    for _ in jsonData {
                        let data = jsonData[i]
                        let id = data["ticket"][0]["fk_event"].intValue
                        let modelUpdate = try! Realm().objects(listEvent.self).filter("id_event = %@", id)
                        if modelUpdate.count == 0 {
                            let model = listEvent()
                            model.id_event = id
                            model.id_group = self.idGroup
                            if let img = data["image"].string {
                                model.image = img
                            }
                            model.name = data["name"].string!
                            model.desc = data["description"].string!
                            model.address = data["address"].string!
                            model.latitude = data["latitude"].string!
                            model.longitude = data["longitude"].string!
                            model.date_from = data["date_from"].string!
                            model.date_to = data["date_to"].string!
                            model.date_submission = data["date_submission"].string!
                            model.id_country = data["fk_country"].intValue
                            model.country = data["country"].string!
                            model.id_city = data["fk_city"].intValue
                            model.city = data["city"].string!
                            model.id_create = data["user"]["id"].intValue
                            model.name_create = data["user"]["name"].string!
                            
                            if let idAttend = data["id_attendance"].string {
                                model.id_attendance = idAttend
                            }
                            if let attend = data["attendance_status"].string {
                                model.attendance_status = attend
                            }
                            
                            //Price
                            if let pric = data["price"].string {
                                var ticket = ""
                                if pric != "0" {
                                    model.priceType = data["ticket"][0]["price_type"].string!.uppercased()
                                    ticket = data["price"].string!
                                }else{
                                    model.priceType = ""
                                    ticket = "FREE"
                                }
                                model.price = ticket
                            }
                            //-----
                            if let amount = data["ticket_amount"].string {
                                model.amount = amount
                            }
                            
                            if let ticketName = data["ticket_name"].string {
                                model.ticket_name = ticketName
                            }
                            
                            //Range Price
                            var arrayPrice = [Int]()
                            var arrayPriceType = [String]()
                            var inx = 0
                            for _ in data["ticket"] {
                                arrayPrice.append(data["ticket"][inx]["price"].intValue)
                                arrayPriceType.append(data["ticket"][inx]["price_type"].string!)
                                inx += 1
                            }
                            let sortedTicket = arrayPrice.sorted(){$0 < $1}
                            let getIndexFirst = arrayPrice.index(of: sortedTicket[0])
                            let getTypeFirst = arrayPriceType[getIndexFirst!]
                            
                            let getIndexLast = arrayPrice.index(of: sortedTicket.last!)
                            let getTypeLast = arrayPriceType[getIndexLast!]
                            
                            var firstArrayTicket = getTypeFirst.uppercased() + " " + String(sortedTicket[0])
                            if sortedTicket[0] == 0 {
                                firstArrayTicket = "Free"
                            }
                            
                            if arrayPrice.count == 1 {
                                let rangeTicket = firstArrayTicket
                                model.range_price = rangeTicket
                            }else{
                                let rangeTicket = firstArrayTicket + " - " + getTypeLast.uppercased() + " " + String(sortedTicket.last!)
                                model.range_price = rangeTicket
                            }
                            //-----------
                            
                            model.payment_status = data["payment_status"].string!
                            
                            //Quota
                            model.quota = data["total_quota"].string!
                            model.use_quota = data["use_quota"].string!
                            //-----
                            
                            DBHelper.insert(model)
                        }else{
                            let update = modelUpdate.first!
                            try! Realm().write({ 
                                if let img = data["image"].string {
                                    update.image = img
                                }
                                update.name = data["name"].string!
                                update.desc = data["description"].string!
                                update.address = data["address"].string!
                                update.latitude = data["latitude"].string!
                                update.longitude = data["longitude"].string!
                                update.date_from = data["date_from"].string!
                                update.date_to = data["date_to"].string!
                                update.date_submission = data["date_submission"].string!
                                update.id_country = data["fk_country"].intValue
                                update.country = data["country"].string!
                                update.id_city = data["fk_city"].intValue
                                update.city = data["city"].string!
                                update.id_create = data["user"]["id"].intValue
                                update.name_create = data["user"]["name"].string!
                                
                                if let idAttend = data["id_attendance"].string {
                                    update.id_attendance = idAttend
                                }
                                if let attend = data["attendance_status"].string {
                                    update.attendance_status = attend
                                }
                                
                                //Price
                                if let pric = data["price"].string {
                                    var ticket = ""
                                    if pric != "0" {
                                        update.priceType = data["ticket"][0]["price_type"].string!.uppercased()
                                        ticket = data["price"].string!
                                    }else{
                                        ticket = "FREE"
                                    }
                                    update.price = ticket
                                }
                                //-----
                                
                                if let amount = data["ticket_amount"].string {
                                    update.amount = amount
                                }
                                
                                if let ticketName = data["ticket_name"].string {
                                    update.ticket_name = ticketName
                                }
                                
                                //Range Price
                                var arrayPrice = [Int]()
                                var arrayPriceType = [String]()
                                var inx = 0
                                for _ in data["ticket"] {
                                    arrayPrice.append(data["ticket"][inx]["price"].intValue)
                                    arrayPriceType.append(data["ticket"][inx]["price_type"].string!)
                                    inx += 1
                                }
                                let sortedTicket = arrayPrice.sorted(){$0 < $1}
                                let getIndexFirst = arrayPrice.index(of: sortedTicket[0])
                                let getTypeFirst = arrayPriceType[getIndexFirst!]
                                
                                let getIndexLast = arrayPrice.index(of: sortedTicket.last!)
                                let getTypeLast = arrayPriceType[getIndexLast!]
                                
                                var firstArrayTicket = getTypeFirst.uppercased() + " " + String(sortedTicket[0])
                                if sortedTicket[0] == 0 {
                                    firstArrayTicket = "Free"
                                }
                                
                                if arrayPrice.count == 1 {
                                    let rangeTicket = firstArrayTicket
                                    update.range_price = rangeTicket
                                }else{
                                    let rangeTicket = firstArrayTicket + " - " + getTypeLast.uppercased() + " " + String(sortedTicket.last!)
                                    update.range_price = rangeTicket
                                }
                                //-----------
                                
                                update.payment_status = data["payment_status"].string!
                                
                                //Quota
                                update.quota = data["total_quota"].string!
                                update.use_quota = data["use_quota"].string!
                                //-----
                            })
                        }
                        
                        i += 1
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.loadLocal()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataData.count
    }
    
    var arrayLat = [String]()
    var arrayLong = [String]()
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listEventCell", for: indexPath) as! ListEventCell
        
        cell.selectionStyle = .none
        
        let data = dataData[indexPath.row]
        
        let dateGet = DateFormatter()
        dateGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        //Check Main Date
        let dateCheck = DateFormatter()
        dateCheck.dateFormat = "yyyy-MM-dd"
        var dateNow = ""
        var dateParams = ""
        if let date = dateGet.date(from: data.date_from) {
             dateNow = dateCheck.string(from: date)
        }
        if indexPath.row > 0 {
            let dataPre = dataData[indexPath.row - 1]
            if let date = dateGet.date(from: dataPre.date_from) {
                dateParams = dateCheck.string(from: date)
            }
            if dateNow == dateParams {
                cell.viewDate.isHidden = true
            }
        }
        //---------------
        
        //Main Date
        let dateDay = DateFormatter()
        dateDay.dateFormat = "dd"
        if let date = dateGet.date(from: data.date_from) {
            let dateShow = dateDay.string(from: date)
            cell.lblDay.text = dateShow
        }
        let dateMount = DateFormatter()
        dateMount.dateFormat = "MMM"
        if let date = dateGet.date(from: data.date_from) {
            let dateShow = dateMount.string(from: date)
            cell.lblMount.text = dateShow
        }
        //---------
        
        if data.image != "" {
            cell.imgAva.loadImageUsingCacheWithUrlString(data.image)
        }else{
            cell.imgAva.image = UIImage(named: "dafault-ava-group")
        }
        cell.imgAva.clipsToBounds = true
        cell.lblTitle.text = data.name
        
        let price = data.range_price
        cell.lblPrice.text = price
        let widthPrice = Config().estimateFrameForText(price, width: Int(cell.imgAva.frame.width)).width
        cell.conWidthPrice.constant = widthPrice + 24
        //-----------
        cell.lblCreated.text = data.name_create
        
        var dateShow = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM d, YYYY"
        let dataDateFrom = data.date_from
        if let date = dateFormatterGet.date(from: dataDateFrom) {
            let dateFrom = dateFormatterPrint.string(from: date)
            dateShow += dateFrom
        }else{
            dateShow += dataDateFrom
        }
        
        dateShow += " - "
        if let dateToP = dateFormatterGet.date(from: data.date_to) {
            let dateTo = dateFormatterPrint.string(from: dateToP)
            dateShow += dateTo
        }else{
            dateShow += data.date_to
        }
        cell.lblDate.text = dateShow
        
        cell.lblQuota.text = data.use_quota + " / " + data.quota
        let ticketsLeft = Int(data.quota)! - Int(data.use_quota)!
        cell.lblTicketLeft.text = String(ticketsLeft) + " tickets left"
        
        if data.attendance_status == "1" {
            cell.viewJoined.isHidden = false
            cell.viewJoined.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
            cell.lblJoined.text = "Joined"
            
            if data.payment_status == "" {
                if data.price == "FREE" {
                    cell.imgScan.image = UIImage(named: "ico_qr_show")
                }else{
                    cell.imgScan.image = UIImage(named: "ico_payment")
                }
            }else{
                cell.imgScan.image = UIImage(named: "ico_qr_show")
            }
        }else if data.attendance_status == "2"{
            cell.viewJoined.isHidden = false
            cell.viewJoined.backgroundColor = UIColor(red: 232, green: 76, blue: 61)
            cell.lblJoined.text = "Will Not Attend"
            cell.imgScan.image = nil
        }else{
            cell.viewJoined.isHidden = true
            cell.imgScan.image = UIImage(named: "ico_set_attendance")
        }
        
        if self.idUser == data.id_create {
            cell.imgScan.image = UIImage(named: "ico_qr_scan")
            cell.viewJoined.isHidden = true
        }
        
        self.arrayLat.append(data.latitude)
        self.arrayLong.append(data.longitude)
        
        //Design
        cell.viewMain.layer.cornerRadius = 5
        cell.viewMain.layer.shadowColor = UIColor.lightGray.cgColor
        cell.viewMain.layer.shadowOffset = CGSize.zero
        cell.viewMain.layer.shadowOpacity = 0.5
        cell.viewMain.layer.shadowRadius = 3
        //------
        
        //Fungsi Menu
        let menuGesture = UITapGestureRecognizer(target: self, action: #selector(EventController.btnAttendTapped(_:)))
        menuGesture.numberOfTapsRequired = 1
        cell.viewScan.isUserInteractionEnabled = true
        cell.viewScan.addGestureRecognizer(menuGesture)
        
        let calendarGesture = UITapGestureRecognizer(target: self, action: #selector(EventController.btnSaveCalenderTapped(_:)))
        calendarGesture.numberOfTapsRequired = 1
        cell.viewCalendar.isUserInteractionEnabled = true
        cell.viewCalendar.addGestureRecognizer(calendarGesture)
        
        let placeGesture = UITapGestureRecognizer(target: self, action: #selector(EventController.btnViewMapTapped(_:)))
        placeGesture.numberOfTapsRequired = 1
        cell.viewLocation.isUserInteractionEnabled = true
        cell.viewLocation.addGestureRecognizer(placeGesture)
        //-----------
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        //Tapped
        let data = dataData[indexPath.row]
        self.idEventSelected = String(data.id_event)
        self.performSegue(withIdentifier: "showDetailEvent", sender: self)
    }
    
    @IBAction func btnAddEventTapped(_ sender: AnyObject) {
        self.untuk = "add"
        self.performSegue(withIdentifier: "showAddEvent", sender: self)
    }
    
    func btnSaveCalenderTapped(_ sender: UITapGestureRecognizer) {
        let cell = sender.view?.superview?.superview?.superview?.superview as! ListEventCell
        let indexPath = self.tbEvent.indexPath(for: cell)
        
        let data = dataData[indexPath!.row]
        let dateFrom = data.date_from
        let dateTo = data.date_to
        let titleEvent = data.name
        let noteEvent = data.desc
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let fromDate = dateFormatter.date(from: dateFrom)
        let toDate = dateFormatter.date(from: dateTo)
        
        let eventStore : EKEventStore = EKEventStore()
        eventStore.requestAccess(to: EKEntityType.event, completion: {
            (granted, error) in
            
            if (granted) && (error == nil) {
                print("granted \(granted)")
                print("error \(error)")
                
                let event:EKEvent = EKEvent(eventStore: eventStore)
                
                event.title = titleEvent
                event.startDate = fromDate!
                event.endDate = toDate!
                event.notes = noteEvent
                event.calendar = eventStore.defaultCalendarForNewEvents
                
                do {
                    try eventStore.save(event, span: .thisEvent)
                    print("Saved Event")
                    let alertLoading = UIAlertController(title: "Saved to your calendar", message: "", preferredStyle: UIAlertControllerStyle.alert)
                    self.present(alertLoading, animated: true, completion: nil)
                    UIView.animate(withDuration: 5.0, animations: {
                        alertLoading.dismiss(animated: true, completion: nil)
                    })
                } catch {
                    print("Bad things happened")
                    
                }
            } 
        })
    }
    
    var latSelected = ""
    var longSelected = ""
    func btnViewMapTapped(_ sender: UITapGestureRecognizer) {
        let cell = sender.view?.superview?.superview?.superview?.superview as! ListEventCell
        let indexPath = self.tbEvent.indexPath(for: cell)
        latSelected = arrayLat[indexPath!.row]
        longSelected = arrayLong[indexPath!.row]
        
        self.performSegue(withIdentifier: "showViewMaps", sender: self)
    }
    
    var idEventSelected = ""
    var nameSelected = ""
    var createdSelected = ""
    var fromSelected = ""
    var toSelected = ""
    var addressSelected = ""
    //var ticketSelected: JSON!
    var typeSelected = ""
    var statusQR = ""
    func btnAttendTapped(_ sender: UITapGestureRecognizer) {
        let cell = sender.view?.superview?.superview?.superview?.superview as! ListEventCell
        let indexPath = self.tbEvent.indexPath(for: cell)
        
        let dataLocal = self.dataData[indexPath!.row]
        print(dataLocal)
        print("---------")
        self.idEventSelected = String(dataLocal.id_event)
        self.nameSelected = dataLocal.name
        self.createdSelected = dataLocal.name_create
        self.fromSelected = dataLocal.date_from
        self.toSelected = dataLocal.date_to
        self.addressSelected = dataLocal.address
        
        if dataLocal.id_create == self.idUser {
            self.statusQR = "scan"
            self.performSegue(withIdentifier: "showQRCode", sender: self)
        }else{
            if dataLocal.attendance_status == "" {
                let data = self.jsonData[indexPath!.row]
                print(data)
                //self.ticketSelected = data["ticket"]
                self.typeSelected = data["ticket"][0]["idtb_ticket_event"].string!
                
                //Check Event Sudah Lewat
                let dateGet = DateFormatter()
                dateGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                if let date = dateGet.date(from: dataLocal.date_from) {
                    let dateCheckData = date.timeIntervalSince1970
                    let dateCheckNow = Date().timeIntervalSince1970
                    if dateCheckData < dateCheckNow {
                        let alertLoading = UIAlertController(title: "Event Expired", message: "", preferredStyle: UIAlertControllerStyle.alert)
                        self.present(alertLoading, animated: true, completion: nil)
                        UIView.animate(withDuration: 5.0, animations: {
                            alertLoading.dismiss(animated: true, completion: nil)
                        })
                    }else{
                        self.setAttendTapped()
                    }
                }else{
                    self.setAttendTapped()
                }
                //-----------------------
            }
            if dataLocal.attendance_status == "1" {
                if dataLocal.payment_status == "" {
                    if dataLocal.price == "FREE" {
                        self.statusQR = "show"
                        self.idEventSelected = String(dataLocal.id_event)
                        self.performSegue(withIdentifier: "showQRCode", sender: self)
                    }else{
                        self.performSegue(withIdentifier: "showPayment", sender: self)
                    }
                }else{
                    self.statusQR = "show"
                    self.idEventSelected = String(dataLocal.id_event)
                    self.performSegue(withIdentifier: "showQRCode", sender: self)
                }
            }
            if dataLocal.attendance_status == "2"{
                //Do Nothing
            }
        }
    }
    
    func setAttendTapped(){
        let alert = UIAlertController(title: "", message: "Are you able to attend this event?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.performSegue(withIdentifier: "showAttend", sender: self)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { action in
            self.noAttendEvent()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func noAttendEvent(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "id_event" : self.idEventSelected as AnyObject,
            "id_ticket" : self.typeSelected as AnyObject,
            "jumlah" : "0" as AnyObject,
            "price" : "0" as AnyObject,
            "status" : "2" as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Attend(jsonString!)
    }
    
    func makeBase64Attend(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getDataAttend(base64Encoded)
        }
    }
    
    func getDataAttend(_ message: String){
        let urlString = Config().urlEvent + "set_attendance"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(urlString)
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
//                self.jsonData = JSON(value)
                print(JSON(value))
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.makeSerialize()
        }
    }
    
    var untuk = "add"
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailEvent" {
            let conn = segue.destination as! DetailEventController
            conn.idEvent = self.idEventSelected
        }
        if segue.identifier == "showViewMaps" {
            let conn = segue.destination as! EventViewMapsController
            conn.lat = Double(self.latSelected)!
            conn.long = Double(self.longSelected)!
            conn.navigationItem.title = "View Map"
        }
        if segue.identifier == "showAttend" {
            let conn = segue.destination as! EventAttendController
            conn.idEventSelected = self.idEventSelected
            conn.nameSelected = self.nameSelected
            conn.createdSelected = self.createdSelected
            conn.fromSelected = self.fromSelected
            conn.toSelected = self.toSelected
            conn.addressSelected = self.addressSelected
            conn.from = 0
            //conn.ticketSelected = self.ticketSelected
        }
        if segue.identifier == "showAddEvent" {
            let conn = segue.destination as! AddEventController
            conn.navigationItem.title = "Add Event"
        }
        if segue.identifier == "showTicketList" {
            let conn = segue.destination as! ListTicketEventController
            conn.idEvent = self.idEventSelected
            conn.navigationItem.title = self.nameSelected
        }
        if segue.identifier == "showQRCode" {
            let conn = segue.destination as! QrCodeController
            conn.statusQR = self.statusQR
            conn.idEvent = self.idEventSelected
        }
        if segue.identifier == "showPayment" {
            let conn = segue.destination as! PaymentSummaryController
            conn.navigationItem.title = "Payment"
            conn.idEvent = Int(self.idEventSelected)!
        }
    }
    
    @IBAction func backToListEvent(_ sender: UIStoryboardSegue){
        print("wb")
        self.makeSerialize()
    }
    
    fileprivate func estimateFrameForText(_ text: String, width: Int) -> CGRect {
        let size = CGSize(width: width, height: 10000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)], context: nil)
    }
    
}
