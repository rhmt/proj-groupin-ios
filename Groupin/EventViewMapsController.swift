//
//  EventViewMapsController.swift
//  Groupin
//
//  Created by Macbook pro on 8/30/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class EventViewMapsController: UIViewController, GMSMapViewDelegate {
    
    var place: GMSPlace!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lblLoc: UILabel!
    @IBOutlet weak var lblLocDetail: UILabel!
    
    var lat :CLLocationDegrees = 0
    var long :CLLocationDegrees = 0
    var locDetail = ""
    var chat = 0
    var chatLoc = ""
    var chatLocDetail = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewMaps()
        
        mapView.settings.myLocationButton = true
        mapView.settings.scrollGestures = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.viewMaps()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func viewMaps(){
        mapView.moveCamera(GMSCameraUpdate.setCamera(GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: self.lat, longitude: self.long), zoom: 17.0)))
        //        mapView.animateToCameraPosition(GMSCameraPosition.cameraWithTarget(CLLocationCoordinate2D(latitude: self.lat, longitude: self.long), zoom: 19.0))
        
        //Set Marker
        let position = CLLocationCoordinate2DMake(self.lat, self.long)
        let point = GMSMarker(position: position)
        point.map = mapView
        point.isFlat = true
        point.tracksInfoWindowChanges = true
        
        let img = Config().ResizeImage(UIImage(named: "ico_place_maps")!, targetSize: CGSize(width: 28.6, height: 40.8))
        point.icon = img.withRenderingMode(.alwaysTemplate)
        
        // Get location name
        let location = CLLocation(latitude: self.lat, longitude: self.long) //changed!!!
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            
            if error != nil {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks![0]
                
                if let street = pm.addressDictionary!["Street"] as? NSString {
                    self.locDetail += street as String
                }
                if let city = pm.addressDictionary!["City"] as? NSString {
                    self.locDetail += " " + (city as String) as String
                    self.lblLoc.text = city as String
                }
                if let state = pm.addressDictionary!["State"] as? NSString {
                    self.locDetail += " " + (state as String) as String
                }
                if let zip = pm.addressDictionary!["ZIP"] as? NSString {
                    self.locDetail += " " + (zip as String) as String
                }
                if let Country = pm.addressDictionary!["Country"] as? NSString {
                    self.locDetail += " " + (Country as String) as String
                }
                
                if self.chat != 1 {
                    if let locName = pm.name {
                        self.lblLoc.text = locName
                    }
                    
                    self.lblLocDetail.text = self.locDetail
                }else{
                    self.lblLoc.text = self.chatLoc
                    self.lblLocDetail.text = self.chatLocDetail
                }
            }
            else {
                print("Problem with the data received from geocoder")
            }
        })
    }
}
