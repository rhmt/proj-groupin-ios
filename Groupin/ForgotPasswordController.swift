//
//  ForgotPasswordController.swift
//  Groupin
//
//  Created by Macbook pro on 12/9/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ForgotPasswordController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var txtInput: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnBackToLogin: UIButton!
    @IBOutlet weak var imgCheckStaff: UIImageView!
    @IBOutlet weak var btnCheckStaf: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        btnSend.backgroundColor = UIColor(red: 83, green: 186, blue: 32)
        btnSend.layer.cornerRadius = 5
        btnSend.layer.borderWidth = 1
        btnSend.layer.borderColor = UIColor.lightGray.cgColor
//        btnBackToLogin.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
        btnBackToLogin.layer.cornerRadius = 5
        btnBackToLogin.layer.borderWidth = 1
        btnBackToLogin.layer.borderColor = UIColor.lightGray.cgColor
        
        let textExtra = " \n* for phone number, use country code before phone number (ex: +62812345678)"
        let text = self.lblDesc.text! + textExtra
        let startText = self.lblDesc.text?.characters.count
        let lengText = textExtra.characters.count
        let myMutableString = NSMutableAttributedString(string: text)
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location:startText!, length:lengText))
        myMutableString.addAttribute(NSFontAttributeName, value: UIFont.italicSystemFont(ofSize: 12), range: NSRange(location:startText!, length:lengText))
        self.lblDesc.attributedText = myMutableString
        
        let tapRemember = UITapGestureRecognizer(target: self, action: #selector(ForgotPasswordController.imgRememberTapped(_:)))
        tapRemember.numberOfTapsRequired = 1
        self.imgCheckStaff.isUserInteractionEnabled = true
        self.imgCheckStaff.addGestureRecognizer(tapRemember)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnCheckStaffTapped(_ sender: AnyObject) {
        self.rememberAction()
    }
    
    func imgRememberTapped(_ sender: UITapGestureRecognizer){
        self.rememberAction()
    }
    
    var staffStatus = "2"
    func rememberAction(){
        let imgButtonOff = UIImage(named: "ico_keep_off")
        let imgButtonOn = UIImage(named: "ico_keep_on")
        if self.staffStatus == "2" {
            self.imgCheckStaff.image = imgButtonOn
            self.staffStatus = "1"
        }else{
            self.imgCheckStaff.image = imgButtonOff
            self.staffStatus = "2"
        }
    }
    
    var alertLoading: UIAlertController!
    @IBAction func btnSendTapped(_ sender: AnyObject) {
        self.alertLoading = UIAlertController(title: "Loading", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        self.present(self.alertLoading, animated: true, completion: nil)
        
        let urlString = Config().urlMain + "forgot_password"
        let params = ["account" : self.txtInput.text!, "is_staff" : self.staffStatus]
        Alamofire.request(urlString, method: .post, parameters: params).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                
                self.msg = jsonData["msg"].string!
            }else{
                print("Something Went Wrong..")
                self.msg = "Can't Connect to Server"
            }
            }.responseData { Response in
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
        }
    }
    
    @IBAction func btnBackToLoginTapped(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showBackToLogin", sender: self)
    }
    
    var msg = ""
    func alertStatus(){
        if msg != "" {
            let alert = UIAlertController(title: "Info", message: msg, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    var activeField: UITextField?
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            let possKeyboard = self.view.frame.height - keyboardSize!.height - activeField!.frame.height
            if activeField?.frame.origin.y > possKeyboard {
                //                if (!CGRectContainsPoint(aRect, activeField!.frame.origin)){
                if view.frame.origin.y == 0{
                    self.view.frame.origin.y -= keyboardSize!.height
                }
                //                }
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
            else {
                
            }
        }
    }
}
