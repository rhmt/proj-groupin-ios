//
//  ForumCell.swift
//  Groupin
//
//  Created by Macbook pro on 8/30/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class ForumCell: UITableViewCell {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblSeen: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    
}