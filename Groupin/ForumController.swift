//
//  ForumController.swift
//  Groupin
//
//  Created by Macbook pro on 8/30/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class ForumController: UIViewController {
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var tbListThread: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var viewNoData: UIView!
    
    var idUser = 0
    var idGroup = 0
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tbListThread.isHidden = true
        self.viewNoData.isHidden = false
        
        //Session
        let dataSession = try! Realm().objects(session.self).first
        self.idUser = dataSession!.id
        self.idGroup = dataSession!.id_group
        self.token = dataSession!.token
        
        //Rounded Button
        btnAdd.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
        btnAdd.layer.cornerRadius = 5
        btnAdd.layer.borderWidth = 1
        btnAdd.layer.borderColor = UIColor.lightGray.cgColor
        
        self.loadLocal()
        self.makeSerialize()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.loadLocal()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var dataData = [listForum]()
    func loadLocal(){
        self.dataData = DBHelper.getAllListForum()
        self.dataData.removeAll()
        let data = try! Realm().objects(listForum.self).filter("id_group =%@", self.idGroup).sorted(byKeyPath: "date", ascending: false)
        if data.count > 0 {
            self.tbListThread.isHidden = false
            self.viewNoData.isHidden = true
            for item in data {
                self.dataData.append(item)
            }
        }else{
            self.tbListThread.isHidden = true
            self.viewNoData.isHidden = false
        }
        
        if self.paramsJson == 1 {
            self.tbListThread.reloadData()
        }
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_group" : self.idGroup as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    var paramsJson = 0
    func getData(_ message: String){
        let urlString = Config().urlThread + "list_thread"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)["msg"]
                print(jsonData)
                if jsonData.count > 0 {
                    self.paramsJson = 1
                    self.tbListThread.isHidden = false
                    self.viewNoData.isHidden = true
                    
                    var i = 0
                    for _ in jsonData {
                        let data = jsonData[i]
                        let id = data["id_thread"].intValue
                        let modelUpdate = try! Realm().objects(listForum.self).filter("id_thread = %@", id)
                        if modelUpdate.count == 0 {
                            let model = listForum()
                            model.id_thread = id
                            model.id_group = self.idGroup
                            if let thumb = data["thumb"].string {
                                model.thumb = thumb
                            }
                            model.subject = data["subject"].string!
                            model.content = data["content"].string!
                            model.date = data["date"].string!
                            model.total_like = data["total_like"].string!
                            model.total_view = data["total_view"].string!
                            model.total_comment = data["total_comment"].string!
                            DBHelper.insert(model)
                        }else{
                            let update = modelUpdate.first!
                            try! Realm().write({ 
                                if let thumb = data["thumb"].string {
                                    update.thumb = thumb
                                }
                                update.subject = data["subject"].string!
                                update.content = data["content"].string!
                                update.date = data["date"].string!
                                update.total_like = data["total_like"].string!
                                update.total_view = data["total_view"].string!
                                update.total_comment = data["total_comment"].string!
                            })
                        }
                        i += 1
                    }
                }else{
                    self.tbListThread.isHidden = true
                    self.viewNoData.isHidden = false
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.loadLocal()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "forumCell", for: indexPath) as! ForumCell
        
        let data = self.dataData[indexPath.row]
        if data.thumb != "" {
            cell.imgAva.loadImageUsingCacheWithUrlString(data.thumb)
        }else{
            cell.imgAva.image = UIImage(named: "dafault-ava-group")
        }
        cell.imgAva.layer.cornerRadius = 5
        cell.imgAva.clipsToBounds = true
        cell.lblTitle.text = data.subject
        let contentStr = data.content
        _ = try! NSAttributedString(
            data: contentStr.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
            options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
            documentAttributes: nil)
//        cell.lblDesc.attributedText = attrStr
        cell.lblDesc.text = data.content
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd, YYYY hh:mm a"
        let dataDateFrom = data.date
        if let date = dateFormatterGet.date(from: dataDateFrom) {
            let dateView = dateFormatterPrint.string(from: date)
            cell.lblTime.text = dateView
        }else{
            cell.lblTime.text = data.date
        }
        
        var seen = "0"
        if data.total_view != "" {
            seen = data.total_view
        }
        cell.lblSeen.text = seen
        var comment = "0"
        if data.total_comment != "" {
            comment = data.total_comment
        }
        cell.lblComment.text = comment
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(ListChatGroupController.longPressCell(_:)))
        longPressRecognizer.minimumPressDuration = 1.00
        idThreadSelected = String(data.id_thread)
        cell.addGestureRecognizer(longPressRecognizer)
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailForum" {
            let conn = segue.destination as! DetailForumController
            print(conn.idThread)
            print(self.idThreadSelected)
            conn.idThread = self.idThreadSelected
        }
        if segue.identifier == "showAddForum" {
            if untuk == "view" {
                let conn = segue.destination as! AddForumController
                conn.idThread = self.idThreadSelected
                conn.untuk = self.untuk
            }
        }
    }
    
    var idThreadSelected = ""
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        idThreadSelected = String(self.dataData[indexPath.row].id_thread)
        self.performSegue(withIdentifier: "showDetailForum", sender: self)
    }
    
    var untuk = ""
    func longPressCell(_ sender: UILongPressGestureRecognizer){
        let alert:UIAlertController=UIAlertController(title: "", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.modalPresentationStyle = .popover
        
        let editAction = UIAlertAction(title: "Edit This Thread", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.untuk = "view"
            self.performSegue(withIdentifier: "showAddForum", sender: self)
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel){ UIAlertAction in }
        
        alert.addAction(editAction)
        alert.addAction(cancelAction)
        
        
        if alert.popoverPresentationController != nil {
//            presenter.sourceView = sender
//            presenter.sourceRect = sender.bounds;
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnAddTapped(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showAddForum", sender: self)
    }
    
    @IBAction func backToListForum(_ segue: UIStoryboardSegue){
        makeSerialize()
    }
    
    let blue = UIColor(red: 63, green: 157, blue: 247)
    func colorNav(){
        navigationController!.navigationBar.barTintColor = self.blue
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationController!.navigationBar.tintColor = UIColor.white
    }
    
    func backNav(){
        // Change Nav Back
        self.navigationItem.backBarButtonItem?.target = self
        self.navigationItem.backBarButtonItem?.action = #selector(ForumController.backNavTapped)
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ForumController.backNavTapped))
        
        let button = UIButton()
        button.setImage(UIImage(named: "ico_back"), for: UIControlState())
        button.addTarget(self, action: #selector(ForumController.backNavTapped), for: UIControlEvents.touchDown)
        button.frame=CGRect(x: 0, y: 0, width: 20, height: 20)
        let backButton:UIBarButtonItem = UIBarButtonItem(customView: button)
        
        navigationItem.leftBarButtonItems = [backButton, newBackButton]
    }
        
    func backNavTapped(_ sender:UIButton){
        print("back")
    }
    
    func titleNav(){
        let data = try! Realm().objects(tb_group.self).filter("id = %@", self.idGroup).first!
        let name = data.name
        self.navigationItem.title = name
    }
    
    func rightMenu(){
        let button = UIButton()
        button.setImage(UIImage(named: "ico_leaderboard"), for: UIControlState())
        button.addTarget(self, action: #selector(ForumController.leaderBoardTapped), for: UIControlEvents.touchDown)
        button.frame=CGRect(x: 0, y: 0, width: 24, height: 24)
        let rightMenuButtonItem:UIBarButtonItem = UIBarButtonItem(customView: button)
        
        self.navigationItem.setRightBarButtonItems([rightMenuButtonItem], animated: true)
    }
    
    func leaderBoardTapped(_ sender:UIButton){
        self.performSegue(withIdentifier: "showLeaderBoard", sender: self)
    }
    
}
