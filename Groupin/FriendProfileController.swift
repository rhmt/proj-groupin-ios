//
//  FriendProfileController.swift
//  Groupin
//
//  Created by Macbook pro on 8/15/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire
import Contacts
import ContactsUI

class FriendProfileController: UIViewController, CNContactViewControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var btnSavePhone: UIButton!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblJlmGroup: UILabel!
    @IBOutlet weak var lblNoData: UILabel!
    
    @IBOutlet weak var tbOwnGroup: UITableView!
    
    var phone = ""
    var idUser = 0
    var idFriend = ""
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = false
        self.tbOwnGroup.tableFooterView = UIView()
        
        self.tbOwnGroup.delegate = self
        self.tbOwnGroup.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //Get id_group form session
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.token = idSession!.token
        
        // Cos Avatar
        self.imgAva.contentMode = .scaleToFill
        self.imgAva.layer.cornerRadius = imgAva.frame.width / 2
        self.imgAva.clipsToBounds = true
        self.lblStatus.textColor = UIColor.gray
        
        makeSerialize()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idFriend as AnyObject,
            "id_login" : self.idUser as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    var jsonDataFriend: JSON!
    func getData(_ message: String){
        let urlString = Config().urlUser + "detail_popup_user"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                let data = jsonData["msg"]
                self.jsonDataFriend = data
                
                if let imgUrl = data["avatar"].string {
                    self.imgAva.loadImageUsingCacheWithUrlString(imgUrl)
                }else{
                    self.imgAva.image = UIImage(named: "default-avatar")
                }
                self.lblName.text = data["name"].string
                self.lblPhone.text = data["phone"].string
                self.lblStatus.text = "\"" + data["msg_status"].string! + "\""
                
                self.navigationItem.title = data["name"].string
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.getDataGroup(message)
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    func getDataGroup(_ message: String){
        let urlString = Config().urlGroup + "list_own_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)["msg"]
                print(self.jsonData)
                let jlmGroup = self.jsonData.count
                if jlmGroup > 0 {
                    self.tbOwnGroup.isHidden = false
                    self.lblNoData.isHidden = true
                }else{
                    self.tbOwnGroup.isHidden = true
                    self.lblNoData.isHidden = false
                }
                self.paramsJson = 1
                
                self.lblJlmGroup.text = String(jlmGroup)
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.tbOwnGroup.reloadData()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.paramsJson == 1 {
            return self.jsonData.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "friendProfileCell", for: indexPath) as! FriendProfileCell
        
        if self.paramsJson == 1  {
            var dataKey = [String]()
            for (key, _) in self.jsonData {
                dataKey.append(key)
            }
            let inx = dataKey[indexPath.row]
            let data = self.jsonData[inx]
            if let img = data["avatar"].string {
                if let imageUrl = URL(string: img) {
                    cell.imgAva.setImage(withUrl: imageUrl)
                }else{
                    cell.imgAva.image = UIImage(named: "dafault-ava-group")
                }
            }else{
                cell.imgAva.image = UIImage(named: "dafault-ava-group")
            }
            cell.imgAva.clipsToBounds = true
            cell.imgAva.layer.cornerRadius = 5
            cell.lblName.text = data["group"].string
        }
        
        return cell
    }
    
    let store = CNContactStore()
    @IBAction func btnSavePhoneTapped(_ sender: AnyObject) {
        let contactData = CNMutableContact()
        
        //Image
        let dataImg = self.jsonDataFriend["avatar"].string!
        let imgUrl = URL(string: dataImg)
        let urlData = try? Data(contentsOf: imgUrl!)
        let imgUi = UIImage(data: urlData!)
        let imgData = UIImageJPEGRepresentation(imgUi!, 1.0)
        contactData.imageData = imgData!
        
        //name
        contactData.givenName = self.jsonDataFriend["name"].string!
        
        //phone
        let homePhone = CNLabeledValue(label: CNLabelPhoneNumberMobile,value: CNPhoneNumber(stringValue: self.jsonDataFriend["phone"].string!))
        contactData.phoneNumbers = [homePhone]
        
        //Save
        let request = CNSaveRequest()
        request.add(contactData, toContainerWithIdentifier: nil)
        do{
            try store.execute(request)
            print("Successfully added the contact")
            self.msgError = "Save to contact success"
        } catch let err{
            print("Failed to save the contact. \(err)")
            self.msgError = "Failed to save the contact"
        }
        self.alertStatus()
    }
    
    var msgError = ""
    func alertStatus(){
        if msgError != "" {
            let alert = UIAlertController(title: "Info", message: msgError, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}
