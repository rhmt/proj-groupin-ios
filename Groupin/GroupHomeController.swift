//
//  GroupHomeController.swift
//  Groupin
//
//  Created by Macbook pro on 8/4/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift

class GroupHomeController: UITabBarController {
    
    var idGroup = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        //Get Image
//        let idSession = try! Realm().objects(session).first
//        self.idGroup = (idSession?.id_group)!
//        let models = try! Realm().objects(tb_group).filter("id_group == \(self.idGroup)").first
//        let imageData = models?.ava_group
//        let image = UIImage(data:imageData!,scale: 1.0)
        
        //Avatar Group
//        let ava = UIButton()
//        ava.setImage(image, forState: UIControlState.Normal)
//        ava.layer.cornerRadius = 15
//        ava.addTarget(self, action: #selector(GroupHomeController.avaTapped), forControlEvents: UIControlEvents.TouchDown)
//        ava.frame=CGRectMake(0, 0, 30, 30)
//        let barAva = UIBarButtonItem(customView: ava)
//        self.navigationItem.setRightBarButtonItems([barAva], animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    var fromCreate = 0
    override func viewWillDisappear(_ animated: Bool) {
        if self.fromCreate == 1 {
            self.navigationController?.popToRootViewController(animated: false)
        }
    }
    
    func avaTapped(_ sender:UIButton){
        print("Ava Tapped")
    }
}
