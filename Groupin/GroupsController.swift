//
//  GroupsController.swift
//  Groupin
//
//  Created by Macbook pro on 7/27/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire
import MapleBacon
import UserNotifications

class GroupsController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var colGroup: UICollectionView!
    
    var models = [tb_group]()
    var nameGroup = ""
    var idUser = 0
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //UIApplication.shared.applicationIconBadgeNumber = 0
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        
        colorNav()
        rightMenu()
        
        //Get ID user
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.token = idSession!.token
        
        colGroup.delegate = self
        
        self.checkFromNotif()
    }
    
    func checkFromNotif(){
        if let data = try! Realm().objects(session_notif.self).first {
            if data.fromNotif == 1 {
                self.performSegue(withIdentifier: "showGroupHome", sender: self)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        
        Config().CekLogin(self)

        //Get ID user
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.token = idSession!.token
    
        makeSerialize()
        
        let msgNotif = "Please allow the app to access your background app refresh through the Settings."
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings(){ (setttings) in
                switch setttings.authorizationStatus{
                case .authorized:
                    //Access allow
                    print("access allow")
                case .denied:
                    self.alertAccessPrivacy(msg: msgNotif)
                case .notDetermined:
                    self.alertAccessPrivacy(msg: msgNotif)
                }
            }
        } else {
            // Fallback on earlier versions
            let isRegisteredForRemoteNotifications = UIApplication.shared.isRegisteredForRemoteNotifications
            if isRegisteredForRemoteNotifications {
                // User is registered for notification
            } else {
                // Show alert user is not registered for notification
                self.alertAccessPrivacy(msg: msgNotif)
            }
        }
        
//        let msgBackground = "Please allow the app to access your notification through the Settings."
//        if UIApplication.shared.backgroundRefreshStatus == .available {
//            self.alertAccessPrivacy(msg: msgBackground)
//        }
//        else if UIApplication.shared.backgroundRefreshStatus == .denied {
//            self.alertAccessPrivacy(msg: msgBackground)
//        }
//        else if UIApplication.shared.backgroundRefreshStatus == .restricted {
//            self.alertAccessPrivacy(msg: msgBackground)
//        }
        
        //Indikator Notif
        Config().notifUpdates(self)
        Config().notifNotification(self)
        Config().notifChatPersonal(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = false
        
        loadLocal()
        makeSerialize()
    }
    
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(true)
//        self.tabBarController?.tabBar.isHidden = true
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func alertAccessPrivacy(msg: String){
        Config().alertForAllowAccess(self, msg: msg)
    }
    
    var dataData = [tb_group]()
    func loadLocal(){
        dataData = DBHelper.getAllGroup()
        if self.paramsJson == 1 {
            //self.colGroup.reloadData()
        }
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idUser as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    func getData(_ message: String){
        let urlString = Config().urlGroup + "list_own_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)
                print(self.jsonData)
                if self.jsonData["msg"].count > 0 {
                    self.paramsJson = 1
                    for (key, _) in self.jsonData["msg"] {
                        let updateModel = try! Realm().objects(tb_group.self).filter("id = %@", Int(key)!).first
                        if updateModel != nil {
                            //Update
                            try! Realm().write {
                                updateModel!.name = self.jsonData["msg"][key]["group"].string!
                                if let img = self.jsonData["msg"][key]["avatar"].string {
                                    updateModel!.avatar = img
                                }else{
                                    updateModel!.avatar = ""
                                }
                            }
                        }else{
                            //Insert
                            let insertModel = tb_group()
                            insertModel.id = Int(key)!
                            insertModel.name = self.jsonData["msg"][key]["group"].string!
                            if let img = self.jsonData["msg"][key]["avatar"].string {
                                insertModel.avatar = img
                            }else{
                                insertModel.avatar = ""
                            }
                            insertModel.city = self.jsonData["msg"][key]["city"].string!
                            insertModel.country = self.jsonData["msg"][key]["country"].string!
                            insertModel.jlm_member = self.jsonData["msg"][key]["member"].string!
                            insertModel.category = self.jsonData["msg"][key]["category"].string!
                            insertModel.user_level = self.jsonData["msg"][key]["user_level"].string!
                            insertModel.desc = self.jsonData[key]["desc"].string!
                            insertModel.date_create = self.jsonData[key]["date_create"].string!
                            insertModel.id_user_create = self.jsonData[key]["id_user_create"].string!
                            insertModel.user_create = self.jsonData[key]["name_user_create"].string!
                            insertModel.is_public = self.jsonData[key]["is_public"].string!
                            insertModel.is_promot = self.jsonData[key]["is_promote"].string!
                            insertModel.is_notif = 1
                            insertModel.is_sound = 1
                            DBHelper.insert(insertModel)
                        }
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.loadLocal()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCreateGroup"{
            self.tabBarController?.tabBar.isHidden = true
            let conn = segue.destination as! CreateGroupController
            conn.navigationItem.title = "Create a Group"
        }else if segue.identifier == "showGroupHome"{
            self.tabBarController?.tabBar.isHidden = true
            let conn = segue.destination as! GroupHomeController
            conn.navigationItem.title = self.nameGroup
        }
    }
    
    @IBAction func btnMoreTapped(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showMenu", sender: self)
    }
    
    @IBAction func btnSearchTapped(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showCreateGroup", sender: self)
    }
    
    //CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return self.dataData.count + 1
    }
    
    var id = [String]()
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeGroupCell", for: indexPath) as! HomeGroupCollectionViewCell
        
        cell.imgBadge.isHidden = true
        
        if indexPath.row < self.dataData.count  {
            let data = self.dataData[indexPath.row]
            
            if let imgUrl = URL(string: data.avatar) {
                cell.imgAvatr.setImage(withUrl: imgUrl)
            }else{
                cell.imgAvatr.image = UIImage(named: "dafault-ava-group")
            }
            cell.lblNama.text = data.name
            cell.imgPlus.isHidden = true
            
            //Indicator Unread chat
            let searchGroup = try! Realm().objects(tb_room.self).filter("id_group = %@", data.id)
            for item in searchGroup {
                let to = "G" + String(item.id_room)
                let indi = try! Realm().objects(tb_chat.self).filter("to = %@", to).filter("is_read = %@", 0)
                if indi.count > 0{
                    cell.imgBadge.isHidden = false
                    continue
                }
            }
        }else{
            cell.imgAvatr.image = UIImage(named: "ico_pure_blue")
            cell.imgPlus.contentMode = .scaleAspectFit
            cell.imgPlus.clipsToBounds = true
            cell.imgPlus.isHidden = false
            cell.lblNama.text = "Add a Group"
        }
        
        //Rounded Cell
        cell.viewMain.layer.cornerRadius = 5
        cell.viewMain.layer.shadowColor = UIColor.lightGray.cgColor
        cell.viewMain.layer.shadowOffset = CGSize.zero
        cell.viewMain.layer.shadowOpacity = 0.5
        cell.viewMain.layer.shadowRadius = 3
//        cell.viewMain.layer.masksToBounds = true
//        cell.layer.masksToBounds = true
//        cell.imgAvatr.layer.cornerRadius = 5
        cell.imgAvatr.clipsToBounds = true
        cell.imgAvatr.layer.masksToBounds = true
        
        widthCell = cell.viewMain.frame.width
        heightCell = cell.viewMain.frame.height
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if indexPath.row < self.dataData.count  {
            let data = self.dataData[indexPath.row]
            self.nameGroup = data.name
            let idGroup = data.id
            
            let model = try! Realm().objects(session.self).first
            try! Realm().write(){
                model?.id_group = idGroup
            }
            
            self.performSegue(withIdentifier: "showGroupHome", sender: self)
        }else{
            self.performSegue(withIdentifier: "showCreateGroup", sender: self)
        }
    }
    
    var widthCell: CGFloat! = 0
    var heightCell: CGFloat = 0
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        widthCell = (self.colGroup.frame.size.width - 8 * 2) / 2 //some width
        heightCell = widthCell * 1.25 //ratio
        
        return CGSize(width: widthCell, height: heightCell)
    }
    
    // METHODE NAV
    let blue = UIColor(red: 63, green: 157, blue: 247)
    func colorNav(){
        navigationController!.navigationBar.barTintColor = self.blue
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationController!.navigationBar.tintColor = UIColor.white
    }
    
    func rightMenu(){
        let searchButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(self.searchTapped))
        //Space
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 10.0
        //Delete
        let imageContact = UIImage(named: "ico_tab_contact")
        let contactButton = UIButton()
        contactButton.setImage(imageContact, for: UIControlState())
        contactButton.addTarget(self, action: #selector(self.contactTapped), for: UIControlEvents.touchDown)
        contactButton.frame=CGRect(x: 0, y: 0, width: 30, height: 30)
        let barAttachContact = UIBarButtonItem(customView: contactButton)
        
        self.navigationItem.setRightBarButtonItems([searchButtonItem, fixedSpace, barAttachContact], animated: true)
    }
    
    func searchTapped(_ sender:UIButton){
        self.performSegue(withIdentifier: "showSearchGroup", sender: self)
    }
    
    func contactTapped(_ sender:UIButton){
        self.performSegue(withIdentifier: "showContact", sender: self)
    }
    
}
