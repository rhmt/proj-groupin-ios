//
//  HelpStaffController.swift
//  Groupin
//
//  Created by Macbook pro on 1/11/17.
//  Copyright © 2017 Macbook pro. All rights reserved.
//

import UIKit

class HelpStaffController: UIViewController {
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        viewMain.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        let gestur = UITapGestureRecognizer(target: self, action: #selector(HelpStaffController.closePopup(_:)))
        gestur.numberOfTapsRequired = 1
        self.viewMain.isUserInteractionEnabled = true
        self.viewMain.addGestureRecognizer(gestur)
        
        self.showAnimate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    func closePopup(_ sender: UITapGestureRecognizer){
        self.removeAnimate()
    }
    
    func showAnimate(){
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0.0
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            }, completion: {(finished: Bool) in
                if(finished){
                    self.view.removeFromSuperview()
                }
        })
    }
}
