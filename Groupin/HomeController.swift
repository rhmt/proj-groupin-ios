//
//  HomeController.swift
//  Groupin
//
//  Created by Macbook pro on 7/27/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class HomeController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func backToHome(_ sender: UIStoryboardSegue){
        print("wb")
    }
}
