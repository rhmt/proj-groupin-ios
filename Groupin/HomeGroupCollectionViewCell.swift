//
//  HomeGroupCollectionViewCell.swift
//  Groupin
//
//  Created by Macbook pro on 7/29/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class HomeGroupCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgAvatr: UIImageView!
    @IBOutlet weak var lblNama: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var imgPlus: UIImageView!
    @IBOutlet weak var imgBadge: UIImageView!
    
    //Tambahan media
    @IBOutlet weak var lblTitleLastUpdate: UILabel!
    @IBOutlet weak var lblUpdateDetail: UILabel!
}
