//
//  IntroController.swift
//  Groupin
//
//  Created by Macbook pro on 6/24/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class IntroController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var pageViewController: UIPageViewController!
    
    let pages = ["IntroOneView", "IntroLastView"]
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore
        viewController: UIViewController) -> UIViewController? {
        
        if let index = pages.index(of: viewController.restorationIdentifier!) {
            if index > 0  {
                return viewControllerAtIndex(index - 1)
            }
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter
        viewController: UIViewController) -> UIViewController? {
        
        if let index = pages.index(of: viewController.restorationIdentifier!) {
            if index < pages.count - 1  {
                return viewControllerAtIndex(index + 1)
            }
        }
        
        return nil
    }
    
    func viewControllerAtIndex(_ index: Int) -> UIViewController? {
        let vc = storyboard?.instantiateViewController(withIdentifier: pages[index])
        
//        if pages[index] == "PageFourViewController" {
//            (vc as! IntroLastController).delegate = self
//        }else{
//            (vc as! IntroSkipController).delegate = self
//        }
        
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "IntroPageView") {
            self.addChildViewController(vc)
            self.view.addSubview(vc.view)
            
            pageViewController = vc as! UIPageViewController
            pageViewController.dataSource = self
            pageViewController.delegate = self
            
            pageViewController.setViewControllers([viewControllerAtIndex(0)!], direction: .forward, animated: true, completion: nil)
            
            pageViewController.didMove(toParentViewController: self)
        }
        
    }
}
