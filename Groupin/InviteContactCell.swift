//
//  InviteContactCell.swift
//  Groupin
//
//  Created by Macbook pro on 8/3/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class InviteContactCell: UITableViewCell {

    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblNama: UILabel!
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var imgSelect: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
