//
//  InviteContactController.swift
//  Groupin
//
//  Created by Macbook pro on 7/29/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class InviteContactController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblContact: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    
    var dari = ""
    var idUser = 0
    var token = ""
    var selected = 0
    var statusSelect = "hide"
    var phoneSelect = [""]
    var idGroup = 0
    var subTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        
        //Get Phone from Session
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.token = idSession!.token
        self.idGroup = idSession!.id_group
        phoneSelect.removeAll()
        
        let rightDoneBarButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(InviteContactController.doneTapped))
        let rightSearchBarButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(InviteContactController.searchTapped))
        self.navigationItem.setRightBarButtonItems([rightDoneBarButtonItem, rightSearchBarButtonItem], animated: true)
        
        self.makeSerialize()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idUser as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    func getData(_ message: String){
        let urlString = Config().urlUser + "list_member_friend"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)
                if self.jsonData["msg"] > 0 {
                    self.tblContact.isHidden = false
                    self.lblNoData.isHidden = true
                }else{
                    self.tblContact.isHidden = true
                    self.lblNoData.isHidden = false
                }
                self.paramsJson = 1
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.tblContact.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.paramsJson == 1 {
            return self.jsonData["msg"].count
        }else{
            return 0
        }
    }
    
    var countDataFriend = 0
    var id = [String]()
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as! InviteContactCell
        
        var data: JSON!
        if countDataFriend == 0 {
            if self.paramsJson == 1 {
                id.removeAll()
                for (key, _) in jsonData["msg"] {
                    id.append(key)
                }
                data = jsonData["msg"][id[indexPath.row]]
                countDataFriend = jsonData["msg"].count
            }
        }else{
            if indexPath.row < countDataFriend{
                if self.paramsJson == 1 {
                    id.removeAll()
                    for (key, _) in jsonData["msg"] {
                        id.append(key)
                    }
                    data = jsonData["msg"][id[indexPath.row]]
                    countDataFriend = jsonData["msg"].count
                }
            }else{
                self.paramsJson = 0
            }
        }
        
        if indexPath.row < countDataFriend {
            if let imgUrl = data["avatar"].string {
                cell.imgAva.loadImageUsingCacheWithUrlString(imgUrl)
            }else{
                cell.imgAva.image = UIImage(named: "default-avatar")
            }
            cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
            cell.imgAva.clipsToBounds = true
            cell.lblNama.text = data["name"].string
            cell.imgSelect.isHidden = true
        }else{}
        
        for index in currentRow {
            if indexPath.row == index {
                cell.imgSelect.isHidden = false
            }
        }
        
        return cell
    }
    
    var currentRow = [Int]()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Find phone is exist or not in phoneSelect
        let data = jsonData["msg"][id[indexPath.row]]
        var status = ""
        for index in phoneSelect {
            if index == data["id"].string {
                status = "ada"
            }
        }
        
        // add or remove phone in phoneSelect
        let selectedRowIndex = indexPath.row
        if status == "" {
            //Select
            self.statusSelect = "show"
            phoneSelect.append(data["id"].string!)
            currentRow.append(selectedRowIndex)
            self.selected += 1
        }else{
            //Deselect
            self.statusSelect = "hide"
            let idData = self.phoneSelect.index(of: data["id"].string!)
            phoneSelect.remove(at: idData!)
            let inx = self.currentRow.index(of: indexPath.row)
            currentRow.remove(at: inx!)
            self.selected -= 1
        }
        tblContact.reloadData()
        
        self.subTitle = "SELECT (\(self.selected))"
        self.navigationItem.titleView = Config().setTitleNavBar("Invite", subtitle: self.subTitle)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showInviteGroup" {
            let conn = segue.destination as! InviteGroupController
            conn.dari = "create"
        }
    }
    
    func searchTapped(_ sender:UIButton){
        
    }
    
    func doneTapped(_ sender:UIButton){
        makeSerializePost()
    }
    
    func makeSerializePost(){
        var dataId = [String:AnyObject]()
        var arrayIdInput = [NSString]()
        for id in self.phoneSelect {
            dataId = [
                "id" : id as AnyObject
            ]
            let data = try! JSONSerialization.data(withJSONObject: dataId, options: .prettyPrinted)
            let stringData = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            arrayIdInput += [stringData!]
        }
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_group" : self.idGroup as AnyObject,
            "inviter" : self.idUser as AnyObject]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        let strJason = String(jsonString!)
        let jasonData = String(strJason.characters.dropLast())
        let passData = ("\(jasonData),\n\"id\" : \(arrayIdInput)\n}")
        
        makeBase64Post(passData as NSString)
    }
    
    func makeBase64Post(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postData(base64Encoded)
        }
    }
    
    func postData(_ message: String){
        let urlString = Config().urlUpdate + "invite_user_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let _ = response.result.value {
                //Success
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                if self.dari == "create" {
                    self.performSegue(withIdentifier: "showInviteGroup", sender: self)
                }else{
                    //balik ke about group
                }
        }
    }
}
