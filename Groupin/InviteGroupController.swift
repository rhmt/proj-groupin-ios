//
//  InviteGroupController.swift
//  Groupin
//
//  Created by Macbook pro on 8/3/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class InviteGroupController: UIViewController {

    @IBOutlet weak var colGroup: UICollectionView!
    @IBOutlet weak var lblNoData: UILabel!
    
    var dari = ""
    var models = [tb_group]()
    var groupSelect = [String]()
    var selected = 0
    var idUser = 0
    var idGroup = 0
    var token = ""
    var nameGroup = ""
    var subTitle = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        models = DBHelper.getAllGroup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // Get idGroup From Session
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.token = idSession!.token
        self.idGroup = idSession!.id_group
        groupSelect.removeAll()
        
        let rightDoneBarButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(InviteGroupController.doneTapped))
        let rightSearchBarButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(InviteGroupController.searchTapped))
        self.navigationItem.setRightBarButtonItems([rightDoneBarButtonItem, rightSearchBarButtonItem], animated: true)
        
        makeSerialize()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idUser as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    func getData(_ message: String){
        let urlString = Config().urlGroup + "list_recommended_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)
                self.paramsJson = 1
                if self.jsonData["msg"] > 0 {
                    self.colGroup.isHidden = false
                    self.lblNoData.isHidden = true
                }else{
                    self.colGroup.isHidden = true
                    self.lblNoData.isHidden = false
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.colGroup.reloadData()
        }
    }
    
    func searchTapped(_ sender:UIButton){
        
    }
    
    //CollectionView
    func numberOfSectionsInCollectionView(_ collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if self.paramsJson == 1 {
            return self.jsonData["msg"].count
        }else{
            return 0
        }
    }
    
    var id = [String]()
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InviteGroupCell", for: indexPath) as! WelcomeCollectionViewCell
        
        id.removeAll()
        for (key, _) in jsonData["msg"] {
            id.append(key)
        }
        let data = jsonData["msg"][id[indexPath.row]]
        
        if let imgUrl = data["avatar"].string {
            cell.imgAvatr.loadImageUsingCacheWithUrlString(imgUrl)
        }else{
            cell.imgAvatr.image = UIImage(named: "dafault-ava-group")
        }
        cell.imgAvatr.contentMode = .scaleToFill
        cell.lblNama.text = data["group"].string
        
//        cell.lblCategori.text = data["category"].string
//        let place = data["country"].string! + ", " + data["city"].string!
//        cell.lblPlace.text = place
//        cell.lblMember.text = data["member"].string
        cell.imgSelect.isHidden = true
        
        for index in currentRow {
            if indexPath.row == index {
                cell.imgSelect.isHidden = false
            }
        }
        
        return cell
        
    }
    
    var currentRow = [Int]()
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath)
    {
        let data = jsonData["msg"][id[indexPath.row]]
        var status = ""
        for index in groupSelect {
            if index == data["id"].string {
                status = "ada"
            }
        }
        
        // add or remove idGroup in groupSelect
        if status == "" {
            //Select
            groupSelect.append(data["id"].string!)
            currentRow.append(indexPath.row)
            self.selected += 1
        }else{
            //Deselect
            let idData = self.groupSelect.index(of: data["id"].string!)
            groupSelect.remove(at: idData!)
            let inx = self.currentRow.index(of: indexPath.row)
            currentRow.remove(at: inx!)
            self.selected -= 1
        }
        self.subTitle = "SELECT (\(self.selected))"
        self.navigationItem.titleView = Config().setTitleNavBar("Invite", subtitle: self.subTitle)
        
        self.colGroup.reloadData()
    }
    
    func doneTapped(_ sender:UIButton){
        self.makeSerializePost()
    }
    
    func makeSerializePost(){
        var dataId = [String:AnyObject]()
        var arrayIdInput = [NSString]()
        for id in self.groupSelect {
            dataId = [
                "id" : id as AnyObject
            ]
            let data = try! JSONSerialization.data(withJSONObject: dataId, options: .prettyPrinted)
            let stringData = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            arrayIdInput += [stringData!]
        }
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_group_parent" : self.idGroup as AnyObject,
            "id_inviter" : self.idUser as AnyObject]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        let strJason = String(jsonString!)
        let jasonData = String(strJason.characters.dropLast())
        let passData = ("\(jasonData),\n\"id\" : \(arrayIdInput)\n}")
        
        makeBase64Post(passData as NSString)
    }
    
    func makeBase64Post(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postData(base64Encoded)
        }
    }
    
    func postData(_ message: String){
        let urlString = Config().urlUpdate + "invite_child_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                //Success
                print(JSON(value))
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                if self.dari == "create" {
                    //SEGUE
                    let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "showGroupHome") as! GroupHomeController
                    let navigationController = UINavigationController(rootViewController: vc)
                    vc.navigationItem.title = self.nameGroup
                    vc.fromCreate = 1
                    self.present(navigationController, animated: true, completion: nil)
                }else {
                    //balik ke about group
                }
        }
    }
}
