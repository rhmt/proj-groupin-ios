//
//  LeaderBoardCell.swift
//  Groupin
//
//  Created by Macbook pro on 1/6/17.
//  Copyright © 2017 Macbook pro. All rights reserved.
//

import UIKit

class LeaderBoardCell: UITableViewCell {
    
    @IBOutlet weak var imgMedal: UIImageView!
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLikes: UILabel!
}