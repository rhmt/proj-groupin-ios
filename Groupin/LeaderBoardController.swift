//
//  LeaderBoardController.swift
//  Groupin
//
//  Created by Macbook pro on 1/6/17.
//  Copyright © 2017 Macbook pro. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import RealmSwift

class LeaderBoardController: UIViewController {
    
    @IBOutlet weak var tbLeaderBoard: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    
    var idGroup = 0
    var token = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tbLeaderBoard.isHidden = true
        self.tbLeaderBoard.tableFooterView = UIView()
        
        let sess = try! Realm().objects(session.self).first!
        self.idGroup = sess.id_group
        self.token = sess.token
        
        self.makeSerialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.navigationItem.title = "Leaderboard"
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_group" : self.idGroup as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    func getData(_ message: String){
        let urlString = Config().urlGroup + "data_leaderboard"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                if JSON(value)["code"].intValue == 1 {
                    self.paramsJson = 1
                    self.jsonData = JSON(value)["msg"]
                    print(self.jsonData)
                    if self.jsonData.count > 0 {
                        self.tbLeaderBoard.isHidden = false
                        self.lblNoData.isHidden = true
                    }else{
                        self.tbLeaderBoard.isHidden = true
                        self.lblNoData.isHidden = false
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.tbLeaderBoard.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.paramsJson == 1 {
            return self.jsonData.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "leaderboardCell", for: indexPath) as! LeaderBoardCell
        
        if self.paramsJson == 1 {
            let data = self.jsonData[indexPath.row]
            
            let list = indexPath.row + 1
            let named = "ico_cup" + String(list)
            cell.imgMedal.image = UIImage(named: named)
            
            if let img = data["avatar"].string {
                cell.imgAva.loadImageUsingCacheWithUrlString(img)
            }else{
                cell.imgAva.image = UIImage(named: "default-avatar")
            }
            cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
            cell.imgAva.clipsToBounds = true
            
            cell.lblName.text = data["name"].string!
            cell.lblLikes.text = data["like"].string! + " Point"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        
    }
    
}
