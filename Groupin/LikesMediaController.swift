//
//  LikesMediaController.swift
//  Groupin
//
//  Created by Macbook pro on 12/1/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class LikesMediaController: UIViewController {
    
    @IBOutlet weak var tbLiker: UITableView!
    
    var idUser = 0
    var idGroup = 0
    var token = ""
    var idMedia = ""
    var totalLikes = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.loadData()
    }
    
    func loadData(){
        self.navigationItem.title = "Likes (\(totalLikes))"
        
        //Session
        let dataSession = try! Realm().objects(session.self).first
        self.idUser = dataSession!.id
        self.idGroup = dataSession!.id_group
        self.token = dataSession!.token
        
        //        let rightMenuButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Search, target: self, action: #selector(LikesForumController.searchLikers))
        //        self.navigationItem.setRightBarButtonItems([rightMenuButtonItem], animated: true)
        
        makeSerialize()
    }
    
    func searchLikers(){
        print("Search Tapped")
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_media" : self.idMedia as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    func getData(_ message: String){
        let urlString = Config().urlMedia + "list_like"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)["msg"]
                print(self.jsonData)
                if self.jsonData.count > 0 {
                    self.paramsJson = 1
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.tbLiker.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.paramsJson == 1 {
            return self.jsonData.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listLikerCell", for: indexPath) as! ListGeneralCell
        
        if self.paramsJson == 1 {
            let data = jsonData[indexPath.row]["user"]
            if let imgUrl = data["thumbnail"].string {
                cell.imgAva.loadImageUsingCacheWithUrlString(imgUrl)
            }
            cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
            cell.imgAva.clipsToBounds = true
            cell.lblTitle.text = data["name"].string!
        }
        
        return cell
    }
    
    var idLikerSelected = ""
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        //        idLikerSelected = idLiker[indexPath.row]
    }
}
