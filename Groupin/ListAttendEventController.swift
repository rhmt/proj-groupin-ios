//
//  ListAttendEventController.swift
//  Groupin
//
//  Created by Macbook pro on 11/23/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import RealmSwift

class ListAttendEventController: UIViewController {
    
    @IBOutlet weak var tbAttend: UITableView!
    
    var token = ""
    var idTicket = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadLocal()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        let sess = try! Realm().objects(session.self).first!
        token = sess.token
        
        self.makeSerialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var dataData = [listBuyTicket]()
    func loadLocal(){
        self.dataData.removeAll()
        let data = try! Realm().objects(listBuyTicket.self).filter("id_ticket = %@", self.idTicket).sorted(byKeyPath: "name_user", ascending: true)
        for item in data {
            self.dataData.append(item)
        }
        
        if self.paramsJson == 1 {
            self.tbAttend.reloadData()
        }
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_ticket" : self.idTicket as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            self.getData(base64Encoded)
        }
    }
    
    var paramsJson = 0
    var jsonData: JSON!
    func getData(_ message: String){
        let urlString = Config().urlEvent + "list_attendance"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)["msg"]
                print(jsonData)
                self.jsonData = jsonData
                if jsonData.count > 0 {
                    self.paramsJson = 1
                    
                    var i = 0
                    for (_, _) in jsonData {
                        let id = jsonData[i]["id_attendance"].intValue
                        let modelUpdate = try! Realm().objects(listBuyTicket.self).filter("id_buy = %@", id)
                        if modelUpdate.count == 0 {
                            let model = listBuyTicket()
                            model.id_buy = id
                            model.id_ticket = self.idTicket
                            model.id_user = jsonData[i]["user"]["id"].intValue
                            model.name_user = jsonData[i]["user"]["name"].string!
                            model.ava_user = jsonData[i]["user"]["thumbnail"].string!
//                            model.status_attend = jsonData[i]["status_attend"].string!
                            model.total = jsonData[i]["ticket_amount"].string!
                            DBHelper.insert(model)
                        }else{
                            let update = modelUpdate.first!
                            try! Realm().write({
                                update.name_user = jsonData[i]["user"]["name"].string!
                                update.ava_user = jsonData[i]["user"]["thumbnail"].string!
//                                update.status_attend = jsonData[i]["status_attend"].string!
                                update.total = jsonData[i]["ticket_amount"].string!
                            })
                        }
                        i += 1
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.loadLocal()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listAttendCell", for: indexPath) as! ListGeneralCell
        
        let data = self.dataData[indexPath.row]
        if data.ava_user != "" {
            cell.imgAva.loadImageUsingCacheWithUrlString(data.ava_user)
        }else{
            cell.imgAva.image = UIImage(named: "default-avatar")
        }
        cell.imgAva.contentMode = .scaleToFill
        cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
        cell.imgAva.clipsToBounds = true
        cell.lblTitle.text = data.name_user
        
//        cell.lblDesc.text = data.status_attend
        cell.lblDesc.isHidden = true
        cell.lblSubTitle.text = data.total
        
        return cell
    }
}
