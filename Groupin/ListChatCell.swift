//
//  ListChatCell.swift
//  Groupin
//
//  Created by Macbook pro on 8/18/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class ListChatCell: UITableViewCell {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgBadge: UIImageView!
}
