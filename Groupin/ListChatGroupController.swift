
//
//  ListChatGroupController.swift
//  Groupin
//
//  Created by Macbook pro on 8/15/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire
import GooglePlaces
import Firebase

class ListChatGroupController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var idUser = 0
    var idGroup = 0
    var token = ""
    
    var models = tb_group()
    
    @IBOutlet weak var tbListChat: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbListChat.tableFooterView = UIView()
        
        let idSession = try! Realm().objects(session.self).first!
        self.idUser = idSession.id
        self.idGroup = idSession.id_group
        self.token = idSession.token
        
        //Subscribe
        let subscribeGroup = "/topics/G" + String(self.idGroup)
        FIRMessaging.messaging().subscribe(toTopic: subscribeGroup)
        
        self.loadLocal()
        
        self.checkFromNotif()
    }
    
    func checkFromNotif(){
        if let data = try! Realm().objects(session_notif.self).first {
            if data.fromNotif == 1 {
                self.idRoom = data.id
                self.nameSelected = data.name
                
                try! Realm().write({
                    data.fromNotif = 0
                    data.id = 0
                    data.name = ""
                    data.avatar = ""
                })
                
                self.performSegue(withIdentifier: "showChating", sender: self)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        loadLocal()
        makeSerialize()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    var dataData = [tb_room]()
    var canEnterChat = 0
    func loadLocal(){
        dataData.removeAll()
        let dataFilter = try! Realm().objects(tb_room.self).filter("id_group = %@", self.idGroup)
        for item in dataFilter {
            dataData += [item]
        }
        if self.paramsJson == 1 {
            self.tbListChat.reloadData()
        }
        
        let dataMember = try! Realm().objects(listMember.self).filter("id_group = %@", self.idGroup)
        if dataMember.count > 0{
            self.idFriend.removeAll()
            self.nameFriend.removeAll()
            self.canEnterChat = 1
            for data in dataMember {
                self.idFriend.append(String(data.id_member))
                self.nameFriend.append(data.name)
                var imgUrl = ""
                let validImgUrl = data.avatar
                if validImgUrl != "" {
                    imgUrl = validImgUrl
                }else{
                    imgUrl = Config().urlImage + "default-avatar.png"
                }
                self.avaFriend.append(imgUrl)
                self.colorFriend.append(Config().randomColor())
            }
        }
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idGroup as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    func getData(_ message: String){
        let urlString = Config().urlRoom + "list_chat_room"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)
                print(self.jsonData)
                if self.jsonData["msg"].count > 0 {
                    self.paramsJson = 1
                    var i = 0
                    for _ in self.jsonData["msg"] {
                        let id = self.jsonData["msg"][i]["id"].string!
                        let updateModel = try! Realm().objects(tb_room.self).filter("id_room = %@", Int(id)!).first
                        if updateModel != nil {
                            //Update
                            try! Realm().write {
                                updateModel!.id_group = self.idGroup
                                updateModel!.id_room = Int(id)!
                                updateModel!.name = self.jsonData["msg"][i]["room"].string!
                            }
                        }else{
                            //Insert
                            let insertModel = tb_room()
                            insertModel.id_group = self.idGroup
                            insertModel.id_room = Int(id)!
                            insertModel.name = self.jsonData["msg"][i]["room"].string!
                            DBHelper.insert(insertModel)
                        }
                        i += 1
                    }
                }
                
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.makeSerializeMember()
        }
    }
    
    var statusEdit = 0
    var nameChat = ""
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCreateChannel" {
            let conn = segue.destination as! CreateChannelController
            conn.navigationItem.title = "Create Chat Room"
            if statusEdit == 1 {
                conn.statusEdit = 1
                conn.nameChat = self.nameChat
                conn.idRoom = self.idRoom
            }
        }
        if segue.identifier == "showChating" {//"showChat" {
            let chatController = segue.destination as! ChatingController //ChatController
            chatController.titleNav = self.nameSelected
            chatController.toId = "G" + String(self.idRoom)
            chatController.idFriend = self.idFriend
            chatController.nameFriend = self.nameFriend
            chatController.avaFriend = self.avaFriend
            chatController.colorFriend = self.colorFriend
            chatController.paramsKindChat = "group"
        }
        if segue.identifier == "showChatGames" {
            let chatController = segue.destination as! ChatGamesController
            //chatController.toId = "G" + String(self.idRoom)
            chatController.idFriend = self.idFriend
            chatController.nameFriend = self.nameFriend
            chatController.avaFriend = self.avaFriend
            chatController.colorFriend = self.colorFriend
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataData.count + 2
    }
    
    var id = [String]()
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listChatCell", for: indexPath) as! ListChatCell
        
        cell.imgBadge.isHidden = true
        
        if indexPath.row < self.dataData.count  {
            let data = self.dataData[indexPath.row]
            if data.name == "General" {
                cell.lblName.text = data.name
                cell.imgAva.image = UIImage(named: "ico_chat_blue")
            }else{
                cell.lblName.text = data.name
                cell.imgAva.image = UIImage(named: "ico_chat_pink")
                
                //gesture long press
                let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(ListChatGroupController.longPressCell(_:)))
                longPressRecognizer.minimumPressDuration = 1.00
                cell.addGestureRecognizer(longPressRecognizer)
            }
            
            //Indicator unread chat
            let to = "G" + String(data.id_room)
            let indi = try! Realm().objects(tb_chat.self).filter("to = %@", to).filter("is_read = %@", 0)
            if indi.count > 0{
                cell.imgBadge.isHidden = false
            }
        }
        if indexPath.row == self.dataData.count  {
            cell.lblName.text = "Games"
            cell.imgAva.image = UIImage(named: "ico_chat_green")
            
            //Indicator unread chat
            let to = "Games" + String(self.idGroup)
            let indi = try! Realm().objects(tb_chat.self).filter("to = %@", to).filter("is_read = %@", 0)
            if indi.count > 0{
                cell.imgBadge.isHidden = false
            }
        }
        if indexPath.row == self.dataData.count + 1  {
            cell.lblName.text = "Add Chat Room"
            cell.imgAva.image = UIImage(named: "ico_plus_gray_nobg")
        }
        
        return cell
    }
    
    var nameSelected = ""
    var indexPathListRoom = 0
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        self.indexPathListRoom = indexPath.row
        
        if indexPath.row < self.dataData.count  {
            if self.canEnterChat > 0 {
                let data = self.dataData[indexPath.row]
                self.idRoom = data.id_room
                if data.name == "General" {
                    print("Push General")
                    // General
                    self.nameSelected = data.name
                    self.performSegue(withIdentifier: "showChating", sender: self)
                }else{
                    //Enter to Chat
                    checkLevelUserRoom()
                }
                
                let to = "G" + String(data.id_room)
                let indi = try! Realm().objects(tb_chat.self).filter("to = %@", to).filter("is_read = %@", 0)
                
                if indi.count > 0 {
                    for item in indi {
                        try! Realm().write {
                            item.is_read = 1
                        }
                    }
                }
                
                if UIApplication.shared.applicationIconBadgeNumber >= indi.count {
                    UIApplication.shared.applicationIconBadgeNumber -= indi.count
                }
            }
        }
        if indexPath.row == self.dataData.count  {
            self.performSegue(withIdentifier: "showChatGames", sender: self)
            
            let to = "Games" + String(self.idGroup)
            let indi = try! Realm().objects(tb_chat.self).filter("to = %@", to).filter("is_read = %@", 0)
            if indi.count > 0 {
                for item in indi {
                    try! Realm().write {
                        item.is_read = 1
                    }
                }
            }
        }
        if indexPath.row == self.dataData.count + 1  {
            self.performSegue(withIdentifier: "showCreateChannel", sender: self)
        }
    }
    
    func checkLevelUserRoom(){
        let levelUser = try! Realm().objects(tb_own_room.self).filter("id_room = %@", self.idRoom).first
        
        if levelUser != nil {
            dataLevelMember = Int(levelUser!.user_level)!
        }else{
            dataLevelMember = 3
        }
        if cekDari == "long" {
            self.more(UIButton.init())
        }else{
            roomSelect()
        }
    }
    
    func roomSelect(){
        let data = self.dataData[self.indexPathListRoom]
        
        if dataLevelMember == 3 {
            print("You are not invited to join this room")
            let alert = UIAlertController(title: "Info", message: "You are not invited to join this room", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }else{
            // Chat Room
            self.nameSelected = data.name
            self.performSegue(withIdentifier: "showChat", sender: self)
        }
    }
    
    var cekDari = ""
    func longPressCell(_ sender: UILongPressGestureRecognizer){
        let button = sender
        let view = button.view
        let cell = view as! ListChatCell
        let indexPath = tbListChat.indexPath(for: cell)
        
        let data = self.dataData[indexPath!.row]
        self.idRoom = data.id_room
        print(self.idRoom)
        self.paramsLevelMember = 1
        
        cekDari = "long"
        checkLevelUserRoom()
    }
    
    var paramsLevelMember = 0
    var dataLevelMember = 0
    var idRoom = 0
    func tableView(_ tableView: UITableView, editActionsForRowAtIndexPath indexPath: IndexPath) -> [UITableViewRowAction]? {
//        let delete = UITableViewRowAction(style: .Destructive, title: "Delete") { (action, indexPath) in
//            // delete item at indexPath
//            self.makeSerializeDelete()
//        }
//        let admin = UITableViewRowAction(style: .Normal, title: "Set Admin") { action, index in
//            self.statusEdit = 1
//            self.performSegueWithIdentifier("showCreateChannel", sender: self)
//        }
//        admin.backgroundColor = UIColor.blueColor()
//        
//        let join = UITableViewRowAction(style: .Normal, title: "Ask to Join") { action, index in
//            
//        }
//        join.backgroundColor = UIColor.blueColor()
//        let more = UITableViewRowAction(style: .Normal, title: "More") { action, index in
//            self.paramsLevelMember = 1
//            self.makeSerializeDelete()
//        }
//        more.backgroundColor = UIColor.lightGrayColor()
//        
//        let count = jsonData["msg"].count
//        if indexPath.row < count  {
//            let data = jsonData["msg"][id[indexPath.row]]
//            if data["room"].string == "General" {
//                return []
//            }else{
//                self.idRoom = Int(jsonData["msg"][id[indexPath.row]]["id"].string!)!
//                return [more]
//            }
//        }else{
//            print("Create")
//            return []
//        }
        
        return []
    }
    
//    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // the cells you would like the actions to appear needs to be editable
//        return true
//    }
    
    //Delete
    func makeSerializeDelete(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "id_room" : self.idRoom as AnyObject,
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Delete(jsonString!)
    }
    
    func makeBase64Delete(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            if self.paramsLevelMember == 0 {
                getDataDelete(base64Encoded)
            }else{
                getDataLevel(base64Encoded)
            }
        }
    }
    
    func getDataDelete(_ message: String){
        let urlString = Config().urlRoom + "delete_chat_room"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)
                print(self.jsonData)
                if self.jsonData["msg"].count > 0 {
                    self.paramsJson = 1
                }
                
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.makeSerialize()
        }
    }
    
    func getDataLevel(_ message: String){
        let urlString = Config().urlUser + "cek_user_room_status"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                if jsonData.count > 0 {
                    self.dataLevelMember = jsonData["msg"].intValue
                }
                
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
        }
    }
    
    // More
    func more(_ sender: UIButton) {
        let alert:UIAlertController=UIAlertController(title: "More Settings", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.modalPresentationStyle = .popover
        
        let deleteAction = UIAlertAction(title: "Delete Room", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.paramsLevelMember = 0
            self.makeSerializeDelete()
        }
        
        let adminAction = UIAlertAction(title: "Set Admin", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.statusEdit = 1
            self.performSegue(withIdentifier: "showCreateChannel", sender: self)
        }
        let joinAction = UIAlertAction(title: "Ask to Join", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.makeSerializeJoin()
        }
        let leaveAction = UIAlertAction(title: "Leave Room", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.makeSerializeLeave()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel){ UIAlertAction in }
        
        if dataLevelMember == 1 {
            alert.addAction(leaveAction) // member room biasa
        }else if dataLevelMember == 2 {
            alert.addAction(deleteAction) // Admin room
            alert.addAction(adminAction)
        }else{
            alert.addAction(joinAction) // bukan member room
        }
        alert.addAction(cancelAction)
        
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds;
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //Data Member Group
    func makeSerializeMember(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idGroup as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Member(jsonString!)
    }
    
    func makeBase64Member(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            self.getMember(base64Encoded)
        }
    }
    
    var idFriend = [String]()
    var nameFriend = [String]()
    var avaFriend = [String]()
    var colorFriend = [UIColor]()
    func getMember(_ message: String){
        let urlString = Config().urlUser + "list_member_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let data = JSON(value)["msg"]
                print("list member")
                print(data)
                if data.count > 0 {
                    for (key, _) in data {
                        var imgUrl = ""
                        if let validImgUrl = data[key]["avatar"].string {
                            imgUrl = validImgUrl
                        }else{
                            imgUrl = Config().urlImage + "default-avatar.png"
                        }
                        
                        let checkData = try! Realm().objects(listMember.self).filter("id_member = %@ AND id_group = %@", Int(key)!, self.idGroup).first
                        if checkData == nil {
                            let model = listMember()
                            model.id_member = Int(key)!
                            model.id_group = self.idGroup
                            model.name = data[key]["name"].string!
                            model.avatar = imgUrl
                            model.status_level = data[key]["user_level"].string!
                            if let fromGroup = data[key]["from_group"].string {
                                model.from_group = fromGroup
                            }
                            DBHelper.insert(model)
                        }else{
                            try! Realm().write({ 
                                checkData!.name = data[key]["name"].string!
                                checkData!.avatar = imgUrl
                                checkData!.status_level = data[key]["user_level"].string!
                            })
                        }
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.loadLocal()
        }
    }
    
    //Data Member Group
    func makeSerializeJoin(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idUser as AnyObject,
            "id_room" : self.idRoom as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Join(jsonString!)
    }
    
    func makeBase64Join(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            self.postJoin(base64Encoded)
        }
    }
    
    func postJoin(_ message: String){
        let urlString = Config().urlUpdate + "join_user_room"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(urlString)
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let data = JSON(value)
                print(data)
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                
        }
    }
    
    //Leave Room
    func makeSerializeLeave(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "id_room" : self.idRoom as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Leave(jsonString!)
    }
    
    func makeBase64Leave(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            self.postLeave(base64Encoded)
        }
    }
    
    func postLeave(_ message: String){
        let urlString = Config().urlRoom + "leave_room"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(urlString)
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let data = JSON(value)
                print(data)
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.updateLevelUser()
        }
    }
    
    func updateLevelUser(){
        let updateLevel = try! Realm().objects(tb_own_room.self).filter("id_room == \(self.idRoom)").first
        try! Realm().write({ 
            (updateLevel?.user_level = "3")!
        })
        
    }
    
    @IBAction func backToGroup(_ segue: UIStoryboardSegue){
        self.tbListChat.reloadData()
    }
}
