//
//  ListEventCell.swift
//  Groupin
//
//  Created by Macbook pro on 1/9/17.
//  Copyright © 2017 Macbook pro. All rights reserved.
//

import UIKit

class ListEventCell: UITableViewCell {
    
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblMount: UILabel!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblCreated: UILabel!
    @IBOutlet weak var imgCreated: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgDate: UIImageView!
    @IBOutlet weak var lblQuota: UILabel!
    @IBOutlet weak var imgQuota: UIImageView!
    @IBOutlet weak var viewMenu: UIView!
    @IBOutlet weak var viewScan: UIView!
    @IBOutlet weak var imgScan: UIImageView!
    @IBOutlet weak var viewCalendar: UIView!
    @IBOutlet weak var imgCalendar: UIImageView!
    @IBOutlet weak var viewLocation: UIView!
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var viewJoined: UIView!
    @IBOutlet weak var lblJoined: UILabel!
    @IBOutlet weak var viewContainerPrice: UIView!
    @IBOutlet weak var conWidthPrice: NSLayoutConstraint!
    @IBOutlet weak var lblTicketLeft: UILabel!
    
}
