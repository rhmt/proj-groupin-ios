//
//  ListGeneralCell.swift
//  Groupin
//
//  Created by Macbook pro on 10/24/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class ListGeneralCell: UITableViewCell {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var heightTitle: NSLayoutConstraint!
}
