//
//  ListStaffEventController.swift
//  Groupin
//
//  Created by Macbook pro on 12/30/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire
import EventKit

class ListStaffEventController: UIViewController {
    
    @IBOutlet weak var tbEvent: UITableView!
    var phone = ""
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.navigationItem.title = "List Event"
        self.colorNav()
        self.rightMenu()
        
        if let sess = try! Realm().objects(session.self).first {
            self.token = sess.token
        }
        
        if let sessPhone = try! Realm().objects(tb_user.self).first {
            self.phone = sessPhone.phone
        }
        
        self.makeSerialize()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "phone" : self.phone as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postData(base64Encoded)
        }
    }
    
    var jasonData: JSON!
    var paramsJson = 0
    func postData(_ message: String){
        let urlString = Config().urlEvent + "list_event_by_staff"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                //Success
                let jsonData = JSON(value)
                print(jsonData)
                if jsonData["code"] == "1" {
                    let data = jsonData["msg"]
                    self.jasonData = data
                    self.paramsJson = 1
                    self.tbEvent.reloadData()
                }else {
                    
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.paramsJson == 1 {
            return self.jasonData.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listEventCell", for: indexPath) as! ListEventCell
        
        if self.paramsJson == 1 {
            let data = self.jasonData[indexPath.row]
            
            //Main Date
            let dateGet = DateFormatter()
            dateGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateDay = DateFormatter()
            dateDay.dateFormat = "dd"
            if let date = dateGet.date(from: data["date_from"].string!) {
                let dateShow = dateDay.string(from: date)
                cell.lblDay.text = dateShow
            }
            let dateMount = DateFormatter()
            dateMount.dateFormat = "MMM"
            if let date = dateGet.date(from: data["date_from"].string!) {
                let dateShow = dateMount.string(from: date)
                cell.lblMount.text = dateShow
            }
            //---------
            
            if let img = data["image"].string {
                cell.imgAva.loadImageUsingCacheWithUrlString(img)
            }else{
                cell.imgAva.image = UIImage(named: "dafault-ava-group")
            }
            cell.imgAva.clipsToBounds = true
            cell.lblTitle.text = data["name"].string!
            cell.lblTitle.textColor = UIColor(red: 63, green: 157, blue: 247)
            
            //Price
            var arrayPrice = [Int]()
            var arrayPriceType = [String]()
            var inx = 0
            for _ in data["ticket"] {
                arrayPrice.append(data["ticket"][inx]["price"].intValue)
                arrayPriceType.append(data["ticket"][inx]["price_type"].string!)
                inx += 1
            }
            let sortedTicket = arrayPrice.sorted(){$0 < $1}
            let getIndexFirst = arrayPrice.index(of: sortedTicket[0])
            let getTypeFirst = arrayPriceType[getIndexFirst!]
            
            let getIndexLast = arrayPrice.index(of: sortedTicket.last!)
            let getTypeLast = arrayPriceType[getIndexLast!]
            
            var firstArrayTicket = getTypeFirst.uppercased() + " " + String(sortedTicket[0])
            if sortedTicket[0] == 0 {
                firstArrayTicket = "Free"
            }
            
            if arrayPrice.count == 1 {
                let rangeTicket = firstArrayTicket
                cell.lblPrice.text = rangeTicket
            }else{
                let rangeTicket = firstArrayTicket + " - " + getTypeLast.uppercased() + " " + String(sortedTicket.last!)
                cell.lblPrice.text = rangeTicket
            }
            cell.lblPrice.textColor = UIColor(red: 83, green: 186, blue: 32)
            //-----------
            
            cell.lblCreated.text = data["user"]["name"].string!
            cell.lblCreated.textColor = UIColor(red: 63, green: 157, blue: 247)
            
            var dateShow = ""
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MMM d, YYYY"//"EEEE, MMM d, h:mm a"
            let dataDateFrom = data["date_from"].string!
            if let date = dateFormatterGet.date(from: dataDateFrom) {
                let dateFrom = dateFormatterPrint.string(from: date)
                dateShow += dateFrom
            }else{
                dateShow += dataDateFrom
            }
            
            dateShow += " - "
            if let dateToP = dateFormatterGet.date(from: data["date_to"].string!) {
                let dateTo = dateFormatterPrint.string(from: dateToP)
                dateShow += dateTo
            }else{
                dateShow += data["date_from"].string!
            }
            cell.lblDate.text = dateShow
            
            cell.imgScan.image = UIImage(named: "ico_qr_scan")
            cell.viewJoined.isHidden = true
            
            self.arrayLat.append(data["latitude"].string!)
            self.arrayLong.append(data["longitude"].string!)
            
            //Design
            cell.viewMain.layer.cornerRadius = 5
            cell.viewMain.layer.shadowColor = UIColor.lightGray.cgColor
            cell.viewMain.layer.shadowOffset = CGSize.zero
            cell.viewMain.layer.shadowOpacity = 0.5
            cell.viewMain.layer.shadowRadius = 3
            //------
            
            //Fungsi Menu
            let menuGesture = UITapGestureRecognizer(target: self, action: #selector(ListStaffEventController.btnScanTapped(_:)))
            menuGesture.numberOfTapsRequired = 1
            cell.viewScan.isUserInteractionEnabled = true
            cell.viewScan.addGestureRecognizer(menuGesture)
            
            let calendarGesture = UITapGestureRecognizer(target: self, action: #selector(EventController.btnSaveCalenderTapped(_:)))
            calendarGesture.numberOfTapsRequired = 1
            cell.viewCalendar.isUserInteractionEnabled = true
            cell.viewCalendar.addGestureRecognizer(calendarGesture)
            
            let placeGesture = UITapGestureRecognizer(target: self, action: #selector(EventController.btnViewMapTapped(_:)))
            placeGesture.numberOfTapsRequired = 1
            cell.viewLocation.isUserInteractionEnabled = true
            cell.viewLocation.addGestureRecognizer(placeGesture)
            //-----------
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        let data = self.jasonData[indexPath.row]
        self.idEvent = data["ticket"][0]["fk_event"].string!
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "showDetailEvent") as! DetailEventController
        vc.idEvent = self.idEvent
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    var idEvent = ""
    func btnScanTapped(_ sender: UITapGestureRecognizer) {
        let cell = sender.view!.superview!.superview!.superview!.superview as! ListEventCell
        let indexPath = self.tbEvent.indexPath(for: cell)
        let data = self.jasonData[indexPath!.row]
        self.idEvent = data["ticket"][0]["fk_event"].string!
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "showQRCode") as! QrCodeController
        vc.statusQR = "scan"
        vc.idEvent = self.idEvent
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func btnSaveCalenderTapped(_ sender: UITapGestureRecognizer) {
        let cell = sender.view?.superview?.superview?.superview?.superview as! ListEventCell
        let indexPath = self.tbEvent.indexPath(for: cell)
        
        let data = jasonData[indexPath!.row]
        let dateFrom = data["date_from"].string!
        let dateTo = data["date_to"].string!
        let titleEvent = data["name"].string!
        let noteEvent = data["description"].string!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let fromDate = dateFormatter.date(from: dateFrom)
        let toDate = dateFormatter.date(from: dateTo)
        
        let eventStore : EKEventStore = EKEventStore()
        eventStore.requestAccess(to: EKEntityType.event, completion: {
            (granted, error) in
            
            if (granted) && (error == nil) {
                print("granted \(granted)")
                print("error \(error)")
                
                let event:EKEvent = EKEvent(eventStore: eventStore)
                
                event.title = titleEvent
                event.startDate = fromDate!
                event.endDate = toDate!
                event.notes = noteEvent
                event.calendar = eventStore.defaultCalendarForNewEvents
                
                do {
                    try eventStore.save(event, span: .thisEvent)
                    print("Saved Event")
                    let alertLoading = UIAlertController(title: "", message: "Saved to your calendar", preferredStyle: UIAlertControllerStyle.alert)
                    self.present(alertLoading, animated: true, completion: nil)
                    UIView.animate(withDuration: 3.0, animations: {
                        alertLoading.dismiss(animated: true, completion: nil)
                    })
                } catch {
                    print("Bad things happened")
                    
                }
            }
        })
    }
    
    var arrayLat = [String]()
    var arrayLong = [String]()
    var latSelected = ""
    var longSelected = ""
    func btnViewMapTapped(_ sender: UITapGestureRecognizer) {
        let cell = sender.view?.superview?.superview?.superview?.superview as! ListEventCell
        let indexPath = self.tbEvent.indexPath(for: cell)
        latSelected = arrayLat[indexPath!.row]
        longSelected = arrayLong[indexPath!.row]
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "showViewMaps") as! EventViewMapsController
        vc.lat = Double(self.latSelected)!
        vc.long = Double(self.longSelected)!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showQRCode" {
            let conn = segue.destination as! QrCodeController
            conn.statusQR = "scan"
            conn.idEvent = self.idEvent
        }
        if segue.identifier == "showDetailEvent" {
            let conn = segue.destination as! DetailEventController
            conn.idEvent = self.idEvent
        }
        if segue.identifier == "showViewMaps" {
            let conn = segue.destination as! EventViewMapsController
            conn.lat = Double(self.latSelected)!
            conn.long = Double(self.longSelected)!
            conn.navigationItem.title = "View Map"
        }
    }
    
    // METHODE NAV
    let blue = UIColor(red: 63, green: 157, blue: 247)
    func colorNav(){
        navigationController!.navigationBar.barTintColor = self.blue
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationController!.navigationBar.tintColor = UIColor.white
    }
    
    func rightMenu(){
        let button = UIButton()
        button.setImage(UIImage(named: "ico_more"), for: UIControlState())
        button.addTarget(self, action: #selector(ListStaffEventController.moreTapped(_:)), for: UIControlEvents.touchDown)
        button.frame=CGRect(x: 0, y: 0, width: 24, height: 24)
        let rightMenuButtonItem:UIBarButtonItem = UIBarButtonItem(customView: button)
        
        self.navigationItem.setRightBarButtonItems([rightMenuButtonItem], animated: true)
    }
    
    func moreTapped(_ sender: UIButton){
        let alert:UIAlertController=UIAlertController(title: "Menu", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.modalPresentationStyle = .popover
        
        let helpAction = UIAlertAction(title: "Help", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.popUpHelp()
        }
        let logoutAction = UIAlertAction(title: "Logout", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.logoutTapped()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        alert.addAction(helpAction)
        alert.addAction(logoutAction)
        alert.addAction(cancelAction)
        
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds;
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func popUpHelp(){
        let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showPopupHelp") as! HelpStaffController
        self.addChildViewController(Popover)
        Popover.view.frame = self.view.frame
        self.view.addSubview(Popover.view)
        Popover.didMove(toParentViewController: self)
    }
    
    func logoutTapped(){
        let alert = UIAlertController(title: "Logout", message: "Are you sure want to Logout?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            Config().BackToLogin(self)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in }))
        self.present(alert, animated: true, completion: nil)
    }
    
}
