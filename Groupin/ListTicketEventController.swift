//
//  ListTicketEventController.swift
//  Groupin
//
//  Created by Macbook pro on 11/23/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import RealmSwift

class ListTicketEventController: UIViewController {
    
    @IBOutlet weak var tbTicket: UITableView!
    
    var token = ""
    var idEvent = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadLocal()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        let sess = try! Realm().objects(session.self).first!
        token = sess.token
        
        self.makeSerialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var dataData = [listTicket]()
    func loadLocal(){
        self.dataData.removeAll()
        let data = try! Realm().objects(listTicket.self).filter("id_event = %@", Int(self.idEvent)!).sorted(byKeyPath: "name", ascending: true)
        for item in data {
            self.dataData.append(item)
        }
        
        if self.paramsJson == 1 {
            self.tbTicket.reloadData()
        }
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_event" : self.idEvent as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            self.getData(base64Encoded)
        }
    }
    
    var paramsJson = 0
    func getData(_ message: String){
        let urlString = Config().urlEvent + "list_ticket_event"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)["msg"]
                print(jsonData)
                if jsonData.count > 0 {
                    self.paramsJson = 1
                    
                    var i = 0
                    for (_, _) in jsonData {
                        let id = jsonData[i]["id_ticket"].intValue
                        let modelUpdate = try! Realm().objects(listTicket.self).filter("id_ticket = %@", id)
                        if modelUpdate.count == 0 {
                            let model = listTicket()
                            model.id_ticket = id
                            model.id_event = Int(self.idEvent)!
                            model.name = jsonData[i]["ticket_name"].string!
                            model.total = jsonData[i]["total"].string!
                            DBHelper.insert(model)
                        }else{
                            let update = modelUpdate.first!
                            try! Realm().write({
                                update.name = jsonData[i]["ticket_name"].string!
                                update.total = jsonData[i]["total"].string!
                            })
                        }
                        i += 1
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.loadLocal()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTicketCell", for: indexPath) as! ListGeneralCell
        
        let data = self.dataData[indexPath.row]
        let detail = data.name + " (" + data.total + ")"
        cell.lblTitle.text = detail
        
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        
        return cell
    }
    
    var idTicketSelected = 0
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        idTicketSelected = self.dataData[indexPath.row].id_ticket
        self.performSegue(withIdentifier: "showListAttend", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showListAttend" {
            let conn = segue.destination as! ListAttendEventController
            conn.idTicket = self.idTicketSelected
            conn.navigationItem.title = "Purchased Ticket"
        }
    }
}
