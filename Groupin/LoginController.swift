//
//  LoginController.swift
//  Groupin
//
//  Created by Macbook pro on 6/29/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import Firebase
import SwiftyJSON
import Alamofire
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class LoginController: UIViewController, UIPopoverPresentationControllerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var viewLogo: UIView!
    @IBOutlet weak var viewLogoHeight: NSLayoutConstraint!
    @IBOutlet weak var viewLogoWidth: NSLayoutConstraint!
    @IBOutlet weak var codePhone: UIButton!
    @IBOutlet weak var txtPhone: UITextField!{
        didSet{
            txtPhone.keyboardType = UIKeyboardType.numberPad
        }
    }
    var keyboardType: UIKeyboardType {
        get{
            return txtPhone.keyboardType
        }
        set{
            if newValue != UIKeyboardType.numberPad{
                self.keyboardType = UIKeyboardType.numberPad
            }
        }
    }
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnRemember: UIButton!
    @IBOutlet weak var imgRemember: UIImageView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnForgotPass: UIButton!
    var codeCall = "+62"
    var phone = ""
    var pass = ""
    var alertProses: UIAlertController = UIAlertController()
    var rememberStatus = "true"
    var height: CGFloat!
    var width: CGFloat!
    let uid = UIDevice.current.identifierForVendor!.uuidString
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        
        self.txtPhone.delegate = self
        self.txtPassword.delegate = self

        print(uid)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let imgButtonOn = UIImage(named: "ico_keep_on")
        imgRemember.image = imgButtonOn
        
        //Rounded Button
        btnLogin.backgroundColor = UIColor(red: 83, green: 186, blue: 32)
        btnLogin.layer.cornerRadius = 5
        btnLogin.layer.borderWidth = 1
        btnLogin.layer.borderColor = UIColor.lightGray.cgColor
        btnRegister.backgroundColor = UIColor(red: 232, green: 76, blue: 61)
        btnRegister.layer.cornerRadius = 5
        btnRegister.layer.borderWidth = 1
        btnRegister.layer.borderColor = UIColor.lightGray.cgColor
        
        let tapRemember = UITapGestureRecognizer(target: self, action: #selector(LoginController.imgRememberTapped(_:)))
        tapRemember.numberOfTapsRequired = 1
        imgRemember.isUserInteractionEnabled = true
        imgRemember.addGestureRecognizer(tapRemember)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.setAnimationsEnabled(true)
        
        //dari pilih code
        codePhone.setTitle(self.codeCall, for: UIControlState())
        txtPhone.text = self.phone
        txtPassword.text = self.pass
        
        self.height = viewLogoWidth.constant//viewLogoHeight.constant
        self.width = viewLogoWidth.constant
        
        // Logo Gif
        viewLogo.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        viewLogo.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.viewLogo.alpha = 1.0
            self.viewLogo.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
        let logoGif = UIImage.gifWithName("logo_login")
        let imgGif = UIImageView(image: logoGif)
        imgGif.frame = CGRect(x: imgGif.frame.origin.x, y: imgGif.frame.origin.y, width: self.width, height: self.height)
        viewLogo.addSubview(imgGif)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginController.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
    }
    
    func networkStatusChanged(_ notification: Notification) {
        _ = notification.userInfo
//        print(userInfo)
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showKodeNumber" {
            let popoverViewController = segue.destination
            popoverViewController.popoverPresentationController?.delegate = self
            let conn = segue.destination as! CallCodes
            conn.phone = txtPhone.text!
            conn.pass = txtPassword.text!
            conn.status = 1
        }
    }
    
    @IBAction func kodeNumberTapped(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showKodeNumber", sender: self)
    }
    
    @IBAction func btnRememberTapped(_ sender: AnyObject) {
        self.rememberAction()
    }
    
    func imgRememberTapped(_ sender: UITapGestureRecognizer){
        self.rememberAction()
    }
    
    func rememberAction(){
        let imgButtonOff = UIImage(named: "ico_keep_off")
        let imgButtonOn = UIImage(named: "ico_keep_on")
        if self.rememberStatus == "false" {
            imgRemember.image = imgButtonOn
            self.rememberStatus = "true"
        }else{
            imgRemember.image = imgButtonOff
            self.rememberStatus = "false"
        }
    }
    
    var alertLoading: UIAlertController!
    @IBAction func loginTapped(_ sender: AnyObject) {
        if self.txtPhone.text == "" || self.txtPassword.text == "" {
            let alert = UIAlertController(title: "Info", message: "Please complete data", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                print("UN and Pass nill")
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
//        let status = String(describing: Reach().connectionStatus())
        let status = Reach().connectionStatus()
        if status == "Offline" || status == "Unknown" {
            //Ga ada Koneksi
            let alert = UIAlertController(title: "Info", message: "Please connect to internet", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                print("Not connected")
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            //LOGIN
            self.alertLoading = UIAlertController(title: "Logging In", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
            self.present(self.alertLoading, animated: true, completion: nil)
            makeSerialize()
        }
    }
    
    func makeSerialize(){
        let code = codePhone.titleLabel?.text
        let phoneFilter = code! + txtPhone.text!
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "user" : phoneFilter as AnyObject,
            "password" : txtPassword.text! as AnyObject,
            "uniqid" : self.uid as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            prosesLogin(base64Encoded)
        }
    }
    
    var idUser = 0
    var msgAlert = ""
    func prosesLogin(_ message: String){
        let urlString = Config().urlUser + "login"
        let token = Config().md5(string: message)
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                if jsonData["code"] == "1" {
                    if jsonData["msg"]["codemember"] == "1" {
                        self.idUser = jsonData["msg"]["id"].intValue
                        
                        self.msgAlert = ""
                        // Session
                        self.modelSess.id = jsonData["msg"]["id"].intValue
                        self.modelSess.token = jsonData["msg"]["token"].string!
                        self.token = jsonData["msg"]["token"].string!
                        
                        // tb_user
                        self.modelUser.id = jsonData["msg"]["id"].intValue
                        self.modelUser.phone = jsonData["msg"]["user"].string!
                        self.modelUser.name = jsonData["msg"]["name"].string!
                        self.modelUser.avatar = jsonData["msg"]["avatar"].string!
                        self.modelUser.status = jsonData["msg"]["msg_status"].string!
                        self.modelUser.gender = jsonData["msg"]["gender"].string!
                        self.modelUser.tgl_lahir = jsonData["msg"]["tgl_lahir"].string!
                        self.modelUser.id_country = jsonData["msg"]["id_country"].string!
                        self.modelUser.country = jsonData["msg"]["name_country"].string!
                        self.modelUser.id_city = jsonData["msg"]["id_city"].string!
                        self.modelUser.city = jsonData["msg"]["name_city"].string!
                        self.modelUser.tgl_join = jsonData["msg"]["tgl_join"].string!
                        self.modelUser.hanya_join_main_group = jsonData["msg"]["hanya_join_main_group"].string!
                        self.modelUser.valid_email = jsonData["msg"]["is_valid_email"].string!
                        
                        //Interest
                        let objGroup = try! Realm().objects(interestUser.self)
                        try! Realm().write(){
                            try! Realm().delete(objGroup)
                        }
                        let interest = jsonData["msg"]["interest"]
                        for (key, _) in interest {
                            let modelInterest = interestUser()
                            let dataInt = jsonData["msg"]["interest"][key]
                            modelInterest.id_table = Int(key)!
                            modelInterest.id_category = dataInt["kode_interest"].intValue
                            modelInterest.category = dataInt["group_interest"].string!
                            modelInterest.detail = dataInt["detail_interest"].string!
                            DBHelper.insert(modelInterest)
                        }
                        
                        self.makeSerializeData()
                    }else if jsonData["msg"]["codemember"] == "2" {
                        //Add Session
                        let model = session()
                        model.id = jsonData["msg"]["id_user"].intValue
                        model.token = jsonData["msg"]["token"].string!
                        model.last_login = 1 // sebagai staff
                        DBHelper.insert(model)
                        
                        let modelUser = tb_user()
                        modelUser.phone = jsonData["msg"]["phone"].string!
                        DBHelper.insert(modelUser)
                            
                        //Masuk sebagai staff event
                        print("masuk sebagai staff event")
                        self.msgAlert = "You are login as Event's Staff"
                        self.alertLoading.dismiss(animated: true, completion: self.loginAsStaff)
                    }
                }else{
                    if jsonData["code"] == "6" {
                        self.continueLoginParams = 1
                        self.continueLoginMsg = message
                    }
                    self.msgAlert = jsonData["msg"].string!
                    self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
                }
            }else{
                print("Something Went Wrong..")
                self.msgAlert = "Cant Request to Server, try again later"
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
            }
        
            }.responseData { Response in
                
            }
    }
    
    var continueLoginParams = 0
    var continueLoginMsg = ""
    func continueLogin(_ message: String){
        self.alertLoading = UIAlertController(title: "Logging In", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        self.present(self.alertLoading, animated: true, completion: nil)
        
        let urlString = Config().urlUser + "continue_login"
        let token = Config().md5(string: message)
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                if jsonData["code"] == "1" {
                    self.idUser = jsonData["msg"]["id"].intValue
                    
                    self.msgAlert = ""
                    // Session
                    self.modelSess.id = jsonData["msg"]["id"].intValue
                    self.modelSess.token = jsonData["msg"]["token"].string!
                    self.token = jsonData["msg"]["token"].string!
                    
                    // tb_user
                    self.modelUser.id = jsonData["msg"]["id"].intValue
                    self.modelUser.phone = jsonData["msg"]["user"].string!
                    self.modelUser.name = jsonData["msg"]["name"].string!
                    self.modelUser.avatar = jsonData["msg"]["avatar"].string!
                    self.modelUser.status = jsonData["msg"]["msg_status"].string!
                    self.modelUser.gender = jsonData["msg"]["gender"].string!
                    self.modelUser.tgl_lahir = jsonData["msg"]["tgl_lahir"].string!
                    self.modelUser.id_country = jsonData["msg"]["id_country"].string!
                    self.modelUser.country = jsonData["msg"]["name_country"].string!
                    self.modelUser.id_city = jsonData["msg"]["id_city"].string!
                    self.modelUser.city = jsonData["msg"]["name_city"].string!
                    self.modelUser.tgl_join = jsonData["msg"]["tgl_join"].string!
                    self.modelUser.hanya_join_main_group = jsonData["msg"]["hanya_join_main_group"].string!
                    self.modelUser.valid_email = jsonData["msg"]["is_valid_email"].string!
                    
                    //Interest
                    let objGroup = try! Realm().objects(interestUser.self)
                    try! Realm().write(){
                        try! Realm().delete(objGroup)
                    }
                    let interest = jsonData["msg"]["interest"]
                    for (key, _) in interest {
                        let modelInterest = interestUser()
                        let dataInt = jsonData["msg"]["interest"][key]
                        modelInterest.id_table = Int(key)!
                        modelInterest.id_category = dataInt["kode_interest"].intValue
                        modelInterest.category = dataInt["group_interest"].string!
                        modelInterest.detail = dataInt["detail_interest"].string!
                        DBHelper.insert(modelInterest)
                    }
                    
                    self.makeSerializeData()
                }else{
                    self.msgAlert = jsonData["msg"].string!
                    self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
                }
            }else{
                print("Something Went Wrong..")
                self.msgAlert = "Cant Request to Server, try again later"
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
            }
            
            }.responseData { Response in
                
        }
    }
    
    func makeSerializeData(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idUser as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Data(jsonString!)
    }
    
    func makeBase64Data(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getDataGroup(base64Encoded)
        }
    }
    
    var jsonGroup: JSON!
    func getDataGroup(_ message: String){
        let urlString = Config().urlGroup + "list_own_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                self.jsonGroup = jsonData["msg"]
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.getDataRoom(message)
        }
    }
    
    var jsonRoom: JSON!
    func getDataRoom(_ message: String){
        let urlString = Config().urlRoom + "list_own_room"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                self.jsonRoom = jsonData["msg"]
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
        }
    }
    
    func alertStatus(){
        if self.msgAlert != "" {
            let alert = UIAlertController(title: "Info", message: self.msgAlert, preferredStyle: UIAlertControllerStyle.alert)
            
            if continueLoginParams == 1 {
                alert.addAction(UIAlertAction(title: "Continue", style: .default, handler: { action in
                    if self.continueLoginParams == 1 {
                        self.continueLoginParams = 0
                        self.msgAlert = ""
                        self.continueLogin(self.continueLoginMsg)
                    }
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                    self.continueLoginParams = 0
                    self.msgAlert = ""
                }))
            }else{
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            }
            self.present(alert, animated: true, completion: nil)
        }else{
            createSession()
        }
    }
    
    let modelSess = session()
    let modelUser = tb_user()
    func createSession() {
        //Delete Data Session
        var data = [session]()
        data = DBHelper.getAllSession()
        if data.count == 0 {
            print("Doesn't Exist")
        }else{
            data = DBHelper.getAllSession()
            try! Realm().write {
                try! Realm().delete(data[0])
            }
        }
        // Delete Data User
        var dataUser = [tb_user]()
        dataUser = DBHelper.getAllUser()
        if dataUser.count == 0 {
            print("Doesn't Exist")
        }else{
            dataUser = DBHelper.getAllUser()
            try! Realm().write {
                try! Realm().delete(dataUser[0])
            }
        }
        
        //Create Session!
//        let lastLogin: NSNumber = NSNumber(Date().timeIntervalSince1970)
//        modelSess.last_login = Int(lastLogin)
        if self.rememberStatus == "true" {
            modelSess.remember = 1
        }
        DBHelper.insert(modelSess)
        
        // tb_user
        modelUser.password = txtPassword.text!
        DBHelper.insert(modelUser)
        
        //Group
        if jsonGroup != nil {
            let objGroup = try! Realm().objects(tb_group.self)
            try! Realm().write(){
                try! Realm().delete(objGroup)
            }
            for (key, _) in jsonGroup {
                let modelGroup = tb_group()
                modelGroup.id = Int(key)!
                modelGroup.name = jsonGroup[key]["group"].string!
                if let ava = jsonGroup[key]["avatar"].string {
                    modelGroup.avatar = ava
                }else{
                    modelGroup.avatar = ""
                }
                modelGroup.city = jsonGroup[key]["city"].string!
                modelGroup.country = jsonGroup[key]["country"].string!
                modelGroup.jlm_member = jsonGroup[key]["member"].string!
                modelGroup.category = jsonGroup[key]["category"].string!
                modelGroup.desc = jsonGroup[key]["desc"].string!
                modelGroup.date_create = jsonGroup[key]["date_create"].string!
                modelGroup.id_user_create = jsonGroup[key]["id_user_create"].string!
                modelGroup.user_create = jsonGroup[key]["name_user_create"].string!
                modelGroup.is_public = jsonGroup[key]["is_public"].string!
                modelGroup.is_promot = jsonGroup[key]["is_promote"].string!
                modelGroup.user_level = jsonGroup[key]["user_level"].string!
                modelGroup.is_notif = 1
                modelGroup.is_sound = 1
                DBHelper.insert(modelGroup)
            }
        }
        
        //Room
        if jsonRoom != nil {
            let objRoom = try! Realm().objects(tb_own_room.self)
            try! Realm().write(){
                try! Realm().delete(objRoom)
            }
            var i = 0
            for (keyGroup, _) in jsonRoom {
                i = 0
                for _ in jsonRoom[keyGroup] {
                    let modelRoom = tb_own_room()
                    modelRoom.id_group = Int(keyGroup)!
                    modelRoom.id_room = jsonRoom[keyGroup][i]["id"].intValue
                    modelRoom.name = jsonRoom[keyGroup][i]["room"].string!
                    modelRoom.user_level = jsonRoom[keyGroup][i]["user_level"].string!
                    DBHelper.insert(modelRoom)
                    i += 1
                }
            }
        }
        
        FIRAuth.auth()?.signInAnonymously( completion: { (user, error) in
            print(user!.isAnonymous)
            print(user!.uid)
        })
//        let email = "chat@groupin.com"
//        let password = "admin123"
//        FIRAuth.auth()?.signInWithEmail(email, password: password, completion: { (user, error) in
//            print(user?.uid)
//        })
        
        let tokenFirB = FIRInstanceID.instanceID().token()!
        self.tokenFir = tokenFirB
        
        //Intro
        let dataIntro = try! Realm().objects(intro.self)
        if dataIntro.count == 0 {
            let demo = intro()
            demo.intro = 1
            DBHelper.insert(demo)
        }
        
        makeSerializeToken()
    }
    
    var token = ""
    var tokenFir = ""
    func makeSerializeToken(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "token" : self.tokenFir as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Token(jsonString!)
    }
    
    func makeBase64Token(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postDataToken(base64Encoded)
        }
    }
    
    func postDataToken(_ message: String){
        let urlString = Config().urlUser + "save_token_notif"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "showHome") as! HomeController
                self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnForgotPassTapped(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showForgotPass", sender: self)
    }
    
    func loginAsStaff(){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "showMenuStaff") as! HomeStaffController
        self.present(vc, animated: true, completion: nil)
    }
    
    var activeField: UITextField?
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            let possKeyboard = self.view.frame.height - keyboardSize!.height - activeField!.frame.height
            if activeField?.frame.origin.y > possKeyboard {
//                if (!CGRectContainsPoint(aRect, activeField!.frame.origin)){
                    if view.frame.origin.y == 0{
                        self.view.frame.origin.y -= keyboardSize!.height
                    }
//                }
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
            else {
                
            }
        }
    }
}
