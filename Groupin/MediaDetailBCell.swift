//
//  MediaDetailBCell.swift
//  Groupin
//
//  Created by Macbook pro on 1/30/17.
//  Copyright © 2017 Macbook pro. All rights reserved.
//

import UIKit

class MediaDetailBCell: UITableViewCell {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    
}
