//
//  MediaDetailBController.swift
//  Groupin
//
//  Created by Macbook pro on 1/30/17.
//  Copyright © 2017 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class MediaDetailBController: UIViewController {
    
    @IBOutlet weak var tbMedia: UITableView!
    
    var params = ""
    var idUser = 0
    var idGroup = 0
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.setupLoadData()
    }
    
    var lastUpdate = ""
    var dataData = [tb_media]()
    func setupLoadData() {
        //Session
        let sess = try! Realm().objects(session.self).first!
        idUser = sess.id
        idGroup = sess.id_group
        token = sess.token
        
        //Data
        dataData.removeAll()
        let idGroupFilter = String(self.idGroup)
        let dataFilter = try! Realm().objects(tb_media.self).filter("id_group = %@ AND type = %@", idGroupFilter, self.params)
        for item in dataFilter {
            dataData += [item]
        }
        
        let last = try! Realm().objects(tb_media.self).last!
        let data = last.date_update
        let update = data.substring(from: data.index(data.startIndex, offsetBy: 0)).substring(to: data.index(data.endIndex, offsetBy: -6))
        lastUpdate = update
        
        makeSerialize()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_group" : self.idGroup as AnyObject,
            "id_user" : self.idUser as AnyObject,
            "date_update" : self.lastUpdate as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postLike(base64Encoded)
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    func postLike(_ message: String){
        let urlString = Config().urlMedia + "statistic_update_media"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let dataJason = JSON(value)["msg"]
                if dataJason.count > 0 {
                    self.jsonData = dataJason
                    print(dataJason)
                    self.paramsJson = 1
                    self.tbMedia.reloadData()
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataData.count
    }
    
    var idComment = [String]()
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mediaCell", for: indexPath) as! MediaDetailBCell
        
        let data = dataData[indexPath.row]
        
        let realName = data.name
        let nameMediaView = realName.substring(with: (realName.characters.index(realName.startIndex, offsetBy: 14) ..< realName.characters.index(realName.endIndex, offsetBy: 0)))
        
        cell.lblTitle.text = nameMediaView
        
        if params == "Audio" {
            cell.imgAva.image = UIImage(named: "ico_media_music")
        }else{
            cell.imgAva.image = UIImage(named: "ico_media_file")
        }
        
        if self.paramsJson == 1 {
            var idKey = [String]()
            for (key, _) in self.jsonData {
                idKey.append(key)
            }
            let dataJason = self.jsonData[idKey[indexPath.row]]
            cell.lblLike.text = dataJason["likes_statistic"].string!
            cell.lblComment.text = dataJason["comment_statistic"].string!
        }
        
        //Click like
        cell.lblLike.tag = indexPath.row
        let tapPhoto = UITapGestureRecognizer(target: self, action: #selector(MediaDetailController.likeTapped(_:)))
        tapPhoto.numberOfTapsRequired = 1
        cell.lblLike.isUserInteractionEnabled = true
        cell.lblLike.addGestureRecognizer(tapPhoto)
        
        return cell
    }
    
    var idMedia = ""
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        idMedia = dataData[indexPath.row].id_media
        self.performSegue(withIdentifier: "showViewMedia", sender: self)
    }
}
