//
//  MediaDetailCell.swift
//  Groupin
//
//  Created by Macbook pro on 8/31/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class MediaDetailCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var viewMain: UIView!
    
}