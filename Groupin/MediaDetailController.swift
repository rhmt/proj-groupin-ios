//
//  MediaDetailController.swift
//  Groupin
//
//  Created by Macbook pro on 8/31/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class MediaDetailController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var params = ""
    var idUser = 0
    var idGroup = 0
    var token = ""
    
    @IBOutlet weak var colMedia: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.setupLoadData()
    }
    
    var lastUpdate = ""
    var dataData = [tb_media]()
    func setupLoadData() {
        //Session
        let sess = try! Realm().objects(session.self).first!
        idUser = sess.id
        idGroup = sess.id_group
        token = sess.token
        
        //Data
        dataData.removeAll()
        let idGroupFilter = String(self.idGroup)
        let dataFilter = try! Realm().objects(tb_media.self).filter("id_group = %@ AND type = %@", idGroupFilter, self.params).sorted(byKeyPath: "date_create", ascending: false)
        for item in dataFilter {
            dataData += [item]
        }
        
        let last = try! Realm().objects(tb_media.self).last!
        let data = last.date_update
        let update = data.substring(from: data.index(data.startIndex, offsetBy: 0)).substring(to: data.index(data.endIndex, offsetBy: -6))
        lastUpdate = update
        
        makeSerialize()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_group" : self.idGroup as AnyObject,
            "id_user" : self.idUser as AnyObject,
            "date_update" : self.lastUpdate as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postLike(base64Encoded)
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    func postLike(_ message: String){
        let urlString = Config().urlMedia + "statistic_update_media"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let dataJason = JSON(value)["msg"]
                print(dataJason)
                if dataJason.count > 0 {
                    self.jsonData = dataJason
                    print(dataJason)
                    self.paramsJson = 1
                    
                    for (key, _) in self.jsonData {
                        if let modelUpdate = try! Realm().objects(tb_media.self).filter("id_media = %@", key).first {
                            let data = self.jsonData[key]
                            try! Realm().write {
                                modelUpdate.like_statistic = data["likes_statistic"].string!
                                modelUpdate.comments_statistic = data["comment_statistic"].string!
                            }
                        }
                    }
                    
                    
                    self.colMedia.reloadData()
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return dataData.count
    }
    
    var idKey = [String]()
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mediaDetailCell", for: indexPath) as! MediaDetailCell
        
        let data = dataData[indexPath.row]
        
//        let realName = data.name
//        let nameMediaView = realName.substring(with: (realName.characters.index(realName.startIndex, offsetBy: 14) ..< realName.characters.index(realName.endIndex, offsetBy: 0)))
//        
//        cell.lblTitle.text = nameMediaView
        
        let pathMedia = Config().getMediaPath()
        let nameMedia = (pathMedia as String) + "/" + data.name

        var ext = ""
        var type = ""
        if params == "Image" {
            ext = ".jpg"
            type = "JPG"
            cell.imgMain.image = UIImage(contentsOfFile: nameMedia + ext)
        }else if params == "Video" {
            ext = ".mov"
            type = "MP4"
            let urlVideo = URL(fileURLWithPath: nameMedia)
            let imageVideo = Config().thumbnailImageForFileUrl(urlVideo)
            cell.imgMain.image = imageVideo
        }
        cell.lblType.text = type
        
        let dataUser = try! Realm().objects(listMember.self).filter("id_member == \(Int(data.id_from_user)!)")
        var nameUser = ""
        if dataUser.count > 0 {
            nameUser = dataUser.first!.name
        }else{
            nameUser = "Admin"
        }
        var dateShow = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "YYYY-MM-d HH:mm:ss Z"
        let dateFormatterPrint = DateFormatter()
        let dataDateFrom = data.date_create
        if let date = dateFormatterGet.date(from: dataDateFrom) {
            dateFormatterPrint.dateFormat = "d MMM YYYY"
            let dateView = dateFormatterPrint.string(from: date)
            dateFormatterPrint.dateFormat = "HH:mm"
            let dateTime = dateFormatterPrint.string(from: date)
            dateShow = dateView + " at " + dateTime
        }else{
            dateShow = data.date_create + " ago"
        }
        let updateText = dateShow + " by " + nameUser
        cell.lblDetail.text = updateText
        
        var like = "0"
        if data.like_statistic != "" {
            like = data.like_statistic
        }
        cell.lblLike.text = like
        var comment = "0"
        if data.comments_statistic != "" {
            comment = data.comments_statistic
        }
        cell.lblComment.text = comment
        
        //Click like
        cell.lblLike.tag = indexPath.row
        let tapPhoto = UITapGestureRecognizer(target: self, action: #selector(MediaDetailController.likeTapped(_:)))
        tapPhoto.numberOfTapsRequired = 1
        cell.lblLike.isUserInteractionEnabled = true
        cell.lblLike.addGestureRecognizer(tapPhoto)
        
        //Rounded Cell
        cell.viewMain.layer.cornerRadius = 5
        cell.viewMain.layer.shadowColor = UIColor.lightGray.cgColor
        cell.viewMain.layer.shadowOffset = CGSize.zero
        cell.viewMain.layer.shadowOpacity = 0.5
        cell.viewMain.layer.shadowRadius = 3
        
        return cell
        
    }
    
    var idMedia = ""
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        idMedia = dataData[indexPath.row].id_media
        self.performSegue(withIdentifier: "showViewMedia", sender: self)
    }
    
    var widthCell: CGFloat! = 0
    var heightCell: CGFloat = 0
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        widthCell = (self.colMedia.frame.size.width - 8 * 2) / 2 //some width
        heightCell = widthCell * 1.25 //ratio
        
        return CGSize(width: widthCell, height: heightCell)
    }
    
    var totalLike = ""
    func likeTapped(_ sender: UIGestureRecognizer){
        let indexPath = sender.view?.tag
        idMedia = dataData[indexPath!].id_media
        //let dataJason = self.jsonData[idKey[indexPath!]]
        totalLike = dataData[indexPath!].like_statistic//dataJason["likes_statistic"].string!
        
        self.performSegue(withIdentifier: "showListLikeMedia", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showViewMedia" {
            let conn = segue.destination as! ViewMediaController
            conn.idMedia = self.idMedia
        }
        if segue.identifier == "showListLikeMedia" {
            let conn = segue.destination as! LikesMediaController
            conn.idMedia = self.idMedia
            conn.totalLikes = self.totalLike
        }
    }
}
