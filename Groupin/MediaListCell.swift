//
//  MediaListCell.swift
//  Groupin
//
//  Created by Macbook pro on 8/31/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class MediaListCell: UITableViewCell {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}