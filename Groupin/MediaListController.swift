//
//  MediaListController.swift
//  Groupin
//
//  Created by Macbook pro on 8/31/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift

class MediaListController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var colData: UICollectionView!
    let TableArray = ["Audio","Video","Document","Image"]
    let imgArray = ["ico_list_media_audio","ico_list_media_video","ico_list_media_doc","ico_list_media_pic"]
    var params = ""
    
    var idGroup = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sess = try! Realm().objects(session.self).first!
        idGroup = String(sess.id_group)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        
        self.colData.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMedia" {
            let conn = segue.destination as! MediaDetailController
            conn.params = self.params
            conn.navigationItem.title = self.params
        }
        if segue.identifier == "showMediaList" {
            let conn = segue.destination as! MediaDetailBController
            conn.params = self.params
            conn.navigationItem.title = self.params
        }
    }
    
    //CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return self.TableArray.count
    }
    
    var id = [String]()
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listMediaCell", for: indexPath) as! HomeGroupCollectionViewCell
        
        let data = try! Realm().objects(tb_media.self).filter("id_group = %@ AND type = %@", self.idGroup, TableArray[indexPath.row])
        let total = data.count
        
        if total > 0 {
            let lastUpdate = data.last!
            
            let dataUser = try! Realm().objects(listMember.self).filter("id_member == \(lastUpdate.id_from_user)")
            var nameUser = ""
            if dataUser.count > 0 {
                nameUser = dataUser.first!.name
            }else{
                nameUser = "Admin"
            }
            
            var dateShow = ""
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "YYYY-MM-d HH:mm:ss Z"
            let dateFormatterPrint = DateFormatter()
            let dataDateFrom = lastUpdate.date_create
            if let date = dateFormatterGet.date(from: dataDateFrom) {
                dateFormatterPrint.dateFormat = "MMM dd, YYYY"
                let dateView = dateFormatterPrint.string(from: date)
                dateFormatterPrint.dateFormat = "hh:mm a"
                let dateTime = dateFormatterPrint.string(from: date)
                dateShow = dateView + " " + dateTime
            }else{
                dateShow = lastUpdate.date_create + " ago"
            }
            
            let updateText = dateShow + " by " + nameUser
            cell.lblUpdateDetail.text = updateText
        }else{
            cell.lblUpdateDetail.text = "-"
        }
        
        cell.imgAvatr.image = UIImage(named: imgArray[indexPath.row])
        cell.lblNama.text = TableArray[indexPath.row]
        cell.lblDetail.text = String(total) + " Files"
        
        //Rounded Cell
        cell.viewMain.layer.cornerRadius = 5
        cell.viewMain.layer.shadowColor = UIColor.lightGray.cgColor
        cell.viewMain.layer.shadowOffset = CGSize.zero
        cell.viewMain.layer.shadowOpacity = 0.5
        cell.viewMain.layer.shadowRadius = 3
        //        cell.imgAvatr.layer.cornerRadius = 5
        cell.imgAvatr.clipsToBounds = true
//
        widthCell = cell.viewMain.frame.width
        heightCell = cell.viewMain.frame.height
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        self.params = TableArray[indexPath.row]
        let data = try! Realm().objects(tb_media.self).filter("id_group = %@ AND type = %@", self.idGroup, TableArray[indexPath.row])
        let total = data.count
        
        if total > 0 {
            if self.params == "Video" || self.params == "Image" {
                self.performSegue(withIdentifier: "showMedia", sender: self)
            }else if self.params == "Audio" || self.params == "Document" {
                self.performSegue(withIdentifier: "showMediaList", sender: self)
            }
        }else{
            //Ga ada Data
            let alert = UIAlertController(title: "Info", message: "No Media Available", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                print("No Data")
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    var widthCell: CGFloat! = 0
    var heightCell: CGFloat = 0
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        widthCell = (self.colData.frame.size.width - 8 * 2) / 2 //some width
        heightCell = widthCell * 1.25 //ratio
        
        return CGSize(width: widthCell, height: heightCell)
    }
}
