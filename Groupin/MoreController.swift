//
//  MenuHomeController.swift
//  Groupin
//
//  Created by Macbook pro on 7/27/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import AVFoundation

class MoreController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tbMore: UITableView!
    var TableArray = [String]()
    let blue = UIColor(red: 63, green: 157, blue: 247)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = false
        self.tbMore.tableFooterView = UIView()
        
        colorNav()
        rightMenu()
        
        self.tbMore.tableFooterView = UIView()
        TableArray = ["Profile","Set Status", "Most Popular Events", "Upcoming Events", "Settings"]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(true)
//        self.tabBarController?.tabBar.isHidden = true
//    }
    
    var eventFor = 0 //1: Popular Event, 2:Upcoming Event
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showProfile" {
            let conn = segue.destination as! EditProfileController
            conn.navigationItem.title = "Profile"
            conn.tabBarController?.tabBar.isHidden = true
        }
        if segue.identifier == "showStatus" {
            let conn = segue.destination as! StatusController
            conn.navigationItem.title = "Set Status"
            conn.tabBarController?.tabBar.isHidden = true
        }
        if segue.identifier == "showPersonalEvent" {
            let conn = segue.destination as! PopularEventController
            conn.eventFor = self.eventFor
            conn.navigationItem.title = "Most Popular Event"
            conn.tabBarController?.tabBar.isHidden = true
        }
//        if segue.identifier == "showEvent" {
//            let conn = segue.destination as! EventController
//            conn.tabBarController?.tabBar.isHidden = true
//        }
        if segue.identifier == "showSetting" {
            let conn = segue.destination as! SettingController
            conn.navigationItem.title = "Settings"
            conn.tabBarController?.tabBar.isHidden = true
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TableArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableArray[indexPath.row], for: indexPath) as UITableViewCell
        
        cell.textLabel?.text = TableArray[indexPath.row]
        cell.textLabel?.font = UIFont(name: "San Francisco", size: 14)
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        
        return cell
    }   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if TableArray[indexPath.row] == "Profile" {
            self.performSegue(withIdentifier: "showProfile", sender: self)
        }
        if TableArray[indexPath.row] == "Set Status" {
            self.performSegue(withIdentifier: "showStatus", sender: self)
        }
        if TableArray[indexPath.row] == "Most Popular Events" {
            self.eventFor = 1
            self.performSegue(withIdentifier: "showPersonalEvent", sender: self)
        }
        if TableArray[indexPath.row] == "Upcoming Events" {
            self.eventFor = 2
            self.performSegue(withIdentifier: "showPersonalEvent", sender: self)
        }
        if TableArray[indexPath.row] == "Settings" {
            self.performSegue(withIdentifier: "showSetting", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    // METHODE NAV
    func colorNav(){
        navigationController!.navigationBar.barTintColor = self.blue
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationController!.navigationBar.tintColor = UIColor.white
    }
    
    func rightMenu(){
        let searchButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(MoreController.searchTapped))
        //Space
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 10.0
        //Delete
        let imageContact = UIImage(named: "ico_tab_contact")
        let contactButton = UIButton()
        contactButton.setImage(imageContact, for: UIControlState())
        contactButton.addTarget(self, action: #selector(self.contactTapped), for: UIControlEvents.touchDown)
        contactButton.frame=CGRect(x: 0, y: 0, width: 30, height: 30)
        let barAttachContact = UIBarButtonItem(customView: contactButton)
        
        self.navigationItem.setRightBarButtonItems([searchButtonItem, fixedSpace, barAttachContact], animated: true)
    }
    
    func searchTapped(_ sender:UIButton){
        self.performSegue(withIdentifier: "showSearchGroup", sender: self)
    }
    
    func contactTapped(_ sender:UIButton){
        self.performSegue(withIdentifier: "showContact", sender: self)
    }
    
}
