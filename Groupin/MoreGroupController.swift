//
//  MoreGroupController.swift
//  Groupin
//
//  Created by Macbook pro on 8/9/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire
import Firebase

class MoreGroupController: UIViewController {
    
    @IBOutlet weak var tbData: UITableView!
    let TableArray = ["About","Report Group", "Leaderboard", "Leave Group"]
    
    var about = ""
    var report = ""
    
    var idUser = 0
    var token = ""
    var idGroup = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tbData.tableFooterView = UIView()
        
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.token = idSession!.token
        self.idGroup = (idSession?.id_group)!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if self.about == "true" {
            let conn = segue.destination as! AboutGroupController
            conn.navigationItem.title = ""
            self.about = ""
        }else{
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TableArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableArray[indexPath.row], for: indexPath) as UITableViewCell
        
        cell.textLabel?.text = TableArray[indexPath.row]
        if TableArray[indexPath.row] != "Leave Group" {
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        if TableArray[indexPath.row] == "About" {
            self.about = "true"
            self.performSegue(withIdentifier: "showAboutGroup", sender: self)
        }else if TableArray[indexPath.row] == "Report Group" {
            self.report = "true"
            self.performSegue(withIdentifier: "showReportGroup", sender: self)
        }else if TableArray[indexPath.row] == "Leaderboard" {
            self.performSegue(withIdentifier: "showLeaderboard", sender: self)
        }else if TableArray[indexPath.row] == "Leave Group" {
            let alert = UIAlertController(title: "Confirmation", message: "Are you sure want to Leave this group?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in }))
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                self.leave()
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }

    var alertLoading: UIAlertController!
    func leave(){
        self.alertLoading = UIAlertController(title: "Processing", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        self.present(self.alertLoading, animated: true, completion: nil)
        
        // Delete Group from tbGroup
        let objG = try! Realm().objects(tb_group).filter("id = %@", self.idGroup).first!
        try! Realm().write {
            try! Realm().delete(objG)
        }
        
        let subscribeGroup = "/topics/G" + String(self.idGroup)
        FIRMessaging.messaging().unsubscribe(fromTopic: subscribeGroup)
        
        makeSerialize()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_group" : self.idGroup as AnyObject,
            "id_user" : self.idUser as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            self.getMember(base64Encoded)
        }
    }
    
    var msgError = ""
    var paramsError = 0
    func getMember(_ message: String){
        let urlString = Config().urlGroup + "leave_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                if jsonData["code"].string == "1" {
                    self.paramsError = 1
                    self.msgError = "Success"
                }else{
                    self.paramsError = 2
                    self.msgError = jsonData["msg"].string!
                }
            }else{
                print("Something Went Wrong..")
                self.msgError = "Failed connect to server"
            }
            }.responseData { Response in
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
        }
    }
    
    func alertStatus(){
        let alert = UIAlertController(title: "Info", message: msgError, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            if self.paramsError == 1 {
                self.backToHome()
            }
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func backToHome(){
        // Back to Home
        let home = storyboard?.instantiateViewController(withIdentifier: "showHome") as! HomeController
        present(home, animated: true, completion: nil)
    }
}
