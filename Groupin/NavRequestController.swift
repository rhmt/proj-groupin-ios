//
//  NavRequestController.swift
//  Groupin
//
//  Created by Macbook pro on 10/14/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import Foundation
import UIKit

class NavRequestController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.tabBarItem.image = UIImage(named: "ico_tab_request")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
//        self.tabBarItem.selectedImage = UIImage(named: "ico_tab_request_selected")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        self.tabBarController?.tabBar.isHidden = false
    }
}
