//
//  PaymentSummaryController.swift
//  Groupin
//
//  Created by Macbook pro on 12/30/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class PaymentSummaryController: UIViewController {
    
    @IBOutlet weak var lblEvent: UILabel!
    @IBOutlet weak var lblTicket: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnTP: UIButton!
    
    var idUser = 0
    var token = ""
    var idEvent = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sess = try! Realm().objects(session.self).first!
        self.idUser = sess.id
        self.token = sess.token
        print(self.idEvent)
        self.viewDesign()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    func viewDesign(){
        btnTP.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
        btnTP.layer.cornerRadius = 5
        btnTP.layer.borderWidth = 1
        btnTP.layer.borderColor = UIColor.lightGray.cgColor
        
        let data = try! Realm().objects(listEvent.self).filter("id_event = %@", self.idEvent).first!
        
        self.lblEvent.text = data.name
        self.lblTicket.text = data.ticket_name
        self.lblQty.text = data.amount
        self.lblPrice.text = data.priceType + " " + data.price
    }
    
    @IBAction func btnTPTapped(_ sender: AnyObject) {        
        let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showTransfer") as! PaymentTransferController
        self.addChildViewController(Popover)
        Popover.view.frame = self.view.frame
        Popover.idEvent = self.idEvent
        self.view.addSubview(Popover.view)
        Popover.didMove(toParentViewController: self)
    }
}
