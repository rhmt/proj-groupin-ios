//
//  PaymentTransferController.swift
//  Groupin
//
//  Created by Macbook pro on 1/12/17.
//  Copyright © 2017 Macbook pro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift

class PaymentTransferController: UIViewController {
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblPaymentCode: UILabel!
    @IBOutlet weak var lblTotalPayment: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    
    var idUser = 0
    var token = ""
    var idEvent = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.designView()
        self.showAnimate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.loadData()
    }
    
    var dataData = listEvent()
    func loadData(){
        let sess = try! Realm().objects(session.self).first!
        self.idUser = sess.id
        self.token = sess.token
        
        self.dataData = try! Realm().objects(listEvent.self).filter("id_event = %@", self.idEvent).first!
        
        self.lblPaymentCode.text = "Payment Code : " + self.dataData.id_payment
        let paymentPrice = Int(self.dataData.price)! + Int(self.dataData.id_payment)!
        let num = NumberFormatter()
        num.numberStyle = .decimal
        num.groupingSeparator = "."
        let price = num.string(from: NSNumber.init(value: paymentPrice))
        self.lblTotalPayment.text = self.dataData.priceType + " " + price!
    }
    
    func designView(){
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        viewMain.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        //Click to close
        let tapBack = UITapGestureRecognizer(target: self, action: #selector(PaymentTransferController.closePopup(_:)))
        tapBack.numberOfTapsRequired = 1
        viewMain.isUserInteractionEnabled = true
        viewMain.addGestureRecognizer(tapBack)
        
        //Rounded
        viewContainer.layer.cornerRadius = 5
        btnDone.backgroundColor = UIColor(red: 83, green: 186, blue: 32)
        btnDone.layer.cornerRadius = 5
        btnDone.layer.borderWidth = 1
        btnDone.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func closePopup(_ sender: UIGestureRecognizer){
        self.removeAnimate()
    }
    
    var alertLoading: UIAlertController!
    @IBAction func btnDoneTapped(_ sender: AnyObject) {
        self.alertLoading = UIAlertController(title: "", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        self.present(self.alertLoading, animated: true, completion: nil)
        
        self.makeSerialize()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "total_price" : self.dataData.price as AnyObject,
            "payment_type" : 1 as AnyObject, // 1:transfer manual 2:... belum didefinisikan
            "payment_status" : 0 as AnyObject, // 0:pending 1:valid
            "id_attendance" : self.dataData.id_attendance as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postData(base64Encoded)
        }
    }
    
    func postData(_ message: String){
        let urlString = Config().urlPayment + "add_payment"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                //Success
                let jsonData = JSON(value)
                print(jsonData)
                if jsonData["code"] == "1" {
                    self.msgAlert = "Success. Waiting for confirmation"
                }else {
                    self.msgAlert = jsonData["msg"].string!
                }
            }else{
                print("Something Went Wrong..")
                self.msgAlert = "Failed connect to server, plaese try again later"
            }
            
            }.responseData { Response in
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
        }
    }
    
    var msgAlert = ""
    func alertStatus(){
        if msgAlert != "" {
            let alert = UIAlertController(title: "Info", message: msgAlert, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.makeSerializeConfirm()
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func makeSerializeConfirm(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "id_event" : self.dataData.id_event as AnyObject,
            "id_payment" : self.dataData.id_payment as AnyObject,
            "id_attendance" : self.dataData.id_attendance as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Confirm(jsonString!)
    }
    
    func makeBase64Confirm(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postDataConfirm(base64Encoded)
        }
    }
    
    func postDataConfirm(_ message: String){
        let urlString = Config().urlPayment + "validate_payment"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                //Success
                let jsonData = JSON(value)
                print("validate_payment")
                print(jsonData)
            }else{
                print("Something Went Wrong..")
                self.msgAlert = "Failed connect to server, plaese try again later"
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
            }
            
            }.responseData { Response in
                self.removeAnimate()
        }
    }
    
    func showAnimate(){
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0.0
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            }, completion: {(finished: Bool) in
                if(finished){
                    self.view.removeFromSuperview()
                }
        })
    }
}
