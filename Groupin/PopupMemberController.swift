//
//  PopupMemberController.swift
//  Groupin
//
//  Created by Macbook pro on 9/13/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire
import ContactsUI

class PopupMemberController: UIViewController, CNContactViewControllerDelegate {
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewAdd: UIView!
    @IBOutlet weak var addAva: UIImageView!
    @IBOutlet weak var addName: UILabel!
    @IBOutlet weak var addPhone: UILabel!
    @IBOutlet weak var addStatus: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var profileAva: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profilePhone: UILabel!
    @IBOutlet weak var profileStatus: UILabel!
    @IBOutlet weak var btnSavePhoneNumber: UIButton!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var btnBlock: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    
    var idLogin = 0
    var idFriend = ""
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showAnimate()
        
        self.viewDesign()
        
        print("did load")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        print("did appear")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print("will appear")
        
        //Get id_group form session
        let idSession = try! Realm().objects(session.self).first
        self.idLogin = idSession!.id
        self.token = idSession!.token
        
        self.makeSerialize()
    }
    
    func viewDesign(){
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        viewMain.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        //Click to close
        let tapBack = UITapGestureRecognizer(target: self, action: #selector(PopupMemberController.closePopup(_:)))
        tapBack.numberOfTapsRequired = 1
        viewMain.isUserInteractionEnabled = true
        viewMain.addGestureRecognizer(tapBack)
        
        viewAdd.isHidden = true
        viewProfile.isHidden = true
    }
    
    func designImage(){
        //Rounded View
        viewAdd.layer.cornerRadius = 5
        viewAdd.layer.borderWidth = 1
        viewAdd.layer.borderColor = UIColor.lightGray.cgColor
        viewProfile.layer.cornerRadius = 5
        viewProfile.layer.borderWidth = 1
        viewProfile.layer.borderColor = UIColor.lightGray.cgColor
        
        //Rounded Ava
        addAva.clipsToBounds = true
        addAva.layer.cornerRadius = addAva.frame.width / 2
        addAva.layer.borderWidth = 1
        addAva.layer.borderColor = UIColor.lightGray.cgColor
        profileAva.clipsToBounds = true
        profileAva.layer.cornerRadius = profileAva.frame.width / 2
        profileAva.layer.borderWidth = 1
        profileAva.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    @IBAction func btnCloseTapped(_ sender: AnyObject) {
        self.removeAnimate()
    }
    
    let store = CNContactStore()
    @IBAction func btnSavePhoneNumberTapped(_ sender: Any) {
        let contactData = CNMutableContact()
        
        //Image
        let dataImg = self.jsonDataFriend["avatar"].string!
        let imgUrl = URL(string: dataImg)
        let urlData = try? Data(contentsOf: imgUrl!)
        let imgUi = UIImage(data: urlData!)
        let imgData = UIImageJPEGRepresentation(imgUi!, 1.0)
        contactData.imageData = imgData!
        
        //name
        contactData.givenName = self.jsonDataFriend["name"].string!
        
        //phone
        let homePhone = CNLabeledValue(label: CNLabelPhoneNumberMobile,value: CNPhoneNumber(stringValue: self.jsonDataFriend["phone"].string!))
        contactData.phoneNumbers = [homePhone]
        
        //Save
        let request = CNSaveRequest()
        request.add(contactData, toContainerWithIdentifier: nil)
        do{
            try store.execute(request)
            print("Successfully added the contact")
            self.msgError = "Save to contact success"
        } catch let err{
            print("Failed to save the contact. \(err)")
            self.msgError = "Failed to save the contact"
        }
        self.alertStatus()
    }
    
    var msgError = ""
    func alertStatus(){
        if msgError != "" {
            let alert = UIAlertController(title: "Info", message: msgError, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idFriend as AnyObject,
            "id_login" : self.idLogin as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            if dari == "" {
                getData(base64Encoded)
            }else{
                postDataBlock(base64Encoded)
            }
        }
    }
    
    var jsonDataFriend: JSON!
    func getData(_ message: String){
        let urlString = Config().urlUser + "detail_popup_user"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                let data = jsonData["msg"]
                self.jsonDataFriend = data
                print(jsonData)
                
                let isFriend = data["is_friend"].string
                if isFriend == "1" {
                    self.viewProfile.isHidden = false
                    if let imgUrl = data["avatar"].string {
                        self.profileAva.loadImageUsingCacheWithUrlString(imgUrl)
                        self.imgAvaFriend = imgUrl
                    }else{
                        self.profileAva.image = UIImage(named: "default-avatar")
                    }
                    self.profileName.text = data["name"].string
                    self.profilePhone.text = data["phone"].string
                    self.profileStatus.text = data["msg_status"].string
                    
                    if let isContact = try! Realm().objects(listFriend.self).filter("phone = %@", data["phone"].string!).first {
                        if isContact.is_contact == 1 {
                            self.btnSavePhoneNumber.isHidden = true
                        }else{
                            self.btnSavePhoneNumber.isHidden = false
                        }
                    }
                }else{
                    self.viewAdd.isHidden = false
                    if let imgUrl = data["avatar"].string {
                        self.addAva.loadImageUsingCacheWithUrlString(imgUrl)
                    }else{
                        self.addAva.image = UIImage(named: "default-avatar")
                    }
                    self.addName.text = data["name"].string
                    self.addPhone.text = data["phone"].string
                    self.addStatus.text = data["msg_status"].string
                }
                self.designImage()
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.paramsAdd = parameters
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showProfileFriend" {
            let conn = segue.destination as! FriendProfileController
            conn.idFriend = self.idFriend
        }
        if segue.identifier == "showChating" {
            let chatController = segue.destination as! ChatingController
            let id = String(self.idFriend)
            chatController.titleNav = self.profileName.text!
            chatController.toId = id!
            chatController.idFriend.append(id!)
            chatController.nameFriend.append(self.profileName.text!)
            chatController.avaFriend.append(imgAvaFriend)
            chatController.colorFriend.append(UIColor(red: 63, green: 157, blue: 247))
            chatController.paramsKindChat = "personal"
            chatController.tabBarController?.tabBar.isHidden = true
            self.tabBarController?.tabBar.isHidden = true
        }
    }
    
    //View Profile
    var imgAvaFriend = ""
    @IBAction func btnChat(_ sender: AnyObject) {
//        let chatController = ChatController(collectionViewLayout: UICollectionViewFlowLayout())
//        chatController.titleNav = self.profileName.text!
//        chatController.toId = self.idFriend
//        chatController.idFriend = [self.idFriend]
//        chatController.nameFriend = [self.profileName.text!]
//        chatController.avaFriend = [imgAvaFriend]
//        chatController.paramsKindChat = "personal"
//        navigationController?.pushViewController(chatController, animated: true)
        
        self.performSegue(withIdentifier: "showChating", sender: self)
    }
    
    var dari = ""
    @IBAction func btnBlockTapped(_ sender: AnyObject) {
        let alert = UIAlertController(title: "", message: "Are you sure want to block \(self.profileName.text!)?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.dari = "block"
            self.makeSerialize()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func postDataBlock(_ message: String){
        let alertLoading = UIAlertController(title: "Sending Request", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        self.present(alertLoading, animated: true, completion: nil)
        
        let urlString = Config().urlUpdate + "block_friend"
        Alamofire.request(urlString, method: .post, parameters: self.paramsAdd).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                alertLoading.dismiss(animated: true, completion: self.viewAlertSuccess)
                
        }
    }
    
    //Block Contact
    @IBAction func btnProfileTapped(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showProfileFriend", sender: self)
    }
    
    //View Add
    var paramsAdd = [String:AnyObject]()
    @IBAction func btnAddTapped(_ sender: AnyObject) {
        // Loading 
        let alertLoading = UIAlertController(title: "Sending Request Friend", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        self.present(alertLoading, animated: true, completion: nil)
        
        let urlString = Config().urlUpdate + "request_as_friend"
        print(self.paramsAdd)
        Alamofire.request(urlString, method: .post, parameters: self.paramsAdd).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                alertLoading.dismiss(animated: true, completion: self.viewAlertSuccess)
                
        }
    }
    
    func sendNotifForSendingRequest(){
        print("tambah notif")
        let model = tb_notif()
        model.id_inviter = self.jsonDataFriend["id"].intValue
        if let img = self.jsonDataFriend["avatar"].string {
            model.icon = img
        }else{
            model.icon = ""
        }
        model.title = "Sending Request Friend"
        
        let name = self.jsonDataFriend["name"].string!
        model.body = "You have sent a friend request to " + name

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let date = dateFormatter.string(from: Date())
        model.date = date
        model.type = 6
        DBHelper.insert(model)
    }
    
    func viewAlertSuccess(){
        let alertSuccess = UIAlertController(title: "Request Sent", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alertSuccess.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            self.sendNotifForSendingRequest()
        }))
        self.present(alertSuccess, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func closePopup(_ sender: UITapGestureRecognizer){
        self.removeAnimate()
    }
    
    func showAnimate(){
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0.0
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            }, completion: {(finished: Bool) in
                if(finished){
                    self.view.removeFromSuperview()
                }
        })
    }
}
