//
//  PopupQrCodeController.swift
//  Groupin
//
//  Created by Macbook pro on 12/22/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire
import RealmSwift

class PopupQrCodeController: UIViewController {
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblTicketName: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var token = ""
    var qrcode = ""
    var idEvent = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(self.token)
        print(self.qrcode)
        print(self.idEvent)
        
        self.getNameEvent()
        self.designView()
        self.showAnimate()
        
        self.makeSerialize()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getNameEvent(){
        let data = try! Realm().objects(listEvent.self).filter("id_event = %@", Int(self.idEvent)!).first!
        self.lblEventName.text = data.name
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "qrcode" : self.qrcode as AnyObject,
            "id_event" : self.idEvent as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postData(base64Encoded)
        }
    }
    
    func postData(_ message: String){
        let urlString = Config().urlPayment + "scan_qrcode"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                //Success
                let jsonData = JSON(value)
                print(jsonData)
                if jsonData["code"] == "1" {
                    let data = jsonData["msg"]
                    self.lblName.text = data["full_name"].string!
                    self.lblTicketName.text = data["ticket_name"].string!
                    self.lblQty.text = data["ticket_amount"].string!
                    self.lblPrice.text = data["total_price"].string!
                }else if jsonData["code"] == "3" {
                    self.msgAlert = jsonData["msg"].string!
                    self.alertStatus()
                    self.removeAnimate()
                    self.performSegue(withIdentifier: "backToScanQRCode", sender: self)
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                
        }
    }
    
    var msgAlert = ""
    func alertStatus(){
        if msgAlert != "" {
            let alert = UIAlertController(title: "Info", message: msgAlert, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func designView(){
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        viewMain.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        //Click to close
        let tapBack = UITapGestureRecognizer(target: self, action: #selector(PopupQrCodeController.closePopup(_:)))
        tapBack.numberOfTapsRequired = 1
        viewMain.isUserInteractionEnabled = true
        viewMain.addGestureRecognizer(tapBack)
        
        //Rounded 
        viewContainer.layer.cornerRadius = 5
        btnDone.backgroundColor = UIColor(red: 83, green: 186, blue: 32)
        btnDone.layer.cornerRadius = 5
        btnDone.layer.borderWidth = 1
        btnDone.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    @IBAction func btnDoneTapped(_ sender: AnyObject) {
        self.makeSerializeDone()
    }
    
    func makeSerializeDone(){
        let strData = self.qrcode.components(separatedBy: "##")
        print(strData)
        let strIdUser = strData[0]
        let strIdEvent = strData[1]
        let strIdAttend = strData[2]
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : strIdUser as AnyObject,
            "id_event" : strIdEvent as AnyObject,
            "id_attendance" : strIdAttend as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Done(jsonString!)
    }
    
    func makeBase64Done(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postDataDone(base64Encoded)
        }
    }
    
    func postDataDone(_ message: String){
        let urlString = Config().urlPayment + "change_status_scan_qrcode"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                //Success
                let jsonData = JSON(value)
                print(jsonData)
                if jsonData["code"] == "1" {
                    let data = jsonData["msg"]
                    print(data)
                    self.removeAnimate()
                    self.performSegue(withIdentifier: "backToScanQRCode", sender: self)
                }else {
                    
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                
        }
    }
    
    @IBAction func btnCancelTapped(_ sender: AnyObject) {
        self.removeAnimate()
        self.performSegue(withIdentifier: "backToScanQRCode", sender: self)
    }
    
    func closePopup(_ sender: UIGestureRecognizer){
        self.removeAnimate()
    }
    
    func showAnimate(){
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0.0
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            }, completion: {(finished: Bool) in
                if(finished){
                    self.view.removeFromSuperview()
                }
        })
    }
}
