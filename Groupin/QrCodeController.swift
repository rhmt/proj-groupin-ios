//
//  qrCodeController.swift
//  Groupin
//
//  Created by Macbook pro on 12/19/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import AVFoundation
import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class QrCodeController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var viewQRCode: UIView!
    @IBOutlet weak var imgQR: UIImageView!
    @IBOutlet weak var viewScan: UIView!
    @IBOutlet weak var viewCameraScan: UIView!
    
    //Scan
    var objCaptureSession:AVCaptureSession?
    var objCaptureVideoPreviewLayer:AVCaptureVideoPreviewLayer?
    var vwQRCode:UIView?
    let supportedBarCodes = [AVMetadataObjectTypeQRCode, AVMetadataObjectTypeCode128Code, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeUPCECode, AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeAztecCode]
    
    //View
    var qrcodeImage: CIImage!
    var idUser = ""
    var token = ""
    var idEvent = ""
    
    var statusQR = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewQRCode.isHidden = true
        self.viewScan.isHidden = true
        self.viewCameraScan.backgroundColor = UIColor.black
        
        let dataUser = try! Realm().objects(session.self).first!
        self.idUser = String(dataUser.id)
        self.token = dataUser.token
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if self.statusQR == "show" {
            self.viewQRCode.isHidden = false
            self.navigationItem.title = "QR Code"
            
            self.generateQRCode()
        }else{
            self.viewScan.isHidden = false
            self.navigationItem.title = "Scan QR Code"
            
            self.configureVideoCapture()
            self.addVideoPreviewLayer()
            self.initializeQRView()
        }
    }
    
    func generateQRCode(){
//        if qrcodeImage == nil {
//            let data = self.idUser.dataUsingEncoding(NSISOLatin1StringEncoding, allowLossyConversion: false)
//            
//            let filter = CIFilter(name: "CIQRCodeGenerator")
//            
//            filter!.setValue(data, forKey: "inputMessage")
//            filter!.setValue("Q", forKey: "inputCorrectionLevel")
//            
//            qrcodeImage = filter!.outputImage
//            
//            let scaleX = imgQR.frame.size.width / qrcodeImage.extent.size.width
//            let scaleY = imgQR.frame.size.height / qrcodeImage.extent.size.height
//            
//            let transformedImage = qrcodeImage.imageByApplyingTransform(CGAffineTransformMakeScale(scaleX, scaleY))
//            
//            imgQR.image = UIImage(CIImage: transformedImage)
//        }
        self.makeSerialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "id_event" : self.idEvent as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postData(base64Encoded)
        }
    }
    
    func postData(_ message: String){
        let urlString = Config().urlPayment + "get_qrcode"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                //Success
                let jsonData = JSON(value)
                print(jsonData)
                if jsonData["code"] == "1" {
                    if let url = jsonData["msg"].string {
                        self.imgQR.loadImageUsingCacheWithUrlString(url)
                    }
                }else {
                    
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                
        }
    }
    
    func configureVideoCapture() {
        let objCaptureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        var error:NSError?
        let objCaptureDeviceInput: AnyObject!
        do {
            objCaptureDeviceInput = try AVCaptureDeviceInput(device: objCaptureDevice) as AVCaptureDeviceInput
        } catch let error1 as NSError {
            error = error1
            objCaptureDeviceInput = nil
        }
        if (error != nil) {
            let alertView = UIAlertController(title: "Device Error", message: "Device not Supported for this Application", preferredStyle: UIAlertControllerStyle.alert)
            alertView.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in }))
            self.present(alertView, animated: true, completion: nil)
            
            return
        }
        objCaptureSession = AVCaptureSession()
        objCaptureSession?.addInput(objCaptureDeviceInput as! AVCaptureInput)
        let objCaptureMetadataOutput = AVCaptureMetadataOutput()
        objCaptureSession?.addOutput(objCaptureMetadataOutput)
        objCaptureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        objCaptureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
    }
    
    func addVideoPreviewLayer(){
        objCaptureVideoPreviewLayer = AVCaptureVideoPreviewLayer(session: objCaptureSession)
        objCaptureVideoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        objCaptureVideoPreviewLayer?.frame = self.viewCameraScan.layer.bounds
        self.viewCameraScan.layer.addSublayer(objCaptureVideoPreviewLayer!)
        objCaptureSession?.startRunning()
    }
    
    func initializeQRView() {
        vwQRCode = UIView()
        vwQRCode?.layer.borderColor = UIColor.green.cgColor
        vwQRCode?.layer.borderWidth = 4
        self.viewCameraScan.addSubview(vwQRCode!)
        self.viewCameraScan.bringSubview(toFront: vwQRCode!)
    }
    
    var qrCodeDetected = ""
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        if metadataObjects == nil || metadataObjects.count == 0 {
            vwQRCode?.frame = CGRect.zero
            print("NO QRCode text detacted")
            
            return
        }
        let objMetadataMachineReadableCodeObject = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        if objMetadataMachineReadableCodeObject.type == AVMetadataObjectTypeQRCode {
            let objBarCode = objCaptureVideoPreviewLayer?.transformedMetadataObject(for: objMetadataMachineReadableCodeObject as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
            vwQRCode?.frame = objBarCode.bounds
            if objMetadataMachineReadableCodeObject.stringValue != nil {
                self.qrCodeDetected = objMetadataMachineReadableCodeObject.stringValue
                print("Hasil : " + self.qrCodeDetected)
                self.showPopUp()
                objCaptureSession?.stopRunning()
            }
        }
    }
    
    func showPopUp(){
        let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showPopupQrCode") as! PopupQrCodeController
        Popover.token = self.token
        Popover.qrcode = self.qrCodeDetected
        Popover.idEvent = self.idEvent
        self.addChildViewController(Popover)
        Popover.view.frame = self.view.frame
        self.view.addSubview(Popover.view)
        Popover.didMove(toParentViewController: self)
    }
    
    @IBAction func backToScanQRCode(_ sender: UIStoryboardSegue){
        vwQRCode?.frame = CGRect.zero
        objCaptureSession?.startRunning()
    }
}
