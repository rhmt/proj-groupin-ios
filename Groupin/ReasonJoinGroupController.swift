//
//  ReasonJoinGroupController.swift
//  Pitung
//
//  Created by Macbook pro on 2/1/17.
//  Copyright © 2017 Macbook pro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift

class ReasonJoinGroupController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var txtReason: UITextView!
    @IBOutlet weak var btnEnter: UIButton!
    
    var idUser = 0
    var token = ""
    var idGroup = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtReason.delegate = self
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        viewMain.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        //Click to close
        let tapBack = UITapGestureRecognizer(target: self, action: #selector(ReasonJoinGroupController.closePopup(_:)))
        tapBack.numberOfTapsRequired = 1
        viewMain.isUserInteractionEnabled = true
        viewMain.addGestureRecognizer(tapBack)
        
        //Get ID user
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.token = idSession!.token
        
        self.txtReason.layer.borderWidth = 0.5
        self.txtReason.layer.borderColor = UIColor.lightGray.cgColor
        
        NotificationCenter.default.addObserver(self, selector: #selector(ReasonJoinGroupController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ReasonJoinGroupController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count
        return 250 > numberOfChars
    }
    
    @IBAction func btnEnterTapped(_ sender: Any) {
        self.makeSerializeJoin()
    }
    
    func makeSerializeJoin(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_group" : self.idGroup as AnyObject,
            "id" : self.idUser as AnyObject,
            "is_private" : 2 as AnyObject,
            "reason" : self.txtReason.text! as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Join(jsonString!)
    }
    
    func makeBase64Join(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getDataJoin(base64Encoded)
        }
    }
    
    func getDataJoin(_ message: String){
        let urlString = Config().urlGroup + "join_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(urlString)
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)["msg"]
                print(jsonData)
                let code = JSON(value)["code"].intValue
                if code == 1 {
                    self.msgError = "Join group success"
                    self.joinSuccess = 1
                }else{
                    self.msgError = jsonData.string!
                }
            }else{
                print("Something Went Wrong..")
                self.msgError = "Failed connet to server, try again"
            }
            }.responseData { Response in
                self.alertStatus()
        }
    }
    
    var joinSuccess = 0
    var msgError = ""
    func alertStatus(){
        if msgError != "" {
            let alert = UIAlertController(title: "", message: msgError, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                if self.joinSuccess > 0 {
                    //self.makeSerializeData()
                    print("Join Success")
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func makeSerializeData(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idUser as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Data(jsonString!)
    }
    
    func makeBase64Data(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getDataGroup(base64Encoded)
        }
    }
    
    func getDataGroup(_ message: String){
        let urlString = Config().urlGroup + "list_own_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                let jsonGroup = jsonData["msg"]
                //Group
                if jsonGroup.count > 0 {
                    let objGroup = try! Realm().objects(tb_group.self)
                    try! Realm().write(){
                        try! Realm().delete(objGroup)
                    }
                    for (key, _) in jsonGroup {
                        let modelGroup = tb_group()
                        modelGroup.id = Int(key)!
                        modelGroup.name = jsonGroup[key]["group"].string!
                        if let ava = jsonGroup[key]["avatar"].string {
                            modelGroup.avatar = ava
                        }else{
                            modelGroup.avatar = ""
                        }
                        modelGroup.city = jsonGroup[key]["city"].string!
                        modelGroup.country = jsonGroup[key]["country"].string!
                        modelGroup.jlm_member = jsonGroup[key]["member"].string!
                        modelGroup.category = jsonGroup[key]["category"].string!
                        modelGroup.desc = jsonGroup[key]["desc"].string!
                        modelGroup.date_create = jsonGroup[key]["date_create"].string!
                        modelGroup.id_user_create = jsonGroup[key]["id_user_create"].string!
                        modelGroup.user_create = jsonGroup[key]["name_user_create"].string!
                        modelGroup.is_public = jsonGroup[key]["is_public"].string!
                        modelGroup.is_promot = jsonGroup[key]["is_promote"].string!
                        modelGroup.user_level = jsonGroup[key]["user_level"].string!
                        DBHelper.insert(modelGroup)
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.directToGroup()
        }
    }
    
    var nameGroupSelected = ""
    func directToGroup(){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "showGroupHome") as! GroupHomeController
        vc.idGroup = self.idGroup
        let model = try! Realm().objects(session.self).first
        try! Realm().write(){
            model?.id_group = self.idGroup
        }
        vc.navigationItem.title = self.nameGroupSelected
        vc.fromCreate = 1
        vc.tabBarController?.tabBar.isHidden = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func closePopup(_ sender: UITapGestureRecognizer){
        self.removeAnimate()
    }
    
    func showAnimate(){
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0.0
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        }, completion: {(finished: Bool) in
            if(finished){
                self.view.removeFromSuperview()
            }
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    var activeField: UITextView?
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            let possKeyboard = self.view.frame.height - keyboardSize!.height - activeField!.frame.height
            if activeField!.frame.origin.y > possKeyboard {
                //                if (!CGRectContainsPoint(aRect, activeField!.frame.origin)){
                if view.frame.origin.y == 0{
                    self.view.frame.origin.y -= keyboardSize!.height
                }
                //                }
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
            else {
                
            }
        }
    }
}
