//
//  RecommendGroupCell.swift
//  Groupin
//
//  Created by Macbook pro on 9/9/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class RecommendGroupCell: UITableViewCell {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCategori: UILabel!
    @IBOutlet weak var lblPlace: UILabel!
    @IBOutlet weak var lblMember: UILabel!
}