//
//  RecommendGroupController.swift
//  Groupin
//
//  Created by Macbook pro on 9/9/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class RecommendGroupController: UIViewController {
    
    @IBOutlet weak var tbList: UITableView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var lblNoData: UILabel!
    
    var idUser = ""
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tbList.isHidden = true
        self.lblNoData.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        tbList.reloadData()
        self.navigationController?.isNavigationBarHidden = true
        
        //Rounded Button
        btnContinue.backgroundColor = UIColor(red: 83, green: 186, blue: 32)
        btnContinue.layer.cornerRadius = 5
        btnContinue.layer.borderWidth = 1
        btnContinue.layer.borderColor = UIColor.lightGray.cgColor
        
        let idSession = try! Realm().objects(session.self).first
        self.token = idSession!.token
        
        makeSerialize()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idUser as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {   
            getData(base64Encoded)
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    func getData(_ message: String){
        let urlString = Config().urlGroup + "list_recommended_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)
                if self.jsonData["msg"].count > 0 {
                    self.tbList.isHidden = false
                    self.lblNoData.isHidden = true
                }else{
                    self.tbList.isHidden = true
                    self.lblNoData.isHidden = false
                }
                print(self.jsonData)
                self.paramsJson = 1
                if self.jsonData["data_user"] != nil {
                    self.updateValidEmail()
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.tbList.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.paramsJson == 1 {
            return self.jsonData["msg"].count
        }else{
            return 0
        }
    }
    
    var id = [String]()
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listRecommendGroupCell", for: indexPath) as! RecommendGroupCell
        
        id.removeAll()
        for (key, _) in jsonData["msg"] {
            id.append(key)
        }
        let data = jsonData["msg"][id[indexPath.row]]
        
        if let imgUrl = data["avatar"].string {
            cell.imgAva.loadImageUsingCacheWithUrlString(imgUrl)
        }else{
            cell.imgAva.image = UIImage(named: "dafault-ava-group")
        }
        
        cell.imgAva.contentMode = .scaleToFill
        cell.imgAva.layer.cornerRadius = 5
        cell.imgAva.clipsToBounds = true
        cell.lblName.text = data["group"].string
        cell.lblCategori.text = data["category"].string
        
        let place = data["country"].string! + ", " + data["city"].string!
        cell.lblPlace.text = place
        cell.lblMember.text = data["member"].string
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showDetailGroup") as! DetailGroupController
        let idG = jsonData["msg"][id[indexPath.row]]["id"].string
        Popover.idGroup = Int(idG!)!
        self.addChildViewController(Popover)
        Popover.view.frame = self.view.frame
        self.view.addSubview(Popover.view)
        Popover.didMove(toParentViewController: self)
    }
    
    @IBAction func btnContinue(_ sender: AnyObject) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "showHome") as! HomeController
        self.present(vc, animated: true, completion: nil)
    }
    
    func updateValidEmail(){
        let model = try! Realm().objects(tb_user.self).filter("id = %@", Int(self.idUser)!).first
        try! Realm().write {
            model!.valid_email = self.jsonData["data_user"]["is_valid_email"].string!
        }
    }
}
