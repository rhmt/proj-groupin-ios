//
//  RegCodePhoneController.swift
//  Groupin
//
//  Created by Macbook pro on 7/25/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class RegCodePhoneController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var lblDesk: UILabel!
    @IBOutlet weak var txtCode: UITextField!{
        didSet{
            txtCode.keyboardType = UIKeyboardType.numberPad
        }
    }
    var keyboardType: UIKeyboardType {
        get{
            return txtCode.keyboardType
        }
        set{
            if newValue != UIKeyboardType.numberPad{
                self.keyboardType = UIKeyboardType.numberPad
            }
        }
    }
    
    var phone = ""
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtCode.delegate = self
        
        self.lblDesk.text = "Waiting to automatically detect on SMS send to " + self.phone
        
        NotificationCenter.default.addObserver(self, selector: #selector(RegCodePhoneController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RegCodePhoneController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        txtCode.textColor = UIColor(red: 63, green: 157, blue: 247)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let numberOfChars = newText.characters.count
        return numberOfChars < 5;
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSetPass" {
            let conn = segue.destination as! SetPasswordController
            conn.phone = self.phone
            conn.token = self.token
            conn.navigationItem.title = "Set Password"
        }
    }
    
    @IBAction func btnNextTapped(_ sender: AnyObject) {
        let countChar = txtCode.text?.characters.count
        if countChar == 4 {
            self.performSegue(withIdentifier: "showSetPass", sender: self)
        }else{
            let alert = UIAlertController(title: "Error", message: "Wrong Code", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                print("Wrong Code")
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnResendSmsTapped(_ sender: AnyObject) {
    }
    
    var activeField: UITextField?
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            let possKeyboard = self.view.frame.height - keyboardSize!.height - activeField!.frame.height
            if activeField?.frame.origin.y > possKeyboard {
//                if (!CGRectContainsPoint(aRect, activeField!.frame.origin)){
                    if view.frame.origin.y == 0{
                        self.view.frame.origin.y -= keyboardSize!.height
                    }
//                }
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
            else {
                
            }
        }
    }
    
}
