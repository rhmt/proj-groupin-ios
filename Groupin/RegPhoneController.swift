//
//  RegPhoneController.swift
//  Groupin
//
//  Created by Macbook pro on 7/25/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class RegPhoneController: UIViewController, UIPopoverPresentationControllerDelegate, UITextFieldDelegate {
    //Testing
    var document: UIDocument!
    
    
    @IBOutlet weak var imgNext: UIImageView!
    @IBOutlet weak var btnCode: UIButton!
    @IBOutlet weak var txtPhone: UITextField!{
        didSet{
            txtPhone.keyboardType = UIKeyboardType.numberPad
        }
    }
    var keyboardType: UIKeyboardType {
        get{
            return txtPhone.keyboardType
        }
        set{
            if newValue != UIKeyboardType.numberPad{
                self.keyboardType = UIKeyboardType.numberPad
            }
        }
    }
    @IBOutlet weak var btnbackLogin: UIButton!
    var codeCall = "+62"
    var phone = ""
    let blue = UIColor(red: 63, green: 157, blue: 247)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtPhone.delegate = self
        
        //dari pilih code
        btnCode.setTitle(self.codeCall, for: UIControlState())
        txtPhone.text = self.phone
        
        //Warna Nav
        navigationController!.navigationBar.barTintColor = self.blue
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationController!.navigationBar.tintColor = UIColor.white
        
        //Rounded Button
        btnbackLogin.backgroundColor = UIColor(red: 83, green: 186, blue: 32)
        btnbackLogin.layer.cornerRadius = 5
        btnbackLogin.layer.borderWidth = 1
        btnbackLogin.layer.borderColor = UIColor.lightGray.cgColor
        
        //Input Ava
        let tapPhoto = UITapGestureRecognizer(target: self, action: #selector(RegPhoneController.btnNextTapped(_:)))
        tapPhoto.numberOfTapsRequired = 1
        imgNext.isUserInteractionEnabled = true
        imgNext.addGestureRecognizer(tapPhoto)
        
        NotificationCenter.default.addObserver(self, selector: #selector(RegPhoneController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RegPhoneController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let phoneNumber = (btnCode.titleLabel?.text)! + txtPhone.text!
        
        if segue.identifier == "showCode" {
            let conn = segue.destination as! RegCodePhoneController
            conn.phone = phoneNumber
//            conn.txtCode.text = pasCodeVerify
            conn.token = self.token
            conn.navigationItem.title = "Verify " + phoneNumber
        }else if segue.identifier == "showKodeNumber" {
            let conn = segue.destination as! CallCodes
            conn.phone = txtPhone.text!
            conn.status = 2
            
            let popoverViewController = segue.destination
            popoverViewController.popoverPresentationController?.delegate = self
        }
    }
    
    @IBAction func kodeNumberTapped(_ sender: AnyObject) {
//        self.performSegueWithIdentifier("showKodeNumber", sender: self)
    }
    
    func btnNextTapped(_ sender: UITapGestureRecognizer) {
        makeCodeVerify()
        sendCodeAndGetToken()
    }
    
    var codeVerify: UInt32 = 0
    var pasCodeVerify = ""
    func makeCodeVerify(){
        codeVerify = arc4random_uniform(9999)
        pasCodeVerify = String(self.codeVerify)
    }
    
    var phoneNumber = ""
    var token = ""
    var alertLoading: UIAlertController!
    func sendCodeAndGetToken(){
        self.alertLoading = UIAlertController(title: "Loading", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        self.present(self.alertLoading, animated: true, completion: nil)
        
        if txtPhone.text != "" {
            phoneNumber = (btnCode.titleLabel?.text)! + txtPhone.text!
        }else{
            phoneNumber = ""
        }
        
        let urlString = Config().urlUser + "register_number"
        var msgAlert = ""
        Alamofire.request(urlString, method: .post, parameters: ["hp" : self.phoneNumber]).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                
                if jsonData["code"] == "1" {
                    self.token = String(describing: jsonData["msg"]["token"])
                    msgAlert = ""
                }else{
                    msgAlert = jsonData["msg"].string!
                }
            }else{
                print("Something Went Wrong..")
            }
        }.responseData { Response in
            self.alertLoading.dismiss(animated: true, completion: nil)
            self.alertStatus(msgAlert)
        }
    }
    
    func alertStatus(_ msg: String){
        if msg != "" {
            let alert = UIAlertController(title: "Info", message: msg, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }else{
            self.performSegue(withIdentifier: "showCode", sender: self)
        }
    }
    
    var activeField: UITextField?
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            let possKeyboard = self.view.frame.height - keyboardSize!.height - activeField!.frame.height
            if activeField?.frame.origin.y > possKeyboard {
//                if (!CGRectContainsPoint(aRect, activeField!.frame.origin)){
                    if view.frame.origin.y == 0{
                        self.view.frame.origin.y -= keyboardSize!.height
                    }
//                }
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
            else {
                
            }
        }
    }
    
}
