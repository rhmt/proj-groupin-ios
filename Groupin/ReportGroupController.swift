//
//  ReportGroupController.swift
//  Groupin
//
//  Created by Macbook pro on 8/18/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import DropDown
import RealmSwift
import SwiftyJSON
import Alamofire

class ReportGroupController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var btnReason: UIButton!
    @IBOutlet weak var txtReason: UITextView!
    @IBOutlet weak var btnSend: UIButton!
    
    let list = DropDown()
    lazy var dropDowns: [DropDown] = { return [ self.list ] }()
    
    var idUser = 0
    var token = ""
    var idGroup = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtReason.delegate = self
        self.txtReason.textColor = UIColor.lightGray
        
        self.navigationItem.title = "Report Group"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.token = idSession!.token
        self.idGroup = idSession!.id_group
        
        //Rounded Button
        btnSend.layer.cornerRadius = 5
        btnSend.layer.borderWidth = 1
        btnSend.layer.borderColor = UIColor.lightGray.cgColor
        
        txtReason.text = "State your reason here"
        txtReason.textColor = UIColor.lightGray
        txtReason.layer.borderWidth = 0.5
        txtReason.layer.borderColor = UIColor.lightGray.cgColor
        
        getDataFilter()
    }
    
    var IdDataReason = [String]()
    var dataReason = [String]()
    var IdDataReasonSelected = ""
    func getDataFilter(){
        let urlString = Config().urlMain + "list_reason_group"
        Alamofire.request(urlString, method: .post).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                self.dataReason.removeAll()
                self.IdDataReason.removeAll()
                for (key, value) in jsonData{
                    self.IdDataReason.append(key)
                    self.dataReason.append(value.string!)
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                print(self.dataReason)
                print(self.IdDataReason)
                self.setupDropDowns()
        }
    }
    
    @IBAction func btnReasonTapped(_ sender: AnyObject) {
        list.show()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtReason.text == "" {
            btnSend.isEnabled = false
            self.btnSend.backgroundColor = UIColor.lightGray
            txtReason.textColor = UIColor.lightGray
            txtReason.text = "State your reason here"
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text! as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count
        if self.txtReason.textColor == UIColor.lightGray {
            txtReason.text = ""
            txtReason.textColor = UIColor.black
        }
        if numberOfChars > 0 {
            self.btnSend.isEnabled = true
            self.btnSend.backgroundColor = UIColor.red
        }else{
            self.btnSend.isEnabled = false
            self.btnSend.backgroundColor = UIColor.lightGray
            self.txtReason.textColor = UIColor.lightGray
            self.txtReason.text = "State your reason here "
        }
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // DropDown Function
    func setupDropDowns() {
        list.anchorView = btnReason
        list.bottomOffset = CGPoint(x: 0, y: btnReason.bounds.height)
        list.dataSource = dataReason
        
        list.selectionAction = { [unowned self] (index, item) in
            self.btnReason.setTitle(item, for: UIControlState())
            let idReason = self.dataReason.index(of: item)
            let inputReason = self.IdDataReason[idReason!]
            self.IdDataReasonSelected = inputReason
            if item == "Other" {
                self.txtReason.isHidden = false
                self.btnSend.isEnabled = false
                self.btnSend.backgroundColor = UIColor.lightGray
                self.txtReason.textColor = UIColor.lightGray
                self.txtReason.becomeFirstResponder()
                self.txtReason.text = "State your reason here"
            }else{
                self.txtReason.isHidden = true
                self.btnSend.isEnabled = true
                self.btnSend.backgroundColor = UIColor(red: 232, green: 76, blue: 61)
                self.txtReason.textColor = UIColor.lightGray
                self.txtReason.text = ""
            }
            
        }
    }
    
    @IBAction func btnSendTapped(_ sender: AnyObject) {
        let alert = UIAlertController(title: "Attention", message: "Are You Sure Want Report This Group?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.makeSerialize()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    var alertLoading: UIAlertController!
    func makeSerialize(){
        self.alertLoading = UIAlertController(title: "Processing", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        self.present(self.alertLoading, animated: true, completion: nil)
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "id_group" : self.idGroup as AnyObject,
            "id_reason" : self.IdDataReasonSelected as AnyObject,
            "reason" : txtReason.text! as AnyObject,
        ]
        print(arrayData)
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    var msgError = ""
    var paramsError = 0
    func getData(_ message: String){
        let urlString = Config().urlGroup + "report_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(urlString)
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                self.paramsError = 1
                self.msgError = jsonData["msg"].string!
            }else{
                print("Something Went Wrong..")
                self.paramsError = 2
                self.msgError = "Failed connect to server"
            }
            }.responseData { Response in
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
        }
    }
    
    func alertStatus(){
        let alert = UIAlertController(title: "Info", message: msgError, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            if self.paramsError == 1 {
//                self.performSegueWithIdentifier("showSuccessDelete", sender: self)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
