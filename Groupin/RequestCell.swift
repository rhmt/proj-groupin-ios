//
//  RequestCell.swift
//  Groupin
//
//  Created by Macbook pro on 8/29/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class RequestCell: UITableViewCell {
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnSavePhone: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnDecline: UIButton!
}