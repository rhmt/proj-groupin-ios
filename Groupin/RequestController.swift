//
//  RequestController.swift
//  Groupin
//
//  Created by Macbook pro on 8/29/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire
import ContactsUI

class RequestController: UIViewController, CNContactViewControllerDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tbNotif: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var viewNoData: UIView!
    
    let blue = UIColor(red: 63, green: 157, blue: 247)
    
    var paramView = ""
    var data = [tb_notif]()
    
    var idUser = 0
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("did load")
        self.tabBarController?.tabBar.isHidden = false
        self.tbNotif.delegate = self
        self.tbNotif.separatorStyle = .none
        colorNav()
        rightMenu()
        
        let sess = try! Realm().objects(session.self).first!
        idUser = sess.id
        token = sess.token
        
        self.tbNotif.rowHeight = UITableViewAutomaticDimension
        self.tbNotif.estimatedRowHeight = 100.0
        
        print("did load")
        self.loadLocal()
        //saveAja()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print("did appear")
        self.loadLocal()
    }
    
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(true)
//        self.tabBarController?.tabBar.isHidden = true
//    }
    
    func loadLocal(){
        self.tabBarController?.tabBar.items?[2].badgeValue = nil
        
        data.removeAll()
        let dataNotif = try! Realm().objects(tb_notif.self).sorted(byKeyPath: "date", ascending: false)
        print("data notif", dataNotif)
        if dataNotif.count > 0 {
            self.tbNotif.isHidden = false
            self.viewNoData.isHidden = true
            for item in dataNotif {
                data.append(item)
            }
            print("ini data")
            print(self.data)
            
            let modelUpdate = try! Realm().objects(tb_notif.self).filter("is_read = %@", 0)
            print("model Update", modelUpdate)
            if UIApplication.shared.applicationIconBadgeNumber >= modelUpdate.count {
                UIApplication.shared.applicationIconBadgeNumber -= modelUpdate.count
            }
            print("minus badge")
            if modelUpdate.count > 0 {
                for update in modelUpdate{
                    try! Realm().write {
                        update.is_read = 1
                    }
                }
            }
        }else{
            self.tbNotif.isHidden = true
            self.viewNoData.isHidden = false
        }
        
        tbNotif.reloadData()
        print("reload data")
    }
    
    func saveAja(){
        print("save aja, buat test")
        let model = tb_notif()
        model.id_inviter = 71
        model.icon = ""
        model.title = "Friend request"
        model.body = "Mrs. whateveryourlastnameis wants to add your contact"
        model.date = "2017-02-10 10:10:10"
        model.type = 1
        DBHelper.insert(model)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "requestCell", for: indexPath) as! RequestCell
        
        //Default
        cell.btnAccept.isHidden = false
        cell.btnDecline.isHidden = false
        cell.btnSavePhone.isHidden = true
        cell.btnSkip.isHidden = true
        tbNotif.rowHeight = 100
        //-------
        
        let item = data[indexPath.row]
        print("table")
        print("data", item)
        
        if item.icon != "" {
            cell.imgAva.loadImageUsingCacheWithUrlString(item.icon)
        }else{
            cell.imgAva.image = UIImage(named: "default-avatar")
        }
        
        cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
        cell.imgAva.clipsToBounds = true
        
        if item.body != "" {
            cell.lblTitle.text = item.body
        }
        if item.date != "" {
            let time = Config().timeElapsed(item.date)
            cell.lblTime.text = time
        }
        
        if item.type == 7 {
            print("info kita accept add friend")
            tbNotif.rowHeight = 65
            cell.btnAccept.isHidden = true
            cell.btnDecline.isHidden = true
        }
        
        if item.type == 6 {
            print("info kita nge-add friend")
            tbNotif.rowHeight = 65
            cell.btnAccept.isHidden = true
            cell.btnDecline.isHidden = true
        }
        
        //Rounded Cell
        cell.viewMain.layer.cornerRadius = 5
        cell.viewMain.layer.shadowColor = UIColor.gray.cgColor
        cell.viewMain.layer.shadowOffset = CGSize.zero
        cell.viewMain.layer.shadowOpacity = 0.5
        cell.viewMain.layer.shadowRadius = 3
        
        //Rounded Button
        cell.btnAccept.backgroundColor = UIColor(red: 83, green: 186, blue: 32)
        cell.btnAccept.layer.cornerRadius = 5
        cell.btnAccept.layer.borderWidth = 0.5
        cell.btnAccept.layer.borderColor = UIColor.lightGray.cgColor
        cell.btnDecline.backgroundColor = UIColor(red: 232, green: 76, blue: 61)
        cell.btnDecline.layer.cornerRadius = 5
        cell.btnDecline.layer.borderWidth = 0.5
        cell.btnDecline.layer.borderColor = UIColor.lightGray.cgColor
        cell.btnSavePhone.backgroundColor = UIColor(red: 83, green: 186, blue: 32)
        cell.btnSavePhone.layer.cornerRadius = 5
        cell.btnSavePhone.layer.borderWidth = 0.5
        cell.btnSavePhone.layer.borderColor = UIColor.lightGray.cgColor
        
        //Rounded Ava
        cell.imgAva.layer.cornerRadius = 24
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if let _ = try! Realm().objects(tb_notif.self).filter("type == \(1) AND body CONTAINS %@", "as friend").first {
//            print("friends")
//            return 65
//        }
//        if let _ = try! Realm().objects(tb_notif.self).filter("type == \(6)").first {
//            print("type 6")
//            return 65
//        }
//        print("selain itu")
//        return 100
//    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    var paramsWhere = ""
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let item = data[indexPath.row]
        
        paramsWhere = item.body
    }
    
    var alertLoading: UIAlertController!
    var response = 0
    var idInviter = 0
    var idGroup = 0
    var idGroupParent = 0
    var idRoom = 0
    var idEvent = 0
    
    @IBAction func btnAcceptTapped(_ sender: AnyObject) {
        print("accept")
        let button = sender
        let view = button.superview!
        let cell = view!.superview?.superview as! RequestCell
        let indexPath = tbNotif.indexPath(for: cell)
        
        response = 1
        let untuk = self.data[indexPath!.row].type
        if untuk == 1 {
            print("accept temen")
            self.idInviter = self.data[indexPath!.row].id_inviter
            self.serializeFriendRespon()
        }
        if untuk == 2 {
            self.idInviter = self.data[indexPath!.row].id_inviter
            self.idGroup = self.data[indexPath!.row].id_group
            self.serializeInviteUser()
        }
        if untuk == 3 {
            self.idInviter = self.data[indexPath!.row].id_inviter
            self.idGroup = self.data[indexPath!.row].id_group
            self.idGroupParent = self.data[indexPath!.row].id_group_parent
            self.serializeInviteGroup()
        }
        if untuk == 4 {
            self.idRoom = self.data[indexPath!.row].id_room
            self.serializeInviteRoom()
        }
        if untuk == 5 {
            // ?
            self.idEvent = self.data[indexPath!.row].id_event
        }
    }
    
    @IBAction func btnDeclineTapped(_ sender: AnyObject) {
        print("Decline")
        let button = sender
        let view = button.superview!
        let cell = view!.superview?.superview as! RequestCell
        let indexPath = tbNotif.indexPath(for: cell)
        
        response = 3
        let untuk = self.data[indexPath!.row].type
        
        let alert = UIAlertController(title: "Comfirm", message: "Are you sure want decline?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            if untuk == 1 {
                self.idInviter = self.data[indexPath!.row].id_inviter
                self.serializeFriendRespon()
            }
            if untuk == 2 {
                self.idInviter = self.data[indexPath!.row].id_inviter
                self.idGroup = self.data[indexPath!.row].id_group
                self.serializeInviteUser()
            }
            if untuk == 3 {
                self.idInviter = self.data[indexPath!.row].id_inviter
                self.idGroup = self.data[indexPath!.row].id_group
                self.idGroupParent = self.data[indexPath!.row].id_group_parent
                self.serializeInviteGroup()
            }
            if untuk == 4 {
                self.idRoom = self.data[indexPath!.row].id_room
                self.serializeInviteRoom()
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func serializeFriendRespon(){
        self.alertLoading = UIAlertController(title: "Processing", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        DispatchQueue.main.async(execute: {
            self.present(self.alertLoading, animated: true, completion: nil)
        })
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_login" : self.idInviter as AnyObject,
            "id" : self.idUser as AnyObject,
            "code" : self.response as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64FriendRespon(jsonString!)
    }
    
    func makeBase64FriendRespon(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postDataFriendRespon(base64Encoded)
        }
    }
    
    func postDataFriendRespon(_ message: String){
        let urlString = Config().urlUpdate + "response_add_friend"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                self.alertLoading.dismiss(animated: true, completion: nil)
                if self.response == 1 {
                    self.updateToSaveNumber()
                }
                if self.response == 3 {
                    self.deleteDataLocal(1)
                }
            }else{
                print("Something Went Wrong..")
                self.msgError = "Failed connect to server"
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
            }
            }.responseData { Response in
                
        }
    }
    
    func serializeInviteUser(){
        self.alertLoading = UIAlertController(title: "Processing", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        DispatchQueue.main.async(execute: {
            self.present(self.alertLoading, animated: true, completion: nil)
        })
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idUser as AnyObject,
            "id_group" : self.idGroup as AnyObject,
            "code" : self.response as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64InviteUser(jsonString!)
    }
    
    func makeBase64InviteUser(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postDataInviteUser(base64Encoded)
        }
    }
    
    func postDataInviteUser(_ message: String){
        let urlString = Config().urlUpdate + "response_user_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                self.alertLoading.dismiss(animated: true, completion: nil)
                self.deleteDataLocal(2)
            }else{
                print("Something Went Wrong..")
                self.msgError = "Failed connect to server"
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
            }
            }.responseData { Response in
                
        }
    }
    
    func serializeInviteGroup(){
        self.alertLoading = UIAlertController(title: "Processing", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        DispatchQueue.main.async(execute: {
            self.present(self.alertLoading, animated: true, completion: nil)
        })
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user_login" : self.idUser as AnyObject,
            "id_group" : self.idGroup as AnyObject,
            "id_group_parent" : self.idGroupParent as AnyObject,
            "code" : self.response as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64InviteGroup(jsonString!)
    }
    
    func makeBase64InviteGroup(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postDataInviteGroup(base64Encoded)
        }
    }
    
    func postDataInviteGroup(_ message: String){
        let urlString = Config().urlUpdate + "response_child_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                self.alertLoading.dismiss(animated: true, completion: nil)
                self.deleteDataLocal(3)
            }else{
                print("Something Went Wrong..")
                self.msgError = "Failed connect to server"
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
            }
            }.responseData { Response in
                
        }
    }
    
    func serializeInviteRoom(){
        self.alertLoading = UIAlertController(title: "Processing", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        DispatchQueue.main.async(execute: {
            self.present(self.alertLoading, animated: true, completion: nil)
        })
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "id_room" : self.idRoom as AnyObject,
            "code" : self.response as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64InviteRoom(jsonString!)
    }
    
    func makeBase64InviteRoom(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postDataInviteRoom(base64Encoded)
        }
    }
    
    func postDataInviteRoom(_ message: String){
        let urlString = Config().urlUpdate + "response_user_room"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                self.alertLoading.dismiss(animated: true, completion: nil)
                self.deleteDataLocal(4)
            }else{
                print("Something Went Wrong..")
                self.msgError = "Failed connect to server"
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
            }
            }.responseData { Response in
                
        }
    }
    
    @IBAction func btnSavePhoneTapped(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview?.superview as! RequestCell
        let indexPath = tbNotif.indexPath(for: cell)
        self.idInviter = self.data[indexPath!.row].id_inviter
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idInviter as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        let utf8str = jsonString!.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
        //Open view Contact UI
//        let controller = CNContactViewController(forNewContact: nil)
//        controller.delegate = self
//        controller.allowsEditing = true
//        
//        let navigationController = UINavigationController(rootViewController: controller)
//        self.presentViewController(navigationController, animated: true, completion: nil)

    }
    
    @IBAction func btnSkipTapped(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview?.superview as! RequestCell
        let indexPath = tbNotif.indexPath(for: cell)
        
        self.idInviter = self.data[indexPath!.row].id_inviter
        
        self.deleteDataLocal(1)
    }
    
    var jsonData: JSON!
    func getData(_ message: String){
        let urlString = Config().urlUser + "save_phone_number"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)["msg"]
                print(self.jsonData)
                if self.jsonData != nil {
                    switch CNContactStore.authorizationStatus(for: .contacts){
                    case .authorized:
                        self.savePhoneNumber()
                    case .notDetermined:
                        self.store.requestAccess(for: .contacts){succeeded, err in
                            guard err == nil && succeeded else{
                                return
                            }
                            self.savePhoneNumber()
                        }
                    default:
                        print("Cant save contact")
                    }
                }
            }else{
                print("Something Went Wrong..")
                self.msgError = "Something Went Wrong.."
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
            }
            }.responseData { Response in
                
        }
    }
    
    //Save Phone Number
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        viewController.navigationController!.dismiss(animated: true, completion: nil);
    }
    var store = CNContactStore()
    func savePhoneNumber(){
        let contactData = CNMutableContact()
        
        //Image
        let dataImg = self.jsonData["avatar"].string!
        let imgUrl = URL(string: dataImg)
        let urlData = try! Data.init(contentsOf: imgUrl!)
        let imgUi = UIImage(data: urlData)
        let imgData = UIImageJPEGRepresentation(imgUi!, 1.0)
        contactData.imageData = imgData!
        
        //name
        contactData.givenName = self.jsonData["name"].string!
        
        //phone
        let homePhone = CNLabeledValue(label: CNLabelPhoneNumberMobile,value: CNPhoneNumber(stringValue: self.jsonData["phone"].string!))
        contactData.phoneNumbers = [homePhone]
        
        //birtday
        let dataBirtday = self.jsonData["birtday"].string!
        let day = dataBirtday.substring(from: dataBirtday.index(dataBirtday.startIndex, offsetBy: 8)).substring(to: dataBirtday.index(dataBirtday.endIndex, offsetBy: 0))
        let month = dataBirtday.substring(from: dataBirtday.index(dataBirtday.startIndex, offsetBy: 5)).substring(to: dataBirtday.index(dataBirtday.endIndex, offsetBy: -3))
        let year = dataBirtday.substring(from: dataBirtday.index(dataBirtday.startIndex, offsetBy: 0)).substring(to: dataBirtday.index(dataBirtday.endIndex, offsetBy: -6))
        var birthday = DateComponents()
        birthday.year = Int(year)!
        birthday.month = Int(month)!
        birthday.day = Int(day)!
        contactData.birthday = birthday
        
        //Save
        let request = CNSaveRequest()
        request.add(contactData, toContainerWithIdentifier: nil)
        do{
            try store.execute(request)
            print("Successfully added the contact")
            self.deleteDataLocal(1)
        } catch let err{
            print("Failed to save the contact. \(err)")
            msgError = "Failed to save the contact"
            alertStatus()
        }
    }
    
    var msgError = ""
    func alertStatus(){
        if msgError != "" {
            let alert = UIAlertController(title: "Info", message: msgError, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //Update to Save Phone number
    func updateToSaveNumber(){
        let model = try! Realm().objects(tb_notif.self).filter("id_inviter = %@ AND type == \(1)", self.idInviter).first
        
        let data = model?.body
        let body = data?.replacingOccurrences(of: " wants to add your contact", with: "")
        let dateForr = DateFormatter()
        dateForr.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let dateSave = dateForr.string(from: Date())
        
        try! Realm().write({
            model?.body = "You added \(body!) as friend"
            model?.date = dateSave
            model?.type = 7
        })
        
        self.loadLocal()
    }
    
    //Delete Data Local
    func deleteDataLocal(_ dari: Int){
        if dari == 1 {
            let model = try! Realm().objects(tb_notif.self).filter("id_inviter = %@ AND type == \(dari)", self.idInviter).first
            try! Realm().write({
                try! Realm().delete(model!)
            })
        }
        if dari == 2 {
            let model = try! Realm().objects(tb_notif.self).filter("id_inviter = %@ AND id_group = %@ AND type == \(dari)", self.idInviter, self.idGroup).first
            try! Realm().write({
                try! Realm().delete(model!)
            })
        }
        if dari == 3 {
            let model = try! Realm().objects(tb_notif.self).filter("id_inviter = %@ AND id_group = %@ AND id_group_parent AND type == \(dari)", self.idInviter, self.idGroup, self.idGroupParent).first
            try! Realm().write({
                try! Realm().delete(model!)
            })
        }
        if dari == 4 {
            let model = try! Realm().objects(tb_notif.self).filter("id_room = %@ AND type == \(dari)", self.idRoom).first
            try! Realm().write({
                try! Realm().delete(model!)
            })
        }
        
        loadLocal()
    }
    
    // METHODE NAV
    func colorNav(){
        navigationController!.navigationBar.barTintColor = self.blue
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationController!.navigationBar.tintColor = UIColor.white
    }
    
    func rightMenu(){
        let searchButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(RequestController.searchTapped))
        //Space
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 10.0
        //Delete
        let imageContact = UIImage(named: "ico_tab_contact")
        let contactButton = UIButton()
        contactButton.setImage(imageContact, for: UIControlState())
        contactButton.addTarget(self, action: #selector(self.contactTapped), for: UIControlEvents.touchDown)
        contactButton.frame=CGRect(x: 0, y: 0, width: 30, height: 30)
        let barAttachContact = UIBarButtonItem(customView: contactButton)
        
        self.navigationItem.setRightBarButtonItems([searchButtonItem, fixedSpace, barAttachContact], animated: true)
    }
    
    func searchTapped(_ sender:UIButton){
        self.performSegue(withIdentifier: "showSearchGroup", sender: self)
    }
    
    func contactTapped(_ sender:UIButton){
        self.performSegue(withIdentifier: "showContact", sender: self)
    }
}
