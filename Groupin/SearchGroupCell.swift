//
//  SearchGroupCell.swift
//  Groupin
//
//  Created by Macbook pro on 8/11/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import Foundation
import UIKit

class SearchGroupCell: UITableViewCell {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblPlace: UILabel!
    @IBOutlet weak var lblMember: UILabel!
}