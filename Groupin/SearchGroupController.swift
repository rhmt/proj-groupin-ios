//
//  SearchGroupController.swift
//  Groupin
//
//  Created by Macbook pro on 8/11/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class SearchGroupController: UITableViewController{
    
    var models = [tb_group]()
    var filteredList = [tb_group]()
    let searchController = UISearchController(searchResultsController: nil)
    let blue = UIColor(red: 63, green: 157, blue: 247)
    var idUser = 0
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadData()
        
        //self.navigationController?.navigationBar.isTranslucent = false
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(true)
//        self.tableView.isScrollEnabled = true
//    }
    
    func loadData(){
        self.navigationItem.title = "Search Group"
        self.tabBarController?.tabBar.isHidden = true
        self.tableView.tableFooterView = UIView()
        
        //Get ID user
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.token = idSession!.token
        
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.sizeToFit()
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.searchBar.placeholder = "Input search here..."
        
        // Search Bar Color
        UISearchBar.appearance().barTintColor = blue
        UISearchBar.appearance().tintColor = UIColor.white
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = blue
        
        self.loadLocalListCatSearch()
        self.getDataAll()
        self.getDataFilter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func loadLocalListCatSearch(){
        self.dataFilter.removeAll()
        self.IdDataFilter.removeAll()
        let data = try! Realm().objects(listCatSearch.self)
        for item in data{
            self.IdDataFilter.append(item.id_cat)
            self.dataFilter.append(item.name)
        }
        
        // Setup the Scope Bar
        if self.dataFilter.count > 0 {
            self.searchController.searchBar.scopeButtonTitles = self.dataFilter
            self.tableView.tableHeaderView = self.searchController.searchBar
        }
    }
    
    var IdDataFilter = [String]()
    var dataFilter = [String]()
    func getDataFilter(){
        let urlString = Config().urlMain + "list_keyword_search"
        Alamofire.request(urlString, method: .post).responseJSON { response in
            if let value = response.result.value {
                let data = JSON(value)
                if data.count > 0{
                    for (key, value) in data{
                        if let modelUpdate = try! Realm().objects(listCatSearch.self).filter("name = %@", value.string!).first {
                            try! Realm().write {
                                modelUpdate.name = value.string!
                            }
                        }else{
                            let model = listCatSearch()
                            model.id_cat = key
                            model.name = value.string!
                            DBHelper.insert(model)
                        }
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                print(self.dataFilter)
                print(self.IdDataFilter)
                self.loadLocalListCatSearch()
        }
    }
    
    var jsonAll: JSON!
    var paramsJson = 0
    func getDataAll(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idUser as AnyObject
        ]
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        var message = ""
        let utf8str = jsonString!.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        { message = base64Encoded }
        
        let urlString = Config().urlGroup + "list_recommended_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                self.jsonAll = JSON(value)
                self.paramsJson = 1

                if self.jsonAll["data_user"] != nil {
                    self.updateValidEmail()
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return jsonData["msg"].count
        }else{
            if self.paramsJson == 1 {
                return self.jsonAll["msg"].count
            }else{
                return 0
            }
        }
    }
    
    var id = [String]()
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as! SearchGroupCell
        
        let data: JSON!
        if searchController.isActive && searchController.searchBar.text != "" {
            id.removeAll()
            for (key, _) in jsonData["msg"] {
                id.append(key)
            }
            data = jsonData["msg"][id[indexPath.row]]
        } else {
            id.removeAll()
            for (key, _) in jsonAll["msg"] {
                id.append(key)
            }
            data = jsonAll["msg"][id[indexPath.row]]
        }
        
        if let imgUrl = data["avatar"].string {
            cell.imgAva.loadImageUsingCacheWithUrlString(imgUrl)
        }else{
            cell.imgAva.image = UIImage(named: "dafault-ava-group")
        }
        cell.imgAva.contentMode = .scaleToFill
        cell.lblName.text = data["group"].string
        cell.lblCategory.text = data["category"].string
        cell.lblPlace.text = data["country"].string! + ", " + data["city"].string!
        var jlmMember = ""
        if let jlm = data["member"].string { jlmMember = jlm }
        cell.lblMember.text = String(jlmMember)
        
        //Rounded Ava
        cell.imgAva.layer.cornerRadius = 10
        cell.imgAva.clipsToBounds = true
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if self.keyboardParams == 1 {
            self.searchController.searchBar.resignFirstResponder()
        }
        
        let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showDetailGroup") as! DetailGroupController
        var idG = ""
        if searchController.isActive && searchController.searchBar.text != "" {
            idG = jsonData["msg"][id[indexPath.row]]["id"].string!
        }else{
            idG = jsonAll["msg"][id[indexPath.row]]["id"].string!
        }
        Popover.idGroup = Int(idG)!
        self.addChildViewController(Popover)
        Popover.view.frame = self.view.frame
        self.view.addSubview(Popover.view)
        
        self.searchController.isActive = false
        
        Popover.didMove(toParentViewController: self)
    }
    
    var keyboardParams = 0
    var filter = "name"
    var like = ""
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        self.keyboardParams = 1
        let searchBar = searchController.searchBar
        self.filter = IdDataFilter[searchBar.selectedScopeButtonIndex]
        self.like = searchText.lowercased()
        
        makeSerialize()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "key" : self.filter as AnyObject,
            "word" : self.like as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    var jsonData: JSON!
    func getData(_ message: String){
        let urlString = Config().urlGroup + "search_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)
                print(self.jsonData)
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.tableView.reloadData()
        }
    }
    
    func updateValidEmail(){
        let model = try! Realm().objects(tb_user.self).filter("id = %@", self.idUser).first
        try! Realm().write {
            model!.valid_email = self.jsonAll["data_user"]["is_valid_email"].string!
        }
    }
    
}

extension SearchGroupController: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
}

extension SearchGroupController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearchText(searchController.searchBar.text!, scope: scope)
    }
}
