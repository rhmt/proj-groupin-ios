//
//  SelectMapsEventController.swift
//  Groupin
//
//  Created by Macbook pro on 9/9/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import GooglePlacePicker

class SelectMapsEventController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {
    
    var place: GMSPlace!
    var placePicker: GMSPlacePicker?
    let marker = GMSMarker()
    
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lblLoc: UILabel!
    @IBOutlet weak var lblLocDetail: UILabel!
    
    var latView :CLLocationDegrees = 0
    var longView :CLLocationDegrees = 0
    var locDetail = ""
    
    let blue = UIColor(red: 63, green: 157, blue: 247)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.rightMenu()
        
        self.lblLoc.text = "Tap Maps"
        self.lblLocDetail.text = "to Get Your Address"
        
        mapView.delegate = self
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        mapView.settings.scrollGestures = true
        mapView.settings.zoomGestures = true
        mapView.settings.indoorPicker = true
        
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
    }
    
    //Current Location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        
        self.latView = (location?.coordinate.latitude)!
        self.longView = (location?.coordinate.longitude)!
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        self.mapView?.animate(to: camera)
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: self.latView, longitude: self.longView), zoom: 17.0)
        mapView.animate(to: camera)
        let position = CLLocationCoordinate2DMake(self.latView, self.longView)
        let point = GMSMarker(position: position)
        point.map = mapView
        point.isFlat = true
        point.tracksInfoWindowChanges = true
        
        let img = Config().ResizeImage(UIImage(named: "ico_place_maps")!, targetSize: CGSize(width: 28.6, height: 40.8))
        point.icon = img.withRenderingMode(.alwaysTemplate)
    }
    
    func mapView(_ mapViewUIView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        mapView.clear()
        
        self.latView = coordinate.latitude
        self.longView = coordinate.longitude
        let position = CLLocationCoordinate2DMake(self.latView, self.longView)
        let point = GMSMarker(position: position)
        point.map = mapView
        point.isFlat = true
        point.tracksInfoWindowChanges = true
        
        let img = Config().ResizeImage(UIImage(named: "ico_place_maps")!, targetSize: CGSize(width: 28.6, height: 40.8))
        point.icon = img.withRenderingMode(.alwaysTemplate)
        
        getInfo()
    }
    
    func getInfo(){
        // Get location name
        let location = CLLocation(latitude: self.latView, longitude: self.longView) //changed!!!
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            
            if error != nil {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks![0]
                
                self.locDetail = ""
                if let street = pm.addressDictionary!["Street"] as? NSString {
                    self.locDetail += street as String
                }
                if let city = pm.addressDictionary!["City"] as? NSString {
                    self.locDetail += " " + (city as String) as String
                    self.lblLoc.text = city as String
                }
                if let state = pm.addressDictionary!["State"] as? NSString {
                    self.locDetail += " " + (state as String) as String
                }
                if let zip = pm.addressDictionary!["ZIP"] as? NSString {
                    self.locDetail += " " + (zip as String) as String
                }
                if let Country = pm.addressDictionary!["Country"] as? NSString {
                    self.locDetail += " " + (Country as String) as String
                }
                
                if let locName = pm.name {
                    self.lblLoc.text = locName
                }
                
                self.lblLocDetail.text = self.locDetail
                self.addressSelect = self.locDetail
            }
            else {
                print("Can't Get Data Info")
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var addressSelect = ""
    func rightMenu(){
        let searchMenuButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(SelectMapsEventController.searchTapped))
        let doneMenuButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(SelectMapsEventController.doneTapped))
        self.navigationItem.setRightBarButtonItems([doneMenuButtonItem, searchMenuButtonItem], animated: true)
    }
    
    func searchTapped(_ sender:UIButton){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        self.present(autocompleteController, animated: true, completion: nil)
    }
    
    func doneTapped(_ sender:UIButton){
        self.performSegue(withIdentifier: "goBackToCreateEvent", sender: self)
    }
    
    func searchDone(){
        mapView.clear()
        let position = CLLocationCoordinate2DMake(self.latView, self.longView)
        let point = GMSMarker(position: position)
        
        point.map = mapView
        point.isFlat = true
        point.tracksInfoWindowChanges = true
        let img = Config().ResizeImage(UIImage(named: "ico_place_maps")!, targetSize: CGSize(width: 28.6, height: 40.8))
        point.icon = img.withRenderingMode(.alwaysTemplate)
    }
    
    var untuk = 0 // 0: Loc Event, 1: Loc Ticket
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goBackToCreateEvent" {
            let conn = segue.destination as! AddEventController
            if self.untuk == 0 {
                print("loc")
                conn.txtAddress.text = self.addressSelect
                conn.latAddress = String(self.latView)
                conn.longAddress = String(self.longView)
            }else{
                print("loc ticket")
                conn.txtTicketLocation.text = self.addressSelect
                conn.latAddressTicket = String(self.latView)
                conn.longAddressTicket = String(self.longView)
            }
            conn.untukLocMap = 0
        }
    }
}

// Maps AutoComplete Address
extension SelectMapsEventController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.addressSelect = place.formattedAddress!
        self.dismiss(animated: true, completion: nil)
        //Buat Tampil
        self.latView = place.coordinate.latitude
        self.longView = place.coordinate.longitude
        self.lblLoc.text = place.name
        self.lblLocDetail.text = place.formattedAddress
        
        searchDone()
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ")//, error.description)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
