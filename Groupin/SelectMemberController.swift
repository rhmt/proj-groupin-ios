//
//  SelectMemberController.swift
//  Groupin
//
//  Created by Macbook pro on 7/26/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class SelectMemberController: UIViewController {
    
    var name = ""
    var statusPublic = ""
    var admin = [""]
    var member = [""]
    var idToSave = 0
//    var models = [tb_channel]()
    var modelGroup = tb_group()
    var user = [tb_user]()
    var idUser = 0
    var idGroup = 0
    var idRoom = 0
    var token = ""
    var phoneSelect = [""]
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var tbListMember: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    
    //Edit
    var statusEdit = 0
    var idChannel = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tbListMember.tableFooterView = UIView()
        self.tbListMember.isHidden = true
        
        let rightSearchBarButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(SelectMemberController.searchTapped))
        //self.navigationItem.setRightBarButtonItems([rightSearchBarButtonItem], animated: true)
        
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.idGroup = idSession!.id_group
        self.token = idSession!.token
        
        phoneSelect.removeAll()
        
        //Edit
        if statusEdit == 1 {
            let titleBtn = "EDIT CHAT ROOM"
            btnSave.setTitle(titleBtn, for: UIControlState())
        }
        
        //Rounded Button
        btnSave.backgroundColor = UIColor(red: 83, green: 186, blue: 32)
        btnSave.layer.cornerRadius = 5
        btnSave.layer.borderWidth = 1
        btnSave.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func searchTapped(_ sender:UIButton) {
        print("search pressed")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        makeSerialize()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idGroup as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            self.getMember(base64Encoded)
        }
    }
    
    var paramsJson = 0
    var dataMember: JSON!
    func getMember(_ message: String){
        let urlString = Config().urlUser + "list_member_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                self.dataMember = jsonData["msg"]
                if self.dataMember.count > 0 {
                    self.tbListMember.isHidden = false
                    self.lblNoData.isHidden = true
                }else{
                    self.tbListMember.isHidden = true
                    self.lblNoData.isHidden = false
                }
                self.paramsJson = 1
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.tbListMember.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if paramsJson == 1 {
            return self.dataMember.count
        }else{
            return 0
        }
    }
    
    var idMember = [String]()
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addMemberChannel", for: indexPath) as! AddMemberChannelCell
        
        for (key, _) in self.dataMember {
            self.idMember.append(key)
        }
        
        if let imageUrl = self.dataMember[self.idMember[indexPath.row]]["avatar"].string {
            cell.imgAva.loadImageUsingCacheWithUrlString(imageUrl)
        }else{
            cell.imgAva.image = UIImage(named: "default-avatar")
        }
        cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
        cell.imgAva.clipsToBounds = true
        cell.lblName.text = self.dataMember[self.idMember[indexPath.row]]["name"].string!
        cell.imgSelect.isHidden = true
        
        for index in currentRow {
            if indexPath.row == index {
                cell.imgSelect.isHidden = false
            }
        }
        
        return cell
    }
    
    var currentRow = [Int]()
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        // Find phone is exist or not in phoneSelect
        
        let data = dataMember[idMember[indexPath.row]]
        var status = ""
        for index in phoneSelect {
            if index == data["id"].string {
                status = "ada"
            }
        }
        
        // add or remove phone in phoneSelect
        if status == "" {
            //Select
            currentRow.append(indexPath.row)
            phoneSelect.append(data["id"].string!)
        }else{
            //Deselect
            let idData = self.phoneSelect.index(of: data["id"].string!)
            phoneSelect.remove(at: idData!)
            let inx = self.currentRow.index(of: indexPath.row)
            currentRow.remove(at: inx!)
        }
        self.tbListMember.reloadData()
    }

    var alertLoading: UIAlertController!
    @IBAction func btnCreateTapped(_ sender: AnyObject) {
        self.alertLoading = UIAlertController(title: "", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        self.present(self.alertLoading, animated: true, completion: nil)
        if statusEdit == 0 {
//            saveChannel()
            makeSerializeCreate()
        }else{
//            editChannel()
            makeSerializePost()
        }
    }
    
    func makeSerializeCreate(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idGroup as AnyObject,
            "id_user" : self.idUser as AnyObject,
            "room_name" : self.name as AnyObject,
            "is_public" : self.statusPublic as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Create(jsonString!)
    }
    
    func makeBase64Create(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postCreate(base64Encoded)
        }
    }
    
    func postCreate(_ message: String){
        let urlString = Config().urlRoom + "add_chat_room"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(token)
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                //Success
                let jsonData = JSON(value)
                if jsonData["code"] == "1" {
                    self.msgAlert = ""
                    self.idRoom = jsonData["msg"].intValue
                    self.makeSerializePost() // add member
                }else {
                    self.msgAlert = jsonData["msg"].string!
                    self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
                }
            }else{
                print("Something Went Wrong..")
                self.msgAlert = "Failed connect to server, try again"
            }
            
            }.responseData { Response in
                
        }
    }
    
    func makeSerializePost(){
        var dataId = [String:AnyObject]()
        var arrayIdInput = [NSString]()
        for id in self.phoneSelect {
            dataId = [
                "id" : id as AnyObject
            ]
            let data = try! JSONSerialization.data(withJSONObject: dataId, options: .prettyPrinted)
            let stringData = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            arrayIdInput += [stringData!]
        }
        
        var dataIdAdmin = [String:AnyObject]()
        var arrayIdAdmin = [NSString]()
        for id in self.admin {
            dataIdAdmin = [
                "id" : id as AnyObject
            ]
            let data = try! JSONSerialization.data(withJSONObject: dataIdAdmin, options: .prettyPrinted)
            let stringData = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            arrayIdAdmin += [stringData!]
        }
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_room" : self.idRoom as AnyObject]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        let strJason = String(jsonString!)
        let jasonData = String(strJason.characters.dropLast())
        let passData = ("\(jasonData),\n \"admin\" : \(arrayIdAdmin),\n \"users\" : \(arrayIdInput)\n}")
        
        makeBase64Post(passData as NSString)
    }
    
    func makeBase64Post(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postData(base64Encoded)
        }
    }
    
    var msgAlert = ""
    func postData(_ message: String){
        let urlString = Config().urlUpdate + "invite_user_room"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                //Success
                let jsonData = JSON(value)
                if jsonData["code"] == "1" {
                    self.msgAlert = ""
                }else {
                    self.msgAlert = jsonData["msg"].string!
                }
            }else{
                self.msgAlert = "Failed connect to server, try again"
            }
            }.responseData { Response in
                self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
        }
    }
    
    func alertStatus(){
        if msgAlert != "" {
            let alert = UIAlertController(title: "Info", message: msgAlert, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }else{
           // Balik ke group
            self.performSegue(withIdentifier: "toBackToGroup", sender: self)
        }
    }
}
