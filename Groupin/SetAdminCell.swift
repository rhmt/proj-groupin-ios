//
//  SetAdminCell.swift
//  Groupin
//
//  Created by Macbook pro on 8/12/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class SetAdminCell: UITableViewCell {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var imgSelect: UIImageView!
    @IBOutlet weak var lblName: UILabel!
}
