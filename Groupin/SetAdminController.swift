//
//  SetAdminController.swift
//  Groupin
//
//  Created by Macbook pro on 8/12/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import RealmSwift
import UIKit
import SwiftyJSON
import Alamofire

class SetAdminController: UIViewController {
    
    @IBOutlet weak var tbData: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    
    var models = tb_user()
    var idUser = 0
    var token = ""
    var idGroup = 0
    var dari = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tbData.isHidden = true
        self.tbData.tableFooterView = UIView()
        
        print("mana")
        print(phoneSelect)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        rightMenu()
        
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.token = idSession!.token
        self.idGroup = idSession!.id_group
        
        makeSerialize()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id" : self.idGroup as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            self.getMember(base64Encoded)
        }
    }
    
    var paramsJson = 0
    var dataMember: JSON!
    var countMember = 0
    func getMember(_ message: String){
        let urlString = Config().urlUser + "list_member_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                self.dataMember = jsonData["msg"]
                if self.dataMember.count > 0 {
                    self.paramsJson = 1
                    for (key, _) in self.dataMember {
                        if self.dataMember[key]["user_level"].string! == "1"{
                            self.countMember += 1
                            self.idMember.append(key)
                        }
                    }
                    self.tbData.isHidden = false
                    self.lblNoData.isHidden = true
                }else{
                    self.tbData.isHidden = true
                    self.lblNoData.isHidden = false
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.tbData.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.paramsJson == 1 {
            return self.countMember
        }else{
            return 0
        }
    }
    
    var idMember = [String]()
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SetAdminCell", for: indexPath) as! SetAdminCell
        
        if self.paramsJson == 1 {
            let data = self.dataMember[idMember[indexPath.row]]
            if let imgUrl = data["avatar"].string {
                cell.imgAva.loadImageUsingCacheWithUrlString(imgUrl)
            }else{
                cell.imgAva.image = UIImage(named: "default-avatar")
            }
            cell.imgAva.contentMode = .scaleToFill
            cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
            cell.imgAva.clipsToBounds = true
            cell.lblName.text = data["name"].string
            cell.imgSelect.isHidden = true
            
            for index in phoneSelect {
                if index == data["id"].string! {
                    cell.imgSelect.isHidden = false
                }
            }
        }
        
//        for index in currentRow {
//            if indexPath.row == index {
//                cell.imgSelect.hidden = false
//            }
//        }
        
        return cell
    }
    
    var phoneSelect = [String]()
    var currentRow = [Int]()
    var statusSelect = "show"
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath){
        // Find phone is exist or not in phoneSelect
        let data = dataMember[idMember[indexPath.row]]
        var status = ""
        for index in phoneSelect {
            if index == data["id"].string! {
                status = "ada"
            }
        }
        
        // add or remove phone in phoneSelect
        let selectedRowIndex = indexPath.row
        if status == "" {
            //Select
            self.statusSelect = "show"
            phoneSelect.append(data["id"].string!)
            nameAdminSelect.append(data["name"].string!)
            avaAdminSelect.append(data["avatar"].string!)
            currentRow.append(selectedRowIndex)
        }else{
            //Deselect
            self.statusSelect = "hide"
            let idData = self.phoneSelect.index(of: data["id"].string!)
            phoneSelect.remove(at: idData!)
            let inx = self.currentRow.index(of: indexPath.row)
            currentRow.remove(at: inx!)
        }
        tbData.reloadData()
    }
    
    func rightMenu(){
        let doneMenuButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(SetAdminController.doneTapped))
        let searchMenuButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(SetAdminController.searchTapped))
        self.navigationItem.setRightBarButtonItems([doneMenuButtonItem], animated: true)
    }
    
    func doneTapped(_ sender:UIButton){
//        self.performSegueWithIdentifier("showSearchGroup", sender: self)
        print("Done")
        if self.dari == "group" {
            makeSerializePost()
        }else{
            self.performSegue(withIdentifier: "goBackToCreateRoom", sender: self)
        }
    }
    
    var nameAdminSelect = [String]()
    var avaAdminSelect = [String]()
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goBackToCreateRoom" {
            let conn = segue.destination as! CreateChannelController
            conn.idAdminSelect = self.phoneSelect
            conn.nameAdminSelect = self.nameAdminSelect
            conn.avaAdminSelect = self.avaAdminSelect
        }else if segue.identifier == "goBackToAboutGroup" {
            let conn = segue.destination as! EditGroupController
            conn.idMember = self.phoneSelect
            conn.countAdmin = self.phoneSelect.count
        }
    }

    func searchTapped(_ sender:UIButton){
//        self.performSegueWithIdentifier("showSearchGroup", sender: self)
        print("Search")
    }
    
    func makeSerializePost(){
        var dataId = [String:AnyObject]()
        var arrayIdInput = [NSString]()
        for id in self.phoneSelect {
            dataId = [
                "id" : id as AnyObject
            ]
            let data = try! JSONSerialization.data(withJSONObject: dataId, options: .prettyPrinted)
            let stringData = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            arrayIdInput += [stringData!]
        }
        
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_group" : self.idGroup as AnyObject]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        let strJason = String(jsonString!)
        let jasonData = String(strJason.characters.dropLast())
        let passData = ("\(jasonData),\n\"id\" : {\"admin\" : \(arrayIdInput)\n} }")
        
        makeBase64Post(passData as NSString)
    }
    
    func makeBase64Post(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postData(base64Encoded)
        }
    }
    
    func postData(_ message: String){
        let urlString = Config().urlUpdate + "invite_user_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(token)
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                //Success
                print(JSON(value))
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.performSegue(withIdentifier: "goBackToAboutGroup", sender: self)
        }
    }
    
}
