//
//  SetNotifCell.swift
//  Groupin
//
//  Created by Macbook pro on 11/23/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class SetNotifCell: UITableViewCell {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgNotif: UIImageView!
    @IBOutlet weak var imgRing: UIImageView!
}