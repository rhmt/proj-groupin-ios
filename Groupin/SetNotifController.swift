//
//  SetNotifController.swift
//  Groupin
//
//  Created by Macbook pro on 8/9/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class SetNotifController: UIViewController, UIPopoverPresentationControllerDelegate {
    
    @IBOutlet weak var btnSetAll: UIButton!
    @IBOutlet weak var tbGroup: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbGroup.tableFooterView = UIView()
        self.tabBarController?.tabBar.isHidden = true
        
        //Rounded Button
        btnSetAll.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
        btnSetAll.layer.cornerRadius = 5
        btnSetAll.layer.borderWidth = 1
        btnSetAll.layer.borderColor = UIColor.lightGray.cgColor
        
        loadLocal()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    var dataData = [tb_group]()
    func loadLocal(){
        dataData = DBHelper.getAllGroup()
        if self.dataData.count > 0 {
            self.tbGroup.isHidden = false
            self.btnSetAll.isHidden = false
            self.lblNoData.isHidden = true
        }else{
            self.tbGroup.isHidden = true
            self.btnSetAll.isHidden = true
            self.lblNoData.isHidden = false
        }
        self.tbGroup.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataData.count
    }
    
    var id = [String]()
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "setNotifCell", for: indexPath) as! SetNotifCell
        
        let data = dataData[indexPath.row]
        if data.avatar != "" {
            cell.imgAva.loadImageUsingCacheWithUrlString(data.avatar)
        }else{
            cell.imgAva.image = UIImage(named: "dafault-ava-group")
        }
        cell.imgAva.layer.cornerRadius = 2
        cell.imgAva.clipsToBounds = true
        cell.lblName.text = data.name
        
        if data.is_notif == 1 {
            cell.imgNotif.image = UIImage(named: "ico_notif_on")
        }else{
            cell.imgNotif.image = UIImage(named: "ico_notif_off")
        }
        if data.is_sound == 1 {
            cell.imgRing.image = UIImage(named: "ico_ring_on")
        }else{
            cell.imgRing.image = UIImage(named: "ico_ring_off")
        }
        
        let tapNotif = UITapGestureRecognizer(target: self, action: #selector(SetNotifController.tapNotif(_:)))
        tapNotif.numberOfTapsRequired = 1
        cell.imgNotif.isUserInteractionEnabled = true
        cell.imgNotif.addGestureRecognizer(tapNotif)
        
        let tapRing = UITapGestureRecognizer(target: self, action: #selector(SetNotifController.tapRing(_:)))
        tapRing.numberOfTapsRequired = 1
        cell.imgRing.isUserInteractionEnabled = true
        cell.imgRing.addGestureRecognizer(tapRing)
        
        return cell
    }
    
    func tapNotif(_ sender: UITapGestureRecognizer){
        var isNotif = 0
        
        //Change image
        let imgData = sender.view as! UIImageView
        if imgData.image == UIImage(named: "ico_notif_on") {
            imgData.image = UIImage(named: "ico_notif_off")
            isNotif = 0
        }else{
            imgData.image = UIImage(named: "ico_notif_on")
            isNotif = 1
        }
        
        //Get IndexPath
        let cell = sender.view?.superview?.superview as! SetNotifCell
        let indexPath = tbGroup.indexPath(for: cell)
        print(indexPath!)
        
        //Change Data in db
        let model = try! Realm().objects(tb_group.self).filter("id = %@", self.dataData[indexPath!.row].id).first!
        try! Realm().write {
            model.is_notif = isNotif
        }
    }
    
    func tapRing(_ sender: UITapGestureRecognizer){
        var isSound = 0
        
        let imgData = sender.view as! UIImageView
        if imgData.image == UIImage(named: "ico_ring_on") {
            imgData.image = UIImage(named: "ico_ring_off")
            isSound = 0
        }else{
            imgData.image = UIImage(named: "ico_ring_on")
            isSound = 1
        }
        
        //Get IndexPath
        let cell = sender.view?.superview?.superview as! SetNotifCell
        let indexPath = tbGroup.indexPath(for: cell)
        print(indexPath!)
        
        //Change Data in db
        let model = try! Realm().objects(tb_group.self).filter("id = %@", self.dataData[indexPath!.row].id).first!
        try! Realm().write {
            model.is_sound = isSound
        }
    }
    
    @IBAction func btnSetAllTapped(_ sender: AnyObject) {
        let alert:UIAlertController=UIAlertController(title: "Choose Settings", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.modalPresentationStyle = .popover
        
        let onAction = UIAlertAction(title: "Notification and sound on", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.setNotifAll(isNotif: 1, isSound: 1)
            self.tbGroup.reloadData()
        }
        let offAction = UIAlertAction(title: "Notification and sound off", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.setNotifAll(isNotif: 0, isSound: 0)
            self.tbGroup.reloadData()
        }
        let bothAction = UIAlertAction(title: "Notification on and sound off", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.setNotifAll(isNotif: 1, isSound: 0)
            self.tbGroup.reloadData()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        alert.addAction(onAction)
        alert.addAction(offAction)
        alert.addAction(bothAction)
        alert.addAction(cancelAction)
        
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender as? UIView
            presenter.sourceRect = sender.bounds;
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func setNotifAll(isNotif: Int, isSound: Int){
        let model = try! Realm().objects(tb_group.self)
        for data in model {
            try! Realm().write {
                data.is_notif = isNotif
                data.is_sound = isSound
            }
        }
    }
}
