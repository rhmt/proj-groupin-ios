//
//  SetPasswordController.swift
//  Groupin
//
//  Created by Macbook pro on 7/25/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class SetPasswordController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var btnPassword: UIButton!
    
    var phone = ""
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtPassword.delegate = self
        self.txtConfirmPassword.delegate = self
        
        print(phone)
        //Rounded Button
        btnPassword.backgroundColor = UIColor(red: 83, green: 186, blue: 32)
        btnPassword.layer.cornerRadius = 5
        btnPassword.layer.borderWidth = 1
        btnPassword.layer.borderColor = UIColor.lightGray.cgColor
        
        NotificationCenter.default.addObserver(self, selector: #selector(SetPasswordController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SetPasswordController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSetProfile" {
            let conn = segue.destination as! SetProfileController
            conn.phone = self.phone
            conn.password = txtPassword.text!
            conn.token = self.token
            conn.navigationItem.title = "Set Profile"
        }
    }
    
    @IBAction func btnOkeTapped(_ sender: AnyObject) {
        if txtPassword.text == "" {
            let alert = UIAlertController(title: "Error", message: "Password cannot null", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }else{
            if txtPassword.text == txtConfirmPassword.text {
                if txtPassword.text?.characters.count > 7 {
                    self.performSegue(withIdentifier: "showSetProfile", sender: self)
                }else{
                    let alert = UIAlertController(title: "Error", message: "Minimal 8 characters", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                        print("Minimal 8 characters")
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                let alert = UIAlertController(title: "Error", message: "Password Not Match", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    print("Password not match")
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    var activeField: UITextField?
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            let possKeyboard = self.view.frame.height - keyboardSize!.height - activeField!.frame.height
            if activeField?.frame.origin.y > possKeyboard {
//                if (!CGRectContainsPoint(aRect, activeField!.frame.origin)){
                    if view.frame.origin.y == 0{
                        self.view.frame.origin.y -= keyboardSize!.height
                    }
//                }
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
            else {
                
            }
        }
    }
}
