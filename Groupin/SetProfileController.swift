//
//  SetProfileController.swift
//  Groupin
//
//  Created by Macbook pro on 7/25/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import DropDown
import SwiftyJSON
import Alamofire
import Photos
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class SetProfileController: UIViewController, UIPopoverPresentationControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtGender: UIButton!
    @IBOutlet weak var birthdayDay: UIButton!
    @IBOutlet weak var birthdayMonth: UIButton!
    @IBOutlet weak var birthdayYear: UIButton!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var tbInterest: UITableView!
    @IBOutlet weak var btnListInterest: UIButton!
    @IBOutlet weak var btnAddInterest: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    
    //Contraint
    @IBOutlet weak var heightTable: NSLayoutConstraint!
    @IBOutlet weak var conSpaceBotView: NSLayoutConstraint!
    
    var phone = ""
    var password = ""
    var token = ""
    
    let imgPicker = UIImagePickerController()
    var imgData = Data()
    
    var idCountry = 0
    var idCity = ""
    
    let listGender = DropDown()
    let listDay = DropDown()
    let listMonth = DropDown()
    let listYear = DropDown()
    let listCountry = DropDown()
    let listCity = DropDown()
    let listInterest = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.listGender,
            self.listDay,
            self.listMonth,
            self.listYear,
            self.listCountry,
            self.listCity,
            self.listInterest
        ] }() 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getDataInterest()
        
        self.txtName.delegate = self
        self.txtEmail.delegate = self
        self.txtCountry.delegate = self
        self.txtCity.delegate = self
        self.scrollView.delegate = self
        
        imgPicker.delegate = self
        setupDropDown()
        
        //Rounded Button
        btnSave.backgroundColor = UIColor(red: 83, green: 186, blue: 32)
        btnSave.layer.cornerRadius = 5
        btnSave.layer.borderWidth = 1
        btnSave.layer.borderColor = UIColor.lightGray.cgColor
        
        //Input Ava
        let tapPhoto = UITapGestureRecognizer(target: self, action: #selector(SetProfileController.fotoTapped(_:)))
        tapPhoto.numberOfTapsRequired = 1
        imgAva.isUserInteractionEnabled = true
        imgAva.addGestureRecognizer(tapPhoto)
        //Layout Ava
        imgAva.layer.cornerRadius = imgAva.frame.width / 2
        
        self.tbInterest.rowHeight = UITableViewAutomaticDimension
        self.tbInterest.estimatedRowHeight = 50.0
        self.heightTable.constant = 100.0
        
        NotificationCenter.default.addObserver(self, selector: #selector(SetProfileController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SetProfileController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func fotoTapped(_ sender: UITapGestureRecognizer){
        BrowseFoto(UIButton.init())
    }
    
    func BrowseFoto(_ sender: UIButton) {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.modalPresentationStyle = .popover
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.cameraTapped()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.libraryTapped()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds;
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // Camera
    func cameraTapped(){
        if Config().checkAccessCamera() == false {
            print("access denied")
            let msg = "Please allow the app to access your camera through the Settings."
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                if granted == true {
                    // User granted
                    Config().alertForAllowAccess(self, msg: msg)
                }else{
                    // User Rejected
                    Config().alertForAllowAccess(self, msg: msg)
                }
            });
        }else{
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
            {
                self.imgPicker.delegate = self;
                
                self.imgPicker.sourceType = UIImagePickerControllerSourceType.camera
                self.imgPicker.mediaTypes = [kUTTypeImage as String]
                self.imgPicker.allowsEditing = true
                self.present(self.imgPicker, animated: true, completion: nil)
            }
            else
            {
                libraryTapped()
            }
        }
    }
    
    // Library Image
    func libraryTapped(){
        if Config().checkAccessPhotos() == false {
            print("access denied")
            let msg = "Please allow the app to access your photos through the Settings."
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.denied {
                    print("denied")
                    Config().alertForAllowAccess(self, msg: msg)
                }else{
                    print("another")
                    Config().alertForAllowAccess(self, msg: msg)
                }
            })
        }else{
            self.imgPicker.allowsEditing = true
            self.imgPicker.sourceType = .photoLibrary
            present(imgPicker, animated: true, completion: nil)
        }
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    var postImageName: String! = ""
    var postImageUrl: String! = ""
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var pickedImage: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            pickedImage = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            pickedImage = originalImage
        }
        
        imgAva.contentMode = .scaleToFill
        imgAva.image = pickedImage
        
        let imageName = UUID().uuidString
        postImageName = imageName
        let imagePath = getDocumentsDirectory().appendingPathComponent(imageName+".jpg")
        postImageUrl = imagePath
        
        let jpegData = UIImageJPEGRepresentation(pickedImage!, 0.8)
        try? jpegData!.write(to: URL(fileURLWithPath: imagePath), options: [.atomic])
        
        let imageSize = jpegData?.count
        print("size of image in KB: ", imageSize! / 1024)
        
        self.imgData = jpegData!
        
        dismiss(animated: true, completion: nil)
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnGenderTapped(_ sender: AnyObject) {
        listGender.show()
    }
    
    @IBAction func btnBirthdayDayTapped(_ sender: AnyObject) {
        listDay.show()
    }
    
    @IBAction func btnBirthdayMonthTapped(_ sender: AnyObject) {
        listMonth.show()
    }
    
    @IBAction func btnBirthdayYearTapped(_ sender: AnyObject) {
        listYear.show()
    }
    
    var idCountrySelected = 0
    var nameCountrySelected = ""
    var listDataCountry = [""]
    @IBAction func txtCountryTyping(_ sender: AnyObject) {
        let urlString = Config().urlMain + "get_country"
        let word = txtCountry.text
        Alamofire.request(urlString, method: .post, parameters: ["like" : word!]).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                self.listDataCountry.removeAll()
                for (key, _) in jsonData{
                    let dataCountry = jsonData[key]["country"]
                    self.listDataCountry.append(String(describing: dataCountry))
                }
            }else{
                print("Something Went Wrong..")
            }
        }.responseData { Response in
            if self.txtCountry.text != "" {
                self.setupCountry()
                self.listCountry.show()
            }else{
                self.listCountry.hide()
            }
        }
    }
    
    var idCitySelected = 0
    var nameCitySelected = ""
    var listDataCity = [""]
    @IBAction func txtCityTyping(_ sender: AnyObject) {
        let urlString = Config().urlMain + "get_city/" + String(idCountrySelected)
        let word = txtCity.text
        Alamofire.request(urlString, method: .post, parameters: ["like" : word!]).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                self.listDataCity.removeAll()
                for (key, value) in jsonData{
                    self.idCitySelected = Int(key)!
                    self.listDataCity.append(String(describing: value))
                }
            }else{
                print("Something Went Wrong..")
            }
        }.responseData { Response in
            if self.txtCity.text != "" {
                self.setupCity()
                self.listCity.show()
            }else{
                self.listCity.hide()
            }
        }
    }
    
    func getIdCountryOrCity(_ params: String){
        var urlString = ""
        var like = ""
        var keySave = ""
        
        if params == "country" {
            urlString = Config().urlMain + "get_country"
            like = self.nameCountrySelected
        }else{
            urlString = Config().urlMain + "get_city/" + String(self.idCountrySelected)
            like = self.nameCitySelected
        }
        Alamofire.request(urlString, method: .post, parameters: ["like" : like]).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                for (key, _) in jsonData{
                    keySave = key
                }
            }else{
                print("Something Went Wrong..")
            }
        }.responseData { Response in
            if params == "country" {
                self.idCountrySelected = Int(keySave)!
                print(self.idCountrySelected)
            }else{
                self.idCitySelected = Int(keySave)!
                print(self.idCitySelected)
            }
        }
    }
    
    //Dropdown Country and City
    func setupCountry() {
        listCountry.anchorView = txtCountry
        listCountry.bottomOffset = CGPoint(x: 0, y: txtCountry.bounds.height)
        listCountry.direction = .bottom
        listCountry.dataSource = listDataCountry
        
        listCountry.selectionAction = { [unowned self] (index, item) in
            self.txtCountry.text = item
            self.txtCountry.resignFirstResponder()
            self.nameCountrySelected = item
            self.getIdCountryOrCity("country")
        }
    }
    func setupCity() {
        listCity.anchorView = txtCity
        listCity.bottomOffset = CGPoint(x: 0, y: txtCity.bounds.height)
        listCity.direction = .bottom
        listCity.dataSource = listDataCity
        
        listCity.selectionAction = { [unowned self] (index, item) in
            self.txtCity.text = item
            self.txtCity.resignFirstResponder()
            self.nameCitySelected = item
            self.getIdCountryOrCity("city")
        }
    }
    
    var listDataInterest = [""]
    var listDataIdInterest = [""]
    func getDataInterest(){
        print("getDataInterest")
        let urlString = Config().urlGroup + "get_group_type"
        Alamofire.request(urlString, method: .post).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
                for (key, value) in jsonData{
                    if let modelUpdate = try! Realm().objects(dataInterest.self).filter("id_interest = %@", Int(key)!).first {
                        print("update")
                        try! Realm().write {
                            modelUpdate.interest = value.stringValue
                        }
                    }else{
                        print("insert")
                        let modelInsert = dataInterest()
                        modelInsert.id_interest = Int(key)!
                        modelInsert.interest = value.stringValue
                        DBHelper.insert(modelInsert)
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                let data = try! Realm().objects(dataInterest.self)
                self.listDataInterest.removeAll()
                self.listDataIdInterest.removeAll()
                for item in data {
                    self.listDataIdInterest.append(String(item.id_interest))
                    self.listDataInterest.append(item.interest)
                }
        }
        
        let data = try! Realm().objects(dataInterest.self)
        self.listDataInterest.removeAll()
        self.listDataIdInterest.removeAll()
        for item in data {
            self.listDataIdInterest.append(String(item.id_interest))
            self.listDataInterest.append(item.interest)
        }
    }
    
    var idInterestSelected = [0,0]
    @IBAction func btnInterestTapped(_ sender: AnyObject) {
        self.refreshInterestHasSelected()
        
        let button = sender
        let view = button.superview!
        let cell = view!.superview as! listInterestCell
        let indexPath = self.tbInterest.indexPath(for: cell)
        
        self.listInterest.anchorView = cell.btnListInterest
        self.listInterest.bottomOffset = CGPoint(x: 0, y: cell.btnListInterest.bounds.height)
        self.listInterest.dataSource = self.listDataInterest
        
        self.listInterest.selectionAction = { [unowned cell] (index, item) in
            cell.btnListInterest.setTitle(item, for: .normal)
            let idInterest = self.listDataInterest.index(of: item)
            let inputInterest = self.listDataIdInterest[idInterest!]
            self.idInterestSelected[(indexPath?.row)!] = Int(inputInterest)!
            if item == "" {
                cell.btnListInterest.setTitleColor(UIColor.gray, for: UIControlState.normal)
                cell.txtInterest.isEnabled = true
                cell.txtInterest.resignFirstResponder()
            }else{
                cell.btnListInterest.setTitleColor(UIColor.black, for: UIControlState.normal)
                cell.txtInterest.isEnabled = true
                cell.txtInterest.becomeFirstResponder()
            }
        }
        
        self.listInterest.show()
    }
    
    func refreshInterestHasSelected(){
        let data = try! Realm().objects(dataInterest.self)
        self.listDataInterest.removeAll()
        self.listDataIdInterest.removeAll()
        for item in data {
            self.listDataIdInterest.append(String(item.id_interest))
            self.listDataInterest.append(item.interest)
        }
        for data in idInterestSelected {
            if data > 0 {
                if let inx = self.listDataIdInterest.index(of: String(data)) {
                    self.listDataIdInterest.remove(at: inx)
                    self.listDataInterest.remove(at: inx)
                }
            }
        }
    }
    
    var detailInterestSelected = ["",""]
    @IBAction func txtInterestEditingEnd(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview as! listInterestCell
        let indexPath = self.tbInterest.indexPath(for: cell)
        
        detailInterestSelected[(indexPath?.row)!] = cell.txtInterest.text!
    }
    
    var interest = ["",""]
    @IBAction func btnAddInterestTapped(_ sender: AnyObject) {
        interest.append("")
        idInterestSelected.append(0)
        detailInterestSelected.append("")
        tbInterest.reloadData()
        
        // Possition
        self.tbInterest.rowHeight = UITableViewAutomaticDimension
        self.tbInterest.estimatedRowHeight = 50.0
        let tbCellHeight = self.tbInterest.estimatedRowHeight
        heightTable.constant = heightTable.constant + tbCellHeight
        
    }
    
    // Table List Interest
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listInterestCell", for: indexPath) as! listInterestCell
        
        cell.txtInterest.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    var alertLoading: UIAlertController!
    @IBAction func btnSaveTapped(_ sender: AnyObject) {
        self.alertLoading = UIAlertController(title: "Processing", message: "Please wait ...", preferredStyle: UIAlertControllerStyle.alert)
        DispatchQueue.main.async(execute: {
            self.present(self.alertLoading, animated: true, completion: nil)
        })
        
        let isEmail = Config().isValidEmail(txtEmail.text!)
        if isEmail == false {
            self.msgInvalid = "Email is Invalid"
        }
//        if birthdayDay.titleLabel == "30" && birthdayMonth.titleLabel == "02" {
//            self.msgInvalid = "Invalid BirthDay Date"
//        }
//        if birthdayDay.titleLabel == "31" && birthdayMonth.titleLabel == "02" {
//            self.msgInvalid = "Invalid BirthDay Date"
//        }
//        if birthdayDay.titleLabel == "31" && birthdayMonth.titleLabel == "04" {
//            self.msgInvalid = "Invalid BirthDay Date"
//        }
//        if birthdayDay.titleLabel == "31" && birthdayMonth.titleLabel == "06" {
//            self.msgInvalid = "Invalid BirthDay Date"
//        }
//        if birthdayDay.titleLabel == "31" && birthdayMonth.titleLabel == "09" {
//            self.msgInvalid = "Invalid BirthDay Date"
//        }
//        if birthdayDay.titleLabel == "31" && birthdayMonth.titleLabel == "11" {
//            self.msgInvalid = "Invalid BirthDay Date"
//        }
        self.alertInvalid()
    }
    
    var msgInvalid = ""
    func alertInvalid(){
        if msgInvalid != "" {
            let alert = UIAlertController(title: "Info", message: msgInvalid, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }else{
            makeSerialize()
        }
    }
    
    func makeSerialize(){
        let emailPost = txtEmail.text!
        let namePost = txtName.text!
        var gender = ""
        if txtGender.titleLabel?.text == "Male"{
            gender = "1"
        }else{gender = "2"}
        let tglLahir = (birthdayYear.titleLabel?.text)! + "-" + (birthdayMonth.titleLabel?.text)! + "-" + (birthdayDay.titleLabel?.text)!
        
        var arrayData = [String:AnyObject]()
        var listInterest = [String:AnyObject]()
        var arrayInterest = [NSString]()
        var i = 0
        for id in self.idInterestSelected {
            listInterest = [
                "fk_interest" : id as AnyObject,
                "interest_detail" : self.detailInterestSelected[i] as AnyObject
            ]
            let data = try! JSONSerialization.data(withJSONObject: listInterest, options: .prettyPrinted)
            let stringData = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            arrayInterest += [stringData!]
            i += 1
        }
        
        arrayData = [
            "fk_country" : self.idCountrySelected as AnyObject,
            "fk_city" : self.idCitySelected as AnyObject,
            "phone" : self.phone as AnyObject,
            "password" : self.password as AnyObject,
            "avatar" : self.postImageName as AnyObject,
            "name" : namePost as AnyObject,
            "email" : emailPost as AnyObject,
            "gender" : gender as AnyObject,
            "birth_date" : tglLahir as AnyObject]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        let strJason = String(jsonString!)
        let jasonData = String(strJason.characters.dropLast())
        let passData = ("\(jasonData),\n\"interest\" : \(arrayInterest)\n}")
        
        makeBase64(passData as NSString)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            print("Encoded:  \(base64Encoded)")
            
            if let base64Decoded = Data(base64Encoded: base64Encoded, options:   NSData.Base64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: String.Encoding.utf8.rawValue) })
            {
                print("Decoded:  \(base64Decoded!)")
            }
            
            postData(base64Encoded)
        }
    }
    
    var msgError = ""
    var idUser = 0
    func postData(_ message: String){
        let urlString = Config().urlUser + "register_member"
        let token = Config().md5(string: message)
        print(token)
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        
        //Upload Image
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            
            if self.postImageUrl != "" {
                multipartFormData.append(URL(fileURLWithPath: self.postImageUrl), withName: "avatar")
            }
            
            for (key, value) in parameters {
                multipartFormData.append( value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: urlString,
        encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON {
                    response in
                    
                    if let value = response.result.value {
                        let jsonData = JSON(value)
                        print(jsonData)
                        if jsonData["code"] == "1"{
                            self.msgError = ""
                            self.idUser = jsonData["msg"].int!
                        }else{
                            self.msgError = jsonData["msg"].string!
                        }
                        self.alertLoading.dismiss(animated: true, completion: self.alertStatus)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        })
    }
    
    func alertStatus(){
        if msgError != "" {
            let alert = UIAlertController(title: "Info", message: msgError, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }else{
            makeSerializeLogin()
        }
    }
    
    func makeSerializeLogin(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "user" : self.phone as AnyObject,
            "password" : self.password as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Login(jsonString!)
    }
    
    func makeBase64Login(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            prosesLogin(base64Encoded)
        }
    }
    
    var doneLogin = 0
    func prosesLogin(_ message: String){
        let urlString = Config().urlUser + "login"
        let token = Config().md5(string: message)
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                self.doneLogin = 1
                let jsonData = JSON(value)
                
                // Session
                self.modelSess.id = jsonData["msg"]["id"].intValue
                self.modelSess.token = jsonData["msg"]["token"].string!
                
                // tb_user
                self.modelUser.id = jsonData["msg"]["id"].intValue
                self.modelUser.phone = jsonData["msg"]["user"].string!
                self.modelUser.name = jsonData["msg"]["name"].string!
                self.modelUser.avatar = jsonData["msg"]["avatar"].string!
                self.modelUser.status = jsonData["msg"]["msg_status"].string!
                self.modelUser.gender = jsonData["msg"]["gender"].string!
                self.modelUser.tgl_lahir = jsonData["msg"]["tgl_lahir"].string!
                self.modelUser.country = jsonData["msg"]["name_country"].string!
                self.modelUser.city = jsonData["msg"]["name_city"].string!
                self.modelUser.tgl_join = jsonData["msg"]["tgl_join"].string!
                self.modelUser.hanya_join_main_group = jsonData["msg"]["hanya_join_main_group"].string!
                self.modelUser.valid_email = "2"
                
                //Interest
                let interest = jsonData["msg"]["interest"]
                for (key, _) in interest {
                    let dataInt = jsonData["msg"]["interest"][key]
                    self.modelInterest.id_category = dataInt["kode_interest"].intValue
                    self.modelInterest.category = dataInt["group_interest"].string!
                    self.modelInterest.detail = dataInt["detail_interest"].string!
                }
            }else{
                self.doneLogin = 2
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                if self.doneLogin == 1 {
                    self.createSession()
                    DispatchQueue.main.async(execute: { 
                        self.performSegue(withIdentifier: "showRecommendGroup", sender: self)
                    })
                }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showRecommendGroup" {
            let conn = segue.destination as! RecommendGroupController
            conn.idUser = String(self.idUser)
        }
    }
    
    // SESSION
    let modelSess = session()
    let modelUser = tb_user()
    let modelInterest = interestUser()
    func createSession() {
        //Delete All
        var data = [session]()
        data = DBHelper.getAllSession()
        if data.count == 0 {
            print("Doesn't Exist")
        }else{
            data = DBHelper.getAllSession()
            try! Realm().write {
                try! Realm().delete(data[0])
            }
        }
        
        //Create Session
//        let lastLogin: NSNumber = NSNumber(Int(Date().timeIntervalSince1970))
//        modelSess.last_login = Int(lastLogin)
        DBHelper.insert(modelSess)
        
        //tb_user
        DBHelper.insert(modelUser)
        
        //Interest
        DBHelper.insert(modelInterest)
    }
    
    // DropDown Function
    func setupDropDown(){
        setupGender()
        setupDay()
        setupMonth()
        setupYear()
    }
    func setupGender() {
        listGender.anchorView = txtGender
        listGender.bottomOffset = CGPoint(x: 0, y: txtGender.bounds.height)
        listGender.dataSource = [
            "Male",
            "Female"
        ]
        listGender.selectionAction = { [unowned self] (index, item) in
            self.txtGender.setTitle(item, for: UIControlState())
            if item == "Gender" {
                self.txtGender.setTitleColor(UIColor.gray, for: UIControlState())
            }else{
                self.txtGender.setTitleColor(UIColor.black, for: UIControlState())
            }
            
        }
    }
    func setupDay() {
        listDay.anchorView = birthdayDay
        listDay.bottomOffset = CGPoint(x: 0, y: birthdayDay.bounds.height)
        for i in 1...31 {
            if i < 10 {
                listDay.dataSource.append("0\(i)")
            }else{
                listDay.dataSource.append("\(i)")
            }
        }
        
        listDay.selectionAction = { [unowned self] (index, item) in
            self.birthdayDay.setTitle(item, for: UIControlState())
            if item == "" {
                self.birthdayDay.setTitleColor(UIColor.gray, for: UIControlState())
            }else{
                self.birthdayDay.setTitleColor(UIColor.black, for: UIControlState())
            }
            
        }
    }
    func setupMonth() {
        listMonth.anchorView = birthdayMonth
        listMonth.bottomOffset = CGPoint(x: 0, y: birthdayMonth.bounds.height)
        for i in 1...12 {
            if i < 10 {
                listMonth.dataSource.append("0\(i)")
            }else{
                listMonth.dataSource.append("\(i)")
            }
        }
        
        listMonth.selectionAction = { [unowned self] (index, item) in
            self.birthdayMonth.setTitle(item, for: UIControlState())
            if item == "" {
                self.birthdayMonth.setTitleColor(UIColor.gray, for: UIControlState())
            }else{
                self.birthdayMonth.setTitleColor(UIColor.black, for: UIControlState())
            }
            
        }
    }
    func setupYear() {
        let currentYearInt = (Calendar.current as NSCalendar).component(NSCalendar.Unit.year, from: Date())
        let minYear = currentYearInt - 16
        let maxYear = minYear - 30
        
        listYear.anchorView = birthdayYear
        listYear.bottomOffset = CGPoint(x: 0, y: birthdayYear.bounds.height)
        for i in (maxYear...minYear).reversed() {
            listYear.dataSource.append("\(i)")
        }
        
        listYear.selectionAction = { [unowned self] (index, item) in
            self.birthdayYear.setTitle(item, for: UIControlState())
            if item == "" {
                self.birthdayYear.setTitleColor(UIColor.gray, for: UIControlState())
            }else{
                self.birthdayYear.setTitleColor(UIColor.black, for: UIControlState())
            }
            
        }
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if activeField != nil{
//            activeField?.resignFirstResponder()
//        }
//    }
    
    var activeField: UITextField?
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            self.conSpaceBotView.constant = keyboardSize!.height
//            let possKeyboard = self.view.frame.height - keyboardSize!.height - activeField!.frame.height
//            if activeField?.frame.origin.y > possKeyboard {
//                if (!CGRectContainsPoint(aRect, activeField!.frame.origin)){
//                    if view.frame.origin.y == 0{
//                        self.view.frame.origin.y -= keyboardSize!.height
//                    }
//                }
//            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.conSpaceBotView.constant = 0
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}
