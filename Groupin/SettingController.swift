//
//  SettingController.swift
//  Groupin
//
//  Created by Macbook pro on 7/27/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class SettingController: UIViewController {
    
    var TableArray = [String]()
    @IBOutlet weak var swOnlyJoinMain: UISwitch!
    @IBOutlet weak var tbSettings: UITableView!
    
    var notif = ""
    var block = ""
    var password = ""
    var delete = ""
    var use = ""
    
    var idUser = 0
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        self.tbSettings.tableFooterView = UIView()
        
        TableArray = ["Set Notification","Blocked Contacts", "Set Password", "Delete Account", "Terms of Use", "Log Out"]
        
        let sess = try! Realm().objects(session.self).first!
        idUser = sess.id
        token = sess.token
        
        let statusJoinMain = try! Realm().objects(tb_user.self).first!
        if statusJoinMain.hanya_join_main_group == "1" {
            swOnlyJoinMain.isOn = true
        }else{
            swOnlyJoinMain.isOn = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewDidDisappear(true)
//        self.tabBarController?.tabBar.isHidden = false
//    }
    
    var statusJoin = 0
    @IBAction func swOnlyJoinMainAction(_ sender: UISwitch) {
        if sender.isOn {
            self.statusJoin = 1
        }else{
            self.statusJoin = 2
        }
        self.postStatusJoin()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if self.notif == "true" {
            let conn = segue.destination as! SetNotifController
            conn.navigationItem.title = "Set Notification"
            self.notif = ""
        }
        if self.block == "true" {
            self.block = ""
            let conn = segue.destination as! BlockedContactController
            conn.navigationItem.title = "Blocked Contacts"
        }
        if self.password == "true" {
            let conn = segue.destination as! ChangePasswordController
            conn.navigationItem.title = "Set Password"
            self.password = ""
        }
        if self.delete == "true" {
            let conn = segue.destination as! DeleteAccountController
            conn.navigationItem.title = "Delete Account"
            self.delete = ""
        }
        if self.use == "true" {
            let conn = segue.destination as! TermsUserController
            conn.navigationItem.title = "Terms of Use"
            self.use = ""
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TableArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableArray[indexPath.row], for: indexPath) as UITableViewCell
        
        cell.textLabel?.text = TableArray[indexPath.row]
        if TableArray[indexPath.row] != "Log Out" {
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        if TableArray[indexPath.row] == "Set Notification" {
            self.notif = "true"
            self.performSegue(withIdentifier: "showNotif", sender: self)
        }
        if TableArray[indexPath.row] == "Blocked Contacts" {
            self.block = "true"
            self.performSegue(withIdentifier: "showBlockedContact", sender: self)
        }
        if TableArray[indexPath.row] == "Set Password" {
            self.password = "true"
            self.performSegue(withIdentifier: "showSetPassword", sender: self)
        }
        if TableArray[indexPath.row] == "Delete Account" {
            self.delete = "true"
            self.performSegue(withIdentifier: "showDelete", sender: self)
        }
        if TableArray[indexPath.row] == "Terms of Use" {
            self.use = "true"
            self.performSegue(withIdentifier: "showTerms", sender: self)
        }
        if TableArray[indexPath.row] == "Log Out" {
            let alert = UIAlertController(title: "Logout", message: "Are you sure want to Logout?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                self.logout()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func postStatusJoin(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "status" : self.statusJoin as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64join(jsonString!)
    }
    
    func makeBase64join(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postDataJoin(base64Encoded)
        }
    }
    
    func postDataJoin(_ message: String){
        let urlString = Config().urlUser + "only_join_main_group"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                if jsonData["code"].string == "1" {
                    print(jsonData["msg"].string!)
                    let model = try! Realm().objects(tb_user.self).first!
                    try! Realm().write {
                        model.hanya_join_main_group = String(self.statusJoin)
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                
        }
    }
    
    func logout(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    func getData(_ message: String){
        let urlString = Config().urlUser + "logout"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                if jsonData["code"].string == "1" {
                    Config().BackToLogin(self)
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                
        }
    }
    
}
