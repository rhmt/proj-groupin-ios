//
//  SlideController.swift
//  Groupin
//
//  Created by Macbook pro on 8/16/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class SlideController: UIViewController {
    
    // create swipe gesture
    let swipeGestureLeft = UISwipeGestureRecognizer()
    let swipeGestureRight = UISwipeGestureRecognizer()
    
    // outlet - page control
    @IBOutlet var myPageControl: UIPageControl!
    
    // current page number label
    @IBOutlet var titlePageLabel: UILabel!
    @IBOutlet var subtitlePageLabel: UILabel!
    
    @IBOutlet weak var btnStart: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set gesture direction
        self.swipeGestureLeft.direction = UISwipeGestureRecognizerDirection.left
        self.swipeGestureRight.direction = UISwipeGestureRecognizerDirection.right
        
        // add gesture target
        self.swipeGestureLeft.addTarget(self, action: #selector(SlideController.handleSwipeLeft(_:)))
        self.swipeGestureRight.addTarget(self, action: #selector(SlideController.handleSwipeRight(_:)))
        
        // add gesture in to view
        self.view.addGestureRecognizer(self.swipeGestureLeft)
        self.view.addGestureRecognizer(self.swipeGestureRight)
        
        // set current page number label.
        self.setCurrentPageLabel()
        
        //Rounded Button
        btnStart.layer.cornerRadius = 5
        btnStart.layer.borderWidth = 1
        btnStart.layer.borderColor = UIColor.gray.cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Utility function
    
    // increase page number on swift left
    func handleSwipeLeft(_ gesture: UISwipeGestureRecognizer){
        if self.myPageControl.currentPage < 3 {
            self.myPageControl.currentPage += 1
            self.setCurrentPageLabel()
        }
    }
    
    // reduce page number on swift right
    func handleSwipeRight(_ gesture: UISwipeGestureRecognizer){
        if self.myPageControl.currentPage != 0 {
            self.myPageControl.currentPage -= 1
            self.setCurrentPageLabel()
            self.btnStart.isHidden = true
        }
    }
    
    // set current page number label
    fileprivate func setCurrentPageLabel(){
        if self.myPageControl.currentPage == 0 {
            animated()
            self.titlePageLabel.text = "Join or Create"
            self.subtitlePageLabel.text = "your favorite group"
        }else if self.myPageControl.currentPage == 1 {
            animated()
            self.titlePageLabel.text = "Connect"
            self.subtitlePageLabel.text = "with your group"
        }else if self.myPageControl.currentPage == 2 {
            animated()
            self.titlePageLabel.text = "Collect"
            self.subtitlePageLabel.text = "points and get rewards"
        }else if self.myPageControl.currentPage == 3 {
            animated()
            self.titlePageLabel.text = "Are You Ready?"
            self.subtitlePageLabel.text = "to find your group"
            self.btnStart.isHidden = false
        }
    }
    
    @IBAction func btnStartTapped(_ sender: AnyObject) {
        
    }
    
    func animated(){
        //Title
        self.titlePageLabel.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.titlePageLabel.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.titlePageLabel.alpha = 1.0
            self.titlePageLabel.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
        
        //Subtitle
        self.subtitlePageLabel.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.subtitlePageLabel.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.subtitlePageLabel.alpha = 1.0
            self.subtitlePageLabel.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
        
        //Button Start
        self.btnStart.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.btnStart.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.btnStart.alpha = 1.0
            self.btnStart.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
}
