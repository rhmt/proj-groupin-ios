//
//  StaffEventCell.swift
//  Groupin
//
//  Created by Macbook pro on 12/30/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import Foundation
import UIKit

class StaffEventCell: UITableViewCell {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblCreate: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblAt: UILabel!
    @IBOutlet weak var btnScan: UIButton!
}