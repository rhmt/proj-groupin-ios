//
//  StatusCell.swift
//  Groupin
//
//  Created by Macbook pro on 8/2/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class StatusCell: UITableViewCell {
    
    @IBOutlet weak var imgCekAva: UIImageView!
    @IBOutlet weak var imgCekBusy: UIImageView!
    @IBOutlet weak var imgCekOtw: UIImageView!
    @IBOutlet weak var imgCekSleep: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
