//
//  StatusController.swift
//  Groupin
//
//  Created by Macbook pro on 7/27/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class StatusController: UIViewController {
    
    var TableArray = [String]()
    @IBOutlet weak var tbStatus: UITableView!
    
    var idUser = 0
    var token = ""
    
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbStatus.tableFooterView = UIView()
        //self.tabBarController?.tabBar.isHidden = true
        
        //Get Phone from Session
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.token = idSession!.token
        
        let dataUser = try! Realm().objects(tb_user.self).first
        txtStatus.text = dataUser!.status
        
        self.loadLocal()
    }
    
    func loadLocal(){
        self.TableArray.removeAll()
        let data = DBHelper.getAllStatus()
        for item in data {
            self.TableArray.append(item.name)
        }
        if self.paramsJson == 1 {
            self.paramsJson = 0
            self.tbStatus.reloadData()
        }
    }
    
    var paramsJson = 0
    func loadServer(){
        let urlString = Config().urlMain + "get_user_msg_status_default"
        Alamofire.request(urlString, method: .post).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                
                self.paramsJson = 1
                for (_, value) in jsonData {
                    let updateModel = try! Realm().objects(listStatus.self).filter("name = %@", value.string!).first
                    
                    if updateModel == nil{
                        let model = listStatus()
                        model.name = value.string!
                        DBHelper.insert(model)
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.loadLocal()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.loadServer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TableArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell", for: indexPath) as! StatusCell
        
        cell.textLabel?.text = TableArray[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        
        let models = try! Realm().objects(tb_user.self).first
        
        txtStatus.text = TableArray[indexPath.row]
        
        try! Realm().write(){
            models?.status = TableArray[indexPath.row]
        }
        
        makeSerialize()
    }
    
    @IBAction func btnEditTapped(_ sender: AnyObject) {
        btnEdit.isHidden = true
        btnSave.isHidden = false
        txtStatus.isEnabled = true
        txtStatus.isSelected = true
    }
    
    @IBAction func btnSaveTapped(_ sender: AnyObject) {
        btnEdit.isHidden = false
        btnSave.isHidden = true
        txtStatus.isEnabled = false
        
        let models = try! Realm().objects(tb_user.self).first
        try! Realm().write(){
            models?.status = txtStatus.text!
        }
        
        makeSerialize()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "msg_status" : txtStatus.text! as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    func getData(_ message: String){
        let urlString = Config().urlUpdate + "update_status_profile"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                print(jsonData)
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                
        }
    }
    
}
