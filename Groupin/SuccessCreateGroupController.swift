//
//  SuccessCreateGroupController.swift
//  Groupin
//
//  Created by Macbook pro on 7/29/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift

class SuccessCreateGroupController: UIViewController {
    
    var nameGroup = ""
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var lblGroupName: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblGroupName.text = self.nameGroup
        
        //Rounded Button
        btnInvite.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
        btnInvite.layer.cornerRadius = 5
        btnInvite.layer.borderWidth = 1
        btnInvite.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showInviteContact" {
            let conn = segue.destination as! InviteContactController
            conn.navigationItem.title = "Invite Contacts"
            conn.dari = "create"
        }else if segue.identifier == "showGroupHome"{
            let conn = segue.destination as! GroupHomeController
            conn.navigationItem.title = self.nameGroup
            conn.fromCreate = 1
        }
    }
    
    @IBAction func btnInviteTapped(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showInviteContact", sender: self)
    }
    
    @IBAction func btnSkipTapped(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showGroupHome", sender: self)
    }
    
}
