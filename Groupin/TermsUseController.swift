//
//  TermsUseController.swift
//  Groupin
//
//  Created by Macbook pro on 8/9/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire

class TermsUserController: UIViewController {
    
    @IBOutlet weak var txtView: UILabel!
    @IBOutlet weak var heigthView: NSLayoutConstraint!
    var idUser = 0
    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        
        let sess = try! Realm().objects(session.self).first
        self.token = sess!.token
        
        self.loadLocal()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.makeSerialize()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func loadLocal(){
        let data = DBHelper.getAllTerm()
        for item in data {
            self.content = item.content
        }
        self.viewContent()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "code_page" : "terms_of_use" as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    var content = ""
    func getData(_ message: String){
        let urlString = Config().urlPages + "get_page"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let jsonData = JSON(value)
                for (key, _) in jsonData["msg"] {
                    self.content = jsonData["msg"][key]["content"].string!
                }
                if self.content != "" {
                    let model = termOfUse()
                    model.content = self.content
                    DBHelper.insert(model)
                    
                    self.viewContent()
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                
        }
    }
    
    func viewContent() {
        let attrStr = try! NSAttributedString(
            data: self.content.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
            options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
            documentAttributes: nil)
        
        self.txtView.attributedText = attrStr
        let width = Int(txtView.frame.width)
        let height = Config().estimateFrameForText(self.content, width: width).height
        self.heigthView.constant = height
    }
}
