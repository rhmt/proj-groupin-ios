//
//  TextViewMention.swift
//  Groupin
//
//  Created by Macbook pro on 9/28/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

extension UITextView {
    
    func chopOffNonAlphaNumericCharacters(_ text:String) -> String {
        let nonAlphaNumericCharacters = CharacterSet.alphanumerics.inverted
        let characterArray = text.components(separatedBy: nonAlphaNumericCharacters)
        return characterArray[0]
    }
    
    /// Call this manually if you want to hash tagify your string.
    func resolveHashTags(){
        
        let schemeMap = [
//            "#":"hash",
            "@":"mention"
        ]
        
        // Turn string in to NSString.
        // NSString gives us some helpful API methods
        let nsText:NSString = self.text as NSString
        
        // Separate the string into individual words.
        // Whitespace is used as the word boundary.
        // You might see word boundaries at special characters, like before a period.
        // But we need to be careful to retain the # or @ characters.
        //let words:[NSString] = nsText.components(separatedBy: CharacterSet.whitespacesAndNewlines)
        let words = nsText.components(separatedBy: .whitespacesAndNewlines)
        
        // Attributed text overrides anything set in the Storyboard.
        // So remember to set your font, color, and size here.
        let attrs = [
            //            NSFontAttributeName : UIFont(name: "Georgia", size: 20.0)!,
//                    NSForegroundColorAttributeName : UIColor(red: 63, green: 157, blue: 247),
            NSFontAttributeName : UIFont(name: "Helvetica", size: 14.0)!
        ]
        
        // Use an Attributed String to hold the text and fonts from above.
        // We'll also append to this object some hashtag URLs for specific word ranges.
        let attrString = NSMutableAttributedString(string: nsText as String, attributes:attrs)
        
        // Iterate over each word.
        // So far each word will look like:
        // - I
        // - visited
        // - #123abc.go!
        // The last word is a hashtag of #123abc
        // Use the following hashtag rules:
        // - Include the hashtag # in the URL
        // - Only include alphanumeric characters.  Special chars and anything after are chopped off.
        // - Hashtags can start with numbers.  But the whole thing can't be a number (#123abc is ok, #123 is not)
        
        var jumChar = 0
        var stringWord = ""
        var scheme:String? = nil
        
        var showWord: String! = nil
        var showWordArray = [String]()
        var wordArray = [String]()
        wordArray.removeAll()
        for word in words {
            stringWord = word as String
            jumChar += stringWord.characters.count + 1
            
//            if word.hasPrefix("#") {
//                scheme = schemeMap["#"]
//            } else
            if word.hasPrefix("@") {
                scheme = schemeMap["@"]
                showWord = word as String
                showWordArray.append(word as String)
            }
            wordArray.append(word as String)
        }
        
        // found a word that is prepended by a hashtag
        if let scheme = scheme {
            
            // convert the word from NSString to String
            // this allows us to call "dropFirst" to remove the hashtag
            var stringifiedWord:String = showWord as String
            
            // example: #123abc.go!
            
            // drop the hashtag
            // example becomes: 123abc.go!
            stringifiedWord = String(stringifiedWord.characters.dropFirst())
            
            // Chop off special characters and anything after them.
            // example becomes: 123abc
            stringifiedWord = chopOffNonAlphaNumericCharacters(stringifiedWord)
            
            if let stringIsNumeric = Int(stringifiedWord) {
                // don't convert to hashtag if the entire string is numeric.
                // example: 123abc is a hashtag
                // example: 123 is not
                print(stringIsNumeric)
            } else if stringifiedWord.isEmpty {
                // do nothing.
                // the word was just the hashtag by itself.
            } else {
                
                // set a link for when the user clicks on this word.
                
                //                    var matchRange:NSRange = nsText.rangeOfString(stringifiedWord as String)
                //                    // Remember, we chopped off the hash tag, so:
                //                    // 1.) shift this left by one character.  example becomes:  #123ab
                //                    matchRange.location--
                //                    // 2.) and lengthen the range by one character too.  example becomes:  #123abc
                //                    matchRange.length++
                //                    // URL syntax is http://123abc
                //
                //                    ada = String(matchRange)
                
                // Replace custom scheme with something like hash://123abc
                // URLs actually don't need the forward slashes, so it becomes hash:123abc
                // Custom scheme for @mentions looks like mention:123abc
                // As with any URL, the string will have a blue color and is clickable
                var inxAwal = 0
                print("ini mention", showWordArray)
                for _ in showWordArray {
                    print("ini loop", inxAwal)
                    let findWord = String(showWordArray[inxAwal].characters.dropFirst())
                    print("kata", showWordArray[inxAwal])
                    let posAt = wordArray.index(of: showWordArray[inxAwal])
                    print("index", posAt!)
                    var strName = ""
                    if wordArray.count - Int(posAt!) > 1 {
                        var inx = 0
                        let loopA = wordArray.count - Int(posAt!)
                        repeat{
                            strName = ""
                            let loop = wordArray.count - Int(posAt!) - inx
                            var i = 0
                            repeat{
                                strName += wordArray[posAt! + i]
                                i += 1
                                if i != loop {
                                    strName += " "
                                }
                            }while loop > i
                            let searchWord = String(strName.characters.dropFirst())
                            let _ = try! Realm().objects(listMember.self).filter("name CONTAINS[c] %@", searchWord).first
                            
                            //Get Start Possition
                            var dataSementara = ""
                            if Int(posAt!) > 0 {
                                var i = 0
                                repeat{
                                    dataSementara += wordArray[i] + " "
                                    i += 1
                                }while Int(posAt!) > i
                            }
                            
                            if let _ = try! Realm().objects(listMember.self).filter("name CONTAINS[c] %@", searchWord).first {
                                let startLokasi = dataSementara.characters.count
                                let untilLokasi = strName.characters.count
                                attrString.addAttribute(NSLinkAttributeName, value: "\(scheme):\(stringifiedWord)", range: NSMakeRange(startLokasi, untilLokasi))
                            }
                            
                            inx += 1
                        }while loopA > inx
                        let changeName = wordArray[posAt!].replacingOccurrences(of: "@", with: "#")
                        wordArray[posAt!] = changeName
                    }else{
                        strName = findWord
                        if let _ = try! Realm().objects(listMember.self).filter("name CONTAINS[c] %@", strName).first {
                            let startLokasi = (jumChar-1) - stringWord.characters.count
                            let untilLokasi = strName.characters.count + 1
                            attrString.addAttribute(NSLinkAttributeName, value: "\(scheme):\(stringifiedWord)", range: NSMakeRange(startLokasi, untilLokasi))
                        }
                    }
                    inxAwal += 1
                }
            }
        }
        
        // Use textView.attributedText instead of textView.text
        self.attributedText = attrString
    }
    
}
