//
//  ThreadReplyCell.swift
//  Groupin
//
//  Created by Macbook pro on 11/11/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import SwiftyJSON

class ThreadReplyCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var heightContentReply: NSLayoutConstraint!
    @IBOutlet weak var btnCommentThread: UIButton!
    @IBOutlet weak var imgCommentThread: UIImageView!
    
    @IBOutlet weak var colImgContent: UICollectionView!
    @IBOutlet weak var heightImgContentReply: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.colImgContent.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    var dataImg: JSON!
    var dataParams = 0
    func numberOfSectionsInCollectionView(_ collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if self.dataParams > 0 {
            return self.dataImg.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgSelectedThreadCell", for: indexPath) as! LikeLessThreadCell
        
        if self.dataParams > 0 {
            if let data = self.dataImg[indexPath.row].string {
                cell.imgLiker.loadImageUsingCacheWithUrlString(data)
            }else{
                cell.imgLiker.image = UIImage(named: "default-avatar")
            }
        }
        
        return cell
        
    }
}
