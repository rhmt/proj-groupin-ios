//
//  TicketAttendanceCell.swift
//  Groupin
//
//  Created by Macbook pro on 11/9/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class TicketAttendanceCell: UITableViewCell {
    
    @IBOutlet weak var lblTicketName: UILabel!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var btnQty: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgQty: UIImageView!
    
}