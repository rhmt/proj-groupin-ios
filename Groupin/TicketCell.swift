//
//  TicketCell.swift
//  Groupin
//
//  Created by Macbook pro on 11/8/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class TicketCell: UITableViewCell {
    
    @IBOutlet weak var txtTicketName: UITextField!
    @IBOutlet weak var btnMataUang: UIButton!
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var txtQty: UITextField!

    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}