//
//  UpcomingEventController.swift
//  Groupin
//
//  Created by Macbook pro on 8/30/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire
import EventKit

class UpcomingEventController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    let blue = UIColor(red: 63, green: 157, blue: 247)
    var idUser = 0
    var idGroup = 0
    var token = ""
    
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var colEvent: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colorNav()
        
        self.colEvent.delegate = self
        self.colEvent.dataSource = self
        
        //Get ID user
        let idSession = try! Realm().objects(session.self).first
        self.idUser = idSession!.id
        self.idGroup = idSession!.id_group
        self.token = idSession!.token
        
        self.navigationItem.title = "Upcoming Event"
        
        makeSerialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getData(base64Encoded)
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    func getData(_ message: String){
        let urlString = Config().urlEvent + "list_event_upcomming"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)["msg"]
                if self.jsonData.count > 0 {
                    self.colEvent.isHidden = false
                    self.lblNoData.isHidden = true
                }else{
                    self.colEvent.isHidden = true
                    self.lblNoData.isHidden = false
                }
                print(self.jsonData)
                if self.jsonData.count > 0 {
                    self.paramsJson = 1
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.colEvent.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if self.paramsJson == 1 {
            return self.jsonData.count
        }
        return 0
    }
    
    var arrayPrice = [String]()
    var arrayPriceType = [String]()
    var arrayLat = [String]()
    var arrayLong = [String]()
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {   
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "eventCell", for: indexPath) as! EventCell
        
        if self.paramsJson == 1 {
            let data = jsonData[indexPath.row]
            if let imgUrl = data["image"].string {
                cell.imgAva.loadImageUsingCacheWithUrlString(imgUrl)
            }else{
                cell.imgAva.image = UIImage(named: "dafault-ava-group")
            }
            cell.imgAva.clipsToBounds = true
            cell.lblTitle.text = data["name"].string
            cell.lblTitle.textColor = UIColor(red: 63, green: 157, blue: 247)
            var i = 0
            for _ in data["ticket"] {
                self.arrayPrice.append(data["ticket"][i]["price"].string!)
                self.arrayPriceType.append(data["ticket"][i]["price_type"].string!)
                i += 1
            }
            let sortedTicket = self.arrayPrice.sorted(){$0 < $1}
            var firstArrayTicket = arrayPriceType[0].uppercased() + " " + sortedTicket[0]
            if sortedTicket[0] == "0" {
                firstArrayTicket = "Free"
            }
            let rangeTicket = firstArrayTicket + " - " + arrayPriceType.last!.uppercased() + " " + sortedTicket.last!
            
            cell.lblHarga.text = rangeTicket
            cell.lblHarga.textColor = UIColor(red: 83, green: 186, blue: 32)
            cell.lblDesc.text = data["description"].string!
            //Heigth Desc
            let heightDesc = self.estimateFrameForText(data["description"].string!, width: Int(cell.widthDesc.constant)).height
            cell.heightDesc.constant = heightDesc
            //-----------
            cell.lblName.text = data["user"]["name"].string!
            cell.lblName.textColor = UIColor(red: 63, green: 157, blue: 247)
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "EEEE, MMM d, h:mm a"
            let dataDateFrom = data["date_from"].string!
            if let date = dateFormatterGet.date(from: dataDateFrom) {
                let dateFrom = dateFormatterPrint.string(from: date)
                cell.lblFrom.text = dateFrom
            }else{
                cell.lblFrom.text = dataDateFrom
            }
            
            if let dateToP = dateFormatterGet.date(from: data["date_to"].string!) {
                let dateTo = dateFormatterPrint.string(from: dateToP)
                cell.lblTo.text = dateTo
            }else{
                cell.lblTo.text = data["date_to"].string!
            }
            
            cell.lblTempat.text = data["address"].string!
            cell.lblTempat.textColor = UIColor(red: 63, green: 157, blue: 247)
            
            let heightAddress = self.estimateFrameForText(data["address"].string!, width: Int(cell.lblTempat.frame.width)).height
            print(heightAddress)
            cell.heightAddress.constant = heightAddress - 3
            
            var attend = ""
            if data["attendance_status"].string == "2" {
                attend = "WILL NOT ATTEND"
                cell.btnAttend.setTitleColor(UIColor(red: 232, green: 76, blue: 61), for: UIControlState())
                cell.btnAttend.isEnabled = false
            }else if data["attendance_status"].string == "1"{
                attend = "WILL ATTEND (\(data["price"].string!))"
                cell.btnAttend.setTitleColor(UIColor(red: 83, green: 186, blue: 32), for: UIControlState())
                cell.btnAttend.isEnabled = false
            }else{
                attend = "SET ATTENDANCE"
                cell.btnAttend.setTitleColor(UIColor.black, for: UIControlState())
                cell.btnAttend.isEnabled = true
            }
            cell.btnAttend.setTitle(attend, for: UIControlState())
            
            self.arrayLat.append(data["latitude"].string!)
            self.arrayLong.append(data["longitude"].string!)
        }
        
        //Rounded Button
        cell.btnSave.backgroundColor = UIColor(red: 63, green: 157, blue: 247)
        cell.btnSave.layer.cornerRadius = 5
        cell.btnSave.layer.borderWidth = 1
        cell.btnSave.layer.borderColor = UIColor.lightGray.cgColor
        cell.btnMap.backgroundColor = UIColor(red: 255, green: 87, blue: 42)
        cell.btnMap.layer.cornerRadius = 5
        cell.btnMap.layer.borderWidth = 1
        cell.btnMap.layer.borderColor = UIColor.lightGray.cgColor
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthCell = self.colEvent.frame.size.width// - 8 * 1) / 1 //some width
        let heightCell = widthCell * 1.25 //ratio
        
        return CGSize(width: widthCell, height: heightCell)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAttend" {
            let conn = segue.destination as! EventAttendController
            conn.idEventSelected = self.idEventSelected
            conn.nameSelected = self.nameSelected
            conn.createdSelected = self.createdSelected
            conn.fromSelected = self.fromSelected
            conn.toSelected = self.toSelected
            conn.addressSelected = self.addressSelected
            conn.ticketSelected = self.ticketSelected
        }
        if segue.identifier == "showViewMaps" {
            let conn = segue.destination as! EventViewMapsController
            conn.lat = Double(self.latSelected)!
            conn.long = Double(self.longSelected)!
            conn.navigationItem.title = "View Map"
        }
    }
    
    @IBAction func btnSaveCalenderTapped(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview as! EventCell
        let indexPath = self.colEvent.indexPath(for: cell)
        
        let data = jsonData[indexPath!.row]
        let dateFrom = data["date_from"].string!
        let dateTo = data["date_to"].string!
        let titleEvent = data["name"].string!
        let noteEvent = data["description"].string!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let fromDate = dateFormatter.date(from: dateFrom)
        let toDate = dateFormatter.date(from: dateTo)
        
        let eventStore : EKEventStore = EKEventStore()
        eventStore.requestAccess(to: EKEntityType.event, completion: {
            (granted, error) in
            
            if (granted) && (error == nil) {
                print("granted \(granted)")
                print("error \(error)")
                
                let event:EKEvent = EKEvent(eventStore: eventStore)
                
                event.title = titleEvent
                event.startDate = fromDate!
                event.endDate = toDate!
                event.notes = noteEvent
                event.calendar = eventStore.defaultCalendarForNewEvents
                
                do {
                    try eventStore.save(event, span: .thisEvent)
                    print("Saved Event")
                    //                    savedEventId = event.eventIdentifier
                } catch {
                    print("Bad things happened")
                }
            }
        })
    }
    
    var latSelected = ""
    var longSelected = ""
    @IBAction func btnViewMapTapped(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview as! EventCell
        let indexPath = self.colEvent.indexPath(for: cell)
        latSelected = arrayLat[indexPath!.row]
        longSelected = arrayLong[indexPath!.row]
        
        self.performSegue(withIdentifier: "showViewMaps", sender: self)
    }
    
    var idEventSelected = ""
    var nameSelected = ""
    var createdSelected = ""
    var fromSelected = ""
    var toSelected = ""
    var addressSelected = ""
    var ticketSelected: JSON!
    var typeSelected = ""
    @IBAction func btnAttendTapped(_ sender: AnyObject) {
        let button = sender
        let view = button.superview!
        let cell = view!.superview as! EventCell
        let indexPath = self.colEvent.indexPath(for: cell)
        
        let data = self.jsonData[indexPath!.row]
        self.idEventSelected = data["ticket"][0]["fk_event"].string!
        self.nameSelected = data["name"].string!
        self.createdSelected = data["user"]["name"].string!
        self.fromSelected = data["date_from"].string!
        self.toSelected = data["date_to"].string!
        self.addressSelected = data["address"].string!
        self.ticketSelected = data["ticket"]
        self.typeSelected = data["ticket"][0]["idtb_ticket_event"].string!
        
        let alert = UIAlertController(title: "", message: "Are you able to attend this event?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.performSegue(withIdentifier: "showAttend", sender: self)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { action in
            self.noAttendEvent()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func noAttendEvent(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "id_event" : self.idEventSelected as AnyObject,
            "id_ticket" : self.typeSelected as AnyObject,
            "jumlah" : "0" as AnyObject,
            "price" : "0" as AnyObject,
            "status" : "2" as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Attend(jsonString!)
    }
    
    func makeBase64Attend(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getDataAttend(base64Encoded)
        }
    }
    
    func getDataAttend(_ message: String){
        let urlString = Config().urlEvent + "set_attendance"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(urlString)
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)
                print(self.jsonData)
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.makeSerialize()
        }
    }
    
    fileprivate func estimateFrameForText(_ text: String, width: Int) -> CGRect {
        let size = CGSize(width: width, height: 10000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)], context: nil)
    }
    
    // METHODE NAV
    func colorNav(){
        navigationController!.navigationBar.barTintColor = self.blue
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationController!.navigationBar.tintColor = UIColor.white
    }
    
}
