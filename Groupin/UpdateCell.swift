//
//  UpdateCell.swift
//  Groupin
//
//  Created by Macbook pro on 8/29/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class UpdateCell: UITableViewCell {
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var txtViewUpdate: UITextView!
    @IBOutlet weak var lblUpdate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var viewUpdate: UIView!
    @IBOutlet weak var viewMain: UIView!
}