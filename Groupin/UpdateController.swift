//
//  UpdateController.swift
//  Groupin
//
//  Created by Macbook pro on 8/29/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import RealmSwift

class UpdateController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var tbUpdate: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var viewNoData: UIView!
    
    let blue = UIColor(red: 63, green: 157, blue: 247)
    
    var idUser = 0
    var token = ""
    var lastUpdate = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = false
        colorNav()
        rightMenu()
        
        self.tbUpdate.isHidden = true
        self.viewNoData.isHidden = false
        
        self.tbUpdate.separatorStyle = .none
        self.tbUpdate.rowHeight = UITableViewAutomaticDimension
        self.tbUpdate.estimatedRowHeight = 50.0
    }
    
    var dateFormatter: DateFormatter!
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.tabBarController?.tabBar.items?[1].badgeValue = nil
        
        self.dateFormatter = DateFormatter()
        
        let sess = try! Realm().objects(session.self).first!
        idUser = sess.id
        token = sess.token
        let last = try! Realm().objects(tb_updates.self)
        if last.count > 0 {
            lastUpdate = last.last!.date_update
        }else{
            let dateJoin = try! Realm().objects(tb_user.self).first!
            lastUpdate = dateJoin.tgl_join
        }
        print(lastUpdate)
        loadLocal()
        GetData()
    }
    
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(true)
//        self.tabBarController?.tabBar.isHidden = true
//    }
    
    var dataData = [tb_updates]()
    func loadLocal(){
        self.dataData.removeAll()
        let dataFilter = try! Realm().objects(tb_updates.self).sorted(byKeyPath: "id_update", ascending: false)
        
        if dataFilter.count > 0 {
            for item in dataFilter {
                dataData += [item]
            }
            print(dataFilter.count)
            self.tbUpdate.isHidden = false
            self.viewNoData.isHidden = true
        }else{
            self.tbUpdate.isHidden = true
            self.viewNoData.isHidden = false
        }
//        if self.paramsJson == 1 {
            self.tbUpdate.reloadData()
//        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    func GetData(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_user" : self.idUser as AnyObject,
            "datetime" : self.lastUpdate as AnyObject
        ]
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        var message = ""
        let utf8str = jsonString!.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        { message = base64Encoded }
        
        let urlString = Config().urlUpdate + "get_update"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)["msg"]
                print(self.jsonData)
                if self.jsonData.count > 0 {
                    self.paramsJson = 1
                    self.saveToLocal()
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func saveToLocal(){
        self.dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let dateUpdate = self.dateFormatter.string(from: Date())
        print(dateUpdate)
        var i = 0
        for _ in jsonData {
            let data = jsonData[i]
            let model = tb_updates()
            model.id_update = data["id_udpate"].intValue
            var imgUrl = ""
            if let dataImgUrl = data["thumbnail"].string {
                imgUrl = dataImgUrl
            }else{
                imgUrl = Config().urlImage + "default-avatar.png"
            }
            model.thumbnail = imgUrl
            model.title = data["title"].string!
            model.content = data["body"].string!
            model.date_create = data["date_create"].string!
            model.date_update = dateUpdate
            model.update_type = data["jenis_update"].string!
            let dataFrom = data["data"]
            if let id = dataFrom["id_user"].string { model.data_id_user = id }
            if let id = dataFrom["id_event"].string { model.data_id_event = id }
            if let id = dataFrom["id_group"].string { model.data_id_group = id }
            if let id = dataFrom["id_thread"].string { model.data_id_thread = id }
            if let name = dataFrom["id_user_name"].string { model.data_id_user_name = name }
            if let name = dataFrom["id_event_name"].string { model.data_id_event_name = name }
            if let name = dataFrom["id_group_name"].string { model.data_id_group_name = name }
            if let name = dataFrom["id_thread_name"].string { model.data_id_thread_name = name }
            DBHelper.insert(model)
            
            i += 1
        }
        self.loadLocal()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "updateCell", for: indexPath) as! UpdateCell
        
        let tapPhoto = UITapGestureRecognizer(target: self, action: #selector(UpdateController.photoTapped(_:)))
        tapPhoto.numberOfTapsRequired = 1
        
        let data = dataData[indexPath.row]
        
        //Default
        cell.viewUpdate.layer.shadowRadius = 0
        cell.viewUpdate.layer.cornerRadius = 2
        cell.viewMain.layer.shadowRadius = 0
        cell.viewMain.layer.cornerRadius = 2
        
        if data.update_type != "3" {
            tbUpdate.rowHeight = 85
            
            //Rounded View Update
            cell.viewMain.isHidden = true
            cell.viewUpdate.layer.shadowColor = UIColor.lightGray.cgColor
            cell.viewUpdate.layer.shadowOffset = CGSize.zero
            cell.viewUpdate.layer.shadowOpacity = 0.5
            cell.viewUpdate.layer.shadowRadius = 3
        }else{
            tbUpdate.rowHeight = 215
            if data.thumbnail != "" {
                cell.imgPhoto.loadImageUsingCacheWithUrlString(data.thumbnail)
            }else{
                cell.imgPhoto.image = UIImage(named: "default-avatar")
            }
            cell.imgPhoto.clipsToBounds = true
            cell.imgPhoto.isUserInteractionEnabled = true
            cell.imgPhoto.addGestureRecognizer(tapPhoto)
            
            //Rounded View Main
            cell.viewMain.isHidden = false
            cell.viewMain.layer.shadowColor = UIColor.lightGray.cgColor
            cell.viewMain.layer.shadowOffset = CGSize.zero
            cell.viewMain.layer.shadowOpacity = 0.5
            cell.viewMain.layer.shadowRadius = 3
        }
        
        if data.thumbnail != "" {
            cell.imgAva.loadImageUsingCacheWithUrlString(data.thumbnail)
        }else{
            cell.imgAva.image = UIImage(named: "default-avatar")
        }
        cell.imgAva.layer.cornerRadius = cell.imgAva.frame.width / 2
        cell.imgAva.clipsToBounds = true
        cell.lblUpdate.text = data.content
        cell.lblTime.text = Config().timeElapsed(data.date_create)
        
        let strData = data.content
        let myMutableString = NSMutableAttributedString(string: strData)
        var scheme = ""
        if data.data_id_user_name != "" {
            scheme = "name"
            let strName = data.data_id_user_name
            let posName = Config().getPos(strData, str: strName)
            let idName = data.data_id_user
            let lengName = strName.characters.count
            myMutableString.addAttribute(NSLinkAttributeName, value: "\(scheme):\(idName)", range: NSMakeRange(posName, lengName))
            myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 63, green: 157, blue: 247), range: NSRange(location:posName,length:lengName))
        }
        if data.data_id_group_name != "" {
            scheme = "group"
            let strName = "\"" + data.data_id_group_name + "\""
            let posName = Config().getPos(strData, str: strName)
            let lengName = strName.characters.count
            myMutableString.addAttribute(NSLinkAttributeName, value: "\(scheme):\(strName)", range: NSMakeRange(posName, lengName))
            myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 63, green: 157, blue: 247), range: NSRange(location:posName,length:lengName))
        }
        if data.data_id_event_name != "" {
            scheme = "event"
            let strName = "\"" + data.data_id_event_name + "\""
            let posName = Config().getPos(strData, str: strName)
            let lengName = strName.characters.count
            myMutableString.addAttribute(NSLinkAttributeName, value: "\(scheme):\(strName)", range: NSMakeRange(posName, lengName))
            myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 63, green: 157, blue: 247), range: NSRange(location:posName,length:lengName))
        }
        if data.data_id_thread_name != "" {
            scheme = "thread"
            let strName = data.data_id_thread_name
            let posName = Config().getPos(strData, str: strName)
            let lengName = strName.characters.count
            myMutableString.addAttribute(NSLinkAttributeName, value: "\(scheme):\(strName)", range: NSMakeRange(posName, lengName))
            myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 63, green: 157, blue: 247), range: NSRange(location:posName,length:lengName))
        }
        cell.txtViewUpdate.delegate = self
        cell.txtViewUpdate.isSelectable = true
        cell.txtViewUpdate.dataDetectorTypes = .link
        cell.txtViewUpdate.tag = indexPath.row
        cell.txtViewUpdate.attributedText = myMutableString
        
        cell.imgPhoto.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
//    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
//    }
//    
    
//    @available(iOS 10.0, *)
//    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
//    }
    
    //For iOS 7...9
    @available(iOS, deprecated: 10.0)
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        // Link
        print("url ios < 10")
        let scheme = URL.scheme!
        let indexPath =  textView.tag
        let data = dataData[indexPath]
        
        switch scheme {
        case "name":
            let id = data.data_id_user
            let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showMember") as! PopupMemberController
            self.addChildViewController(Popover)
            Popover.view.frame = self.view.frame
            Popover.idFriend = id
            self.view.addSubview(Popover.view)
            Popover.didMove(toParentViewController: self)
        default:
            print("just url")
        }
        
        return true

    }
    
    //For iOS 10
    @available(iOS 10.0, *)
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        // Link
        let scheme = URL.scheme!
        let indexPath = textView.tag
        let data = dataData[indexPath]
        
        switch scheme {
        case "name":
            let id = data.data_id_user
            let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showMember") as! PopupMemberController
            self.addChildViewController(Popover)
            Popover.view.frame = self.view.frame
            Popover.idFriend = id
            self.view.addSubview(Popover.view)
            Popover.didMove(toParentViewController: self)
        default:
            print("just url")
        }
        
        return true

    }
    
    func photoTapped(_ sender: UITapGestureRecognizer) {
        let indexPath = sender.view!.tag
        let data = dataData[indexPath]
        
        let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showUpdateDetailPhoto") as! UpdateDetailPhotoController
        self.addChildViewController(Popover)
        Popover.view.frame = self.view.frame
        let size = Popover.view.frame.width - 50
        Popover.valueConHeightViewImg = size
        Popover.valueConWidthViewImg = size
        Popover.imgPhoto.loadImageUsingCacheWithUrlString(data.thumbnail)
        self.view.addSubview(Popover.view)
        Popover.didMove(toParentViewController: self)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath){
        
    }
    
    // METHODE NAV
    func colorNav(){
        navigationController!.navigationBar.barTintColor = self.blue
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationController!.navigationBar.tintColor = UIColor.white
    }
    
    func rightMenu(){
        let searchButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(self.searchTapped))
        //Space
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 10.0
        //Delete
        let imageContact = UIImage(named: "ico_tab_contact")
        let contactButton = UIButton()
        contactButton.setImage(imageContact, for: UIControlState())
        contactButton.addTarget(self, action: #selector(self.contactTapped), for: UIControlEvents.touchDown)
        contactButton.frame=CGRect(x: 0, y: 0, width: 30, height: 30)
        let barAttachContact = UIBarButtonItem(customView: contactButton)
        
        self.navigationItem.setRightBarButtonItems([searchButtonItem, fixedSpace, barAttachContact], animated: true)
    }
    
    func searchTapped(_ sender:UIButton){
        self.performSegue(withIdentifier: "showSearchGroup", sender: self)
    }
    
    func contactTapped(_ sender:UIButton){
        self.performSegue(withIdentifier: "showContact", sender: self)
    }
    
}
