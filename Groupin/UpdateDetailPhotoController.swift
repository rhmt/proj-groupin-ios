//
//  UpdateDetailPhotoController.swift
//  Groupin
//
//  Created by Macbook pro on 8/29/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class UpdateDetailPhotoController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var scrollImg: UIScrollView!
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var conHeightViewImg: NSLayoutConstraint!
    @IBOutlet weak var conWidthViewImg: NSLayoutConstraint!
    
    var valueConWidthViewImg: CGFloat = 0
    var valueConHeightViewImg: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showAnimate()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        //Click to close
        let tapBack = UITapGestureRecognizer(target: self, action: #selector(UpdateDetailPhotoController.closePopup(_:)))
        tapBack.numberOfTapsRequired = 1
        mainView.isUserInteractionEnabled = true
        mainView.addGestureRecognizer(tapBack)
        
        //Zooming Image
        self.scrollImg.delegate = self
        self.scrollImg.minimumZoomScale = 1.0
        self.scrollImg.maximumZoomScale = 7.0
        self.scrollImg.alwaysBounceVertical = false
        self.scrollImg.alwaysBounceHorizontal = false
        self.scrollImg.showsVerticalScrollIndicator = true
        self.scrollImg.flashScrollIndicators()
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(self.zoom(_:)))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.numberOfTouchesRequired = 1
        self.scrollImg.addGestureRecognizer(doubleTap)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.conHeightViewImg.constant = self.valueConHeightViewImg
        self.conWidthViewImg.constant = self.valueConWidthViewImg
        
        print("did Appear")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgPhoto
    }
    
    func zoom(_ tapGesture: UITapGestureRecognizer) {
        let scale = min(self.scrollImg.zoomScale * 2, self.scrollImg.maximumZoomScale)
        
        if scale != self.scrollImg.zoomScale {
            let point = tapGesture.location(in: self.imgPhoto)
            
            let scrollSize = self.scrollImg.frame.size
            let size = CGSize(width: scrollSize.width / scale,
                              height: scrollSize.height / scale)
            let origin = CGPoint(x: point.x - size.width / 2,
                                 y: point.y - size.height / 2)
            self.scrollImg.zoom(to:CGRect(origin: origin, size: size), animated: true)
        } else {
            self.scrollImg!.setZoomScale(self.scrollImg!.minimumZoomScale, animated: true)
        }
    }
    
    @IBAction func btnCloseTapped(_ sender: AnyObject) {
        removeAnimate()
    }
    
    func closePopup(_ sendet: UITapGestureRecognizer){
        removeAnimate()
    }
    
    func showAnimate(){
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0.0
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            }, completion: {(finished: Bool) in
                if(finished){
                    self.view.removeFromSuperview()
                }
        })
    }
    
}
