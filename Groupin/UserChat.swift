//
//  UserChat.swift
//  Groupin
//
//  Created by Macbook pro on 10/13/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class UserChat: NSObject {
    var id: String?
    var name: String?
    var profileImageUrl: String?
}
