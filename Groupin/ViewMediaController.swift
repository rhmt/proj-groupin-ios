//
//  ViewMediaController.swift
//  Groupin
//
//  Created by Macbook pro on 9/8/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import RealmSwift
import SwiftyJSON
import Alamofire

class ViewMediaController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate{
    
    @IBOutlet weak var viewPlayer: UIView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var scrollImg: UIScrollView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var colLike: UICollectionView!
    @IBOutlet weak var lblTotalLike: UILabel!
    @IBOutlet weak var lblTotalComment: UILabel!
    @IBOutlet weak var viewSeparatorBtn: UIView!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var imgComment: UIImageView!
    @IBOutlet weak var btnComment: UIButton!
    
    //Constraint
    @IBOutlet weak var heightImgView: NSLayoutConstraint!
    @IBOutlet weak var widthColLike: NSLayoutConstraint!
    @IBOutlet weak var widthLblTotalLike: NSLayoutConstraint!
    @IBOutlet weak var widthLblTotalComment: NSLayoutConstraint!
    
    
    var idMedia = ""
    var mediaURL = ""
    var token = ""
    var idUser = 0
    
    //If Media
    var mediaImage: UIImage!
    var mediaVideo = ""
    
    var playerViewController = AVPlayerViewController()
    var playerView = AVPlayer()
    
    var dataMedia = [tb_media]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sess = try! Realm().objects(session.self).first
        self.idUser = sess!.id
        token  = sess!.token
        
        self.colLike.backgroundColor = UIColor.clear
        self.widthColLike.constant = 0
        
        self.colLike.delegate = self
        self.colLike.dataSource = self
        //Zooming Image
        self.heightImgView.constant = self.view.frame.height
        self.scrollImg.delegate = self
        self.scrollImg.minimumZoomScale = 1.0
        self.scrollImg.maximumZoomScale = 7.0
        self.scrollImg.alwaysBounceVertical = false
        self.scrollImg.alwaysBounceHorizontal = false
        self.scrollImg.showsVerticalScrollIndicator = true
        self.scrollImg.flashScrollIndicators()
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(self.zoom(_:)))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.numberOfTouchesRequired = 1
        self.scrollImg.addGestureRecognizer(doubleTap)
        
        let dataMedia = try! Realm().objects(tb_media.self).filter("id_media == %@", idMedia).first!
        
        let realName = dataMedia.name //14 0
        let nameMediaView = ""//realName.substring(with: realName.)
        
        self.navigationItem.title = nameMediaView
        
        let path = Config().getMediaPath()
        
        if dataMedia.type == "Image" {
            btnPlay.isHidden = true
            let img = (path as String) + "/" + dataMedia.name + ".jpg"
            imgView.image = UIImage(contentsOfFile: img)
        }
        
        //Like
        let likeTap = UITapGestureRecognizer(target: self, action: #selector(self.likeTapped(_:)))
        likeTap.numberOfTapsRequired = 1
        self.imgLike.isUserInteractionEnabled = true
        self.imgLike.addGestureRecognizer(likeTap)
        
        //Comment
        let commTap = UITapGestureRecognizer(target: self, action: #selector(self.commentTapped(_:)))
        commTap.numberOfTapsRequired = 1
        self.imgComment.isUserInteractionEnabled = true
        self.imgComment.addGestureRecognizer(commTap)
        
        //View Like
        let viewLikeTap = UITapGestureRecognizer(target: self, action: #selector(self.viewLikeTapped(_:)))
        viewLikeTap.numberOfTapsRequired = 1
        self.lblTotalLike.isUserInteractionEnabled = true
        self.lblTotalLike.addGestureRecognizer(viewLikeTap)
        
        //View Comment
        let viewCommTap = UITapGestureRecognizer(target: self, action: #selector(self.viewCommentTapped(_:)))
        viewCommTap.numberOfTapsRequired = 1
        self.lblTotalComment.isUserInteractionEnabled = true
        self.lblTotalComment.addGestureRecognizer(viewCommTap)
        
        self.makeSerializeLike()
        self.makeSerializeComment()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.makeSerializeComment()
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgView
    }
    
    func zoom(_ tapGesture: UITapGestureRecognizer) {
        let scale = min(self.scrollImg.zoomScale * 2, self.scrollImg.maximumZoomScale)
        
        if scale != self.scrollImg.zoomScale {
            let point = tapGesture.location(in: self.imgView)
            
            let scrollSize = self.scrollImg.frame.size
            let size = CGSize(width: scrollSize.width / scale,
                              height: scrollSize.height / scale)
            let origin = CGPoint(x: point.x - size.width / 2,
                                 y: point.y - size.height / 2)
            self.scrollImg.zoom(to:CGRect(origin: origin, size: size), animated: true)
        } else {
            self.scrollImg!.setZoomScale(self.scrollImg!.minimumZoomScale, animated: true)
        }
    }
    
    @IBAction func btnPlayTapped(_ sender: AnyObject) {
        let videoURL = URL(string: mediaURL)
        playerView = AVPlayer(url: videoURL!)
        
        playerViewController.player = playerView
        self.present(playerViewController, animated: true){
            self.playerViewController.player?.play()
        }
    }
    
    func makeSerializeLike(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_media" : self.idMedia as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Like(jsonString!)
    }
    
    func makeBase64Like(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getDataLike(base64Encoded)
        }
    }
    
    var jsonData: JSON!
    var paramsJson = 0
    func getDataLike(_ message: String){
        let urlString = Config().urlMedia + "list_like/0/50"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)["msg"]
                print(self.jsonData)
                if self.jsonData.count > 0 {
                    self.paramsJson = 1
                    let count = self.jsonData.count
                    self.totalLike = String(count)
                    var likes = " Like"
                    if count > 1 {
                        likes = " Likes"
                    }
                    let textLbl = String(count) + likes
                    self.lblTotalLike.text = textLbl
                    //Width lbl
                    let plusLbl = Config().estimateFrameForText(textLbl, width: 200).width
                    self.widthLblTotalLike.constant = plusLbl + 8
                    //Width Col
                    let plusCol = count * 30
                    self.widthColLike.constant = CGFloat(plusCol)
                    if plusCol > 180 {
                        self.widthColLike.constant = 180
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
                self.colLike.reloadData()
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if paramsJson == 1{
            return self.jsonData.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "likeCell", for: indexPath) as! LikeLessThreadCell
        
        if paramsJson == 1{
            var idLiker = [String]()
            idLiker.removeAll()
            for (key, _) in self.jsonData {
                idLiker.append(key)
            }
            let data = self.jsonData[indexPath.row]
            if let imgUrl = data["user"]["thumbnail"].string {
                cell.imgLiker.loadImageUsingCacheWithUrlString(imgUrl)
            }else{
                cell.imgLiker.image = UIImage(named: "default-avatar")
            }
            cell.imgLiker.layer.cornerRadius = cell.imgLiker.frame.width / 2
            cell.imgLiker.clipsToBounds = true
        }
        
        return cell
    }
    
    @IBAction func btnLikeTapped(_ sender: UIButton) {
        self.makeSerialize()
    }
    
    func likeTapped(_ sender: UITapGestureRecognizer){
        self.makeSerialize()
    }
    
    func makeSerialize(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_media" : self.idMedia as AnyObject,
            "id_user" : self.idUser as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64(jsonString!)
    }
    
    func makeBase64(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            postLike(base64Encoded)
        }
    }
    
    func postLike(_ message: String){
        let urlString = Config().urlMedia + "add_like"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(urlString)
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON() { response in
            if let value = response.result.value {
                let dataJason = JSON(value)
                print(dataJason)
                if dataJason.count > 0 {
                    let code = dataJason["code"].intValue
                    if code == 1{
                        print("Liked")
                        self.imgLike.image = UIImage(named: "ico_liked")
                        self.btnLike.setTitleColor(UIColor(red: 63, green: 157, blue: 247), for: .normal)
                    }else{
                        self.imgLike.image = UIImage(named: "ico_like")
                        self.btnLike.setTitleColor(UIColor.white, for: .normal)
                    }
                }
            }else{
                print("Something Went Wrong..")
            }
            }.responseData { Response in
                self.makeSerializeLike()
        }
    }
    
    @IBAction func btnCommentTapped(_ sender: UIButton) {
        self.comment()
    }
    
    func commentTapped(_ sender: UITapGestureRecognizer){
        self.comment()
    }
    
    func comment(){
        let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showCommentMedia") as! CommentMediaController
        
        print("awalnya bang: ", self.idMedia)
        Popover.idMedia = self.idMedia
        self.addChildViewController(Popover)
        Popover.view.frame = self.view.frame
        self.view.addSubview(Popover.view)
        Popover.didMove(toParentViewController: self)
        
    }
    
    func makeSerializeComment(){
        var arrayData = [String:AnyObject]()
        arrayData = [
            "id_media" : self.idMedia as AnyObject
        ]
        
        let data = try! JSONSerialization.data(withJSONObject: arrayData, options: .prettyPrinted)
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        makeBase64Comment(jsonString!)
    }
    
    func makeBase64Comment(_ data: NSString){
        let utf8str = data.data(using: String.Encoding.utf8.rawValue)
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            getDataComment(base64Encoded)
        }
    }
    
    func getDataComment(_ message: String){
        print("get comment gan")
        let urlString = Config().urlMedia + "list_comment/0/50"
        let token = self.token
        var parameters = [String:AnyObject]()
        parameters = ["token" : token as AnyObject, "msg": message as AnyObject]
        print(urlString)
        print(parameters)
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON { response in
            if let value = response.result.value {
                self.jsonData = JSON(value)["msg"]
                print(self.jsonData)
                if self.jsonData.count > 0 {
                    let textLbl = String(self.jsonData.count) + " Comments"
                    self.lblTotalComment.text = textLbl
                    let plusLbl = Config().estimateFrameForText(textLbl, width: 250).width
                    self.widthLblTotalComment.constant = plusLbl + 8
                }
            }else{
                print("Something Went Wrong..")
            }
            
            }.responseData { Response in
        }
    }
    
    var totalLike = ""
    func viewLikeTapped(_ sender: UITapGestureRecognizer){
        self.performSegue(withIdentifier: "showListLikeMedia", sender: self)
    }
    
    func viewCommentTapped(_ sender: UITapGestureRecognizer){
        self.comment()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showListLikeMedia" {
            let conn = segue.destination as! LikesMediaController
            conn.idMedia = self.idMedia
            conn.totalLikes = self.totalLike
        }
    }
}
