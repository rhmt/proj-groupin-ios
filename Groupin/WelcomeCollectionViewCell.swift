//
//  WelcomeCollectionViewCell.swift
//  Groupin
//
//  Created by Macbook pro on 7/30/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit

class WelcomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgAvatr: UIImageView!
    @IBOutlet weak var lblNama: UILabel!
    @IBOutlet weak var imgSelect: UIImageView!
}