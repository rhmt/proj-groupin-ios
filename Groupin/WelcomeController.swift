//
//  WelcomeController.swift
//  Groupin
//
//  Created by Macbook pro on 7/30/16.
//  Copyright © 2016 Macbook pro. All rights reserved.
//

import UIKit
import RealmSwift

class WelcomeController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var colGroup: UICollectionView!
    var models = [tb_group]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        models = DBHelper.getAllGroup()
        colGroup.reloadData()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return models.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WelcomeCell", for: indexPath) as! WelcomeCollectionViewCell
        
        let imageData = models[indexPath.row].avatar
        cell.imgAvatr.loadImageUsingCacheWithUrlString(imageData)
        cell.imgAvatr.contentMode = .scaleToFill
        cell.lblNama.text = models[indexPath.row].name
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showDetailGroup") as! DetailGroupController
        Popover.idGroup = models[indexPath.row].id
        self.addChildViewController(Popover)
        Popover.view.frame = self.view.frame
        self.view.addSubview(Popover.view)
        Popover.didMove(toParentViewController: self)
    }
}
